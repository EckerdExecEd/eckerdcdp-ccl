<?php 
/*=============================================================================*
* 04-22-2005 TRM: Added support for "Self Only" licensing.
*
*=============================================================================*/
require_once "../meta/licfns.php";
require_once "consfn.php";
session_start();

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("License History",$msg);
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
$conid=$_SESSION['conid'];
?>
<table border=1>
<tr>
<td>
Purchase licenses (standard)
</td>
<td>
Consumed Licenses (standard)
</td>
</tr>
<tr>
<td>
Total: <?=totPurchased($conid)?>
</td>
<td>
Total: <?=totConsumed($conid)?>
</td>
</tr>
<tr>
<td>
<?=purchased($conid)?>
</td>
<td>
<?=consumed($conid)?>
</td>
</tr>

<tr>
<td>
Purchase licenses (self-only)
</td>
<td>
Consumed Licenses (self-only)
</td>
</tr>
<tr>
<td>
Total: <?=totPurchased($conid,"3")?>
</td>
<td>
Total: <?=totConsumed($conid,"3")?>
</td>
</tr>
<tr>
<td>
<?=purchased($conid,"3")?>
</td>
<td>
<?=consumed($conid,"3")?>
</td>
</tr>


</table>
<?php
writeFooter(false);
?>
