<?php 
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$pid=$_POST['pid'];
$tid=$_POST['tid'];
$msg="";

if("save"==$_POST['what']){
    $pid=$_POST['pid'];
    $descr=addslashes($_POST['descr']);
    $startDt=date('Y-m-d',strtotime($_POST['startdt']));
    $endDt=date('Y-m-d',strtotime($_POST['enddt']));
    $exp=$_POST['exp'];
    $pgmData=array($pid,$descr,$startDt,$endDt,$exp);
    if(updatePgm($pgmData)){
	$msg="<font color='#00aa00'>Successfully saved data</font>";
    }
    else{
	$msg="<font color='#aa0000'>Error saving data</font>";
    }
}
writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("Edit Program",$msg);
?>
<form name="listfrm" action="pgmdetail.php" method=POST>
<input type="hidden" name="what" value="save">
<input type="hidden" name="pid" value="<?=$pid?>">
<input type="hidden" name="tid" value="<?=$tid?>">
<table border=1 cellpadding=5>

<?php
if(!showPgm($pid)){
    echo "<font color='#aa0000'>Error displaying program data</font></br>";
}
?>
<tr><td colspan=2><input type="submit" value="Save Changes"></td></tr>
</table>

<?php
$urls=array('listpgm.php');
$txts=array('Back');
menu($urls,$txts,"listfrm");
?>
</form>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>
