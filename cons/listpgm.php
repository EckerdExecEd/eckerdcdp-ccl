<?php
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$msg="";

$tid=$_POST['tid'];

if("archive"==$_POST['what']){
    if(chgProgramStatus($_POST['pid'],"Y")){
	$msg="<font color='#00aa00'>Successfully archived program</font>";
    }
    else{
	$msg="<font color='#aa0000'>Error archiving program</font>";
    }
}
writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("Manage Active Programs",$msg);
?>
<form name="listfrm" action="listpgm.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="pid" value="">
<input type="hidden" name="tid" value="<?=$tid?>">
<table border=1 cellpadding=5>

<tr>
<td colspan=5 align="left">
Search:&nbsp;
<input type="text" name="narrow" value="" maxlength="255">&nbsp;
<input type="submit" value="Refresh View!">
</td>
</tr>

<?php
    $narrow=$_POST['narrow'];
    $conid=$_SESSION['conid'];
    if(!listPrograms($narrow,"N","listfrm",$conid,$tid)){
		echo "<font color='#aa0000'>Error listing programs</font></br>";
    }
?>

</table>
</form>

<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>
