<?php
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$pid=$_POST['pid'];
$tid=$_POST['tid'];
$msg="";

writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("View/Print Reports",$msg);
?>
<form name="listfrm" action="pgmdetail.php" method=POST>
<table border=1 cellpadding=5>
<input type="hidden" name="tid" value="<?=$tid?>">
<?php
// Potentuially all reports can be multilingual
listAllReportsML($pid,$tid);

/* This is the old way
if("3"==$tid){
	// Survey(s) with multilingual support
	listAllReportsML($pid,$tid);
}
else{
	// No multilingual support
	listAllReports($pid,$tid);
}
*/
?>
</table>

<?php
$urls=array('listpgm.php');
$txts=array('Back');
menu($urls,$txts,"listfrm");
?>
</form>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>

