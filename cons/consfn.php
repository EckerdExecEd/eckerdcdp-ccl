<?php
// Common functions for the consultant site
function writeHead($title,$buffered){
    if($buffered){
	ob_start();
    }
	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n<html>\n<head>\n<title>$title</title>\n";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
	echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">\n";
	echo "<style>\n.ivanC10400569321381 {visibility:hidden; position:absolute;}\n";
	echo ".adminLink a:link{color:black;}\n";
	echo ".adminLink a:visited{color:black;}\n";
	echo "</style>\n</head>\n";
}

function writeBody($title,$msg){
    echo "<body class=\".body\">\n";
    echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"750\" border=\"0\">\n";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">\n";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>\n";
    echo "<tr valign=\"top\" align=\"right\" bgcolor=\"#4f8d97\" height=\"22\"><td colspan=3></td></tr>\n";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">\n";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>\n";
    echo "<tr valign=\"center\" height=\"42\" bgcolor=\"#253355\">\n";
    echo "<td align=\"left\"><img height=\"64\" src=\"../index_files/eclogotop.gif\" width=\"155\" border=\"0\"></td>\n";
    echo "<td><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td>\n";
    echo "<td align=\"right\"><img height=\"64\" src=\"../index_files/ldi-title.gif\" width=\"430\" border=\"0\"></td></tr>\n";
    echo "</table>\n";
	echo "<font face='Arial, Helvetica, Sans-Serif'><h2>$title.</h2>$msg<br>&nbsp;<br>\n";
}

function writeFooter($buffered){
    if($buffered){
	ob_end_flush();
    }
    echo "</font>\n </body>\n </html>\n";
}

function menu($url,$txt,$frm){
    # if no form is passed in, well create one called frm1
    $len=strlen($frm);
    if(0==$len){
		echo "<form name='frm1' method=POST>";
		$frm="frm1";
    }

    echo "<table border=0 cellpadding=5>";
    $cnt=count($url);
    $cnt-=1;
    for($i=0;$i<=$cnt;$i++){
		if(strlen($txt[$i])>0){
			// only if we have text to go with it
			echo '<tr> <td onClick="javascript:'.$frm.'.action=\''.$url[$i].'\';'.$frm.'.submit();">';
			if($i!=$cnt){
			   echo '<img src="../images/g.gif" onMouseOver="this.src=\'../images/b.gif\';"';
			   echo ' onMouseOut="this.src=\'../images/g.gif\';" border=0> &nbsp;'.$txt[$i].'</td></tr>';
			}
			else{
			   # last option has a red bullet for log out, back etc.
			   echo '<img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
			   echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp;'.$txt[$i].'</td></tr>';
			}
		}
    }
    echo "</table>";

    if(0==$len){
		echo "</form>";
    }
}


?>
