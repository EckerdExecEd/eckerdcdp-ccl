<?php
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
// get program and candidate
$pid=$_GET['pid'];
$cid=$_GET['cid'];
// and a language ID
$lid=$_GET['lid'];
$conid=$_SESSION['conid'];

header("Content-Type: application/pdf");
header("Content-Disposition: inline; filename=0-$cid.pdf");
include_once "../meta/scoreandreportselfonly.php";

$pdf=pdf_new();
pdf_set_parameter($pdf,"license","L700102-010500-734253-ZZ4ND2-FMQ922");
pdf_open_file($pdf,"");

pdf_set_info($pdf,'Creator','conflictdynamics.org');
pdf_set_info($pdf,'Author','conflictdynamics.org');
pdf_set_info($pdf,'Title','Conflict Dynamics Profile');

$rc=false;
$msg=array();
$rc=isItComplete($cid,$pid);

if(false==$rc){
	$msg[]="Cannot display report - Incomplete assessment.";	
}
else{
	// delete any old answers that we may have
	 getRidOfOldScores($cid,$pid);
	 $msg[]="Deleting existing scores - OK";
}

if($rc){
	// calculate the standardized scores
	$rc=computeSelfOnlyScaleScores($cid,$pid);
	if(false==$rc){
		$msg[]="Error computing scale scores";
	}
	else{
		$msg[]=$rc;
	}
}

if($rc){
	// generate the self-only report scores
	$rc=calculateSelfOnlyReportScores($cid,$pid);
	if(false==$rc){
		$msg[]="Error computing report score";
	}
	else{
		$msg[]=$rc;
	}
}

//---------------------------------------------------------------------------
// Just for totally standalone reports, i.e. without assocaition to a program
// for these we use the dummy program code 0
//---------------------------------------------------------------------------
if($rc&&$pid=="0"){
	// see if a license is already consumed
	if(alreadyConsumedLicense($cid,$conid,"3")){
		$msg[]="License already consumed for this candidate - rescore?";
	}
	else{
		// consume the license
		$rc=consumeSelfOnlyLicense($cid,$conid,"3");
		if(false==$rc){
			$msg[]="Error consuming license";	
		}
		else{
			$msg[]="Consuming License - OK";
		}
	}
}

//------------------------------------------------------------------------
//uncomment to force the status page to print instead of the report
//$rc=false;
//------------------------------------------------------------------------
if(false==$rc){
	// can't generate the report
	renderStatusPage($cid,&$pdf,$msg);
}
else{
	// if we're here we can generate the report
	$page=1;
	renderPage1($cid,$pdf,$page,$pid,$lid);
	renderPage2($cid,$pdf,$page,$pid,$lid);
	renderPage3($cid,$pdf,$page,$pid,$lid);
	renderPage4($cid,$pdf,$page,$pid,$lid);
	renderPage5($cid,$pdf,$page,$pid,$lid);
	renderPage6($cid,$pdf,$page,$pid,$lid);
	renderPage7($cid,$pdf,$page,$pid,$lid);
	renderPage8($cid,$pdf,$page,$pid,$lid);
}
pdf_close($pdf);
echo pdf_get_buffer($pdf);
pdf_delete($pdf);
?>

