<?php
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$cid=$_GET['cid'];
$pid=$_GET['pid'];

header("Content-Type: application/pdf");
header("Content-Disposition: inline; filename=$pid-$cid.pdf");
include_once "../meta/individualreport.php";
/*
// for testing...
$ip = $_SERVER['REMOTE_ADDR'];
if($ip=="70.61.126.77")
  include_once "../meta/individualreport_test.php";
else
  include_once "../meta/individualreport.php";
*/
$pdf=pdf_new();
pdf_set_parameter($pdf,"license","L700102-010500-734253-ZZ4ND2-FMQ922");
pdf_open_file($pdf,"");

pdf_set_info($pdf,'Creator','conflictdynamics.org');
pdf_set_info($pdf,'Author','conflictdynamics.org');
pdf_set_info($pdf,'Title','Conflict Dynamics Profile');

$page=1;
// Default to NOT showing any data for Others of any kind
$noOther=true;
renderPage1($cid,$pdf,$page,$noOther);
renderPage2($cid,$pdf,$page,$noOther);
renderPage3($cid,$pdf,$page,$noOther);
renderPage4($cid,$pdf,$page,$noOther);
renderPage5($cid,$pdf,$page,$noOther);
renderPage6($cid,$pdf,$page,$noOther);
renderPage7($cid,$pdf,$page,$noOther);
renderPage8($cid,$pdf,$page,$noOther);
renderPage9($cid,$pdf,$page,$noOther);
renderPage10($cid,$pdf,$page,$noOther);
renderPage11($cid,$pdf,$page,$noOther);
renderPage12($cid,$pdf,$page,$noOther);
renderPage13($cid,$pdf,$page,$noOther);
renderPage14($cid,$pdf,$page,$noOther);
renderPage15($cid,$pdf,$page,$noOther);
renderPage16($cid,$pdf,$page,$noOther);
renderPage17($cid,$pdf,$page,$noOther);
renderPage18($cid,$pdf,$page,$noOther);
renderPage19($cid,$pdf,$page,$noOther);
renderPage20($cid,$pdf,$page,$noOther);
renderPage21($cid,$pdf,$page,$noOther);
renderPage22($cid,$pdf,$page,$noOther);

pdf_close($pdf);
echo pdf_get_buffer($pdf);
pdf_delete($pdf);
?>

