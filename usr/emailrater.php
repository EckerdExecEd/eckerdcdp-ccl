<?php 
$msg="";
session_start();
if(empty($_SESSION['cid'])){
    die("Not Logged in.");
}

$cid=$_SESSION['cid'];
require_once "../meta/candidate.php";
require_once "../meta/rater.php";
require_once "candfn.php";

$msg="";

if("send"==$_POST['what']){
    $type=$_POST['mailtype'];
    $msgbody=$_POST['mailbody'];
    $rcp=array();
    $count=$_POST['count'];
    $cid=$_SESSION['cid'];
    //echo $count;
    for($i=0;$i<$count;$i++){
		$val=$_POST["rcp".$i];
		if("N"!=$val){
			$rcp[]=$val;
		}
    }
    $lst=sendRaterEmail($rcp,$type,$msgbody,$cid);
    if(!$lst){
		$msg="<font color='#aa0000'>Unable to send email(s)</font>";
    }
    else{
		$msg="<font color='#00aa00'>Sent emails to $lst.</font>";
    } 
}
writeHead("Conflict Dynamics Profile - Candidates",false);
writeBody("Email Raters",$msg);
?>
<form name="mailfrm" action="emailrater.php" method=POST>
<input type="hidden" name="cid" value="">
<input type="hidden" name="what" value="">
<table border=1>
<tr>
<td><small>Name</small></td><td><small>Email</small></td><td><small> Include in mailing </small></td>
</tr>
<?
raterMailList($cid);
?>
<tr>
<td colspan=3>
<table border=1><tr>
<td bgcolor="#dddddd">
<small>
Initial Email <input type="radio" name="mailtype" value="I" checked>
</small>
</td>
<td bgcolor="#ffffff">
<small>
Reminder Email <input type="radio" name="mailtype" value="R" >
</small>
</td>
<td bgcolor="#dddddd">
<small>
Custom Email <input type="radio" name="mailtype" value="C" > 
</small>
</td>
</tr>
</table>
</tr>
<tr>
<td colspan=3><small>
Enter text for Custom Email:<br><textarea name="mailbody" cols=40 rows=5></textarea>
<br>
<input type="button" value="Send!" onClick="javascript:submitFrm(mailfrm);">
&nbsp;&nbsp;<input type="button" value="Cancel" onClick="javascript:mailfrm.action='home.php';mailfrm.submit();">
</small></td>
</tr>
</table>
</form>
<?php
writeFooter(false);
?>
<script language="JavaScript">
function submitFrm(frm){
    if(confirm("Are you sure you want to send the emails?")){
	frm.what.value="send";
	frm.submit();
    }
}
</script>
