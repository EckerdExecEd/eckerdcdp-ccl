<?php
require_once "../meta/dbfns.php";

define("SELF_",1);
define("BOSS_",2);
define("PEER_",3);
define("DR_",4);
define("OTHER_",5);


// The following is navigational / selection code for creating the group
// Renders various form editing scripts
function showScripts($frm){
	echo "<script language=\"JavaScript\">";
	// Submit script for consultants
	echo "  function submitCons($frm){";
	echo "    if ($frm.conid.value.length<1){";
	echo "      alert('Please select consultant');";
	echo "    } else {";
	echo "      $frm.submit();";
	echo "    }";
	echo "  }";
	
	// Submit script for programs
	// -- nothing here ---
	
	// Submit script for candidates / report generation
	echo "  function submitCands($frm){";
	echo "    if ($frm.descr.value.length<1){";
	echo "      alert('Please enter a name for report');";
	echo "    } else {";
	echo "      $frm.action=\"scoregroup.php\";";
	echo "      $frm.what.value=\"score\";";
	echo "      $frm.submit();";
	echo "    }";
	echo "  }";
	echo "</script>";
}

// retrieves a list of all active consultants
function getConsultants(){
	$conn=dbConnect();
	$query="select CONID, FNAME, LNAME, EMAIL from CONSULTANT where ACTIVE='Y' order by LNAME";
	$rs=mysql_query($query);
	return false==$rs?false:dbRes2Arr($rs);
}

// retrieves all closed/scored programs for a consultant
function getProgramsForCons($conid){
	$conn=dbConnect();
	$query="select a.PID, a.DESCR from PROGRAM a, PROGCONS b where a.PID=b.PID and a.EXPIRED<>'N' and a.ARCHFLAG='N' and b.CONID=$conid"; 	
	$rs=mysql_query($query);
	return false==$rs?false:dbRes2Arr($rs);
}

// retrieves candidates for selected programs, they m,ust have completed the survey i.e. end date is not null
// and they must have anm expired flag set to 'Y' i.e. they have been scored.
function getCandidatesForProgs($pids){
	$conn=dbConnect();
	// Note that the "self" has categoryid 1
	$query="select a.RID,a.FNAM, a.LNAM, c.DESCR, c.PID from RATER a, CANDIDATE b, PROGRAM c where a.CID=b.CID and a.CATID=1 and a.ENDDT is not null and a.EXPIRED='Y' and b.PID=c.PID and b.PID in ( ".implode($pids,",")." ) order by c.DESCR, a.LNAM";
	$rs=mysql_query($query);
	return false==$rs?false:dbRes2Arr($rs);
}

function showPrograms($conid,$frm){
	$rows=getProgramsForCons($conid);
	if(false!=$rows){
		foreach($rows as $row){
			echo "<tr>";
			echo "<td align=left>";
			echo $row[1];
			echo"</td>";
			echo "<td align=left>";
			echo "Include in Report <input type=checkbox name=\"$row[0]\"> ";
			echo"</td>";
			echo "</tr>";
		}
	}
	else{
		echo "<tr>";
		echo "<td align=left colspan=2>";
		echo "Unable to retrieve scored programs for consultant";
		echo"</td>";
		echo "</tr>";
	}
	echo "<tr>";
	echo "<td><input type='button' value='<< Back' onClick=\"javascript:$frm.show.value='';$frm.submit();\"></td>";
	echo "<td><input type='button' value='Next >>' onClick=\"javascript:$frm.show.value='cands';$frm.submit();\"></td>";
	echo "</tr>";
}

function showCandidates($pids,$frm){
	if(count($pids)>0){
		$rows=getCandidatesForProgs($pids);
		if(false!=$rows){
			$oldPid=0;
			foreach($rows as $row){
				if($row[4]!=$oldPid){
					echo "<tr>";
					echo "<td align=left colspan=2 bgcolor=\"#dddddd\">";
					echo "Program: ".$row[3];
					echo"</td>";
					echo "</tr>";
					$oldPid=$row[4];
				}
				echo "<tr>";
				echo "<td align=left>";
				echo $row[2].", ".$row[1];
				echo"</td>";
				echo "<td align=left>";
				echo "Include in Report <input type=checkbox name=\"$row[0]\"> ";
				echo"</td>";
				echo "</tr>";
			}
		}
		else{
					echo "<tr>";
			echo "<td align=left colspan=2>";
			echo "<font color=\"#ff0000\">Unable to retrieve candidates for selected program(s)</font>";
			echo"</td>";
			echo "</tr>";
		}
	}
	else{
		echo "<td align=left colspan=2>";
		echo "<font color=\"#ff0000\">You must select at least one program!</font>";
		echo"</td>";
		echo "</tr>";
	}
	echo "<tr>";
	echo "<td align=left colspan=2  bgcolor=\"#dddddd\">";
	echo "Enter a name for the report:&nbsp;<input type='text' name='descr'>";
	echo"</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td><input type='button' value='<< Back' onClick=\"javascript:$frm.show.value='progs';$frm.submit();\"></td>";
	echo "<td><input type='button' value='Finish >>' onClick=\"javascript:submitCands($frm);\"></td>";
	echo "</tr>";
}

function showConsultants($frm){
	echo "<tr>";
	echo "<td align=left>";
	$rows=getConsultants();
	if(false!=$rows){
		echo "<select name='conid'><option value=''> -- Select Consultant -- </option>";
		foreach($rows as $row){
			echo "<option value='$row[0]'>	$row[2], $row[1] ( $row[3] ) </option>";
		}
		echo "</select>";
	}
	else{
		echo "Error retrieving consultants";
	}
	
	echo"</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td align=left><input type='button' value='Next >>' onClick=\"javascript:$frm.show.value='progs';submitCons($frm);\"></td>";
	echo "</tr>";
}

// Here is the code for actually scoring a group report
function scoreGroupReport($rids,$conid,$descr){
	$rc="<br>";
	//-- first we need to create a placeholder for the group
	$gid=createGrp($conid,$descr);
	if(false==$gid){
		return $rc."<font color=\"#ff0000\">Error creating group.</font>";
	}
	$rc=$rc."Created group using Unique Group ID $gid<br>";

	//-- Next add raters to the group
	$cnt=addRatersToGroup($rids,$gid);
	if(false==$gid){
		cleanGroup($gid);
		return $rc."<font color=\"#ff0000\">Error accessing raters.</font>";
	}
	$rc=$rc."Using data from $cnt self raters...<br>";

	//-- 1. get number of raters in each category
	$ratCount=getValidRaterCount($gid);
	
	//-- 2. Calculate Scale Scores
	if(0==$ratCount[SELF_]){
		return $rc."No self raters - cannot continue scoring<br>";
	}
	
	$rc=$rc."Calculating scale score for ".$ratCount[SELF_]." self raters<br>";
	$raters=getRaterIds($gid,SELF_);
	if($raters){
		foreach($raters as $rater){
			$rc=$rc."Processing $rater[0]<br>";
			$rc=$rc.computeRaterScaleScores($rater[0],$gid,SELF_);
		}
	}
	
	if(0<$ratCount[BOSS_]){
		$rc=$rc."Calculating scale score for ".$ratCount[BOSS_]." boss raters<br>";
		$raters=getRaterIds($gid,BOSS_);
		if($raters){
			foreach($raters as $rater){
				$rc=$rc."Processing $rater[0]<br>";	
				$rc=$rc.computeRaterScaleScores($rater[0],$gid,BOSS_);
			}
		}
	}
	
	if(2<$ratCount[PEER_]){
		$rc=$rc."Calculating scale score for ".$ratCount[PEER_]." peer raters<br>";
		$raters=getRaterIds($gid,PEER_);
		if($raters){
			foreach($raters as $rater){
				$rc=$rc."Processing $rater[0]<br>";	
				$rc=$rc.computeRaterScaleScores($rater[0],$gid,PEER_);
			}
		}
	}

	if(2<$ratCount[DR_]){
		$rc=$rc."Calculating scale score for ".$ratCount[DR_]." direct report raters<br>";
		$raters=getRaterIds($gid,DR_);
		if($raters){
			foreach($raters as $rater){
				$rc=$rc."Processing $rater[0]<br>";	
				$rc=$rc.computeRaterScaleScores($rater[0],$gid,DR_);
			}
		}
	}

	$rc=$rc.calculateGroupReportScores($gid,$ratCount);
	//-- 
	return $rc;	
}

// This cleans out all the data for an entire group
function cleanGroup($gid){
	$conn=dbConnect();
	$query="delete from GRPORGPERSP where GID=$gid";
	mysql_query($query);
	$query="delete from GRPSCALESCORE where GID=$gid";
	mysql_query($query);
	$query="delete from GRPRPTSCORE where GID=$gid";
	mysql_query($query);
	$query="delete from GRPRATER where GID=$gid";
	mysql_query($query);
	$query="delete from GRPRPT where GID=$gid";
	mysql_query($query);
}

// creates a group for group report
function createGrp($conid,$descr){
	$conn=dbConnect();
	// Generate a random key
	$i=getKey("GRPRPT","GID");
	$query="insert into GRPRPT (GID,DESCR,SCOREDT,CONID) values ($i,'$descr',NOW(),$conid)"; 
	if(!mysql_query($query)){
	    return false;
	}
	return $i;
}

// associates the appropriate raters with the group
function addRatersToGroup($rids,$gid){
	$conn=dbConnect();
	// first get all the candidate IDs, based on the self-raters raterid
	$query="select CID from RATER where RID in ( ".implode($rids,",")." ) and ENDDT is not NULL";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$cids=array();
	for($i=0;$row=mysql_fetch_row($rs);$i++){
		$cids[$i]=$row[0];			
	}
	// Prepare to use data from raters
	$query="insert into GRPRATER (GID,RID,CATID) select $gid,RID,CATID from RATER where CID in ( ".implode($cids,",")." ) and ENDDT is not NULL";
	return false==mysql_query($query)?false:count($cids); 
}

// get rater count by category
function getValidRaterCount($gid){
	$conn=dbConnect();
	$query="select CATID,count(*) from GRPRATER where GID=$gid group by CATID order by CATID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$rows=dbRes2Arr($rs);
	if(!$rows){
		return false;
	}
	$rc=array(0,0,0,0,0,0,0,0,0,0,0);
	foreach($rows as $row){
		$key=$row[0];
		$rc[$key]=$row[1];
	}
	return $rc;
}

// Gets all raters belonging to a specific category
function getRaterIds($gid,$catid){
	$conn=dbConnect();
	$query="select RID from GRPRATER where GID=$gid and CATID=$catid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// function to return the appropriate Mean and Standard deviation etc for a Scale
function getScaleMeanAndStd($rid,$gid,$catid){
	$conn=dbConnect();
	$query="select b.RAWSCORE,a.MEANVAL$catid,a.DEVVAL$catid,a.SID from SCALE a,GRPSCALESCORE b where a.SID=b.SID and b.RID=$rid and b.GID=$gid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// Compute the raw and standardized scale scores for a single rater
function computeRaterScaleScores($rid,$gid,$catid){
	$conn=dbConnect();
	
	// Raw score
	$query="insert into GRPSCALESCORE (SID,RID,GID,CATID,RAWSCORE,STDSCORE) ";
	$query=$query."select b.SID, a.RID, $gid, $catid, AVG(VAL), 0 from RATERRESP a , SCALEITEM b, GRPRATER c ";
	$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and c.CATID=$catid and a.RID=$rid and VAL is not NULL group by b.SID,a.RID";
	$rs=mysql_query($query);
	if(!$rs){
		return "Error calculating RAW SCORE<br>$query<br>";	
	}

	// Standardized score
	$rows=getScaleMeanAndStd($rid,$gid,$catid);
	if(!$rows){
		return "";	
	}
	foreach($rows as $row){
		if(0!=$row[2]){
			$std=50+(10*(($row[0]-$row[1])/$row[2]));
			$query="update GRPSCALESCORE set STDSCORE=$std where SID=$row[3] and RID=$rid and GID=$gid";
			if(false==mysql_query($query)){
				return "Error calculate STANDARDIZED SCORE<br>$query<br>";
			}
		}
	}

	return "";	
}

// calculates the appropriate report scores for a group
function calculateGroupReportScores($gid,$ratNum){
	$verbose=false;
	$conn=dbConnect();
	$rc="";
	// always calculate for Self
	$query="insert into GRPRPTSCORE (SID,GID,CATID,VALIDRATERS,AVGSCORE) select SID,GID,CATID,$ratNum[1],AVG(STDSCORE) from GRPSCALESCORE where GID=$gid and CATID=1 and STDSCORE is not NULL group by SID,GID,CATID";
	mysql_query($query);
	$rc=$rc."Performing roll-up for self<br>";
	if($verbose){
		$rc=$rc.$query."<br>";	
	}
	$rc=$rc.calcOrgPerspective($gid,"1");
	
	// calculate Bosses is we have them
	if($ratNum[2]>0){
		$query="insert into GRPRPTSCORE (SID,GID,CATID,VALIDRATERS,AVGSCORE) select SID,GID,CATID,$ratNum[2],AVG(STDSCORE) from GRPSCALESCORE where GID=$gid and CATID=2 and STDSCORE is not NULL group by SID,GID,CATID";
		mysql_query($query);
		$rc=$rc."Performing roll-up for boss category<br>";
		if($verbose){
			$rc=$rc.$query."<br>";	
		}
		$rc=$rc.calcOrgPerspective($gid,"2");
	}
	else{
		$rc=$rc."No boss data<br>";	
	}
	
	// If we have enough Peers and DRs we calculate them separately
	// do we have at least 3 in each category ?
	if($ratNum[3]>=3 && $ratNum[4]>=3){
		$query="insert into GRPRPTSCORE (SID,GID,CATID,VALIDRATERS,AVGSCORE) select SID,GID,CATID,$ratNum[3],AVG(STDSCORE) from GRPSCALESCORE where GID=$gid and CATID=3 and STDSCORE is not NULL group by SID,GID,CATID";
		mysql_query($query);
		$rc=$rc."Performing roll-up for peer category<br>";
		if($verbose){
			$rc=$rc.$query."<br>";	
		}
		$query="insert into GRPRPTSCORE (SID,GID,CATID,VALIDRATERS,AVGSCORE) select SID,GID,CATID,$ratNum[4],AVG(STDSCORE) from GRPSCALESCORE where GID=$gid and CATID=4 and STDSCORE is not NULL group by SID,GID,CATID";
		mysql_query($query);
		$rc=$rc."Performing roll-up for direct report category<br>";
		if($verbose){
			$rc=$rc.$query."<br>";	
		}
		// Now do the Combined Others category
		$cnt=($ratNum[2]+$ratNum[3]+$ratNum[4]);
		$query="insert into GRPRPTSCORE (SID,GID,CATID,VALIDRATERS,AVGSCORE) select SID,GID,6,$cnt,AVG(STDSCORE) from GRPSCALESCORE where GID=$gid and CATID in (2,3,4)  and STDSCORE is not NULL group by SID,GID";
		mysql_query($query);
		$rc=$rc."Performing roll-up for Combined Others category<br>";
		if($verbose){
			$rc=$rc.$query."<br>";	
		}
		$rc=$rc.calcOrgPerspective($gid,"3");
		$rc=$rc.calcOrgPerspective($gid,"4");
	}
	else{
		// Otherwise we calculate them as a Combined Peers+DR Group
		$cnt=($ratNum[3]+$ratNum[4]);
		// do we have at least 3 in the combined group?
		if($cnt>=3){
			$query="insert into GRPRPTSCORE (SID,GID,CATID,VALIDRATERS,AVGSCORE) select SID,GID,5,$cnt,AVG(STDSCORE) from GRPSCALESCORE where GID=$gid and CATID in (3,4)  and STDSCORE is not NULL group by SID,GID";
			mysql_query($query);
			$rc=$rc."Performing roll-up for $cnt raters in combined direct report and peer category<br>";
			if($verbose){
				$rc=$rc.$query."<br>";	
			}
			// Now do the Combined Others category
			$cnt=($ratNum[2]+$ratNum[3]+$ratNum[4]);
			$query="insert into GRPRPTSCORE (SID,GID,CATID,VALIDRATERS,AVGSCORE) select SID,GID,6,$cnt,AVG(STDSCORE) from GRPSCALESCORE where GID=$gid and CATID in (2,3,4)  and STDSCORE is not NULL group by SID,GID";
			mysql_query($query);
			$rc=$rc."Performing roll-up for Combined Others category<br>";
			if($verbose){
				$rc=$rc.$query."<br>";	
			}
			// organizational perspective
			$rc=$rc.calcOrgPerspective($gid,"5");
		}
		else{
			// Nope - but we can possibly do a combined other category that only contains bosses	
			if($ratNum[2]>0){
				$query="insert into GRPRPTSCORE (SID,GID,CATID,VALIDRATERS,AVGSCORE) select SID,GID,6,$ratNum[2],AVG(STDSCORE)from GRPSCALESCORE where GID=$gid and CATID=2  and STDSCORE is not NULL group by SID,GID";
				mysql_query($query);
				$rc=$rc."Performing roll-up for Combined Others category<br>";
				if($verbose){
					$rc=$rc.$query."<br>";	
				}
			}
			else{
				$rc=$rc."No other roll-ups possible<br>";
			}
		}
	}
	return $rc;
}

// Calculates the organization perespective
function calcOrgPerspective($gid,$catid){
	$conn=dbConnect();
	$query="insert into GRPORGPERSP (GID,CATID,ITEMID,SCORE) ";
	$rc="Calculating Organizational Perspective for ";	
	if("1"==$catid){
		// for self it's just a value and quetions are 100-114
		$query=$query."select a.GID,a.CATID,b.ITEMID,AVG(b.VAL) from GRPRATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>99 and a.GID=$gid and a.CATID=1  and b.VAL is not NULL group by a.GID,a.CATID,b.ITEMID";
		$rc=$rc." Self<br>";
	}
	else{
		if("5"==$catid){
			//for combined Peers and DR 
			$query=$query."select a.GID,5,b.ITEMID,AVG(b.VAL) from GRPRATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>63 and b.ITEMID<79 and a.GID=$gid and a.CATID in (3,4)  and b.VAL is not NULL group by a.GID,b.ITEMID";
		}
		else{
			//for all others 
			$query=$query."select a.GID,a.CATID,b.ITEMID,AVG(b.VAL) from GRPRATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>63 and b.ITEMID<79 and a.GID=$gid and a.CATID=$catid and b.VAL is not NULL group by a.GID,a.CATID,b.ITEMID";
		}
		$rc=$rc." Rater category $catid<br>";
	}
	mysql_query($query);
	return $rc;
}

//-- This function lists available group reports by consultant
//-- Note: we don't do anything with the parameter "tid" at this point, since
//-- group reporst are not an option for anything other than 360
function listGroupReportsByConsultant($conid,$frm,$tid){
	$conn=dbConnect();
	$query="select GID,DESCR,SCOREDT from GRPRPT where CONID=$conid";
	//die($query);
	$rs=mysql_query($query);
	if(false==$rs){
		return "<tr><td><font color=\"#ff0000\"><small>No group reports defined</small></font></td></tr>";
	}
	$rc="<tr><td><small>Click on report name to view report </small></td></tr>";
	while($row=mysql_fetch_array($rs)){
		$rc.="<tr><td> <a href=\"showgroupreport.php?gid=$row[0]\" target=\"grprpt\">$row[1]</a> </td></tr>";
	}
	return $rc;
}

?>
