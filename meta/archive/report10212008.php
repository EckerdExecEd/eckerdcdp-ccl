<?php
include_once "dbfns.php";
// Database functions for generating reports

// This is the graph on Page 7 "Constructive Responses"
// Include categories 1 and 6 (Self and Others) and sacles 1-7
function getConstructiveResponses($cid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.CID, a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID<8 and a.CATID in (1,6) order by a.CATID, a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is the detail breakdown on Page 8 "Active Constructive"
// Include the 
function getActiveConstructiveResponses($cid){
	$conn=dbConnect();
	$query="select a.AVGSCORE,a.AGREEMENTSCORE,  b.DESCR,  a.CATID,a.VALIDRATERS, a.CID, a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID<5 and a.CATID<>6 order by a.SID, a.CATID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is the detail breakdown on Page 9 "Passive Constructive"
function getPassiveConstructiveResponses($cid){
	$conn=dbConnect();
	$query="select a.AVGSCORE,a.AGREEMENTSCORE,  b.DESCR,  a.CATID,a.VALIDRATERS, a.CID, a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID<8 and b.SID>4 and a.CATID<>6 order by a.SID, a.CATID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is the graph on Page 10 "Destructive Responses"
// Include categories 1 and 6 (Self and Others) and sacles 8-15
function getDestructiveResponses($cid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.CID, a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID<16 and b.SID>7 and a.CATID in (1,6) order by a.CATID, a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is the detail breakdown on Page 11 "Active Destructive"
function getActiveDestructiveResponses($cid){
	$conn=dbConnect();
	$query="select a.AVGSCORE,a.AGREEMENTSCORE,  b.DESCR,  a.CATID,a.VALIDRATERS, a.CID, a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID>7 and b.SID<12  and a.CATID<>6 order by a.SID, a.CATID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is the detail breakdown on Page 12 "Passive Destructive"
function getPassiveDestructiveResponses($cid){
	$conn=dbConnect();
	$query="select a.AVGSCORE,a.AGREEMENTSCORE,  b.DESCR,  a.CATID,a.VALIDRATERS, a.CID, a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID<16 and b.SID>11 and a.CATID<>6 order by a.SID, a.CATID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is the Scale Profile on page 13
function getScaleScore($cid,$catid){
	$conn=dbConnect();
	$query="(select HITEXT,AVGSCORE,VALIDRATERS from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and a.CATID=$catid and b.SID<16 and round(AVGSCORE)>=60 order by AVGSCORE desc) UNION (select LOTEXT,AVGSCORE,VALIDRATERS from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and a.CATID=$catid and b.SID<16 and round(AVGSCORE)<=40 order by AVGSCORE desc)";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$rc=dbRes2Arr($rs);
	$cnt=count($rc);
	if($cnt<2){
		// we have a result, but how many rows do we have in it? We should have at least 2
		// if we don't, we get the one or two most extreme outlayers whether up or down
		// make sure not to double-dip, though
		$query="select HITEXT,LOTEXT,ABS(AVGSCORE-50),AVGSCORE,VALIDRATERS from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and a.CATID=$catid  and b.SID<16"; 
		$query=$query." AND ((round(AVGSCORE)<60 and round(AVGSCORE)>=50) ";
		$query=$query." OR (round(AVGSCORE)<50 and round(AVGSCORE)>40)) order by 3 DESC";
		
		$rs=mysql_query($query);
		if(!$rs){
			return false;
		}
		$tmp=dbRes2Arr($rs);

		// we might already have something in the first position rc[0]
		if($cnt==0){
			$arr2=array();
			if($tmp[1][3]>=50){
				$arr2[0]=$tmp[1][0];
			}
			else{
				$arr2[0]=$tmp[1][1];
			}
			$arr2[1]=$tmp[1][2];
			$arr2[2]=$tmp[1][4];
			$rc[0]=$arr2;
		}

		// we will always need one in position rc[1]
		$arr1=array();
		if($tmp[0][3]>=50){
			$arr1[0]=$tmp[0][0];
		}
		else{
			$arr1[0]=$tmp[0][1];
		}
		$arr1[1]=$tmp[0][2];
		$arr1[2]=$tmp[0][4];
		$rc[1]=$arr1;

	}
	return $rc;
}

// This is the discrepancy profile on page 14
// 03/06/2007: Exclude 0 values
function getDiscrepancyProfile($cid,$catid){
	$conn=dbConnect();
	$query="select b.DESCR, a.CATID,a.ITEMID,a.AVGSCORE from DISCREPANCYPROFILE a, DISCREPANCYITEM b where a.SID=b.SID and a.ITEMID=b.ITEMID and a.CID=$cid and a.CATID=$catid and a.AVGSCORE>0 order by CATID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is a helper function to do the graphs on P 14 - call once for each item
// 03/06/2007: Exclude 0 values
function getDiscrepancyProfileGraph($cid,$catid,$itemid){
	$conn=dbConnect();
	$query="";
	if($catid!="5"){
		$query="select b.CATID, AVG(a.VAL),COUNT(a.RID) from RATERRESP a, RATER b where a.RID=b.RID and a.ITEMID=$itemid and b.CID=$cid and b.CATID in (1,$catid) and a.VAL is not NULL and a.VAL>0 group by b.CATID order by b.CATID asc";
	}
	else{
		// merge peers/dr
		$query="(select b.CATID, AVG(a.VAL),COUNT(a.RID) from RATERRESP a, RATER b where a.RID=b.RID and a.ITEMID=$itemid and b.CID=$cid and b.CATID=1 and a.VAL is not NULL  and a.VAL>0 group by b.CATID)";
		$query=$query." UNION ";
		$query=$query."(select 5,AVG(a.VAL),COUNT(a.RID) from RATERRESP a, RATER b where a.RID=b.RID and a.ITEMID=$itemid and b.CID=$cid and b.CATID in(3,4) and a.VAL is not NULL  and a.VAL>0 )";
	}
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is the Dynamic Conflict Sequence on page 15
// This is the sequence Graph on page 16 if called with $catid="6"
function getDynamicSequence($cid,$catid=NULL){
	$conn=dbConnect();
	$query="select AVGSCORE,SID,CATID,VALIDRATERS from REPORTSCORE where CID=$cid and SID>15 and SID<22 ";
	if("6"==$catid){
		$query=$query." and CATID=6 order by SID ASC";
	}
	elseif("1"==$catid){
		// This is a special for Mercer-Deltas bar graphs
		$query=$query." and CATID=1 order by SID ASC";
	}
	else{
		$query=$query." order by CATID ASC,SID ASC";
	}
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// Shows how many bosses we have 
function getNumberOfBosses($cid){
	$conn=dbConnect();
	$rs=mysql_query("select VALIDRATERS from REPORTSCORE where CID=$cid and CATID=2 and SID=1");
	return $rs?dbRes2Arr($rs):false;	
}

// This is the Organizational perspective on page 17
function getOrgPersp($cid){
	$conn=dbConnect();
	$query="select a.ITEMID,a.SCORE,a.CATID from ORGPERSPECTIVE a where a.CID=$cid order by a.CATID asc, a.ITEMID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;	
}

// This is the hot buttons graph on page 19
function getHotButtons($cid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID>21 and a.CATID=1 order by a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}
?>

