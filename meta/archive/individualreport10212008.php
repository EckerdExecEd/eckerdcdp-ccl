<?php
/*=============================================================================*
* Revision History:
* 04-20-2005 TRM: We are now returning to the "clipped" graphs, i.e. don't go
* outside the graph with lines for Constructive/desctrictive Graphs,
* Sequence Graph and Hot Buttons graph. This is per request by Craig Runde.
*
* 04-20-2005 TRM: Fixed small bug in Scale Profile page 13, where no Boss
* data would show up as two small periods.
*
*=============================================================================*/
include_once "report.php";

// a standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);

// RGB values for the report
define("R_",0.85);
define("G_",0.85);
define("B_",1.0);

function getNumRaters($cid){
	$conn=dbConnect();
	// 2005-04-14: The original query caught some partial responses, so it has been replaced with the query from scoring.php
	//	$query="select a.CATID, count(distinct a.RID) from RATER a, RATERRESP b where a.RID=b.RID and a.CATID<>1 and a.CID=$cid and b.VAL is not NULL group by a.CATID order by a.CATID asc";
	$query="select CATID,count(*)  from RATER where CID=$cid and ENDDT is not NULL group by CATID order by CATID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function getRatersByCat($cid){
	$rows=getNumRaters($cid);
	if(!$rows){
		return false;
	}
	$data=array(0,0,0,0,0,0,0,0);
	foreach($rows as $row){
		$data[$row[0]]=$row[1];
	}
	$data[5]=($data[3]+$data[4]);
	return $data;
}

function getCandidateName($cid){
	$conn=dbConnect();
	$query="select FNAME, LNAME from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return utf8_decode($row[0])." ".utf8_decode($row[1]);
}

function getRaterInteraction($cid){
	$conn=dbConnect();
	// 2005-04-29: The original query caught some partial responses, so it has been replaced with the query from scoring.php
	//$query="select a.CATID, b.TXT,count(b.TXT) from RATER a, RATERDEMOGR b where a.RID=b.RID and b.DMID=2 and a.cid=$cid group by a.CATID, b.TXT order by a.CATID asc";
	$query="select a.CATID, b.TXT,count(b.TXT) from RATER a, RATERDEMOGR b where a.RID=b.RID and a.ENDDT is not NULL and b.DMID=2 and a.cid=$cid group by a.CATID, b.TXT order by a.CATID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}


function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function renderPage1($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,600,0.2);

	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),550);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="Sal Capobianco, Ph.D.               Mark Davis, Ph.D.               Linda Kraus, Ph.D.";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),512);

	// 6 inches form top of 11 inch page
	$size=14.0;
	$ypos=round((5.25*PAGE_HEIGHT)/11);
	$linehgth=1.5*$size;

	pdf_setfont($pdf,$font,$size);
	$title="Prepared for:";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos);

//	pdf_show_xy($pdf,strtoupper($name),calcCenterStartPos($pdf,strtoupper($name),$font,$size),$ypos-$linehgth);
// 2007-09-19 Thor: removed upper case from name
	pdf_show_xy($pdf,$name,calcCenterStartPos($pdf,$name,$font,$size),$ypos-$linehgth);

	$title=date("F j, Y");
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos-$linehgth-$linehgth);

	$raters=getNumRaters($cid);
	if($raters){
		// get rater interaction by rater category
		$interaction=getRaterInteraction($cid);
		$dummy=array();
		$boss=array(0,0,0,0,0);
		$peer=array(0,0,0,0,0);
		$dr=array(0,0,0,0,0);
		$howWell=array($dummy,$dummy,$boss,$peer,$dr);
		foreach($interaction as $act){
			$cat=$act[0];
			$val=$act[1];
			$cnt=$act[2];
			$howWell[$cat][$val]=$cnt;
		}

		// Build the appropriate text
		// the counts and interaction levels
		$descr=array("","Self","Boss","Peers","Direct Reports");
		$cont=array("","Hardly","Somewhat","Well","Extremely Well");
		$number=array(0,0,0,0,0);
		$interaction=array("","","","","");
		foreach($raters as $rater){
			$number[$rater[0]]=$rater[1];
			// Yikes, all this to display the interaction level!
			$str="";
			$catData=$howWell[$rater[0]];
			for($i=1;$i<5;$i++){
				if(0!=$catData[$i]){
					if(0!=strlen($str)){
						$str=$str."; ";
					}
					$str=$str.$cont[$i]."(".$catData[$i].")";
				}
			}
			$interaction[$rater[0]]=$str;
			// Wow!
		}

		// Display the results
		$y=150;
		$size=10.0;
		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Rater",75,$y);
		pdf_show_xy($pdf,"Number",175,$y);
		pdf_show_xy($pdf,"How Well Does Rater Know the Individual?",250,$y);

		$size=12.0;
		pdf_setfont($pdf,$font,$size);
		$oCnt=0;
		for($i=2;$i<5;$i++){
			$y-=20;
			pdf_show_xy($pdf,$descr[$i],75,$y);
			pdf_show_xy($pdf,$number[$i],190,$y);
			pdf_show_xy($pdf,$interaction[$i],250,$y);
			// determine whether to show data for Other than Self
			if(2==$i){
				// Any Boss...
				if($number[$i]>0){
					$noOther=false;
				}
			}
			elseif(3==$i||4==$i){
				// ...or any combination of at least 3 in any other category
				$oCnt+=$number[$i];
				if($oCnt>=3){
					$noOther=false;
				}
			}
		}
	}

	writeFooter($pdf,$name,$page);
}

function renderPage2($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=16.0;
	pdf_setfont($pdf,$bfont,$size);

	$title="Table of Contents";

	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),650);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$y=650;
	$x=55;
	$x2=475;

	pdf_show_xy($pdf,"Introduction.......................................................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"3",$x2,$y);

	pdf_show_xy($pdf,"Guide to Your Feedback Report.......................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"6",$x2,$y);

	pdf_show_xy($pdf,"Active-Constructive Response Profile..............................................................",$x,$y-=30);
	pdf_show_xy($pdf,"8",$x2,$y);

	pdf_show_xy($pdf,"Passive-Constructive Response Profile.............................................................",$x,$y-=30);
	pdf_show_xy($pdf,"9",$x2,$y);

	pdf_show_xy($pdf,"Active-Destructive Response Profile.................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"11",$x2,$y);

	pdf_show_xy($pdf,"Passive-Destructive Response Profile...............................................................",$x,$y-=30);
	pdf_show_xy($pdf,"12",$x2,$y);

	pdf_show_xy($pdf,"Scale Profile......................................................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"13",$x2,$y);

	pdf_show_xy($pdf,"Discrepancy Profile..........................................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"14",$x2,$y);

	pdf_show_xy($pdf,"Dynamic Conflict Sequence..............................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"15",$x2,$y);

	pdf_show_xy($pdf,"Organizational Perspective on Conflict............................................................",$x,$y-=30);
	pdf_show_xy($pdf,"17",$x2,$y);

	pdf_show_xy($pdf,"Hot Buttons Profile...........................................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"18",$x2,$y);

	pdf_show_xy($pdf,"Developmental Feedback..................................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"20",$x2,$y);

	pdf_show_xy($pdf,"Developmental Worksheets...............................................................................",$x,$y-=30);
	pdf_show_xy($pdf,"21",$x2,$y);

	writeFooter($pdf,$name,$page);
}

function renderPage3($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Introduction";

	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=11.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Conflict refers to any situation in which people have incompatible interests, goals, principles, or",50,640);
	pdf_continue_text($pdf,"feelings. This is, of course, a broad definition and encompasses many different situations. A conflict");
	pdf_continue_text($pdf,"could arise, for instance, over a long-standing set of issues, a difference of opinion about strategy or");
	pdf_continue_text($pdf,"tactics in the accomplishment of some business goal, incompatible beliefs, competition for resources,");
	pdf_continue_text($pdf,"and so on. Conflicts can also result when one person acts in a way that another individual sees as");
	pdf_continue_text($pdf,"insensitive, thoughtless, or rude. A conflict, in short, can result from anything that places you and");
	pdf_continue_text($pdf,"another person in opposition to one another.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,"Thus, conflict in life is inevitable. Despite our best efforts to prevent it, we inevitably find ourselves");
	pdf_continue_text($pdf,"in disagreements with other people at times. This is not, however, necessarily bad. Some kinds of");
	pdf_continue_text($pdf,"conflict can be productive--differing points of view can lead to creative solutions to problems. What");
	pdf_continue_text($pdf,"largely separates useful conflict from destructive conflict is how the individuals respond when the");
	pdf_continue_text($pdf,"conflict occurs. Thus, while conflict itself is inevitable, ineffective and harmful responses to conflict can");
	pdf_continue_text($pdf,"be avoided, and effective and beneficial responses to conflict can be learned. That proposition is at");
	pdf_continue_text($pdf,"the heart of the Conflict Dynamics Profile (CDP) Feedback Report you have received.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,"Some responses to conflict, whether occurring at its earliest stages or after it develops, can be");
	pdf_continue_text($pdf,"thought of as constructive responses. That is, these responses have the effect of not escalating the");
	pdf_continue_text($pdf,"conflict further. They tend to reduce the tension and keep the conflict focused on ideas, rather than");
	pdf_continue_text($pdf,"personalities. Destructive responses, on the other hand, tend to make things worse--they do little to");
	pdf_continue_text($pdf,"reduce the conflict, and allow it to remain focused on personalities. If conflict can be thought of as a");
	pdf_continue_text($pdf,"fire, then constructive responses help to put the fire out, while destructive responses make the fire");
	pdf_continue_text($pdf,"worse. Obviously, it is better to respond to conflict with constructive rather than destructive responses.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,"It is also possible to think of responses to conflict not simply as constructive or destructive, but as");
	pdf_continue_text($pdf,"differing in terms of how active or passive they are. Active responses are those in which the individual");
	pdf_continue_text($pdf,"takes some overt action in response to the conflict or provocation. Such responses can be either");
	pdf_continue_text($pdf,"constructive or destructive--what makes them active is that they require some overt effort on the part");
	pdf_continue_text($pdf,"of the individual. Passive responses, in contrast, do not require much in the way of effort from the");
	pdf_continue_text($pdf,"person. Because they are passive, they primarily involve the person deciding to not take some kind of");
	pdf_continue_text($pdf,"action. Again, passive responses can be either constructive or destructive--that is, they can make");
	pdf_continue_text($pdf,"things better or they can make things worse.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Responses to Conflict: Four Profiles");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,"Given that responses can be either constructive or destructive, and that they can be either active or");
	pdf_continue_text($pdf,"passive, it is possible to think of responses to conflict as falling into one of four categories: Active-");
	pdf_continue_text($pdf,"Constructive responses, Passive-Constructive responses, Active-Destructive responses, and Passive-");
	pdf_continue_text($pdf,"Destructive responses. The first part of this report will describe how you see yourself--and how others");
	pdf_continue_text($pdf,"see you--in each of these four areas. Because there are several different behaviors which fall into");
	pdf_continue_text($pdf,"each of these areas, your self-ratings and the ratings by your boss, your peers, and your direct reports");
	pdf_continue_text($pdf,"will be compared for each kind of behavior. You should pay special attention to those cases in which");
	pdf_continue_text($pdf,"others see your behavior differently than you do.");
	writeFooter($pdf,$name,$page);
}

function renderPage4($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,"Scale Profile",50,680);
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"The next portion of this report is the Scale Profile. This section summarizes the ways in which you");
	pdf_continue_text($pdf,"are seen by your boss, your peers, and your direct reports. The manner in which these people view");
	pdf_continue_text($pdf,"you has a powerful impact on what they expect from you, how they will interpret your actions, and how");
	pdf_continue_text($pdf,"they behave towards you. Any feedback from others that you are acting in destructive ways during");
	pdf_continue_text($pdf,"conflict situations strongly suggests that working to change those views of you will be helpful.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Discrepancy Profile");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"The next portion of the report is the Discrepancy Profile. This section identifies the specific items");
	pdf_continue_text($pdf,"from the CDP on which your self-perceptions and the observations of others are the most different.");
	pdf_continue_text($pdf,"Because the way you view yourself can often differ dramatically from the way others view you,");
	pdf_continue_text($pdf,"included here are the specific responses to conflict on which your perceptions are most divergent from");
	pdf_continue_text($pdf,"those of other people. These items may be especially helpful in understanding how you are coming");
	pdf_continue_text($pdf,"across to other people during times of conflict.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Dynamic Conflict Sequence");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"The next portion of the report is the Dynamic Conflict Sequence. Based on the idea that conflict is");
	pdf_continue_text($pdf,"a dynamic process which unfolds over time, this section describes the ways in which you respond to");
	pdf_continue_text($pdf,"conflict when it is just beginning, when it is fully underway, and after it is over. This information may be");
	pdf_continue_text($pdf,"especially useful in helping to identify when in the sequence you handle conflict most constructively,");
	pdf_continue_text($pdf,"and when in the sequence you handle it least constructively.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Organizational Perspective on Conflict");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"The next portion of this report is the Organizational Perspective on Conflict, and is based on the");
	pdf_continue_text($pdf,"fact that organizations differ in terms of which particular responses to conflict are especially valued and");
	pdf_continue_text($pdf,"which are especially frowned upon. This section describes what you, your boss, peers, and direct");
	pdf_continue_text($pdf,"reports feel are the most \"toxic\" responses to conflict in your organization--the responses which will do");
	pdf_continue_text($pdf,"the most to damage one's career with the organization.");
	pdf_continue_text($pdf,"   ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Hot Buttons Profile");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"The next portion of the report is the Hot Buttons section. Presented here is information regarding");
	pdf_continue_text($pdf,"the kinds of situations and individuals that you find most annoying, and thus are more likely to provoke");
	pdf_continue_text($pdf,"conflicts--in short, your \"hot buttons.\" By learning something about the situations in which you are");
	pdf_continue_text($pdf,"most likely to feel upset, you can, we hope, better avoid conflicts in the future.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Developmental Feedback and Worksheets");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"The final portion of the report is a little different. Your boss, peers, and direct reports were given");
	pdf_continue_text($pdf,"the opportunity to directly offer comments to you regarding how you handle conflict; any comments");
	pdf_continue_text($pdf,"that they provided are reported in this section. In addition, two worksheets are included to help you");
	pdf_continue_text($pdf,"identify the areas in which the CDP suggests that you have the clearest opportunities for development.");

	writeFooter($pdf,$name,$page);
}

function renderPage5($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Final Note: Interpreting Feedback From Others",50,640);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);

	pdf_continue_text($pdf,"One of the most powerful features of the Conflict Dynamics Profile is that it provides you with an");
	pdf_continue_text($pdf,"accurate picture of how you are viewed by other people. For each of the different ways of responding");
	pdf_continue_text($pdf,"to conflict--constructive and destructive, active and passive--the CDP provides a measure of how your");
	pdf_continue_text($pdf,"boss, peers, and direct reports perceive you. It is difficult to overestimate how important these");
	pdf_continue_text($pdf,"impressions can be in affecting how these people will evaluate you and act toward you. Thus, any");
	pdf_continue_text($pdf,"substantial discrepancies between your self-perceptions and the views held by others should be given");
	pdf_continue_text($pdf,"serious attention.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"The way in which the information in this Feedback Report is usually presented is through");
	pdf_continue_text($pdf,"standardized scores. This method takes the responses of you, your boss, peers, and direct reports,");
	pdf_continue_text($pdf,"and compares them to the responses of thousands of people who have also taken the CDP. By doing");
	pdf_continue_text($pdf,"so, this provides a standard by which to evaluate the way you, your boss, peers, and direct reports see");
	pdf_continue_text($pdf,"your behavior. These standardized scores take the form of numbers ranging from 0 to 100, although");
	pdf_continue_text($pdf,"most scores will fall between 35 and 65. Whenever such scores are presented, there will also be");
	pdf_continue_text($pdf,"some indication as to whether--compared to thousands of others--that score is very low, low, average,");
	pdf_continue_text($pdf,"high, or very high.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"Two additional issues should be mentioned. First, sometimes there is good agreement among");
	pdf_continue_text($pdf,"others regarding your behavior; everyone essentially views you in the same way. Other times there is");
	pdf_continue_text($pdf,"poor agreement among others; different people view your behavior in different ways. The CDP");
	pdf_continue_text($pdf,"provides an estimate of how much \"rater agreement\" there is among the people who rated you. For");
	pdf_continue_text($pdf,"both the \"peer\" and \"direct report\" categories, for each scale separately, the right-hand column will");
	pdf_continue_text($pdf,"indicate whether rater agreement regarding this behavior was \"high\", \"moderate\", or \"low\". In the case");
	pdf_continue_text($pdf,"of \"high\" or \"moderate\" rater agreement, it means that the raters are in generally good agreement");
	pdf_continue_text($pdf,"regarding how you act during conflict situations. In the case of \"low\" rater agreement, there are two");
	pdf_continue_text($pdf,"possibilities: 1) it may be that the raters generally disagree about this behavior, with some seeing you");
	pdf_continue_text($pdf,"as frequently acting this way, and others seeing you as infrequently doing so; or 2) it may be that there");
	pdf_continue_text($pdf,"is one rater in particular who differs markedly from the others. In either case, it indicates that there is");
	pdf_continue_text($pdf,"less consensus about this particular behavior. It will be a good idea to consider the reasons why such");
	pdf_continue_text($pdf,"disagreement might exist.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"Second, in order to maintain the anonymity of the peers and direct reports who answered questions");
	pdf_continue_text($pdf,"about you, no information from these groups is provided unless there are at least three respondents.");
	pdf_continue_text($pdf,"If only one or two peers or direct reports return information, then the peers and direct reports");
	pdf_continue_text($pdf,"categories are combined into a single \"peers/reports\" category.");

	writeFooter($pdf,$name,$page);
}

function renderPage6($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Guide to Your Feedback Report";

	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	// the bold headings
	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf," ");

	pdf_show_xy($pdf,"Active-Constructive",50,640);
	pdf_continue_text($pdf,"Response Profile");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Passive-Constructive");
	pdf_continue_text($pdf,"Response Profile");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Active-Destructive");
	pdf_continue_text($pdf,"Response Profile");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Passive-Destructive");
	pdf_continue_text($pdf,"Response Profile");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Scale Profile");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Discrepancy Profile");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Dynamic Conflict");
	pdf_continue_text($pdf,"Sequence");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Organizational");
	pdf_continue_text($pdf,"Perspective on");
	pdf_continue_text($pdf,"Conflict");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Hot Buttons Profile");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Developmental");
	pdf_continue_text($pdf,"Feedback");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Developmental");
	pdf_continue_text($pdf,"Worksheets");


	// the explanatory text
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Four ways of responding to conflict which require some effort on the part of",200,640);
	pdf_continue_text($pdf,"the individual, and which have the effect of reducing conflict: Perspective");
	pdf_continue_text($pdf,"Taking, Creating Solutions, Expressing Emotions, and Reaching Out.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Three ways of responding to conflict which have the effect of dampening");
	pdf_continue_text($pdf,"the conflict, or preventing escalation, but which do not require any active");
	pdf_continue_text($pdf,"response from the individual: Reflective Thinking, Delay Responding, and");
	pdf_continue_text($pdf,"Adapting.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Four ways of responding to conflict which through some effort on the part");
	pdf_continue_text($pdf,"of the individual have the effect of escalating the conflict: Winning at All");
	pdf_continue_text($pdf,"Costs, Displaying Anger, Demeaning Others, and Retaliating.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Four ways of responding to conflict which due to lack of effort or action by");
	pdf_continue_text($pdf,"the individual cause the conflict to either continue, or to be resolved in an");
	pdf_continue_text($pdf,"unsatisfactory manner: Avoiding, Yielding, Hiding Emotions, and Self-");
	pdf_continue_text($pdf,"Criticizing.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"How your typical responses during conflict are viewed by your boss, peers,");
	pdf_continue_text($pdf,"and direct reports.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"The particular responses to conflict on which your own self-perceptions and");
	pdf_continue_text($pdf,"those of others differ most markedly.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"How constructively and destructively you respond to conflict before it");
	pdf_continue_text($pdf,"begins, after it is underway, and after it is over.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"The particular responses to conflict which are especially discouraged in");
	pdf_continue_text($pdf,"your organization; regularly engaging in these responses can have severe");
	pdf_continue_text($pdf,"negative effects on one's career.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"The types of situations and individuals most likely to irritate you and");
	pdf_continue_text($pdf,"provoke conflict.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Direct comments about your responses to conflict from boss, peers, and");
	pdf_continue_text($pdf,"direct reports.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_continue_text($pdf,"Two worksheets to aid you in identifying your clearest opportunites for");
	pdf_continue_text($pdf,"development.");

	writeFooter($pdf,$name,$page);
}

// This is the box that's common for page 7 and 10
function drawBigGraph(&$pdf,$x,$y,$width,$height,$data,$noOther){
	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=(count($data)/2);
	if(true==$noOther){
		$scales=count($data);
	}
	$ystep=round($height/8);
	$xstep=round($width/($scales+1));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$size=9.0;
	pdf_setfont($pdf,$font,$size);

	// 1. the box outline
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);

	// 2. Horizontal divisions
	$i;
	for($i=round($y+$ystep);$i<($y+round($ystep*8));$i=round($i+$ystep)){
		pdf_moveto($pdf,($x+$xstep),$i);
		pdf_lineto($pdf,($x+$xtsep+$width),$i);
	}
	// little box that juts out on the left
	pdf_moveto($pdf,$x,$y);
	pdf_lineto($pdf,($x+$xstep),$y);
	pdf_moveto($pdf,$x,$y);
	pdf_lineto($pdf,$x,($y+$ystep));
	pdf_moveto($pdf,$x,($y+$ystep));
	pdf_lineto($pdf,($x+$xstep),($y+$ystep));
	// divide the first box
	pdf_moveto($pdf,$x,($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));

	// 3. Vertical divisions
	for($i=($x+$xstep);$i<($x+$width);$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,($y+(2*$ystep)));
	}

	// draw the legend box
	pdf_rect($pdf,$x+125,$y-50,250,25);
	pdf_stroke($pdf);

	// 4. Vertical Scale
	$vs=array("35","40","45","50","55","60");
	$j=0;
	for($i=($y+(2*$ystep));($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$j++;
	}
	pdf_show_xy($pdf,"65",($x+round(2*$xstep/3)),$y+$height-2);

	$vs=array("Very","Low","","","High","Very");
	$vs1=array("Low","","","","","High");
	$j=0;
	for($i=($y+round(2.5*$ystep));($i<$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],$x,$i);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}
	pdf_show_xy($pdf,"Average",$x,$y+(5*$ystep));

	$size=10.0;
	pdf_setfont($pdf,$font,$size);

	pdf_show_boxed($pdf,"Self",$x,($y+round(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");
	pdf_show_boxed($pdf,"Others",$x,$y,round(0.95*$xstep),round(0.375*$ystep),"center","");

	// Legend
	pdf_show_xy($pdf,"Self",$x+210,$y-round(0.75*$ystep));
	pdf_show_xy($pdf,"Others",$x+310,$y-round(0.75*$ystep));

	// 5. Draw the scale
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$sTxt=$data[$i][1];
		if("Self-Criticizing"==$sTxt){
			$sTxt="Self- Criticizing";
		}
		pdf_show_boxed($pdf,$sTxt,$j,($y+$ystep),round(0.95*$xstep),round(0.75*$ystep),"center","");
		$j+=$xstep;
	}

	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,($y+round(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");
		if(false==$noOther){
			pdf_show_boxed($pdf,round($data[($i+$scales)][0]),$j,$y,round(0.95*$xstep),round(0.375*$ystep),"center","");
		}
		else{
			pdf_show_boxed($pdf,"No data",$j,$y,round(0.95*$xstep),round(0.375*$ystep),"center","");
		}
		$j+=$xstep;
	}

	// 7. Draw the graph for Self
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1.5);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$prevval=$va;
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5)));
		}

		$yc=$y+(2*$ystep)+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	// Legend
	pdf_moveto($pdf,$x+150,$y-round(0.7*$ystep));
	pdf_lineto($pdf,$x+200,$y-round(0.7*$ystep));
	pdf_moveto($pdf,$x+250,$y-round(0.7*$ystep));
	pdf_lineto($pdf,$x+300,$y-round(0.7*$ystep));
	pdf_stroke($pdf);


	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	// Legend
	pdf_circle($pdf,$x+175,$y-round(0.7*$ystep),3.5);
	pdf_fill_stroke($pdf);

	if(false==$noOther){
		// 8. Draw the graph for Others
		$clip1=false;
		$clip2=false;
		pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
		pdf_setlinewidth($pdf,1.5);
		$j=$x+round(1.5*$xstep);
		$val=round($data[$scales][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		// We're not clipped, move to the point
		if(!$clip2){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_moveto($pdf,$j,$yc);
		}
		for($i=1;$i<$scales;$i++){
			// Save previous valuae and clip status
			$preval=$val;
			$clip1=$clip2;
			$val=round($data[$i+$scales][0]);
			$val-=35;
			if($val<0){
				$clip2=true;
				$val=0;
			}
			elseif($val>30){
				$clip2=true;
				$val=30;
			}
			else{
				$clip2=false;
			}
			// We now know if we're within the graph etc
			// If we were outside the graph, move in w/o drawing
			if($clip1){
				pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5)));
			}

			$yc=$y+(2*$ystep)+($val*($ystep/5));
			$j+=$xstep;
			// If we're inside, draw
			if(!$clip2){
				pdf_lineto($pdf,$j,$yc);
			}
			// If we were inside and are moving out, draw, except on the edge
			elseif(!$clip1 && $clip2 && $preval!=$val){
				pdf_lineto($pdf,$j,$yc);
			}
		}
		pdf_stroke($pdf);
	}

	pdf_setcolor($pdf,'both','rgb',0.3,0.6,0.3,0);
	pdf_setlinewidth($pdf,1);
	if(false==$noOther){
		$j=$x+round(1.5*$xstep);
		$val=round($data[$scales][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_rect($pdf,$j-4.5,$yc-4.5,9,9);
		}
		for($i=1;$i<$scales;$i++){
			$j+=$xstep;
			$val=round($data[$i+$scales][0]);
			$val-=35;
			if($val>=0&&$val<=30){
				$yc=$y+(2*$ystep)+($val*($ystep/5));
				pdf_rect($pdf,($j-4.5),($yc-4.5),9,9);
			}
		}
	}
	// Legend
	pdf_rect($pdf,$x+275,($y-round(0.7*$ystep)-2),6,6);
	pdf_fill_stroke($pdf);
}

function renderPage7($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Constructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Higher numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getConstructiveResponses($cid);
	if($rows){
		drawBigGraph($pdf,50,150,500,450,$rows,$noOther);
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeFooter($pdf,$name,$page);
}

function renderPage10($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Destructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Lower numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getDestructiveResponses($cid);
	if($rows){
		drawBigGraph($pdf,50,150,500,450,$rows,$noOther);
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeFooter($pdf,$name,$page);
}

// New version of the Graph due to large changes
function drawSmallGraph($cid,&$pdf,$x,$y,$width,$height,$data,$sid,$noOther){
	$ystep=round($height/6);
	$xstep=round($width/7);
	// 1. the box outline
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_moveto($pdf,$x,round($y+$ystep));
	pdf_lineto($pdf,$x+$width-(2*$xstep),round($y+$ystep));

	pdf_moveto($pdf,$x,$y+($height-$ystep));
	pdf_lineto($pdf,$x+$width-(2*$xstep),$y+($height-$ystep));
	pdf_stroke($pdf);

	// The shaded boxes
	pdf_setcolor($pdf,'stroke','rgb',0.0,0.0,0.0,0);
	pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
	pdf_rect($pdf,$x,round($y+$ystep),$xstep,($height-(2*$ystep)));
	pdf_rect($pdf,$x+(2*$xstep),round($y+$ystep),(2*$xstep),($height-(2*$ystep)));
	pdf_rect($pdf,$x+(5*$xstep),round($y+$ystep),$xstep,($height-(2*$ystep)));
	pdf_fill_stroke($pdf);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);

	// The divider between the fixed text lines
	pdf_moveto($pdf,$x,($y+$height-round($ystep/2)));
	pdf_lineto($pdf,($x+($width-$xstep)),($y+$height-round($ystep/2)));
	pdf_stroke($pdf);

	// 2. The fixed text
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,8.0);
	$i=0;
	$msg=array("35","40","45","50","55","60","65");
	for($i=0;$i<7;$i++){
		pdf_show_xy($pdf,$msg[$i],$x+(round($i*$xstep)-3),($y+$height-$ystep+1));
	}
	pdf_setfont($pdf,$font,10.0);
	$msg=array("Very Low","    Low","              Average","","   High","Very High");
	for($i=0;$i<7;$i++){
		pdf_show_xy($pdf,$msg[$i],$x+round($i*$xstep)+4,($y+$height-round($ystep/2)+1));
	}
	pdf_show_xy($pdf,"Actual",$x+round(7*$xstep),($y+$height-round($ystep/2)+1));
	pdf_set_parameter($pdf,"underline","true");
	pdf_continue_text($pdf,"Score");
	pdf_set_parameter($pdf,"underline","false");
	pdf_show_xy($pdf,"     Rater",$x+round(8*$xstep),($y+$height-round($ystep/2)+1));
	pdf_set_parameter($pdf,"underline","true");
	pdf_continue_text($pdf,"Agreement");
	pdf_set_parameter($pdf,"underline","false");


	// 3. The dynamic text
	// The query is like so:
	//select a.AVGSCORE,a.AGREEMENTSCORE,  b.DESCR,  a.CATID,a.VALIDRATERS, a.CID, a.SID
	//from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID(something) and a.CATID<>6 order by a.SID, a.CATID
	// Strip out the rows we care about in this particular graph
	// This is brute force, and can be fixed up nicely later
	$self=NULL;
	$boss=NULL;
	$peer=NULL;
	$dr=NULL;
	$peerdr=NULL;
	foreach($data as $row){
		if($sid==$row[6]){
			// the right scale
			$catid=$row[3];
			switch($catid){
				case 1:
					$self=$row;
					break;
				case 2:
					$boss=$row;
					break;
				case 3:
					$peer=$row;
					break;
				case 4:
					$dr=$row;
					break;
				case 5:
					$peerdr=$row;
					break;
			}
		}
	}
	$xdelta=($xstep/5);

	$realRatCount=getRatersByCat($cid);

	if(!is_null($self)){
		$val=round($self[0]);
		$xval=$val;
		if($val>65){
			$xval=65;
		}
		elseif($val<35){
			$xval=35;
		}
		pdf_show_xy($pdf,"* Self",$x+(($xval-35)*$xdelta),$y+round(4*$ystep));
		pdf_show_xy($pdf,$val,$x+round(7*$xstep),$y+round(4*$ystep));
	}

	if(!is_null($boss)&&false==$noOther){
		$val=round($boss[0]);
		$xval=$val;
		if($val>65){
			$xval=65;
		}
		elseif($val<35){
			$xval=35;
		}
		pdf_show_xy($pdf,"* Boss",$x+(($xval-35)*$xdelta),$y+round(3.25*$ystep));
		pdf_show_xy($pdf,$val,$x+round(7*$xstep),$y+round(3.25*$ystep));
		if(1<$boss[4]){
			// More than one boss
			$agr="Moderate";
			if($boss[1]<10.00){
				$agr="High";
			}
			elseif($boss[1]>20.0){
				$agr="Low";
			}
			pdf_show_xy($pdf,$agr,$x+round(8*$xstep),$y+round(3.25*$ystep));
		}
	}
	else{
		pdf_show_xy($pdf,"no data available",$x+2,$y+round(3.25*$ystep));
		pdf_show_xy($pdf,"0",$x+round(7*$xstep),$y+round(3.25*$ystep));
	}
	$doCombo=true;

	// We have BOTH Peer and DR reports
	if(!is_null($peer)&&!is_null($dr)&&false==$noOther){
		if(round($peer[4])>=3&&round($dr[4])>=3){
			// don't merge
			$doCombo=false;
			$val=round($peer[0]);
			$xval=$val;
			if($val>65){
				$xval=65;
			}
			elseif($val<35){
				$xval=35;
			}
			pdf_show_xy($pdf,"* Peers",$x+(($xval-35)*$xdelta),$y+round(2.5*$ystep));
			pdf_show_xy($pdf,$val,$x+round(7*$xstep),$y+round(2.5*$ystep));
			$agr="Moderate";
			if($peer[1]<10.00){
				$agr="High";
			}
			elseif($peer[1]>20.0){
				$agr="Low";
			}
			pdf_show_xy($pdf,$agr,$x+round(8*$xstep),$y+round(2.5*$ystep));

			$val=round($dr[0]);
			$xval=$val;
			if($val>65){
				$xval=65;
			}
			elseif($val<35){
				$xval=35;
			}
			pdf_show_xy($pdf,"* Report",$x+(($xval-35)*$xdelta),$y+round(1.75*$ystep));
			pdf_show_xy($pdf,$val,$x+round(7*$xstep),$y+round(1.75*$ystep));
			$agr="Moderate";
			if($dr[1]<10.00){
				$agr="High";
			}
			elseif($dr[1]>20.0){
				$agr="Low";
			}
			pdf_show_xy($pdf,$agr,$x+round(8*$xstep),$y+round(1.75*$ystep));
		}
	}

	if($doCombo&&!is_null($peerdr)&&false==$noOther){
		// merge
		if($peerdr[4]>=3){
			// if we have more than 3 we need to do dombined peer/direct reports
			// What happens next depends on how many we have in each group
			if($realRatCount[3]<1){
				// No peers
				pdf_show_xy($pdf,"no data available",$x+2,$y+round(2.5*$ystep));
				pdf_show_xy($pdf,"0",$x+round(7*$xstep),$y+round(2.5*$ystep));

				$val=round($peerdr[0]);
				$xval=$val;
				if($val>65){
					$xval=65;
				}
				elseif($val<35){
					$xval=35;
				}
				pdf_show_xy($pdf,"* Report",$x+(($xval-35)*$xdelta),$y+round(1.75*$ystep));
				pdf_show_xy($pdf,$val,$x+round(7*$xstep),$y+round(1.75*$ystep));
				$agr="Moderate";
				if($peerdr[1]<10.00){
					$agr="High";
				}
				elseif($peerdr[1]>20.0){
					$agr="Low";
				}
				pdf_show_xy($pdf,$agr,$x+round(8*$xstep),$y+round(1.75*$ystep));
			}
			elseif($realRatCount[4]<1){
				// No direct reports
				$val=round($peerdr[0]);
				$xval=$val;
				if($val>65){
					$xval=65;
				}
				elseif($val<35){
					$xval=35;
				}
				pdf_show_xy($pdf,"* Peers",$x+(($xval-35)*$xdelta),$y+round(2.5*$ystep));
				pdf_show_xy($pdf,$val,$x+round(7*$xstep),$y+round(2.5*$ystep));
				$agr="Moderate";
				if($peerdr[1]<10.00){
					$agr="High";
				}
				elseif($peerdr[1]>20.0){
					$agr="Low";
				}
				pdf_show_xy($pdf,$agr,$x+round(8*$xstep),$y+round(2.5*$ystep));

				pdf_show_xy($pdf,"no data available",$x+2,$y+round(1.75*$ystep));
				pdf_show_xy($pdf,"0",$x+round(7*$xstep),$y+round(1.75*$ystep));
			}
			else{
				// together they make up 3
				$val=round($peerdr[0]);
				$xval=$val;
				if($val>65){
					$xval=65;
				}
				elseif($val<35){
					$xval=35;
				}
				if($xval<=64){
					pdf_show_xy($pdf,"* Peer/Report",$x+(($xval-35)*$xdelta),$y+round(2.5*$ystep));
				}
				else{
					pdf_show_xy($pdf,"Peer/Report *",$x+(($xval-40)*$xdelta),$y+round(2.5*$ystep));
				}
				pdf_show_xy($pdf,$val,$x+round(7*$xstep),$y+round(2.5*$ystep));
				$agr="Moderate";
				if($peerdr[1]<10.00){
					$agr="High";
				}
				elseif($peerdr[1]>20.0){
					$agr="Low";
				}
				pdf_show_xy($pdf,$agr,$x+round(8*$xstep),$y+round(2.5*$ystep));
			}
		}
	}
}

function renderPage8($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=18.0;
	pdf_setfont($pdf,$font,$size);
	$title="Active-Constructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),720);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Higher numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);

	$rows=getActiveConstructiveResponses($cid);
	if($rows){
		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Perspective",50,650);
		pdf_continue_text($pdf,"Taking");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by putting yourself in the other",150,650);
		pdf_continue_text($pdf,"person's position and trying to understand that person's");
		pdf_continue_text($pdf,"point of view.");

		drawSmallGraph($cid,$pdf,50,525,400,100,$rows,1,$noOther);

		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Creating",50,500);
		pdf_continue_text($pdf,"Solutions");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by brainstorming with the other",150,500);
		pdf_continue_text($pdf,"person, asking questions, and trying to create solutions");
		pdf_continue_text($pdf,"to the problem.");

		drawSmallGraph($cid,$pdf,50,375,400,100,$rows,2,$noOther);

		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Expressing",50,350);
		pdf_continue_text($pdf,"Emotions");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by talking honestly with the other",150,350);
		pdf_continue_text($pdf,"person and expressing your thoughts and feelings.");

		drawSmallGraph($cid,$pdf,50,225,400,100,$rows,3,$noOther);


		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Reaching",50,200);
		pdf_continue_text($pdf,"Out");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by reaching out to the other",150,200);
		pdf_continue_text($pdf,"person, making the first move, and trying to make");
		pdf_continue_text($pdf,"amends.");

		drawSmallGraph($cid,$pdf,50,75,400,100,$rows,4,$noOther);
	}
	else{
		pdf_continue_text($pdf,"Can't graph Active Constructive responses");
	}
	writeFooter($pdf,$name,$page);
}

function renderPage9($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=18.0;
	pdf_setfont($pdf,$font,$size);
	$title="Passive-Constructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),720);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Higher numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);

	$rows=getPassiveConstructiveResponses($cid);
	if($rows){
		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Reflective",50,650);
		pdf_continue_text($pdf,"Thinking");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by analyzing the situation,",150,650);
		pdf_continue_text($pdf,"weighing the pros and cons, and thinking about the best");
		pdf_continue_text($pdf,"response.");

		drawSmallGraph($cid,$pdf,50,525,400,100,$rows,5,$noOther);

		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Delay",50,500);
		pdf_continue_text($pdf,"Responding");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by waiting things out, letting",150,500);
		pdf_continue_text($pdf,"matters settle down, or taking a \"time out\" when");
		pdf_continue_text($pdf,"emotions are running high.");

		drawSmallGraph($cid,$pdf,50,375,400,100,$rows,6,$noOther);

		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Adapting",50,350);
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by staying flexible, and trying to",150,350);
		pdf_continue_text($pdf,"make the best of the situation.");

		drawSmallGraph($cid,$pdf,50,225,400,100,$rows,7,$noOther);
	}
	else{
		pdf_continue_text($pdf,"Can't graph Passive Destructive responses");
	}
	writeFooter($pdf,$name,$page);
}

function renderPage11($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=18.0;
	pdf_setfont($pdf,$font,$size);
	$title="Active-Destructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),720);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Lower numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);

	$rows=getActiveDestructiveResponses($cid);
	if($rows){
		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Winning",50,650);
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by arguing vigorously for your own",150,650);
		pdf_continue_text($pdf,"position and trying to win at all costs.");

		drawSmallGraph($cid,$pdf,50,525,400,100,$rows,8,$noOther);

		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Displaying",50,500);
		pdf_continue_text($pdf,"Anger");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by expressing anger, raising your",150,500);
		pdf_continue_text($pdf,"voice, and using harsh, angry words.");

		drawSmallGraph($cid,$pdf,50,375,400,100,$rows,9,$noOther);

		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Demeaning",50,350);
		pdf_continue_text($pdf,"Others");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by laughing at the other person,",150,350);
		pdf_continue_text($pdf,"ridiculing the other's ideas, and using sarcasm.");

		drawSmallGraph($cid,$pdf,50,225,400,100,$rows,10,$noOther);


		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Retaliating",50,200);
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by obstructing the other person,",150,200);
		pdf_continue_text($pdf,"retaliating against the other, and trying to get revenge.");

		drawSmallGraph($cid,$pdf,50,75,400,100,$rows,11,$noOther);
	}
	else{
		pdf_continue_text($pdf,"Can't graph Active Destructive responses");
	}
	writeFooter($pdf,$name,$page);
}

function renderPage12($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=18.0;
	pdf_setfont($pdf,$font,$size);
	$title="Passive-Destructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),720);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Lower numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);

	$rows=getPassiveDestructiveResponses($cid);
	if($rows){
		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Avoiding",50,650);
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by avoiding or ignoring the other",150,650);
		pdf_continue_text($pdf,"person, and acting distant and aloof.");

		drawSmallGraph($cid,$pdf,50,525,400,100,$rows,12,$noOther);

		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Yielding",50,500);
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by giving in to the other person in",150,500);
		pdf_continue_text($pdf,"order to avoid further conflict.");

		drawSmallGraph($cid,$pdf,50,375,400,100,$rows,13,$noOther);

		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Hiding",50,350);
		pdf_continue_text($pdf,"Emotions");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by concealing your true emotions",150,350);
		pdf_continue_text($pdf,"even though feeling upset.");

		drawSmallGraph($cid,$pdf,50,225,400,100,$rows,14,$noOther);


		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Self-",50,200);
		pdf_continue_text($pdf,"Criticizing");
		pdf_setfont($pdf,$font,10.0);
		pdf_show_xy($pdf,"Responding to conflict by replaying the incident over in",150,200);
		pdf_continue_text($pdf,"your mind, and criticizing yourself for not handling it");
		pdf_continue_text($pdf,"better.");

		drawSmallGraph($cid,$pdf,50,75,400,100,$rows,15,$noOther);
	}
	else{
		pdf_continue_text($pdf,"Can't graph Passive Destructive responses");
	}
	writeFooter($pdf,$name,$page);
}

// To do: roll over into next page if too many answers.
function renderPage13($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Scale Profile";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),720);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Based on their responses to the Conflict Dynamics Profile, your boss, your peers, and your direct",50,680);
	pdf_continue_text($pdf,"reports have some fairly clear, specific views about the ways in which you respond to conflict situations;");
	pdf_continue_text($pdf,"their most strongly held views appear below. These opinions may or may not agree with your own self-perception;");
	pdf_continue_text($pdf,"the thing to remember, however, is that others' impressions of you are extremely important,");
	pdf_continue_text($pdf,"whether you believe they are accurate or not. The observations of your boss, your peers, and your");
	pdf_continue_text($pdf,"direct reports have a powerful impact on what they expect from you, how they will interpret your");
	pdf_continue_text($pdf,"actions, and how they behave toward you. Any belief by others that you are acting in destructive ways");
	pdf_continue_text($pdf,"during conflict situations strongly suggests that working to change these views of you would be");
	pdf_continue_text($pdf,"worthwhile.");
	pdf_continue_text($pdf,"   ");

	if(false==$noOther){
		$rows=getScaleScore($cid,"2");
		if($rows){
			pdf_setfont($pdf,$bfont,$size);
			pdf_show_xy($pdf,"Boss Feedback",50,580);
			pdf_continue_text($pdf," ");
			pdf_setfont($pdf,$ifont,$size);
			pdf_continue_text($pdf,"With regard to conflict, your boss sees you as someone who:");
			pdf_setfont($pdf,$font,$size);
			pdf_continue_text($pdf,"   ");
			$fix04052005=false;
			foreach($rows as $row){
				if(strlen($row[0])>0){
					pdf_continue_text($pdf,"> ".$row[0].".");
					$fix04052005=true;
				}
			}
			if($fix04052005==false){
				pdf_continue_text($pdf,"> No data available");
			}
			pdf_continue_text($pdf,"   ");
		}
		else{
			pdf_setfont($pdf,$bfont,$size);
			pdf_show_xy($pdf,"Boss Feedback",50,580);
			pdf_continue_text($pdf," ");
			pdf_setfont($pdf,$ifont,$size);
			pdf_continue_text($pdf,"With regard to conflict, your boss sees you as someone who:");
			pdf_setfont($pdf,$font,$size);
			pdf_continue_text($pdf,"   ");
			pdf_continue_text($pdf,"> No data available");
			pdf_continue_text($pdf,"   ");
		}

		$doCommon=true;
		$rows3=getScaleScore($cid,"3");
		$rows4=getScaleScore($cid,"4");
		if($rows3 && $rows4){
			if(round($rows3[0][2])>=3 &&round($rows4[0][2])>=3){
				$doCommon=false;
				pdf_setfont($pdf,$bfont,$size);
				pdf_continue_text($pdf," ");
				pdf_continue_text($pdf,"Peer Feedback");
				pdf_continue_text($pdf," ");
				pdf_setfont($pdf,$ifont,$size);
				pdf_continue_text($pdf,"With regard to conflict, your peers see you as someone who:");
				pdf_setfont($pdf,$font,$size);
				pdf_continue_text($pdf,"   ");
				foreach($rows3 as $row){
					pdf_continue_text($pdf,"> ".$row[0].".");
				}

				pdf_continue_text($pdf,"   ");
				pdf_continue_text($pdf," ");
				pdf_setfont($pdf,$bfont,$size);
				pdf_continue_text($pdf,"Direct Report Feedback");
				pdf_continue_text($pdf," ");
				pdf_setfont($pdf,$ifont,$size);
				pdf_continue_text($pdf,"With regard to conflict, your direct reports see you as someone who:");
				pdf_setfont($pdf,$font,$size);
				pdf_continue_text($pdf,"   ");
				foreach($rows4 as $row){
					pdf_continue_text($pdf,"> ".$row[0].".");
				}
			}
		}

		if($doCommon){
			// Can we display combinedd Peers/Dr?
			$rows=getScaleScore($cid,"5");
			if($rows){
				$realRatCount=getRatersByCat($cid);
				if(round($rows[0][2])>=3){
					if($realRatCount[3]<1){
						// No Peers
						pdf_setfont($pdf,$bfont,$size);
						pdf_continue_text($pdf," ");
						pdf_continue_text($pdf,"Peer Feedback");
						pdf_continue_text($pdf," ");
						pdf_setfont($pdf,$ifont,$size);
						pdf_continue_text($pdf,"With regard to conflict, your peers see you as someone who:");
						pdf_setfont($pdf,$font,$size);
						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf,"> No Data Available");

						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf," ");
						pdf_setfont($pdf,$bfont,$size);
						pdf_continue_text($pdf,"Direct Report Feedback");
						pdf_continue_text($pdf," ");
						pdf_setfont($pdf,$ifont,$size);
						pdf_continue_text($pdf,"With regard to conflict, your direct reports see you as someone who:");
						pdf_setfont($pdf,$font,$size);
						pdf_continue_text($pdf,"   ");
						pdf_setfont($pdf,$font,$size);
						pdf_continue_text($pdf,"   ");
						foreach($rows as $row){
							pdf_continue_text($pdf,"> ".$row[0].".");
						}
					}
					elseif($realRatCount[4]<1){
						// No Direct Reports
						pdf_setfont($pdf,$bfont,$size);
						pdf_continue_text($pdf," ");
						pdf_continue_text($pdf,"Peer Feedback");
						pdf_continue_text($pdf," ");
						pdf_setfont($pdf,$ifont,$size);
						pdf_continue_text($pdf,"With regard to conflict, your peers see you as someone who:");
						pdf_setfont($pdf,$font,$size);
						pdf_continue_text($pdf,"   ");
						foreach($rows as $row){
							pdf_continue_text($pdf,"> ".$row[0].".");
						}

						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf," ");
						pdf_setfont($pdf,$bfont,$size);
						pdf_continue_text($pdf,"Direct Report Feedback");
						pdf_continue_text($pdf," ");
						pdf_setfont($pdf,$ifont,$size);
						pdf_continue_text($pdf,"With regard to conflict, your direct reports see you as someone who:");
						pdf_setfont($pdf,$font,$size);
						pdf_continue_text($pdf,"   ");
						pdf_setfont($pdf,$font,$size);
						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf,"> No Data Available");
					}
					else{
						// We can still display them together
						pdf_setfont($pdf,$bfont,$size);
						pdf_continue_text($pdf," ");
						pdf_continue_text($pdf,"P/R Feedback");
						pdf_continue_text($pdf," ");
						pdf_setfont($pdf,$ifont,$size);
						pdf_continue_text($pdf,"With regard to conflict, your peers and direct reports see you as someone who:");
						pdf_setfont($pdf,$font,$size);
						pdf_continue_text($pdf,"   ");
						foreach($rows as $row){
							pdf_continue_text($pdf,"> ".$row[0].".");
						}
					}
				}
			}
			else{
					// Couldn't draw peers, dr or p/r by any means
					pdf_setfont($pdf,$bfont,$size);
					pdf_continue_text($pdf," ");
					pdf_continue_text($pdf,"P/R Feedback");
					pdf_continue_text($pdf," ");
					pdf_setfont($pdf,$ifont,$size);
					pdf_continue_text($pdf,"With regard to conflict, your peers and direct reports see you as someone who:");
					pdf_setfont($pdf,$font,$size);
					pdf_continue_text($pdf,"   ");
					pdf_continue_text($pdf,"> No data available");
			}
		}
	}
	else{
		pdf_setfont($pdf,$font,$size);
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf,"No data available");
	}
	writeFooter($pdf,$name,$page);
}

function drawDiscrepancyGraph(&$pdf,$x,$y,$width,$height,$cid,$catid,$dispCat,$drawIt){
	$xstep=round($width/8);
	$ystep=round($height/5);

	// 1. The text
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$msg="";
	$cat="";
	switch($dispCat){
		case 2:
			$cat="* Boss";
			$msg="Boss Discrepancies";
			break;
		case 3:
			$cat="* Peers";
			$msg="Peer Discrepancies";
			break;
		case 4:
			$cat="* Reports";
			$msg="Direct Report Discrepancies";
			break;
		case 5:
			$cat="* Peer/Report";
			$msg="Peer/Report Discrepancies";
			break;
		case 0:
			$cat="* Peer/Report";
			$msg="Peer/Report Discrepancies";
	}
	pdf_setfont($pdf,$bfont,10.0);
	pdf_show_xy($pdf,$msg,$x,$y+$height);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,"Never",$x+(4*$xstep)-6,$y+$height);
	pdf_show_xy($pdf,"Rarely",$x+(5*$xstep)-6,$y+$height);
	pdf_show_xy($pdf,"Sometimes",$x+(6*$xstep)-12,$y+$height);
	pdf_show_xy($pdf,"Often",$x+(7*$xstep)-6,$y+$height);
	pdf_show_xy($pdf,"Almost",$x+(8*$xstep)-12,$y+$height+5);
	pdf_show_xy($pdf,"Always",$x+(8*$xstep)-12,$y+$height-5);
	for($i=1;$i<6;$i++){
		pdf_show_xy($pdf,$i,$x+((3+$i)*$xstep),$y+$height-(3*$ystep/4));
	}
	// 2. the box outline
	if($drawIt){
		pdf_setcolor($pdf,'stroke','rgb',0.0,0.0,0.0,0);
		pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
		pdf_setlinewidth($pdf,1);
		pdf_rect($pdf,$x+round(4*$xstep),$y+$ystep,round(4*$xstep),$ystep);
		pdf_rect($pdf,$x+round(4*$xstep),$y+round(3*$ystep),round(4*$xstep),$ystep);
		pdf_fill_stroke($pdf);
		pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
		for($i=0;$i<5;$i++){
			pdf_moveto($pdf,$x+((4+$i)*$xstep),$y);
			pdf_lineto($pdf,$x+((4+$i)*$xstep),$y+round(4*$ystep));
		}
		pdf_moveto($pdf,$x+round(4*$xstep),$y);
		pdf_lineto($pdf,$x+round(8*$xstep),$y);
		pdf_stroke($pdf);
	}

	// 3. Draw dynamic data - using smaller font
	$rows=getDiscrepancyProfile($cid,$catid);
	$j=1.5;
	if($rows&&$drawIt){
		pdf_setfont($pdf,$font,8.0);
		foreach($rows as $row){
			$details=getDiscrepancyProfileGraph($cid,$catid,$row[2]);
			if($details){
				$printIt=true;
				$num=$details[1][2];
				// Always print for Boss
				if(2!=$catid){
					if($num<3){
						$printIt=false;
					}
				}
				if($printIt){
					pdf_show_boxed($pdf,$row[0],$x,$y+$height-round(($j+1)*$ystep),$x+round(3*$xstep),$ystep,"left","");
					// show if they're not the same, i.e. there's actually a discrepancy
					if(round($details[0][1])!=round($details[1][1])){
						// Set boundaries to box edges
						$selfVal=$details[0][1];
						if($selfVal<1){
							$selfVal=1;
						}
						elseif ($selfVal>5){
							$selfVal=5;
						}

						$otherVal=$details[1][1];
						if($otherVal<1){
							$otherVal=1;
						}
						elseif ($otherVal>5){
							$otherVal=5;
						}

						pdf_show_xy($pdf,"* Self",$x+round(3*$xstep)+round(($xstep*$selfVal)),$y+$height-round($j*$ystep));
						pdf_show_xy($pdf,$cat,$x+round(3*$xstep)+round(($xstep*$otherVal)),$y+$height-round($j*$ystep));
					}
				}
				else{
					pdf_show_xy($pdf,"Insufficient number of raters",$x,$y+$height-round($j*$ystep));
				}
			}
			$j++;
		}
	}
	else{
		pdf_setcolor($pdf,'stroke','rgb',0.0,0.0,0.0,0);
		pdf_setcolor($pdf,'fill','rgb',R_,G_,B_,0);
		pdf_setlinewidth($pdf,1);
		pdf_rect($pdf,$x+round(4*$xstep),$y+round(3*$ystep),round(4*$xstep),$ystep);
		pdf_fill_stroke($pdf);

		pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
		for($i=0;$i<5;$i++){
			pdf_moveto($pdf,$x+((4+$i)*$xstep),$y+round(3*$ystep));
			pdf_lineto($pdf,$x+((4+$i)*$xstep),$y+round(4*$ystep));
		}
		pdf_stroke($pdf);

		pdf_setfont($pdf,$font,8.0);
		pdf_show_xy($pdf,"No Data Available",$x,$y+round(3.5*$ystep));
	}
}

function renderPage14($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Discrepancy Profile";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),720);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"One outcome of taking the Conflict Dynamics Profile is discovering how your impressions of your",50,700);
	pdf_continue_text($pdf,"own behavior can differ from those of others. Because others' observations of you are what determine");
	pdf_continue_text($pdf,"their behavior toward you, such observations are very important. It is critical, then, to know the areas in");
	pdf_continue_text($pdf,"which the way you think you are behaving differ from the way that others think you are behaving. That");
	pdf_continue_text($pdf,"is the purpose of this section of the report.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Below you will find a series of graphs which display the individual items from the Conflict Dynamics");
	pdf_continue_text($pdf,"Profile for which the gap between your viewpoint and the viewpoint of others is the greatest. For each");
	pdf_continue_text($pdf,"of these items, it may be useful for you to ask the following questions: Why does this discrepancy");
	pdf_continue_text($pdf,"exist? On what evidence am I basing my perception of my behavior? On what evidence are other");
	pdf_continue_text($pdf,"people basing their impressions? What does this say about the way I present myself to others?");

	$pos=425;
	if(false==$noOther){
		$rows=getRatersByCat($cid);
		if($rows[2]>0){
			drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,2,2,true);
			$pos-=175;
		}
		else{
			drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,2,2,false);
			$pos-=100;
		}

		if($rows[3]>=3&&$rows[4]>=3){
			// we have more than 3 peers and more than 3 dr
			drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,3,3,true);
			$pos-=175;
			drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,4,4,true);
		}
		else{
			if($rows[5]>=3){
				// we have more than 3 peers+dr together
				if($rows[3]<1){
					// No Peers
					drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,5,3,false);
					$pos-=100;
					drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,5,4,true);
				}
				elseif($rows[4]<1){
					// No DRs
					drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,5,3,true);
					$pos-=175;
					drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,5,4,false);
				}
				else{
					drawDiscrepancyGraph($pdf,50,$pos,450,150,$cid,5,5,true);
				}
			}
			else{
				// Couldn't draw the graph through any means
				drawDiscrepancyGraph(&$pdf,50,$pos,450,150,$cid,0,0,false);
			}
		}
	}
	else{
		pdf_setfont($pdf,$font,$size);
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf,"No data available");
	}
	writeFooter($pdf,$name,$page);
}

function renderPage15($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Dynamic Conflict Sequence";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Behavior changes as conflict progresses. Some people are most effective as a conflict is just",50,680);
	pdf_continue_text($pdf,"beginning, and can diffuse a tense situation early. Others may be most constructive after a conflict has");
	pdf_continue_text($pdf,"ended by reaching out to the other person and attempting to make amends. It can therefore be useful");
	pdf_continue_text($pdf,"to gain insight into how you react throughout the entire conflict sequence. Even more important is how");
	pdf_continue_text($pdf,"your actions over the course of conflict are viewed by others, and the discrepancy between your");
	pdf_continue_text($pdf,"impressions of your behavior and theirs.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"This section of the CDP Feedback Report provides you with a description of the dynamics of your");
	pdf_continue_text($pdf,"behavior as conflict unfolds. As a part of the CDP, you and your co-workers rated your behavior at");
	pdf_continue_text($pdf,"three points in time: as conflict is beginning, during the conflict, and after the conflict has ended. Both");
	pdf_continue_text($pdf,"the graph on the following page and the table below describe your and others' impressions of your");
	pdf_continue_text($pdf,"behavior--constructive and destructive--at each of these three stages. The data on the graph represent");
	pdf_continue_text($pdf,"the ratings of your boss, peers and direct reports combined, while the table specifies the ratings given");
	pdf_continue_text($pdf,"by each of those groups as well as your own ratings. You will probably find that it is helpful to look first");
	pdf_continue_text($pdf,"at the graph on the next page, and then at the table below. As you study this section, you will want to");
	pdf_continue_text($pdf,"consider these questions:");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"           > How does your behavior change over the course of a conflict?");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"           > At each point in time, how does your view of your behavior differ from those of others?");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"           > At each point in time, what is the discrepancy between your constructive and");
	pdf_continue_text($pdf,"              destructive responses?");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"           > Based on what you have learned so far from this Feedback Report, when you act in");
	pdf_continue_text($pdf,"              constructive (or destructive) ways, what specific behaviors are you likely to engage in?");

	$ypos=100;
	$xpos=75;
	$width=450;
	$cellh=70;
	// Where to plot what, index into these arrays using catid
	$ycoord=array(
		0,
		($ypos+(3*$cellh)+($cellh/2)),
		($ypos+(2*$cellh)+($cellh/2)),
		($ypos+($cellh)+($cellh/2)),
		($ypos+($cellh/2)),
		($ypos+($cellh)+($cellh/2)));
	$who=array("","Self","Boss","Peers","Direct Reports","Peers/Reports");

	// Take away 16 from itemid to index into thes arrays
	$xcoord=array(
	($xpos+($width/4)+20),
	($xpos+($width/4)+20),
	($xpos+($width/2)+20),
	($xpos+($width/2)+20),
	($xpos+(3*$width/4)+20),
	($xpos+(3*$width/4)+20));
	$ystep=array(10,-10,10,-10,10,-10);
	$txt=array("Constructive","Destructive","Constructive","Destructive","Constructive","Destructive");

	//AVGSCORE,SID,CATID,VALIDRATERS
	$havePeers=false;
	$haveDRs=false;
	$doCombo=false;
	$haveBoss=false;
	// Get the data here
	$rows=getDynamicSequence($cid);
	if($rows){
		// See what we really have
		foreach($rows as $row){
			$catid=$row[2];
			$cnt=$row[3];
			if(3==$catid&&$cnt>=3){
				$havePeers=true;
			}
			elseif(4==$catid&&$cnt>=3){
				$haveDRs=true;

			}
			elseif(5==$catid&&$cnt>=3){
				if(!($havePeers&&$haveDRs)){
					$doCombo=true;
					$havePeers=false;
					$haveDRs=false;
				}
			}
			elseif(2==$catid){
				$haveBoss=true;
			}
		}

	}

	// How many of peers and drs do we have?
	$peerCnt=0;
	$drCnt=0;
	$raters=getNumRaters($cid);
	if($raters){
		foreach($raters as $rater){
			if(3==$rater[0]){
				$peerCnt=$rater[1];
			}
			elseif(4==$rater[0]){
				$drCnt=$rater[1];
			}
		}
	}

	$height=20+(2*$cellh);
	$cells=2;
	// Height may vary depending on how many categories we have to plot
	// The minimum is Self, Boss and an extra 20 for the headers

	// we can show the Boss
	$cells=2;
	if($havePeers){
		$height+=$cellh;
		$cells++;
	}
	if($haveDRs){
		$height+=$cellh;
		$cells++;
	}
	if($doCombo){
		$height+=$cellh;
		$cells++;
		// If we have a group that is 0 we should use the full names
		// i.e. we could need another box...
		if($peerCnt==0||$drCnt==0){
			$height+=$cellh;
			$cells++;
		}
	}

	// draw horizontal lines
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_moveto($pdf,$xpos,$ypos+4*$cellh);
	pdf_lineto($pdf,$xpos+$width,$ypos+4*$cellh);
	pdf_moveto($pdf,$xpos,$ypos+(3*$cellh));
	pdf_lineto($pdf,$xpos+$width,$ypos+(3*$cellh));
	$yoffs=2*$cellh;
	if(3<=$cells){
		pdf_moveto($pdf,$xpos,$ypos+(2*$cellh));
		pdf_lineto($pdf,$xpos+$width,$ypos+(2*$cellh));
		$yoffs=$cellh;
	}
	if(4<=$cells){
		pdf_moveto($pdf,$xpos,$ypos+$cellh);
		pdf_lineto($pdf,$xpos+$width,$ypos+$cellh);
		$yoffs=0;
	}

	// draw box
	pdf_rect($pdf,$xpos,$ypos+$yoffs,$width,$height);

	// draw vert lines
	pdf_moveto($pdf,$xpos+($width/4),$ypos+$yoffs);
	pdf_lineto($pdf,$xpos+($width/4),$ypos+$yoffs+$height);
	pdf_moveto($pdf,$xpos+($width/2),$ypos+$yoffs);
	pdf_lineto($pdf,$xpos+($width/2),$ypos+$yoffs+$height);
	pdf_moveto($pdf,$xpos+(3*$width/4),$ypos+$yoffs);
	pdf_lineto($pdf,$xpos+(3*$width/4),$ypos+$yoffs+$height);

	pdf_stroke($pdf);

	// Draw static text
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Constructive vs. Destructive Behaviors Over Time",calcCenterStartPos($pdf,"Constructive vs. Destructive Behaviors Over Time",$font,$size),$ypos+$yoffs+$height+10);
	pdf_show_boxed($pdf,"As Conflict Begins",$xpos+($width/4),$ypos+(4*$cellh),$width/4,14,"center","");
	pdf_show_boxed($pdf,"During Conflict",$xpos+($width/2),$ypos+(4*$cellh),$width/4,14,"center","");
	pdf_show_boxed($pdf,"After Conflict",$xpos+(3*$width/4),$ypos+(4*$cellh),$width/4,14,"center","");

	pdf_setfont($pdf,$font,$size);

	if($rows){
		// Now we know what to plot
		foreach($rows as $row){
			$score=round($row[0]);
			$sid=$row[1];
			$sid-=16;
			$catid=$row[2];
			if($catid==1||$catid==2||($catid==3&&$havePeers&&$haveDRs)||($catid==4&&$havePeers&&$haveDRs)||($catid==5&&$doCombo)){
				if($catid==5&&$doCombo&&($peerCnt==0||$drCnt==0)){
					// If we're here we need to fill out those "data not available" things
					if(0==$peerCnt){
						// No Peer Data available
						pdf_setfont($pdf,$bfont,$size);
						pdf_show_xy($pdf,$who[3],$xpos+5,$ycoord[3]);
						pdf_setfont($pdf,$font,$size);
						pdf_show_xy($pdf,"No Data available",$xcoord[$sid],$ycoord[3]+$ystep[$sid]);
						$who[3]="";

						pdf_setfont($pdf,$bfont,$size);
						pdf_show_xy($pdf,$who[4],$xpos+5,$ycoord[4]);
						pdf_setfont($pdf,$font,$size);
						pdf_show_xy($pdf,"[$score] ".$txt[$sid],$xcoord[$sid],$ycoord[4]+$ystep[$sid]);
						// So as to only print it once
						$who[4]="";
						$who[$catid]="";
					}
					else if(0==$drCnt){
						// No DR Data available
						pdf_setfont($pdf,$bfont,$size);
						pdf_show_xy($pdf,$who[4],$xpos+5,$ycoord[4]);
						pdf_setfont($pdf,$font,$size);
						pdf_show_xy($pdf,"No Data available",$xcoord[$sid],$ycoord[4]+$ystep[$sid]);
						$who[4]="";

						pdf_setfont($pdf,$bfont,$size);
						pdf_show_xy($pdf,$who[3],$xpos+5,$ycoord[3]);
						pdf_setfont($pdf,$font,$size);
						pdf_show_xy($pdf,"[$score] ".$txt[$sid],$xcoord[$sid],$ycoord[3]+$ystep[$sid]);
						// So as to only print it once
						$who[3]="";
						$who[$catid]="";
					}
				}
				else{
					pdf_setfont($pdf,$bfont,$size);
					pdf_show_xy($pdf,$who[$catid],$xpos+5,$ycoord[$catid]);
					pdf_setfont($pdf,$font,$size);
					pdf_show_xy($pdf,"[$score] ".$txt[$sid],$xcoord[$sid],$ycoord[$catid]+$ystep[$sid]);
					// So as to only print it once
					$who[$catid]="";
				}
			}
		}
	}

	// Print Boss row, even if we don't have Boss Data
	if(!$haveBoss){
				pdf_setfont($pdf,$bfont,$size);
				pdf_show_xy($pdf,$who[2],$xpos+5,$ycoord[2]);
				pdf_setfont($pdf,$font,$size);
				for($i=0;$i<6;$i++){
					pdf_show_xy($pdf,"No Data available",$xcoord[$i],$ycoord[2]+$ystep[$i]);
				}
	}
	// the footer
	pdf_show_xy($pdf,"60 and above = Very High; 55-60 = High; 45-55 = Average; 40-45 = Low; Less than 40 = Very Low",$xpos,$ypos+$yoffs-20);
	writeFooter($pdf,$name,$page);
}

// This is the graph on Page16
function drawSequenceGraph(&$pdf,$x,$y,$width,$height,$data){
	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=(count($data)/2);
	$ystep=round($height/8);
	$xstep=round($width/($scales+1));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$size=9.0;
	pdf_setfont($pdf,$font,$size);

	// 1. the box outline
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,($x+$xstep),$y+($ystep/2),($width-$xstep),$height-($ystep/2));

	// 2. Horizontal divisions
	$i;
	for($i=round($y+$ystep);$i<($y+round($ystep*8));$i=round($i+$ystep)){
		pdf_moveto($pdf,($x+$xstep),$i);
		pdf_lineto($pdf,($x+$xtsep+$width),$i);
	}
	pdf_moveto($pdf,$x,$y+(3*$ystep/2));
	pdf_lineto($pdf,$x+$width,$y+(3*$ystep/2));

	// little box that juts out on the left
	pdf_moveto($pdf,$x,$y+($ystep/2));
	pdf_lineto($pdf,($x+$xstep),$y+($ystep/2));
	pdf_moveto($pdf,$x,($y+$ystep/2));
	pdf_lineto($pdf,$x,$y+(3*$ystep/2));
	pdf_moveto($pdf,$x,($y+$ystep));
	pdf_lineto($pdf,($x+$xstep),($y+$ystep));

	// 3. Vertical divisions
	for($i=($x+$xstep);$i<($x+$width);$i+=$xstep){
		pdf_moveto($pdf,$i,$y+($ystep/2));
		pdf_lineto($pdf,$i,($y+(2*$ystep)));
	}

	// draw the legend box
	pdf_rect($pdf,$x+(3*$xstep/2),$y-($ystep/2),$xstep*2,$ystep/2);
	pdf_stroke($pdf);

	// 4. Vertical Scale
	$vs=array("35","40","45","50","55","60");
	$j=0;
	for($i=($y+(2*$ystep));($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+(7*$xstep/8)),$i);
		$j++;
	}
	pdf_show_xy($pdf,"65",($x+(7*$xstep/8)),$y+$height-2);

	$vs=array("Very","Low","","","High","Very");
	$vs1=array("Low","","","","","High");
	$j=0;
	for($i=($y+round(2.5*$ystep));($i<$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],$x+($xstep/2),$i);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}
	pdf_show_xy($pdf,"Average",$x+($xstep/2),$y+(5*$ystep));

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_boxed($pdf,"Constructive",$x,$y+($ystep),$xstep,(0.375*$ystep),"center","");
	pdf_show_boxed($pdf,"Destructive",$x,$y+($ystep/2),$xstep,(0.375*$ystep),"center","");

	// Legend
	pdf_show_boxed($pdf,"Constructive",$x+(2*$xstep),$y-($ystep/2),$xstep/2,(0.375*$ystep),"center","");
	pdf_show_boxed($pdf,"Destructive",$x+(3*$xstep),$y-($ystep/2),$xstep/2,(0.375*$ystep),"center","");

	// 5. Draw the scale
	$j=$x+$xstep;
	$when=array("Before","During","After");
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,$when[$i],$j,$y+(3*$ystep/2),$xstep,(0.375*$ystep),"center","");
		$j+=$xstep;
	}

	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i]),$j,$y+($ystep),$xstep,(0.375*$ystep),"center","");
		pdf_show_boxed($pdf,round($data[($i+$scales)]),$j,$y+($ystep/2),$xstep,(0.375*$ystep),"center","");
		$j+=$xstep;
	}

	// 7. Draw the graph for Constructive
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1.5);
	$j=$x+round(1.5*$xstep);
	$val=$data[0];
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=$data[$i];
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5)));
		}

		$yc=$y+(2*$ystep)+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	// Legend
	pdf_moveto($pdf,$x+(3*$xstep/2)+5,$y-($ystep/4));
	pdf_lineto($pdf,$x+(3*$xstep/2)+60,$y-($ystep/4));
	pdf_moveto($pdf,$x+(5*$xstep/2)+5,$y-($ystep/4));
	pdf_lineto($pdf,$x+(5*$xstep/2)+60,$y-($ystep/4));
	pdf_stroke($pdf);

	// Constructive - green - no blue - no green rectangles
//	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setcolor($pdf,'both','rgb',0.3,0.6,0.3,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=$data[0];
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
		pdf_rect($pdf,$j-4.5,$yc-4.5,9,9);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=$data[$i];
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_rect($pdf,($j-4.5),($yc-4.5),9,9);
		}
	}
	// Legend
	pdf_rect($pdf,$x+(3*$xstep/2)+31,$y-($ystep/4)-2.75,6,6);
	pdf_fill_stroke($pdf);

	// 8. Draw the graph for Destructive
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1.5);
	$j=$x+round(1.5*$xstep);
	$val=$data[$scales];
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=($data[$i+$scales]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5)));
		}

		$yc=$y+(2*$ystep)+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_stroke($pdf);
	// Green -no - red -no Green circles for destructive
	// No again - Red circles
	pdf_setcolor($pdf,'both','rgb',0.8,0.3,0.3,0);
//	pdf_setcolor($pdf,'both','rgb',0.3,0.6,0.3,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=$data[$scales];
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=$data[$i+$scales];
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}

	// Legend
	pdf_circle($pdf,$x+(5*$xstep/2)+32.5,$y-($ystep/4),3.5);
	pdf_fill_stroke($pdf);
}

function renderPage16($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Dynamic Conflict Sequence";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Combined ratings by Boss, Peers, and Direct Reports)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);

	if(false==$noOther){
		$rows=getDynamicSequence($cid,"6");
		if($rows){
			$data=array();
			// know where to put each value
			$idx=array();
			$idx[16]=0;
			$idx[17]=3;
			$idx[18]=1;
			$idx[19]=4;
			$idx[20]=2;
			$idx[21]=5;
			foreach($rows as $row){
				$sid=$row[1];
				$i=$idx[$sid];
				$data[$i]=$row[0];
			}
			drawSequenceGraph($pdf,50,150,500,450,$data);
		}
		else{
			pdf_continue_text($pdf," ");
			pdf_continue_text($pdf,"Can't graph conflict squence");
		}
	}
	else{
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf,"No data available");
	}
	writeFooter($pdf,$name,$page);
}

// Here's the new one, almost 4 times as big...
function drawOrgGraph(&$pdf,$x,$y,$width,$height,$data,$cid,$noOther){
	// 0. get the metrics
	$ystep=$height/17;
	$xstep=$width/10;
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$bfont,10.0);

	// The item text
	$txt=array();
	$txt[0]="Being insensitive to the other person's point of view";
	$txt[1]="Failing to work with the other person to create solutions";
	$txt[2]="Failing to communicate honestly with the other person by expressing thoughts and feelings";
	$txt[3]="Ignoring opportunities to reach out to the other person and repair things";
	$txt[4]="Reacting impulsively rather than analyzing the situation and thinking about the best response";
	$txt[5]="Responding immediately to conflict rather than letting emotions settle down";
	$txt[6]="Failing to adapt and be flexible during conflict situations";
	$txt[7]="Arguing vigorously for one's own position, trying to win at all costs";
	$txt[8]="Expressing anger, raising one's voice, using harsh, angry words";
	$txt[9]="Laughing at the other person, ridiculing the other, using sarcasm";
	$txt[10]="Obstructing or retaliating against the other, trying to get revenge later";
	$txt[11]="Avoiding or ignoring the other person, acting distant and aloof";
	$txt[12]="Giving in to the other person in order to avoid further conflict";
	$txt[13]="Concealing one's true emotions even though feeling upset";
	$txt[14]="Replaying the incident over in one's mind, criticizing oneself for not handling it better";

	// 1. How many bosses, peers and DRs do we have - all depends on that
	$bossNo=0;
	$peerNo=0;
	$drNo=0;
	$raters=getNumRaters($cid);
	if($raters){
		foreach($raters as $rater){
			if(3==$rater[0]){
				$peerNo=$rater[1];
			}
			elseif(4==$rater[0]){
				$drNo=$rater[1];
			}
			elseif(2==$rater[0]){
				$bossNo=$rater[1];
			}
		}
	}

	// 2. Examine the data and put it in appropriate places
	$selfData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$bossData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$peerData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$drData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$comboData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

	$hasPeers=false;
	$hasDRs=false;
	$doCombo=$false;
	foreach($data as $row){
		// the rows are returned in this order: a.ITEMID,a.SCORE,a.CATID
		$item=$row[0];
		$score=$row[1];
		$cat=$row[2];
		if(1==$cat){
			$selfData[$item-100]=$score;
		}
		elseif(2==$cat){
			$bossData[$item-64]=$score;
		}
		elseif(3==$cat){
			$peerData[$item-64]=$score;
			$hasPeers=true;
		}
		elseif(4==$cat){
			$drData[$item-64]=$score;
			$hasDRs=true;
		}
		elseif(5==$cat){
			$comboData[$item-64]=$score;
			$doCombo=true;
			$hasPeers=false;
			$hasDRs=false;
		}
	}

	// 3. Draw the outline - depends on how many rows we're to display
	// we'll always display Self and Boss even if Boss has no data at all
	$columns=2;
	if($hasPeers){
		$columns++;
	}
	if($hasDRs){
		$columns++;
	}
	if($doCombo){
		$columns++;
		if($drNo==0||$peerNo==0){
			$columns++;
		}
	}

	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,$x,$y+$ystep,(6+$columns)*$xstep,$height-$ystep);
	// 1-3 vertical lines
	for($i=0;$i<$columns;$i++){
		pdf_moveto($pdf,$x+((6+$i)*$xstep),$y+$height);
		pdf_lineto($pdf,$x+((6+$i)*$xstep),$y+$ystep);
	}
	// horizontal lines
	for($i=1;$i<16;$i++){
		pdf_moveto($pdf,$x,$y+$height-($i*$ystep));
		pdf_lineto($pdf,$x+((6+$columns)*$xstep),$y+$height-($i*$ystep));
	}
	pdf_stroke($pdf);

	// 4. Draw static text
	// categories
	pdf_show_xy($pdf,"Responses to Conflict",$x+5,$y+$height-round($ystep/2));
	pdf_show_boxed($pdf,"Self",$x+(6*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	if($bossNo<2){
		pdf_show_boxed($pdf,"Boss",$x+(7*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	}
	else{
		pdf_show_boxed($pdf,"Boss*",$x+(7*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	}
	if($hasPeers){
		pdf_show_boxed($pdf,"Peers*",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	}
	if($hasPeers){
		pdf_show_boxed($pdf,"Direct Reports*",$x+(9*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	}
	if($doCombo){
		if(0==$peerNo||0==$drNo){
			pdf_show_boxed($pdf,"Peers*",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
			pdf_show_boxed($pdf,"Direct Reports*",$x+(9*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
		}
		else{
			pdf_show_boxed($pdf,"Peers/ Reports*",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
		}
	}

	// Item text
	pdf_setfont($pdf,$font,10.0);
	for($i=0;$i<16;$i++){
		pdf_show_boxed($pdf,$txt[$i],$x+3,$y+$height-round(($i+2)*$ystep),round(6*$xstep),$ystep,"left","");
	}

	// 5. Show data
	// The darned things are displayed out of order for no apparent reason
	// Thus we do this
	// Start with the last 7
	$hfont=pdf_findfont($pdf,"Times-Bold","host",0);
	pdf_setfont($pdf,$hfont,14.0);
	for($i=8;$i<15;$i++){
		// First do self
		$val=$selfData[$i];
		$severity="";
		if($val>1.0){
			$severity="M";
		}
		if($val>2.0){
			$severity="S";
		}
		pdf_show_boxed($pdf,$severity,$x+round(6*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");

		// Boss
		$val=$bossData[$i];
		$severity="";
		if($bossNo<2){
			if($val>1.0){
				$severity="M";
			}
			if($val>2.0){
				$severity="S";
			}
		}
		else{
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";
			}
		}
		pdf_show_boxed($pdf,$severity,$x+round(7*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");

		// Peers
		if($hasPeers){
			$val=$peerData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round(8*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
		}

		// Direct Reports
		if($hasPeers){
			$val=$drData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round(9*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
		}

		// Combined Peers/DRs
		if($doCombo){
			$whichCol=8;
			if($peerNo==0){
				$whichCol=9;
			}
			$val=$comboData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round($whichCol*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
		}
	}

	// Then do the first 8
	for($i=0;$i<8;$i++){
		// First do self
		$val=$selfData[$i];
		$severity="";
		if($val>1.0){
			$severity="M";
		}
		if($val>2.0){
			$severity="S";
		}
		pdf_show_boxed($pdf,$severity,$x+round(6*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");

		// Boss
		$val=$bossData[$i];
		$severity="";
		if($bossNo<2){
			if($val>1.0){
				$severity="M";
			}
			if($val>2.0){
				$severity="S";
			}
		}
		else{
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";
			}
		}
		pdf_show_boxed($pdf,$severity,$x+round(7*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");

		// Peers
		if($hasPeers){
			$val=$peerData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round(8*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
		}

		// Direct Reports
		if($hasPeers){
			$val=$drData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round(9*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
		}

		// Combined Peers/DRs
		if($doCombo){
			$whichCol=8;
			if($peerNo==0){
				$whichCol=9;
			}
			$val=$comboData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round($whichCol*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
		}
	}

	// footer
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,"* For this category, \"severe negative impact\" reflects a mean response of 2.5 or higher, and \"moderate",$x,$y+round((2*$ystep)/3));
	pdf_continue_text($pdf,"negative impact\" reflects a mean response between 2.0 and 2.49. (on a 1 - 3 scale)");

}

function renderPage17($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Organizational Perspective on Conflict";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);

	pdf_show_xy($pdf,"You, your boss, peers, and direct reports were asked to indicate which kinds of responses to",75,660);
	pdf_continue_text($pdf,"conflict within your organization have the most negative effect on a person's career--that is, the");
	pdf_continue_text($pdf,"responses to conflict which are most frowned upon within your organization. The grid below displays");
	pdf_continue_text($pdf,"what each of you believes are the behaviors which, in your organization, have either a severe, or");
	pdf_continue_text($pdf,"moderate, negative impact on one's career. It does not indicate whether you are seen as engaging");
	pdf_continue_text($pdf,"in such behaviors; that information appears elsewhere. As you read and review this CDP Feedback");
	pdf_continue_text($pdf,"Report, you should pay special attention to:  1) those areas identified by your boss as especially");
	pdf_continue_text($pdf,"important, and 2) those areas which at least three groups identify as important.");
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Behaviors Seen As Having Severe(S)",350,575);
	pdf_continue_text($pdf,"or Moderate(M) Impact on Careers");

	$rows=getOrgPersp($cid);
	if($rows){
		drawOrgGraph(&$pdf,75,100,450,450,$rows,$cid,$noOther);
	}
	else{
		pdf_show_xy($pdf,"Cannot graph organizational perspective",200,500);
	}

	writeFooter($pdf,$name,$page);
}

function renderPage18($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Hot Buttons Profile";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"This portion of the Conflict Dynamics Profile Feedback Report is a bit different from the others.",70,660);
	pdf_continue_text($pdf,"Instead of indicating how you typically respond to conflict situations, this section provides insight into");
	pdf_continue_text($pdf,"the kinds of people and situations which are likely to upset you and potentially cause conflict to occur:");
	pdf_continue_text($pdf,"in short, your hot buttons. Because other people are often unable to detect what our own personal hot");
	pdf_continue_text($pdf,"buttons are, this section is based only on your own responses to the CDP; your boss, peers, and direct");
	pdf_continue_text($pdf,"reports were not asked these questions.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Below you will find a brief description of each of the hot buttons measured by the CDP, and on the");
	pdf_continue_text($pdf,"following page a graph which illustrates how upsetting--compared to people in general--you find each");
	pdf_continue_text($pdf,"situation. Obviously, these do not represent every possible hot button that people may have; they are");
	pdf_continue_text($pdf,"simply some of the most common ones. In each case, a higher score on the scale indicates that you");
	pdf_continue_text($pdf,"get especially irritated and upset by that particular situation.");

	$size=10.0;
	pdf_setfont($pdf,$font,$size);

	pdf_show_xy($pdf,"You get especially irritated and upset when working with people who are",180,500);
	pdf_continue_text($pdf,"unreliable, miss deadlines, and cannot be counted on.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"You get especially irritated and upset when working with people who are");
	pdf_continue_text($pdf,"perfectionists, overanalyze things, and focus too much on minor issues.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"You get especially irritated and upset when working with people who fail to");
	pdf_continue_text($pdf,"give credit to others or seldom praise good performance.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"You get especially irritated and upset when working with people who");
	pdf_continue_text($pdf,"isolate themselves, do not seek input from others, or are hard to approach.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"You get especially irritated and upset when working with people who");
	pdf_continue_text($pdf,"constantly monitor and check up on the work of others.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"You get especially irritated and upset when working with people who are");
	pdf_continue_text($pdf,"self-centered, or believe they are always correct.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"You get especially irritated and upset when working with people who are");
	pdf_continue_text($pdf,"arrogant, sarcastic, and abrasive.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"You get especially irritated and upset when working with people who");
	pdf_continue_text($pdf,"exploit others, take undeserved credit, or cannot be trusted.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"You get especially irritated and upset when working with people who lose");
	pdf_continue_text($pdf,"their tempers, become angry, or yell at others.");

	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,"Unreliable",70,500);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Overly-Analytical");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Unappreciative");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Aloof");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Micro-Managing");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Self-Centered");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Abrasive");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Untrustworthy");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Hostile");
	writeFooter($pdf,$name,$page);
}

// This is the graph on Page 19
// Surptisingly, this one is a bear!!!!
function drawButtonGraph(&$pdf,$x,$y,$width,$height,$data){
	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=count($data);
	$ystep=round($height/7);
	$xstep=round($width/($scales+1));
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10.0);

	// 1. the box outline
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);

	// 2. Horizontal divisions
	$i;
	for($i=0;$i<6;$i++){
		pdf_moveto($pdf,($x+$xstep),$y+$ystep+round($i*$ystep));
		pdf_lineto($pdf,($x+$width),$y+$ystep+round($i*$ystep));
	}

	// divide the first box
	pdf_moveto($pdf,($x+$xstep),($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));

	// little box that juts out
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x,$y);
	pdf_lineto($pdf,$x,$y+round($ystep/2));
	pdf_lineto($pdf,$x+$xstep,$y+round($ystep/2));


	// 3. Vertical divisions
	for($i=$x+round(2*$xstep);$i<$x+$width;$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,$y+$ystep);
	}
	pdf_stroke($pdf);

	// 4. Vertical Scale
	pdf_setfont($pdf,$font,10.0);

	$vs=array("35","40","45","50","55","60","");
	$j=0;
	for($i=($y+$ystep);($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		if(3==$j){
			pdf_setfont($pdf,$font,10.0);
			pdf_show_xy($pdf,"Average",$x-10,$i);
			pdf_setfont($pdf,$font,10.0);
		}
		$j++;
	}

	pdf_show_xy($pdf,"65",($x+round(2*$xstep/3)),$y+$height-2);
	pdf_setfont($pdf,$font,10.0);

	$vs=array("Very","Low","","","High","Very");
	$vs1=array("Low","","","","","High");
	$j=0;
	for($i=($y+round(1.5*$ystep));($i<$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],$x-3,$i);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}

	$vs=array("Unrely","Over Analyze","Unapp","Aloof","Micro Manage","Self Centered","Abrasive","Untrust","Hostile");
	// 5. Draw the scale
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$offset=round(0.35*$ystep);
		if($i==1||$i==4||$i==5){
			$offset=round(0.45*$ystep);
		}
		pdf_show_boxed($pdf,$vs[$i],$j,($y+round($ystep/2)),$xstep,$offset,"center","");
		$j+=$xstep;
	}

	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,$y,$xstep,round(0.35*$ystep),"center","");
		$j+=$xstep;
	}

	pdf_show_xy($pdf,"Value",$x+5,$y+5);

	// 7. Draw the graph
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
			$n1=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
			$n1=31;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+$ystep+($n1*($ystep/5)));
		}

		$yc=$y+$ystep+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_moveto($pdf,$x+5,$y+round($ystep/3));
	pdf_lineto($pdf,$x+$xstep-5,$y+round($ystep/3));
	pdf_stroke($pdf);

	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+$ystep+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}

	pdf_circle($pdf,$x+round($xstep/2),$y+round($ystep/3),4);
	pdf_fill_stroke($pdf);
}

function renderPage19($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Hot Buttons";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Lower numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);

	$rows=getHotButtons($cid);
	if($rows){
		drawButtonGraph(&$pdf,75,225,450,375,$rows);
	}
	else{
		pdf_show_xy($pdf,"Cannot graph hot buttons",200,500);
	}

	writeFooter($pdf,$name,$page);
}

function getRaterComments($cid,$cats,$item){
	if($cats=="boss"){
		$cats="2";
	}
	else{
		$cats="3,4";
	}
	$conn=dbConnect();
	// How many valid raters in this category answered the survey?
	$query="select count(distinct a.RID) from RATER a, RATERRESP b where a.RID=b.RID and a.CATID in ($cats) and a.CID=$cid and b.VAL is not NULL";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	$cnt=$row[0];
	if($cnt<1){
		return false;
	}
	if($cnt<3 && $cats=="3,4"){
		return false;
	}
	$query="select distinct a.VAL from RATERCMT a, RATER b  where a.RID=b.RID and b.CID=$cid and b.CATID in ($cats) and a.ITEMID=$item and a.VAL is not NULL";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

/*
function showComments($cid,$pdf,$x,$y){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);
	$size=11.0;
	pdf_setfont($pdf,$ifont,$size);

	$endOfPage = 70;

	// first question
	pdf_show_xy($pdf,"Based on your experience, what behaviors of this person are most likely to lead to",$x+60,$y);
	pdf_continue_text($pdf,"conflict with others - that is, what actions does the person engage in which lead to");
	pdf_continue_text($pdf,"conflicts?");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	// find y position
	$texty = pdf_get_value($pdf, "texty", 0);
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Boss",$x,$texty);
	pdf_setfont($pdf,$font,$size);
	$data=getRaterComments($cid,"boss",79);
	if(false==$data){
		pdf_show_xy($pdf,"No comments provided",$x+60,$texty);
	}
	else{
		$txt="";
		$first=true;
		foreach($data as $val){
			if(strlen(trim($val[0]))>0){
				if(!$first){
					$txt=$txt."... ";
				}
				else{
					$first=false;
				}
				$txt.=trim(utf8_decode($val[0]));
			}
		}
		pdf_show_boxed($pdf,$txt,$x+60,12,450,$texty,"left","");
	}
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	$texty = pdf_get_value($pdf, "texty", 0);
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Others",$x,$texty);
	pdf_setfont($pdf,$font,$size);
	$data=getRaterComments($cid,"others",79);
	if(false==$data){
		pdf_show_xy($pdf,"No comments provided",$x+60,$texty);
	}
	else{
		$txt="";
		$first=true;
		foreach($data as $val){
			if(strlen(trim($val[0]))>0){
				if(!$first){
					$txt=$txt."... ";
				}
				else{
					$first=false;
				}
				$txt.=trim(utf8_decode($val[0]));
			}
		}
		pdf_show_boxed($pdf,$txt,$x+60,12,450,$texty,"left","");
	}
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	// second question
	pdf_setfont($pdf,$ifont,$size);
	$texty = pdf_get_value($pdf, "texty", 0);
	pdf_show_xy($pdf,"Based on your experience, how could this person handle conflict in a more",$x+60,$texty);
	pdf_continue_text($pdf,"effective way?");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	// find y position
	$texty = pdf_get_value($pdf, "texty", 0);
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Boss",$x,$texty);
	pdf_setfont($pdf,$font,$size);
	$data=getRaterComments($cid,"boss",80);
	if(false==$data){
		pdf_show_xy($pdf,"No comments provided",$x+60,$texty);
	}
	else{
		$txt="";
		$first=true;
		foreach($data as $val){
			if(strlen(trim($val[0]))>0){
				if(!$first){
					$txt=$txt."... ";
				}
				else{
					$first=false;
				}
				$txt.=trim(utf8_decode($val[0]));
			}
		}
		pdf_show_boxed($pdf,$txt,$x+60,12,450,$texty,"left","");
	}
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	$texty = pdf_get_value($pdf, "texty", 0);
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Others",$x,$texty);
	pdf_setfont($pdf,$font,$size);
	$data=getRaterComments($cid,"others",80);
	if(false==$data){
		pdf_show_xy($pdf,"No comments provided",$x+60,$texty);
	}
	else{
		$txt="";
		$first=true;
		foreach($data as $val){
			if(strlen(trim($val[0]))>0){
				if(!$first){
					$txt=$txt."... ";
				}
				else{
					$first=false;
				}
				$txt.=trim(utf8_decode($val[0]));
			}
		}
		pdf_show_boxed($pdf,$txt,$x+60,12,450,$texty,"left","");
	}
}
*/
function renderPage20($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);

	// draw title
	$size=16.0;
	pdf_setfont($pdf,$bfont,$size);
	$title="Developmental Feedback";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),720);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Your boss, peers and direct reports were given the opportunity to respond directly to two",110,700);
	pdf_continue_text($pdf,"questions regarding your own conflict-related behavior. These responses are reproduced");
	pdf_continue_text($pdf,"below.");
	if(false==$noOther){
		$page=showComments($cid,$pdf,50,660,$page,$name);
	}
	else{
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf,"No data available");
	}
	writeFooter($pdf,$name,$page);
}

function renderPage21($cid,&$pdf,&$page,&$noOther){
	$name=writeHeader($cid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="CDP Developmental Worksheet:  Hot Buttons";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Hot Buttons, those situations and individuals that you find most annoying, can provoke and escalate",70,660);
	pdf_continue_text($pdf,"conflict. By learning about the situations in which you are most likely to feel upset, you can better avoid");
	pdf_continue_text($pdf,"conflicts in the future. By understanding and examining the links between provocation and response,");
	pdf_continue_text($pdf,"you can better control your behavior.");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Indicate below each of your Hot Button Scores. Then, for the three Hot Buttons with the highest");
	pdf_continue_text($pdf,"scores, rank-order their \"Importance of Cooling,\" taking into consideration the following factors:");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"> Your level of frustration and irritation (that is, your score)");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"> How frequently this Hot Button provokes you into conflict");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"> The degree to which provocation of this Hot Button interferes with your job");
	pdf_continue_text($pdf,"    performance");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"> The degree to which provocation of this Hot Button affects your physical");
	pdf_continue_text($pdf,"    and emotional well-being");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"The Hot Button ranked Number 1 should be the Hot Button that you most want (or need) to change.");

	pdf_setfont($pdf,$bfont,10.0);
	pdf_set_parameter($pdf,"underline","true");
	pdf_show_xy($pdf,"Hot Button",100,400);
	pdf_set_parameter($pdf,"underline","false");
	pdf_continue_text($pdf," ");
	pdf_show_xy($pdf,"Your",250,400);
	pdf_set_parameter($pdf,"underline","true");
	pdf_continue_text($pdf,"Score ");
	pdf_set_parameter($pdf,"underline","false");
	pdf_show_xy($pdf,"Importance",350,400);
	pdf_set_parameter($pdf,"underline","true");
	pdf_continue_text($pdf,"of Cooling");
	pdf_set_parameter($pdf,"underline","false");

	pdf_setfont($pdf,$font,10.0);

	$vs=array("Unreliable","Overly-Analytical","Unappreciative","Aloof","Micro-Managing","Self-Centered","Abrasive","Untrustworthy","Hostile");
	$j=350;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);

	for($i=0;$i<count($vs);$i++){
		pdf_show_xy($pdf,$vs[$i],100,$j);
		pdf_rect($pdf,250,$j-3,40,18);
		pdf_rect($pdf,350,$j-3,40,18);
		pdf_stroke($pdf);
		$j-=30;
	}

	writeFooter($pdf,$name,$page);
}

function writeHeader($cid,&$pdf){
	$name=getCandidateName($cid);
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,750,515,15);
	pdf_fill_stroke($pdf);
	// Note: (R) is hex AE, so well use sprintf to format properly
	$what="Conflict Dynamics Profile ".sprintf("%c",0xae);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),754);

	return $name;
}

function writeFooter(&$pdf,$name,&$page){
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,35,515,15);
	pdf_fill_stroke($pdf);
	$rptdt=date('D M d, Y');
	$what=$rptdt."             This report was prepared for: ".$name."               Page ".$page;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),39);
	pdf_end_page($pdf);
	// Increment the page number
	$page+=1;
}

function showComments($cid,$pdf,$x,$y,$page,$name){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);
	$size=11.0;
	pdf_setfont($pdf,$ifont,$size);

	$endOfPage = 75;

	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	// first question
	$texty = pdf_get_value($pdf, "texty", 0);
	$qTxt1 = "Based on your experience, what behaviors of this person are most likely to lead to ";
	$qTxt1.= "conflict with others - that is, what actions does the person engage in which lead to ";
	$qTxt1.= "conflicts?";
  $page =	showText($pdf,"iii",$qTxt1,$x+60,$y,$endOfPage,$page,$cid,$name);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	// find y position
	$texty = pdf_get_value($pdf, "texty", 0);

	$data=getRaterComments($cid,"boss",79);

	if($data==false){
		pdf_show_xy($pdf,"No comments provided",$x+60,$texty);
	}
	else{
	  // format, trim, decode...
    $txt = formatText($data);
    $page = showText($pdf,"Boss",$txt,$x+60,$texty,$endOfPage,$page,$cid,$name);
	}

	pdf_continue_text($pdf," ");
	$texty = pdf_get_value($pdf, "texty", 0);
	$data=getRaterComments($cid,"others",79);

	if(!$data){
		pdf_show_xy($pdf,"No comments provided",$x+60,$texty);
	}
	else{
	  // format, trim, decode...
    $txt = formatText($data);
    $page = showText($pdf,"Others",$txt,$x+60,$texty,$endOfPage,$page,$cid,$name);
	}

	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	// second question
	$texty = pdf_get_value($pdf, "texty", 0);
	$qTxt2 = "Based on your experience, how could this person handle conflict in a more effective way?";
  $page =	showText($pdf,"iii",$qTxt2,$x+60,$texty,$endOfPage,$page,$cid,$name);

	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	// find y position
	$texty = pdf_get_value($pdf, "texty", 0);

	$data=getRaterComments($cid,"boss",80);

	if(!$data){
		pdf_show_xy($pdf,"No comments provided",$x+60,$texty);
	}
	else{
	  // format, trim, decode...
    $txt = formatText($data);
    $page = showText($pdf,"Boss",$txt,$x+60,$texty,$endOfPage,$page,$cid,$name);
	}
	pdf_continue_text($pdf," ");

	$texty = pdf_get_value($pdf, "texty", 0);

	$data=getRaterComments($cid,"others",80);

	if(!$data){
		pdf_show_xy($pdf,"No comments provided",$x+60,$texty);
	}
	else{
	  // format, trim, decode...
    $txt = formatText($data);
    $page =	showText($pdf,"Others",$txt,$x+60,$texty,$endOfPage,$page,$cid,$name);
	}
	return $page;
}

function showText($pdf,$title,$txt,$x,$y,$endOfPage,$page,$cid,$name){

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);
	$size=11.0;

  // italicized questions
  if($title=="iii"){
    pdf_setfont($pdf,$ifont,$size);
    $wrapLength=80;
  }
  else{
	  pdf_setfont($pdf,$bfont,$size);
	  pdf_show_xy($pdf,$title,$x-60,$y);
	  pdf_setfont($pdf,$font,$size);
	  $wrapLength=88;
  }

	//apply wrapping to text...
  $text = wordwrap($txt,$wrapLength,'7C_7C',1);

  // put text into array...
  $split_text = explode("7C_7C",$text);

  // loop thru each line of text
  foreach($split_text as $value){

    // check the vertical position on page
    $texty = pdf_get_value($pdf, "texty", 0);

    // if not at the end of page, display another line of text
    if($texty > $endOfPage){
      pdf_show_xy($pdf,$value,$x,$texty);
      pdf_continue_text($pdf," ");
    }
    else{
      // begin new page...
      writeFooter($pdf,$name,$page);

      $name=writeHeader($cid,$pdf);

      // draw title
	    $size=14.0;
	    pdf_setfont($pdf,$bfont,$size);
	    $title="Developmental Feedback - continued...";
	    pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),720);
      pdf_continue_text($pdf," ");
      pdf_continue_text($pdf," ");
      pdf_continue_text($pdf," ");
      $size=11.0;
      pdf_setfont($pdf,$font,$size);
      $texty = pdf_get_value($pdf, "texty", 0);
      pdf_show_xy($pdf,$value,$x,$texty);
      pdf_continue_text($pdf," ");

    }
  }
  return $page;
}

function formatText($data){
	$txt="";
	$first=true;
	foreach($data as $val){
		if(strlen(trim($val[0]))>0){
			if(!$first){
				$txt.="... ";
			}
			else{
				$first=false;
			}
			$txt.=trim(utf8_decode($val[0]));
		}
	}
	return $txt;
}

?>
