<?php
/*=============================================================================*
* 04-22-2005 TRM: Added support for multiple instruments, by adding INSTR 
* to queries
*
* 02-22-2008 TRM: Added additional checks for zero answers
*=============================================================================*/
require_once "dbfns.php";
require_once "mailfns.php";

//-- Functions to score an individual Candidate and her Raters
// get the Name of the candidate
function getScoreCandidateName($cid, $pid){
	$conn=dbConnect();
	$query="select FNAME,LNAME from CANDIDATE where CID=$cid and PID=$pid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return !$row?false:$row[0]." ".$row[1];
}

// get count of the ones that have completed the survey
function getValidRaterCount($cid){
	$conn=dbConnect();
	$query="select CATID,count(*)  from RATER where CID=$cid and ENDDT is not NULL group by CATID order by CATID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$rows=dbRes2Arr($rs);
	if(!$rows){
		return false;
	}
	$rc=array(0,0,0,0,0,0,0,0,0,0,0);
	foreach($rows as $row){
		$key=$row[0];
		$rc[$key]=$row[1];
	}
	return $rc;
}

// function to return the appropriate Mean and Standard deviation etc for a Scale
function getScaleMeanAndStd($rid,$cid,$pid,$catid){
	$conn=dbConnect();
	$query="select b.RAWSCORE,a.MEANVAL$catid,a.DEVVAL$catid,a.SID from SCALE a,SCALESCORE b where a.SID=b.SID and b.RID=$rid and b.CID=$cid and b.RID=$rid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// Compute the raw and standardized scale scores for a single rater
function computeRaterScaleScores($rid,$cid,$pid,$catid){
	$conn=dbConnect();
	
	// Raw score
	if($cid=="1"){
		// New improved query for self leaves out one table
		$query="insert into SCALESCORE (SID,RID,PID,CID,CATID,RAWSCORE,STDSCORE) ";
		$query=$query."select b.SID, a.RID, $pid, $cid, $catid, AVG(VAL), 0 from RATERRESP a , SCALEITEM b ";
// 2/22/08		$query=$query."	where a.RID=$cid and a.ITEMID=b.ITEMID and VAL is not NULL group by b.SID,a.RID";
		$query=$query."	where a.RID=$cid and a.ITEMID=b.ITEMID and VAL is not NULL and VAL>0 group by b.SID,a.RID";
	}
	else{
		$query="insert into SCALESCORE (SID,RID,PID,CID,CATID,RAWSCORE,STDSCORE) ";
		$query=$query."select b.SID, a.RID, $pid, $cid, $catid, AVG(VAL), 0 from RATERRESP a , SCALEITEM b, RATER c ";
// 2/22/08		$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and c.CATID=$catid and a.RID=$rid and VAL is not NULL group by b.SID,a.RID";
		$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and c.CATID=$catid and a.RID=$rid and VAL is not NULL and VAL>0 group by b.SID,a.RID";
	}
	
	$rs=mysql_query($query);
	if(!$rs){
		return "Error calculating RAW SCORE<br>$query<br>";	
	}
	
	// Standardized score
	$rows=getScaleMeanAndStd($rid,$cid,$pid,$catid);
	if(!$rows){
		return "";	
	}
	foreach($rows as $row){
		if(0!=$row[2]){
			$std=50+(10*(($row[0]-$row[1])/$row[2]));
			$query="update SCALESCORE set STDSCORE=$std where SID=$row[3] and RID=$rid and CID=$cid and PID=$pid";
			mysql_query($query);
		}
	}
	return "";	
}

// get rid of all pre-existing scores for this candidate
function getRidOfOldScores($cid,$pid){
	$conn=dbConnect();
	$query="delete from REPORTSCORE where CID=$cid";
	mysql_query($query);
	$query="delete from SCALESCORE where CID=$cid and PID=$pid";
	mysql_query($query);
	$query="delete from DISCREPANCYPROFILE where CID=$cid";
	mysql_query($query);
	$query="delete from ORGPERSPECTIVE where CID=$cid";
	mysql_query($query);
}

// Gets all raters belonging to a specific category
function getRaterIds($cid,$catid){
	$conn=dbConnect();
	$query="select RID,FNAM,LNAM from RATER where CID=$cid and CATID=$catid and ENDDT is not NULL";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// calculates the appropriate Individual report scores for a Candidate
function calculateIndividualReportScores($CID,$pid,$ratNum){
	$verbose=false;
	$conn=dbConnect();
	$rc="";
	// always calculate for Self
	$query="insert into REPORTSCORE (SID,CID,CATID,VALIDRATERS,AVGSCORE,AGREEMENTSCORE) select SID,CID,CATID,1,AVG(STDSCORE),0 from SCALESCORE where CID=$CID and PID=$pid and CATID=1 and STDSCORE is not NULL group by SID,CID,CATID";
	mysql_query($query);
	$rc=$rc."Performing roll-up for self<br>";
	if($verbose){
		$rc=$rc.$query."<br>";	
	}
	$rc=$rc.calcOrgPerspective($CID,"1");
	
	// calculate Bosses is we have them
	if($ratNum[2]>0){
		$query="insert into REPORTSCORE (SID,CID,CATID,VALIDRATERS,AVGSCORE,AGREEMENTSCORE) select SID,CID,CATID,$ratNum[2],AVG(STDSCORE),0 from SCALESCORE where CID=$CID and PID=$pid and CATID=2  and STDSCORE is not NULL group by SID,CID,CATID";
		mysql_query($query);
		$rc=$rc."Performing roll-up for boss category<br>";
		if($verbose){
			$rc=$rc.$query."<br>";	
		}
		if($ratNum[2]>1){
			// If we have more than one boss we can calculate RaterAgreement
			$rc=$rc.calcRaterAgreement($CID,$pid,"2");
		}
		// Calculate Discrepancy Profile
		$rc=$rc.calcDiscrepancyProfiles($CID,"2");
		$rc=$rc.calcOrgPerspective($CID,"2");
	}
	else{
		$rc=$rc."No boss data<br>";	
	}
	
	// If we have enough Peers and DRs we calculate them separately
	// do we have at least 3 in each category ?
	if($ratNum[3]>=3 && $ratNum[4]>=3){
		$query="insert into REPORTSCORE (SID,CID,CATID,VALIDRATERS,AVGSCORE,AGREEMENTSCORE) select SID,CID,CATID,$ratNum[3],AVG(STDSCORE),0 from SCALESCORE where CID=$CID and PID=$pid and CATID=3  and STDSCORE is not NULL group by SID,CID,CATID";
		mysql_query($query);
		$rc=$rc."Performing roll-up for peer category<br>";
		if($verbose){
			$rc=$rc.$query."<br>";	
		}
		$query="insert into REPORTSCORE (SID,CID,CATID,VALIDRATERS,AVGSCORE,AGREEMENTSCORE) select SID,CID,CATID,$ratNum[4],AVG(STDSCORE),0 from SCALESCORE where CID=$CID and PID=$pid and CATID=4  and STDSCORE is not NULL group by SID,CID,CATID";
		mysql_query($query);
		$rc=$rc."Performing roll-up for direct report category<br>";
		if($verbose){
			$rc=$rc.$query."<br>";	
		}
		// Now do the Combined Others category
		$cnt=($ratNum[2]+$ratNum[3]+$ratNum[4]);
		$query="insert into REPORTSCORE (SID,CID,CATID,VALIDRATERS,AVGSCORE,AGREEMENTSCORE) select SID,CID,6,$cnt,AVG(STDSCORE),0 from SCALESCORE where CID=$CID and PID=$pid and CATID in (2,3,4)  and STDSCORE is not NULL group by SID,CID";
		mysql_query($query);
		$rc=$rc."Performing roll-up for Combined Others category<br>";
		if($verbose){
			$rc=$rc.$query."<br>";	
		}
		$rc=$rc.calcRaterAgreement($CID,$pid,"3");
		$rc=$rc.calcRaterAgreement($CID,$pid,"4");
		// Calculate Discrepancy Profile
		$rc=$rc.calcDiscrepancyProfiles($CID,"3");
		$rc=$rc.calcDiscrepancyProfiles($CID,"4");
		$rc=$rc.calcOrgPerspective($CID,"3");
		$rc=$rc.calcOrgPerspective($CID,"4");
	}
	else{
		// Otherwise we calculate them as a Combined Peers+DR Group
		$cnt=($ratNum[3]+$ratNum[4]);
		// do we have at least 3 in the combined group?
		if($cnt>=3){
			$query="insert into REPORTSCORE (SID,CID,CATID,VALIDRATERS,AVGSCORE,AGREEMENTSCORE) select SID,CID,5,$cnt,AVG(STDSCORE),0 from SCALESCORE where CID=$CID and PID=$pid and CATID in (3,4)  and STDSCORE is not NULL group by SID,CID";
			mysql_query($query);
			$rc=$rc."Performing roll-up for $cnt raters in combined direct report and peer category<br>";
			if($verbose){
				$rc=$rc.$query."<br>";	
			}
			// Now do the Combined Others category
			$cnt=($ratNum[2]+$ratNum[3]+$ratNum[4]);
			$query="insert into REPORTSCORE (SID,CID,CATID,VALIDRATERS,AVGSCORE,AGREEMENTSCORE) select SID,CID,6,$cnt,AVG(STDSCORE),0 from SCALESCORE where CID=$CID and PID=$pid and CATID in (2,3,4)  and STDSCORE is not NULL group by SID,CID";
			mysql_query($query);
			$rc=$rc."Performing roll-up for Combined Others category<br>";
			if($verbose){
				$rc=$rc.$query."<br>";	
			}
			$rc=$rc.calcRaterAgreement($CID,$pid,"5");
			// Calculate Discrepancy Profile
			$rc=$rc.calcDiscrepancyProfiles($CID,"5");
			// organizational perspective
			$rc=$rc.calcOrgPerspective($CID,"5");
		}
		else{
			// Nope - but we can possibly do a combined other category that only contains bosses	
			if($ratNum[2]>0){
				$query="insert into REPORTSCORE (SID,CID,CATID,VALIDRATERS,AVGSCORE,AGREEMENTSCORE) select SID,CID,6,$ratNum[2],AVG(STDSCORE),0 from SCALESCORE where CID=$CID and PID=$pid and CATID=2  and STDSCORE is not NULL group by SID,CID";
				mysql_query($query);
				$rc=$rc."Performing roll-up for Combined Others category<br>";
				if($verbose){
					$rc=$rc.$query."<br>";	
				}
			}
			else{
				$rc=$rc."No other roll-ups possible<br>";
			}
		}
	}
	
	// Now correct the ones that are below 35 or above 65
/*	
	$query="update REPORTSCORE set AVGSCORE=65 where CID=$CID and AVGSCORE>65";
	mysql_query($query);
	$rc=$rc."Pruning values over 65<br>";
	if($verbose){
		$rc=$rc.$query."<br>";	
	}
	$query="update REPORTSCORE set AVGSCORE=35 where CID=$CID and AVGSCORE<35 or AVGSCORE is NULL";
	mysql_query($query);
	$rc=$rc."Inflating values below 35<br>";
	if($verbose){
		$rc=$rc.$query."<br>";	
	}
*/	
	return $rc;
}

// Calculates rater Agreement scale
function calcRaterAgreement($cid,$pid,$catid){
	$verbose=false;
	$cats="";
	// This is to accomodate a combined Peers + DR group
	if("5"==$catid){
		$cats="in (3,4)";
	}
	else{
		$cats="= ".$catid;		
	}
	$rc="Calculating Rater Agreement<br>";
	$conn=dbConnect();
	$query="select SID from REPORTSCORE where CID=$cid and CATID=$catid and SID<22";
	$rs=mysql_query($query);
	if($verbose){
		$rc=$rc.$query."<br>";
	}
	$sids=dbRes2Arr($rs);
	// Iterate over each scale
	foreach($sids as $sid){
		$query="select STDSCORE,RID from SCALESCORE where SID=$sid[0] and PID=$pid and CID=$cid and CATID $cats";
		if($verbose){
			$rc=$rc.$query."<br>";
		}
		$rss=mysql_query($query);
		$rows=dbRes2Arr($rss);
		$len=count($rows);
		$len=$len;
		$i=0;
		$j=0;
		$cnt=0;
		$sum=0;
		for($i=0;$i<$len-1;$i++){
			for($j=$i+1;$j<$len;$j++){
				$diff=$rows[$i][0]-$rows[$j][0];
				$sum=$sum+abs($diff);
				$cnt++;
				if($verbose){
					$rc=$rc."   compare $i - $j sum: $sum count $cnt<br>";
				}
			}
		}
		if(0<$cnt){
			$val=$sum/$cnt;
			$query="update REPORTSCORE set AGREEMENTSCORE=$val where CID=$cid and CATID=$catid and SID=$sid[0]";
			mysql_query($query);
		}
	}
	return $rc;
}

// Calculate Discrepancy Profile
// 03/06/2007: Exclude 0 and null values from calculations
function calcDiscrepancyProfiles($cid,$catid){
	$verbose=false;
	$conn=dbConnect();
	$query="";
	if("5"==$catid){
		// Spec says Calculate 'separately' for Boss/Peer/DR categories
//		return "";
		$query="select c.ITEMID, e.SID, ABS(a.VAL-AVG(c.VAL)) from RATERRESP a, RATER b, RATERRESP c, RATER d, SCALEITEM e where a.RID=$cid and b.CID=$cid and a.RID=b.RID and a.RID=b.CID and b.CID=d.CID and c.RID=d.RID and d.CATID in (3,4) and a.ITEMID=c.ITEMID and a.ITEMID < 64 and e.ITEMID=a.ITEMID and e.SID < 16 and c.VAL>0 and c.VAL is not null  and a.VAL>0 and a.VAL is not null group by c.ITEMID, e.SID  order by 3 desc";
	}
	else{
		// Select the discrepancies for items 1-63 and scales 1-15
		// Now this is one heckuva query - might have to alter later
		$query="select c.ITEMID, e.SID, ABS(a.VAL-AVG(c.VAL)) from RATERRESP a, RATER b, RATERRESP c, RATER d, SCALEITEM e where a.RID=$cid and b.CID=$cid and a.RID=b.RID and a.RID=b.CID and b.CID=d.CID and c.RID=d.RID and d.CATID=$catid and a.ITEMID=c.ITEMID and a.ITEMID < 64 and e.ITEMID=a.ITEMID and e.SID < 16  and c.VAL>0 and c.VAL is not null  and a.VAL>0 and a.VAL is not null group by c.ITEMID, e.SID  order by 3 desc";
	}
	$rc="Calcualting discrepancy profile ";
	$rs=mysql_query($query);
	if(!$rs){
		if($verbose){
			$rc=$rc."<br>$query";	
		}
		return $rc."<br>Error selecting records for category $catid<br>";
	}
	$rows=dbRes2Arr($rs);
	// we need to keep track of how many we selected form each scale somehow
	$sel=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	// and also keep track of the 4 we selected
	$cnt=0;
	foreach($rows as $row){
		$sid=$row[1];
		$tmp=$sel[$sid];
		if(2>$tmp){
			$query="insert into DISCREPANCYPROFILE (CID,CATID,SID,ITEMID,AVGSCORE) values ($cid,$catid,$row[1],$row[0],$row[2])";
			mysql_query($query);
			$tmp++;
			$sel[$sid]=$tmp;
			$cnt++;
			$rc=$rc."pick item $row[0] from scale $row[1] ";
			if($verbose){
				$rc=$rc."<br>".$query."<br>";
			}
		}
		if($cnt>3){
			// select only four items
			return $rc."<br>";	
		}
	}
	return $rc."<br>";
}

function calcOrgPerspective($cid,$catid){
	$conn=dbConnect();
	$query="insert into ORGPERSPECTIVE (CID,CATID,ITEMID,SCORE,AGREEMENTSCORE) ";
	$rc="Calculating Organizational Perspective for ";	
	if("1"==$catid){
		// for self it's just a value and quetions are 100-114
// 2/22/08		$query=$query."select a.CID,a.CATID,b.ITEMID,b.VAL,0 from RATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>99 and a.CID=$cid and a.CATID=1  and b.VAL is not NULL ";
		$query=$query."select a.CID,a.CATID,b.ITEMID,b.VAL,0 from RATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>99 and a.CID=$cid and a.CATID=1  and b.VAL is not NULL and b.VAL>0";
		$rc=$rc." Self<br>";
	}
	else{
		if("5"==$catid){
			//for combined Peers and DR 
// 2/22/08			$query=$query."select a.CID,5,b.ITEMID,AVG(b.VAL),0 from RATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>63 and b.ITEMID<79 and a.CID=$cid and a.CATID in (3,4)  and b.VAL is not NULL group by a.CID,b.ITEMID";
			$query=$query."select a.CID,5,b.ITEMID,AVG(b.VAL),0 from RATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>63 and b.ITEMID<79 and a.CID=$cid and a.CATID in (3,4)  and b.VAL is not NULL and b.VAL>0 group by a.CID,b.ITEMID";
		}
		else{
			//for all others 
// 2/22/08			$query=$query."select a.CID,a.CATID,b.ITEMID,AVG(b.VAL),0 from RATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>63 and b.ITEMID<79 and a.CID=$cid and a.CATID=$catid  and b.VAL is not NULL group by a.CID,a.CATID,b.ITEMID";
			$query=$query."select a.CID,a.CATID,b.ITEMID,AVG(b.VAL),0 from RATER a, RATERRESP b where a.RID=b.RID and b.ITEMID>63 and b.ITEMID<79 and a.CID=$cid and a.CATID=$catid  and b.VAL is not NULL and b.VAL>0 group by a.CID,a.CATID,b.ITEMID";
		}
		$rc=$rc." Rater category $catid<br>";
	}
	mysql_query($query);
	return $rc;
}

// calculate the scores for an individual candidate and her raters
// Note: the algorithm is laid out for calrity and ease of trouble shooting - optimize when 100% correct
function scoreIndividual($cid,$pid,$conid){
	// 0. Preliminaries:
	
	// get rid of old stuff
	getRidOfOldScores($cid,$pid);
	
	// get the Name
	$candNm=getScoreCandidateName($cid, $pid);
	if(!$candNm){
		return "<u>Unable to locate Candidate with PIN=$cid - Skipping</u><br>";	
	}
	$rc="<br><u>Scoring $candNm</u><br>";
	
	// 1. Get how many completed responses we have in each category
	$ratNum=getValidRaterCount($cid);

	// 2. Calculate Scale Scores
	if(0==$ratNum[1]){
		return $rc."Self survey not completed - no point in continuing - Skipping<br>";	
	}

	$rc=$rc."Calculating Self scale score<br>";
	$rc=$rc.computeRaterScaleScores($cid,$cid,$pid,"1");
	
	// calculate Boss scale scores
	if($ratNum[2]>0){
		$rc=$rc."Calculating Boss scale score for $ratNum[2] Boss(es)<br>";
		$raters=getRaterIds($cid,"2");
		if($raters){
			foreach($raters as $rater){
				$rc=$rc."Processing rater $rater[1] $rater[2]...<br>";
				$rc=$rc.computeRaterScaleScores($rater[0],$cid,$pid,"2");
			}
		}
	}
	else{
		$rc=$rc."No Bosses<br>";
	}
	
	// calculate Peer scale scores
	if($ratNum[3]>0){
		$rc=$rc."Calculating Peer scale score for $ratNum[3] Peer(s)<br>";
		$raters=getRaterIds($cid,"3");
		if($raters){
			foreach($raters as $rater){
				$rc=$rc."Processing rater $rater[1] $rater[2]...<br>";
				$rc=$rc.computeRaterScaleScores($rater[0],$cid,$pid,"3");
			}
		}
	}
	else{
		$rc=$rc."No Peers<br>";
	}
	
	// calculate Direct Report scale scores
	if($ratNum[4]>0){
		$rc=$rc."Calculating Direct Report scale score for $ratNum[4] Direct Report(s)<br>";
		$raters=getRaterIds($cid,"4");
		if($raters){
			foreach($raters as $rater){
				$rc=$rc."Processing rater $rater[1] $rater[2]...<br>";
				$rc=$rc.computeRaterScaleScores($rater[0],$cid,$pid,"4");
			}
		}
	}
	else{
		$rc=$rc."No Direct Reports<br>";
	}
	
	// 3. Calculate the rolled up Report Scores and Rater Agreement Scores
	$rc=$rc.calculateIndividualReportScores($cid,$pid,$ratNum);

	// 4. If we're here the scoring went well
	// it is ok to conume a license, unless this is a rescore
	if(0==alreadyConsumed($cid,$pid)){
		if(false==consumeLicense($conid,$cid,$pid)){
			$rc=$rc."<font color=\"#ff0000\">Error! unable to consume license for candidate  ID=$cid program=$pid consultant=$conid</font><br>";
		}
		else{
			$rc=$rc."Consuming 1 license for candidate ID=$cid program=$pid consultant=$conid<br>";
		}
	}
	else{
		$rc=$rc."A license has alreday been consumed for this candidate  ID=$cid program=$pid consultant=$conid<br>(Is this a rescore?)<br>";
	}
	
	return $rc;
}

// Consume a license
function consumeLicense($conid,$cid,$tid){ 
	$conn=dbConnect();
	$ts=date("Y-m-d");
	// Insert the CANDLIC row
	$query="insert into CANDLIC (CONID,CID,TID,TS) values ( $conid,$cid,$tid,'$ts')";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	
	return true;
}

// Checks to see if a license is already consumed for this candidate within this program
function alreadyConsumed($cid,$pid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from CANDLIC where CID=$cid and TID=$pid");
	$row=mysql_fetch_row($rs);
	return $row[0];
}

// get all candidates in a program
function getAllCandidates($pid){
	$conn=dbConnect();
	$query="select CID,FNAME,LNAME from CANDIDATE where PID=$pid";
	$rs=mysql_query($query);
	return dbRes2Arr($rs);
}

// Shows the license status at the end of scoring
function showLicenseStatus($conid,$when){
	$conn=dbConnect();
	$rs=mysql_query("select SUM(CNT) from LICENSE where CONID=$conid and INSTR=1");
	$row=mysql_fetch_row($rs);
	$bought=$row[0];
	$rs=mysql_query("select count(*) from CANDLIC where CONID=$conid");
	$row=mysql_fetch_row($rs);
	$consumed=$row[0];
	$balance=$bought-$consumed;
	
	$rc=$balance>=0?"<br><BIG><font color=\"#00ff00\">":"<br><BIG><font color=\"#ff0000\">";
	return $rc."$when scoring this program the consultant has a balance of $balance available licenses.<br>(Total Purchased: $bought Total Consumed: $consumed)</font></BIG><br>";
}


// Get the consultant ID
function getConidForScoring($pid){
	$conn=dbConnect();
	$query="select CONID from PROGCONS where PID=$pid";
	$rs=mysql_query($query);
	return dbRes2Arr($rs);
}

// Entry point for scoring a program
function scoreProgram($pid){
	$conn=dbConnect();
	// get the consultantid for the program
	$consid=getConidForScoring($pid);

	$msg="<u>Scoring program $pid</u><br>";

	// Show the Licensing status before scoring
	$rs=showLicenseStatus($consid[0][0],"Before");
	$msg=$msg.$rs."<br>";

	// Get all the candidates in the program
	$candidates=getAllCandidates($pid);

	// Show the number of candidates in the program
	$msg=$msg."Number of candidates in program: ".count($candidates)."<br>";
	
	// Score each Candidate in the program
	foreach($candidates as $cand){
		$rs=scoreIndividual($cand[0],$pid,$consid[0][0]);
		$msg=$msg.$rs;
	}
	
	// Show the Licensing status after scoring
	$rs=showLicenseStatus($consid[0][0],"After");
	$msg=$msg.$rs;
	
	// Set status to "Scored"
	$query="update PROGRAM set EXPIRED='S' where PID=$pid";
	//echo $query."<br>";
	mysql_query($query);
	return $msg;
}

?>
