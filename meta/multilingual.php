<?php
require_once "dbfns.php";
//---------------------------------------------
//-- General Multilingual support
//---------------------------------------------

// get the File ID for the file we're using
function getFLID($path,$filename){
	$conn=dbConnect();
	$query="select FLID from MLFile where Path='$path' and Descr='$filename'";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	return $row[0];

}

// get text for specific location(s) within a file
// is returned as an array if multiple values
// or as a single value if a single location was requested
function getMLText($flid,$tid,$lid,$pos=""){
	$single=false;
	$conn=dbConnect();
	$query="select Descr, Pos from MLText where FLID=$flid and LID=$lid and TID=$tid";
	if(strlen($pos)>0){
		$query=$query." and Pos=$pos";
		$single=true;
	}
	else{
		$query=$query." order by Pos";
	}
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	
	if(true==$single){
		// we only asked for one value
		$row=mysql_fetch_array($rs);
		return utf8_decode($row[0]);
	}
	
	// potentially multiple values
	$rc=array();
	$rows=dbRes2Arr($rs);
	foreach($rows as $row){
		// Stuff in the correctt position of the array
		// Note that we decode UTF-8 multibyte characters
		$idx=$row[1];
		$rc[$idx]=utf8_decode($row[0]);
	}
	return $rc;
}

// Does a rater lang row exist for this Rater (Candidate)?
function doesRaterLangExist($rid){
	$conn=dbConnect();
	$query="select count(*) from RATERLANG where RID=$rid";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	$cnt=$row[0];
	return $cnt>0;
}

// Get the Program ID for a Candidate
function getPID4Candidate($cid){
	$conn=dbConnect();
	$query="select PID from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	return $row[0];
}

// Get the Candidate ID for a Rater
function getCID4RATER($rid){
	$conn=dbConnect();
	$query="select CID from RATER where RID=$rid";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	return $row[0];
}

// Is this a valid language choice for this program?
function isLIDValidForPID($lid,$pid){
	$conn=dbConnect();
	$query="select count(*) from PROGLANG where PID=$pid and LID=$lid";
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_array($rs);
	$cnt=$row[0];
	return $cnt>0;
}

//----------------------------------------------------------
//-- Candidates --------------------------------------------
//----------------------------------------------------------

// Ensure a RATERLANG Row Exists for a Candidate
// Note: default is English
// Note: the third parameter dictates the behavior if language
// has not been specified for a PROGRAM
function insertCandidateLanguage($cid,$lid,$addProgLang=false){
	
	if("8"!=$lid&&"1"!=$lid){
		$lid="1";
	}
	
	$conn=dbConnect();
	// get the program id
	$pid=getPID4Candidate($cid);
	
	if(false==isLIDValidForPID($lid,$pid)){
		// This language is not valid for this program
		if(true==$addProgLang){
			// this means we should add a PROGLANG if it
			// doesn't already exist
			$query="insert into PROGLANG (PID,LID) values ($pid,$lid)";
			mysql_query($query);
		}
		else{
			// if we're here we shouldn't add a PROGLANG
			return false;
		}
	}
	// if we're here we have a valid program language
	// and can go ahead and add the RATERLANG for the Candidate
	// unless we already have it
	if(false==doesRaterLangExist($cid)){
		$query="insert into RATERLANG (RID,PID,LID) values ($cid,$pid,$lid)";
		mysql_query($query);
	}
	
	return true;
}

//----------------------------------------------------------
//-- Raters -----------------------------------------------
//---------------------------------------------------------

?>
