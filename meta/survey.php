<?php
/* 11-21-2005: Modified to accept multiple languages
*/
require_once 'dbfns.php';
require_once 'mailfns.php';
require_once 'rater.php';

// for multilingual support
require_once 'multilingual.php';

// Functions for displaying/taking surveys including demographic data
function insertSurvey($cid,$rid,$tid){
	if(!hasDemographics($rid,$tid)){
		// we don't have any demographics yet, so insert them now
		startSurvey($rid);
		insertDemographics($rid,$tid);
	}
	if(!hasItems($rid,$tid)){
		// we don't have items, so insert them
		startSurvey($rid);
		insertItems($rid,$tid);
	}
}

// get the first question on a page
function getQ1($page,$tid){
	if("1"==$tid||"3"==$tid){
		$q=array(0,0,1,21,43,64,80,100,0,0,0);
		return $q[$page];
	}
	if("2"==$tid){
		$q=array(0,0,0,1,11,32,53,64,79,0,0,0);
		return $q[$page];
	}
	return 0;
}

// get the last question on a page
function getQn($page,$tid){
	if("1"==$tid||"3"==$tid){
		$q=array(0,0,20,42,63,79,99,114,0,0,0);
		return $q[$page];
	}
	if("2"==$tid){
		$q=array(0,0,0,10,31,52,63,78,80,0,0,0);
		return $q[$page];
	}
	return 0;
}

// retreive all the items for a page
function getItems($rid,$tid,$lid,$page){
	$conn=dbConnect();
	$q1=getQ1($page,$tid);
	$qN=getQn($page,$tid);
	if($tid=="1"||"3"==$tid){
		$query="select a.ITEMID, b.DESCR, a.CATID, a.SECTID, a.STYLEID, a.TID, c.VAL from ITEM a, ITEMTXT b, RATERRESP c where a.ITEMID=b.ITEMID and a.ITEMID=c.ITEMID and a.TID=b.TID and a.TID=c.TID and b.LID=$lid and c.RID=$rid and a.ITEMID>=$q1 and a.ITEMID<=$qN order by 1";
	}
	elseif($tid=="2"){
		if($qN<79){
			// multiple choice questions
			$query="select a.ITEMID, b.DESCR, a.CATID, a.SECTID, a.STYLEID, a.TID, c.VAL from ITEM a, ITEMTXT b, RATERRESP c where a.ITEMID=b.ITEMID and a.ITEMID=c.ITEMID and a.TID=b.TID and a.TID=c.TID and b.LID=$lid and c.RID=$rid and a.ITEMID>=$q1 and a.ITEMID<=$qN order by 1";
		}
		else{
			// free form questions
			$query="select a.ITEMID, b.DESCR, a.CATID, a.SECTID, a.STYLEID, a.TID, c.VAL from ITEM a, ITEMTXT b, RATERCMT c where a.ITEMID=b.ITEMID and a.ITEMID=c.ITEMID and a.TID=b.TID and a.TID=c.TID and b.LID=$lid and c.RID=$rid and a.ITEMID>=$q1 and a.ITEMID<=$qN order by 1";
		}
	}
	//echo $query."<br>";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// Checks to see if items exist
function hasItems($rid,$tid){
	$conn=dbConnect();
	$query="select * from RATERRESP where RID=$rid and TID=$tid";
	$rs=mysql_query($query);
	return mysql_fetch_row($rs);
}

// Inserts survey items
function insertItems($rid,$tid){
	$conn=dbConnect();
	if("1"==$tid||"3"==$tid){
		$query="insert into RATERRESP (ITEMID,TID,RID,VAL) select ITEMID,TID,$rid,NULL from ITEM where TID=$tid";
	}
	elseif("2"==$tid){
		$query="insert into RATERRESP (ITEMID,TID,RID,VAL) select ITEMID,TID,$rid,NULL from ITEM where TID=$tid and ITEMID<79";
		mysql_query($query);
		$query="insert into RATERCMT (ITEMID,TID,RID,VAL) select ITEMID,TID,$rid,NULL from ITEM where TID=$tid and ITEMID>78";
	}
	//echo $query."<br>";
	return mysql_query($query);
}

// Does just that
function saveItems($rid,$tid,$keys,$values){
	$conn=dbConnect();
	$i=0;
	if("1"==$tid||"3"==$tid){
		foreach($keys as $key){
			$query="update RATERRESP set VAL=".$values[$i]." where RID=$rid and TID=$tid and ITEMID=$key";
			//echo $query."<br>";
			mysql_query($query);
			$i++;
		}
	}
	elseif("2"==$tid){
		foreach($keys as $key){
			if($key<79){
				$query="update RATERRESP set VAL=".$values[$i]." where RID=$rid and TID=$tid and ITEMID=$key";
			}
			else{
				$query="update RATERCMT set VAL='".$values[$i]."' where RID=$rid and TID=$tid and ITEMID=$key";
			}
			//echo $query."<br>";
			mysql_query($query);
			$i++;
		}
	}
}

function getNoAnswerCount($rid,$tid){
	$conn=dbConnect();
	$query="select count(*)  from RATERRESP where VAL is NULL and RID=$rid and TID=$tid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0];
}

function getUnansweredItems($rid,$tid){
	$conn=dbConnect();
	$query="select ITEMID from RATERRESP where VAL is NULL and RID=$rid and TID=$tid";
	//echo $query;
	$rs=mysql_query($query);
	$rows=dbRes2Arr($rs);
	$rc=array();
	foreach($rows as $row){
		$rc[]=$row[0];
	}
	return $rc;
}

// --- things to do with display of a survey

function show5Q($id,$val){
	$chkd=array();
	$chkd[$val]="checked";
	echo "</td><td>";
	echo "<table border=1><tr><td bgcolor='#dddddd'>";
	echo "<input name='resp$id' type='radio' value='1' ".$chkd[1]. " ><small> 1</small>";
	echo "</td><td bgcolor='#ffffff'>";
	echo "<input name='resp$id' type='radio' value='2' ".$chkd[2]. " ><small> 2</small>";
	echo "</td><td bgcolor='#dddddd'>";
	echo "<input name='resp$id' type='radio' value='3' ".$chkd[3]. " ><small> 3</small>";
	echo "</td><td bgcolor='#ffffff'>";
	echo "<input name='resp$id' type='radio' value='4' ".$chkd[4]. " ><small> 4</small>";
	echo "</td><td bgcolor='#dddddd'>";
	echo "<input name='resp$id' type='radio' value='5' ".$chkd[5]. " ><small> 5</small>";
	echo "</tr></table>";
}

function show3Q($id,$val){
	$chkd=array();
	$chkd[$val]="checked";
	echo "</td><td>";
	echo "<table border=1><tr><td bgcolor='#dddddd'>";
	echo "<input name='resp$id' type='radio' value='1' ".$chkd[1]. " ><small> 1</small>";
	echo "</td><td bgcolor='#ffffff'>";
	echo "<input name='resp$id' type='radio' value='2' ".$chkd[2]. " ><small> 2</small>";
	echo "</td><td bgcolor='#dddddd'>";
	echo "<input name='resp$id' type='radio' value='3' ".$chkd[3]. " ><small> 3</small>";
	echo "</tr></table>";
}

function showFree($id,$val){
	echo "<br><textarea name='resp$id' cols=40 rows=5>".stripslashes($val)."</textarea>";
	echo "</td><td>";

}

// Show the legend for a section
function showLegend($sectid,$tid,$lid){
	$flid=getFLID("meta","survey.php");
	$mlText=getMLText($flid,$tid,$lid);
	if($tid=="1"){
		if($sectid=="1"){
			echo "Definitions of Rating Scale";
			echo "<br>1  =  I never respond in this way";
			echo "<br>2  = I rarely respond in this way";
			echo "<br>3  = I sometimes respond in this way";
			echo "<br>4  = I often respond in this way";
			echo "<br>5  = I almost always respond in this way";
		}
		elseif($sectid=="2"){
			echo "Definitions of Rating Scale";
			echo "<br>1  =  This situation does not upset me at all";
			echo "<br>2  = This situation upsets me to a small degree";
			echo "<br>3  = This situation upsets me to a moderate degree";
			echo "<br>4  = This situation upsets me to a considerable degree";
			echo "<br>5  = This situation makes me extremely upset";
		}
		elseif($sectid=="3"){
			echo "Definitions of Rating Scale";
			echo "<br>1  = No negative effect";
			echo "<br>2  = Moderatley negative effect";
			echo "<br>3  = Severely negative effect";
		}
	}
	elseif($tid=="3"){
		if($sectid=="1"){
			echo $mlText[1];
		}
		elseif($sectid=="2"){
			echo $mlText[2];
		}
		elseif($sectid=="3"){
			// there is no section 3 for self only, so this is empty
			echo $mlText[3];
		}
	}
	elseif($tid=="2"){
		if($sectid=="1"){
			echo "Definitions of Rating Scale";
			echo "<br>1  =  This individual never responds in this way";
			echo "<br>2  = This individual rarely responds in this way";
			echo "<br>3  = This individual sometimes responds in this way";
			echo "<br>4  = This individual often responds in this way";
			echo "<br>5  = This individual almost always responds in this way";
		}
		elseif($sectid=="2"){
			echo "Definitions of Rating Scale";
			echo "<br>1  =  No negative effect";
			echo "<br>2  = Moderately negative effect";
			echo "<br>3  = Severely negative effect";
		}
	}
}

// Show section text
function showSection($sectid,$tid,$lid){
  $name = getCandName($_SESSION['rid']);
	$flid=getFLID("meta","survey.php");
	$mlText=getMLText($flid,$tid,$lid);
	if("1"==$tid){
		echo "<tr><td colspan=3>Section $sectid</td></tr>";
		if($sectid=="1"){
			echo "<tr><td colspan=3>Interpersonal conflict is extremely common, both at home and in the workplace.  When such conflicts arise, there are many different ways to react, and none of them is always right or always wrong.  The following items ask about the way you usually respond before, during, and after interpersonal conflicts occur in your life.  Please answer each one as honestly and as accurately as you can. Fill in the appropriate response on the answer sheet to indicate how frequently you respond in that way during interpersonal conflicts. Be sure to complete every item.</td></tr>";
			echo "<tr><td colspan=3>";
			showLegend($sectid,$tid,$lid);
			echo "</td></tr>";
		}
		elseif($sectid=="2"){
			echo "<tr><td colspan=3>Not only is it important to understand how people respond to conflict, it is useful to know what kinds of situations and circumstances are most likely to create conflicts.  In this section, please indicate how upset you get when you have to deal with various kinds of people and behavior. These items ask you to indicate which situations produce the most and least irritation and frustration for you - the kind of irritation and frustration which leads to interpersonal conflict in the workplace. For each of the following items, please indicate how upset you get in this situation.</td></tr>";
			echo "<tr><td colspan=3>";
			showLegend($sectid,$tid,$lid);
			echo "</td></tr>";
		}
		elseif($sectid=="3"){
			echo "<tr><td colspan=3>While certain responses are generally helpful in dealing with conflict effectively, and other responses are generally harmful, the social and organizational environment can make some responses especially helpful or harmful.  That is, organizations often differ in terms of the kind of responses to conflict which are encouraged, and the kind which are frowned upon. Consistently responding in ways which are seen by the organization as undesirable can have serious negative implications for one's career in that particular organization.<br>&nbsp;<br><b>In your organization, which of the following responses to conflict have the most negative effect on a person's career?</b> That is, if the person consistently displayed this response to conflict, it would seriously slow or derail his/her advancement in your organization.  For each of the responses listed below, please indicate whether it would have <i>no negative effect</i>, a <i>moderately negative effect</i>, or a <i>severely negative effect</i> on a person's career.</td></tr>";
			echo "<tr><td colspan=3>";
			showLegend($sectid,$tid,$lid);
			echo "</td></tr>";
		}
	}
	elseif($tid=="3"){
		echo "<tr><td colspan=3>$mlText[4] $sectid</td></tr>";
		if($sectid=="1"){
			echo "<tr><td colspan=3>$mlText[5]</td></tr>";
			echo "<tr><td colspan=3>";
			showLegend($sectid,$tid,$lid);
			echo "</td></tr>";
		}
		elseif($sectid=="2"){
			echo "<tr><td colspan=3>$mlText[6]</td></tr>";
			echo "<tr><td colspan=3>";
			showLegend($sectid,$tid,$lid);
			echo "</td></tr>";
		}
		elseif($sectid=="3"){
			echo "<tr><td colspan=3>$mlText[7]</td></tr>";
			echo "<tr><td colspan=3>";
			showLegend($sectid,$tid,$lid);
			echo "</td></tr>";
		}
	}
	elseif($tid=="2"){
		echo "<tr><td colspan=3>Section $sectid</td></tr>";
		if($sectid=="1"){
// Old wording
//			echo "<tr><td colspan=3>Interpersonal conflict is extremely common, both at home and in the workplace. When such conflicts arise, there are many different ways to react, and none of them is always right or always wrong. The following items ask you to rate a particular individual in terms of the way he or she usually responds before, during, and after interpersonal conflicts occur. Please answer each one as honestly and as accurately as you can.<br>&nbsp;<br>Fill in the appropriate response to each question to indicate how frequently this individual responds in that way during interpersonal conflicts.<br></td></tr>";
// New wording
			echo "<tr><td colspan=3>Interpersonal conflict is extremely common, both at home and in the workplace. When such conflicts arise, there are many different ways to react, and none of them is always right or always wrong. The following items ask you to rate a particular individual in terms of the way he or she usually responds before, during, and after interpersonal conflicts occur. Please answer each one as honestly and as accurately as you can.";
			echo "<p>A few of the items will ask you about the person's thoughts or feelings. You may find these questions somewhat difficult, but please do your best to answer these questions by considering what the person says and does. Even if you are less confident about making these judgments, please answer all questions. Please answer each one as honestly and as accurately as you can.</p>";
 			echo "<p>Fill in the appropriate response to each question to indicate how frequently this individual responds in that way during interpersonal conflicts.</p></td></tr>";
			echo "<tr><td colspan=3>";
			showLegend($sectid,$tid,$lid);
			echo "</td></tr>";
		}
		elseif($sectid=="2"){
			echo "<tr><td colspan=3>While certain responses are generally helpful in dealing with conflict effectively, and other responses are generally harmful, the social and organizational environment can make some responses <i>especially</i> helpful or harmful. That is, organizations often differ in terms of the kind of responses to conflict which are encouraged, and the kind which are frowned upon. Consistently responding in ways which are seen by the organization as undesirable can have serious negative implications for one’s career in that particular organization.<br><b>In your organization, which of the following responses to conflict have the most negative effect on a person's career?</b> That is, if a person consistently displayed this response to conflict, it would seriously slow or derail his/her advancement in <b>your</b> organization. For each of the responses listed below, please indicate whether it would have <i>no negative effect</i>, a <i>moderately negative effect</i>, or a <i>severely negative effect</i> on a person’s career.</td></tr>";
			echo "<tr><td colspan=3>";
			showLegend($sectid,$tid,$lid);
			echo "</td></tr>";
		}
		elseif($sectid=="3"){
			echo "<tr><td colspan=3>For this section please think about your experiences with the person you are rating and answer as honestly as you can. The more specific the information, the more helpful it will be to the individual.";
			echo " NOTE: In <strong>".$name."</strong>'s report, your responses will be displayed exactly as you have typed them here.</td></tr>";
		}
	}
}

// Show category text
function showCat($catid,$tid,$lid){
	$flid=getFLID("meta","survey.php");
	$mlText=getMLText($flid,$tid,$lid);
	if($tid==1){
		if($catid=="1"){
			echo "<tr><td colspan=3><b>When an unpleasant conflict with another person is just beginning, I . . .</b></td></tr>";
		}
		elseif($catid=="2"){
			echo "<tr><td colspan=3><b>When another person seriously provokes me during a conflict, I . . .</b></td></tr>";
		}
		elseif($catid=="3"){
			echo "<tr><td colspan=3><b>When I am having a conflict with someone, I . . .</b></td></tr>";
		}
		elseif($catid=="4"){
			echo "<tr><td colspan=3><b>During a tense conflict with another person, I . . .</b></td></tr>";
		}
		elseif($catid=="5"){
			echo "<tr><td colspan=3><b>When a conflict has been going on for some time, I . . .</b></td></tr>";
		}
		elseif($catid=="6"){
			echo "<tr><td colspan=3><b>After an intense conflict with someone has ended, I . . .</b></td></tr>";
		}
		elseif($catid=="7"){
			echo "<tr><td colspan=3><b>When I have to work with someone who . . .</b></td></tr>";
		}
		elseif($catid=="8"){
			echo "<tr><td colspan=3><b>When I have to work with someone who . . .</b></td></tr>";
		}
		elseif($catid=="9"){
			echo "<tr><td colspan=3><b>Please indicate the degree of negative effect each of the following responses would have on a person's career in <i>your</i> organization.</b></td></tr>";
		}
		elseif($catid=="10"){
			echo "<tr><td colspan=3><b>Please indicate the degree of negative effect each of the following responses would have on a person's career in <i>your</i> organization.</b></td></tr>";
		}
	}
	elseif($tid==3){
		if($catid=="1"){
			echo "<tr><td colspan=3><b>$mlText[8]</b></td></tr>";
		}
		elseif($catid=="2"){
			echo "<tr><td colspan=3><b>$mlText[9]</b></td></tr>";
		}
		elseif($catid=="3"){
			echo "<tr><td colspan=3><b>$mlText[10]</b></td></tr>";
		}
		elseif($catid=="4"){
			echo "<tr><td colspan=3><b>$mlText[11]</b></td></tr>";
		}
		elseif($catid=="5"){
			echo "<tr><td colspan=3><b>$mlText[12]</b></td></tr>";
		}
		elseif($catid=="6"){
			echo "<tr><td colspan=3><b>$mlText[13]</b></td></tr>";
		}
		elseif($catid=="7"){
			echo "<tr><td colspan=3><b>$mlText[14]</b></td></tr>";
		}
		elseif($catid=="8"){
			echo "<tr><td colspan=3><b>$mlText[15]</b></td></tr>";
		}
		elseif($catid=="9"){
			echo "<tr><td colspan=3><b>$mlText[16]</b></td></tr>";
		}
		elseif($catid=="10"){
			echo "<tr><td colspan=3><b>$mlText[17]</b></td></tr>";
		}
	}
	elseif($tid==2){
		if($catid=="1"){
			echo "<tr><td colspan=3><b>When an unpleasant conflict with another person is just beginning, this individual . . .</b></td></tr>";
		}
		elseif($catid=="2"){
			echo "<tr><td colspan=3><b>When another person seriously provokes him/her during a conflict,  this individual . . .</b></td></tr>";
		}
		elseif($catid=="3"){
			echo "<tr><td colspan=3><b>When  this individual is having a conflict with someone, he/she . . .</b></td></tr>";
		}
		elseif($catid=="4"){
			echo "<tr><td colspan=3><b>During a tense conflict with another person,  this individual . . .</b></td></tr>";
		}
		elseif($catid=="5"){
			echo "<tr><td colspan=3><b>When a conflict has been going on for some time,  this individual . . .</b></td></tr>";
		}
		elseif($catid=="6"){
			echo "<tr><td colspan=3><b>After an intense conflict with someone has ended,  this individual . . .</b></td></tr>";
		}
		elseif($catid=="7"){
			echo "<tr><td colspan=3><b>Please indicate the degree of negative effect each of the following responses would have on a person's career in <i>your</i> organization.</b></td></tr>";
		}
	}
}

// display a multiple choice question page
function showSurvey($rid,$tid,$lid,$page){
	$flid=getFLID("meta","survey.php");
	$mlText=getMLText($flid,$tid,$lid);
	$data=getItems($rid,$tid,$lid,$page);
	echo "<table border=0 cellpadding=5>";
	if("1"==$tid){
		if("2"==$page||"5"==$page||"7"==$page){
			showSection($data[0][3],$tid,$lid);
		}
		else{
			echo "\n<tr><td colspan=3 align='left'>Section ".$data[0][3]." - Continued</td></tr>\n";
			echo "\n<tr><td colspan=3 align='left'>";
			showLegend($data[0][3],$tid,$lid);
			echo "</td></tr>\n";
		}
	}
	elseif("3"==$tid){
		if("2"==$page||"5"==$page||"7"==$page){
			showSection($data[0][3],$tid,$lid);
		}
		else{
			echo "\n<tr><td colspan=3 align='left'>$mlText[4] ".$data[0][3]." - $mlText[18]</td></tr>\n";
			echo "\n<tr><td colspan=3 align='left'>";
			showLegend($data[0][3],$tid,$lid);
			echo "</td></tr>\n";
		}
	}
	if("2"==$tid){
		if("3"==$page||"7"==$page||"8"==$page){
			showSection($data[0][3],$tid,$lid);
		}
		else{
			echo "\n<tr><td colspan=3 align='left'>Section ".$data[0][3]." - Continued</td></tr>\n";
			echo "\n<tr><td colspan=3 align='left'>";
			showLegend($data[0][3],$tid,$lid);
			echo "</td></tr>\n";
		}
	}

	$catid="0";
	foreach($data as $rs){
		if($catid!=$rs[2]){
			$catid=$rs[2];
			showCat($catid,$tid,$lid);
		}
		echo "<tr><td valign='top' align='left'>$rs[0]</td><td valign='top' align='left'>";
		echo stripslashes(utf8_decode($rs[1]));
		switch($rs[4]){
			case 1:
				show5Q($rs[0],$rs[6]);
				break;
			case 2:
				show3Q($rs[0],$rs[6]);
				break;
			case 3:
				showFree($rs[0],$rs[6]);
				break;
		}
		echo "</td></tr>";
	}
	echo "</table>";
}


//----------------- Demographics -------------------

// Saves demographic data. Note that the first datum i.e. $answers[0] is ignored
function saveDemographics($cid,$tid,$answers){
	$conn=dbConnect();
	if($tid=="1"||"3"==$tid){
		for($i=1;$i<=18;$i++){
			$query="update DEMOGR set TXT='".$answers[$i]."' where CID=$cid and TID=$tid and DMID=$i";
			//echo $query."<br>";
			mysql_query($query);
		}
	}
	if($tid=="2"){
		for($i=1;$i<=4;$i++){
			// Only save some of it, since it's split over 2 pages
			if(0<strlen($answers[$i])){
				$query="update RATERDEMOGR set TXT='".$answers[$i]."' where RID=$cid and TID=$tid and DMID=$i";
				//echo $query."<br>";
				mysql_query($query);
			}
		}
	}
}

// Checks to see if demographics exist
function hasDemographics($cid,$tid){
	$conn=dbConnect();
	if($tid=="1"||$tid=="3"){
		$query="select * from DEMOGR where CID=$cid and TID=$tid";
	}
	elseif($tid=="2"){
		$query="select * from RATERDEMOGR where RID=$cid and TID=$tid";
	}
	$rs=mysql_query($query);
	return mysql_fetch_row($rs);
}


// Inserts demographics
function insertDemographics($cid,$tid){
	$conn=dbConnect();

	if($tid=="1"||"3"==$tid){
		$query="insert into DEMOGR (CID,DMID,TID,TXT,VAL) select $cid,DMID,TID,NULL,NULL from INSTRDEMOGR where TID=$tid";
	}
	elseif($tid=="2"){
		$query="insert into RATERDEMOGR (RID,DMID,TID,TXT,VAL) select $cid,DMID,TID,NULL,NULL from INSTRDEMOGR where TID=$tid";
	}
	return mysql_query($query);
}

// Lists all demographics
function listDemographics($cid,$tid){
	$conn=dbConnect();
	if($tid=="1"||"3"==$tid){
		$query="select DMID,VAL,TXT from DEMOGR where CID=$cid and TID=$tid order by DMID asc";
	}
	elseif($tid=="2"){
		$query="select DMID,VAL,TXT from RATERDEMOGR where RID=$cid and TID=$tid order by DMID asc";
	}

	//echo "$query<br>";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// Shows the demogrpahics page for Self
function showSelfDemographics($cid,$tid,$lid="1"){
	$flid=getFLID("meta","survey.php");
	$mlTxt=getMLText($flid,$tid,$lid);
	$data=listDemographics($cid,$tid);
	echo "<table border=0>";
	echo "<tr><td>$mlTxt[20]<br><input type='text' name='dm1' value='".stripslashes($data[0][2])."'></td></tr>";
	echo "<tr><td>$mlTxt[21]<br><input type='text' name='dm2' value='".stripslashes($data[1][2])."'></td></tr>";
	echo "<tr><td>$mlTxt[22]<br><input type='text' name='dm3' value='".stripslashes($data[2][2])."'></td></tr>";

	$tmparr=array();
	if("M"==$data[3][2]){
		$tmparr[0]="checked";
	}
	elseif("F"==$data[3][2]){
		$tmparr[1]="checked";
	}
	echo "<tr><td>1. $mlTxt[23]<br><input type='radio' name='dm4' value='M' ".$tmparr[0]."> $mlTxt[24]<br><input type='radio' name='dm4' value='F' ".$tmparr[1]."> $mlTxt[25]</td></tr>";

	echo "<tr><td>2. $mlTxt[26]<br><input type='text' name='dm5' value='".stripslashes($data[4][2])."'></td></tr>";

//	echo "<tr><td>3. Race or Ethnicity (Mark all that apply)<br> <small><i>U.S. law encourages us to collect information on the racial mix of our clients.<br>This question is for data purposes only and need not be answered by non-U.S. citizens.</i></small></td></tr>";
	echo "<tr><td>3. $mlTxt[27]<br> <small><i>$mlTxt[28]</i></small></td></tr>";

	echo "<tr><td><input type='checkbox' name='dm6' ".($data[5][2]=="on"?"checked":"")."> $mlTxt[29] </td></tr>";
	echo "<tr><td><input type='checkbox' name='dm7' ".($data[6][2]=="on"?"checked":"")."> $mlTxt[30] </td></tr>";
	echo "<tr><td><input type='checkbox' name='dm8' ".($data[7][2]=="on"?"checked":"")."> $mlTxt[31] </td></tr>";
	echo "<tr><td><input type='checkbox' name='dm9' ".($data[8][2]=="on"?"checked":"")."> $mlTxt[32] </td></tr>";
	echo "<tr><td><input type='checkbox' name='dm10' ".($data[9][2]=="on"?"checked":"")."> $mlTxt[33] </td></tr>";
	echo "<tr><td><input type='checkbox' name='dm11' ".($data[10][2]=="on"?"checked":"")."> $mlTxt[34] </td></tr>";
	echo "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;($mlTxt[35])<br>&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='dm12' value='".stripslashes($data[11][2])."'></td></tr>";

	$tmparr=array();
	$idx=$data[12][2];
	if(!is_null($idx)){
		$tmparr[$idx]="checked";
	}
	echo "<tr><td>4. $mlTxt[36]";
	echo "<br><input type='radio' name='dm13' value='1' ".$tmparr[1]."> $mlTxt[37]";
	echo "<br><input type='radio' name='dm13' value='2' ".$tmparr[2]."> $mlTxt[38]";
	echo "<br><input type='radio' name='dm13' value='3' ".$tmparr[3]."> $mlTxt[39]";
	echo "<br><input type='radio' name='dm13' value='4' ".$tmparr[4]."> $mlTxt[40]";
	echo "<br><input type='radio' name='dm13' value='5' ".$tmparr[5]."> $mlTxt[41]</td></tr>";

	$tmparr=array();
	$idx=$data[13][2];
	if(!is_null($idx)){
		$tmparr[$idx]="checked";
	}
	echo "<tr><td>5. $mlTxt[42]";
	echo "<br><input type='radio' name='dm14' value='7' ".$tmparr[7]."> $mlTxt[43]";
	echo "<br><input type='radio' name='dm14' value='6' ".$tmparr[6]."> $mlTxt[44]";
	echo "<br><input type='radio' name='dm14' value='5' ".$tmparr[5]."> $mlTxt[45]";
	echo "<br><input type='radio' name='dm14' value='4' ".$tmparr[4]."> $mlTxt[46]";
	echo "<br><input type='radio' name='dm14' value='3' ".$tmparr[3]."> $mlTxt[47]";
	echo "<br><input type='radio' name='dm14' value='2' ".$tmparr[2]."> $mlTxt[48]";
	echo "<br><input type='radio' name='dm14' value='1' ".$tmparr[1]."> $mlTxt[49]";
	echo "</td></tr>";

	$tmparr=array();
	$idx=$data[14][2];
	if(!is_null($idx)){
		$tmparr[$idx]="checked";
	}
	echo "<tr><td>6. $mlTxt[50]";
	echo "<br><b>A.</b><br><input type='radio' name='dm15' value='1'  ".$tmparr[1]."> $mlTxt[51]";
	echo "<br><input type='radio' name='dm15' value='2' ".$tmparr[2]."> $mlTxt[52]";

	$tmparr=array();
	$idx=$data[15][2];
	if(!is_null($idx)){
		$tmparr[$idx]="checked";
	}
	echo "<br><b>B.</b><br><input type='radio' name='dm16' value='1' ".$tmparr[1]."> $mlTxt[53]";
	echo "<br><input type='radio' name='dm16' value='2' ".$tmparr[2]."> $mlTxt[54]";
	echo "<br><input type='radio' name='dm16' value='3' ".$tmparr[3]."> $mlTxt[55]";
	echo "<br><input type='radio' name='dm16' value='4' ".$tmparr[4]."> $mlTxt[56]";
	echo "<br><input type='radio' name='dm16' value='5' ".$tmparr[5]."> $mlTxt[57]";
	echo "<br><input type='radio' name='dm16' value='6' ".$tmparr[6]."> $mlTxt[58]";
	echo "<br><input type='radio' name='dm16' value='7' ".$tmparr[7]."> $mlTxt[59]";
	echo "<br><input type='radio' name='dm16' value='8' ".$tmparr[8]."> $mlTxt[60]";
	echo "<br><input type='radio' name='dm16' value='9' ".$tmparr[9]."> $mlTxt[61]";
	echo "<br><input type='radio' name='dm16' value='10' ".$tmparr[10]."> $mlTxt[62]";
	echo "<br><input type='radio' name='dm16' value='11' ".$tmparr[11]."> $mlTxt[63]";
//	echo "<br><input type='radio' name='dm16' value='12' ".$tmparr[12]."> Paramilitary/Terrorism";
	echo "</td></tr>";

	$tmparr=array();
	$idx=$data[16][2];
	if(!is_null($idx)){
		$tmparr[$idx]="checked";
	}
	echo "<tr><td>7. $mlTxt[64]";
	echo "<br><input type='radio' name='dm17' value='1' ".$tmparr[1]."> $mlTxt[65]";
//	echo "<br><input type='radio' name='dm17' value='2' ".$tmparr[2]."> Executive, vice president, director board-level professional";
	echo "<br><input type='radio' name='dm17' value='3' ".$tmparr[3]."> $mlTxt[66]";
	echo "<br><input type='radio' name='dm17' value='4' ".$tmparr[4]."> $mlTxt[67]";
	echo "<br><input type='radio' name='dm17' value='5' ".$tmparr[5]."> $mlTxt[68]";
	echo "<br><input type='radio' name='dm17' value='6' ".$tmparr[6]."> $mlTxt[69]";
	echo "<br><input type='radio' name='dm17' value='7' ".$tmparr[7]."> $mlTxt[70]";
	echo "<br><input type='radio' name='dm17' value='8' ".$tmparr[8]."> $mlTxt[71]";
	echo "<br><input type='radio' name='dm17' value='9' ".$tmparr[9]."> $mlTxt[72]";
	echo "<br><input type='radio' name='dm17' value='10' ".$tmparr[10]."> $mlTxt[73]";
	echo "<br><input type='radio' name='dm17' value='11' ".$tmparr[11]."> $mlTxt[74]";
	echo "<br><input type='radio' name='dm17' value='12' ".$tmparr[12]."> $mlTxt[75]";
	echo "<br><input type='radio' name='dm17' value='13' ".$tmparr[12]."> $mlTxt[76]";
	echo "<br><input type='radio' name='dm17' value='14' ".$tmparr[14]."> $mlTxt[77]";
	echo "<br><input type='radio' name='dm17' value='15' ".$tmparr[15]."> $mlTxt[78]";
	echo "<br><input type='radio' name='dm17' value='16' ".$tmparr[16]."> $mlTxt[79]";
	echo "<br><input type='radio' name='dm17' value='17' ".$tmparr[17]."> $mlTxt[80]";
	echo "<br><input type='radio' name='dm17' value='18' ".$tmparr[18]."> $mlTxt[81]";
	echo "<br><input type='radio' name='dm17' value='19' ".$tmparr[19]."> $mlTxt[82]";
	echo "<br><input type='radio' name='dm17' value='20' ".$tmparr[20]."> $mlTxt[83]";
	echo "<br><input type='radio' name='dm17' value='21' ".$tmparr[21]."> $mlTxt[84]";
	echo "<br><input type='radio' name='dm17' value='22' ".$tmparr[22]."> $mlTxt[85]";
	echo "<br><input type='radio' name='dm17' value='23' ".$tmparr[23]."> $mlTxt[86]";
	echo "<br><input type='radio' name='dm17' value='24' ".$tmparr[24]."> $mlTxt[87]";
	echo "<br><input type='radio' name='dm17' value='25' ".$tmparr[25]."> $mlTxt[88]";
	echo "<br><input type='radio' name='dm17' value='26' ".$tmparr[26]."> $mlTxt[34]";
	echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;($mlTxt[35])<br>&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='dm18' value='".stripslashes($data[17][2])."'>";
	echo "</td></tr>";
	echo "</table>";
}

function showRespondentDemographics($cid,$tid,$page){
	$data=listDemographics($cid,$tid);
	echo "<table border=0>";
	if($page=="1"){
		echo "<tr><td>Organization Name of the person being rated<br><input type='text' name='dm1' value='".stripslashes($data[0][2])."'></td></tr>";
		$tmparr=array();
		$idx=$data[1][2];
		if(!is_null($idx)){
			$tmparr[$idx]="checked";
		}

		echo "<tr><td>1. How well do you know the person being rated>";
		echo "<br><input type='radio' name='dm2' value='1'  ".$tmparr[1]."> I hardly know the person";
		echo "<br><input type='radio' name='dm2' value='2' ".$tmparr[2]."> I know the person somewhat";
		echo "<br><input type='radio' name='dm2' value='3'  ".$tmparr[3]."> I know the person well";
		echo "<br><input type='radio' name='dm2' value='4' ".$tmparr[4]."> I know the person very well";
		echo "</td></tr>";
	}
	if($page=="2"){
		echo "<tr><td>The following two questions are optional and ask you to report your sex and age. These items are included for research purposes only and will <i>never be used to identify your individual answers</i>. If you would answer these items, it will be most appreciated.</td></tr>";

		$tmparr=array();
		if("M"==$data[2][2]){
			$tmparr[0]="checked";
		}
		elseif("F"==$data[2][2]){
			$tmparr[1]="checked";
		}
		echo "<tr><td>2.Sex<br><input type='radio' name='dm3' value='M' ".$tmparr[0]."> Male<br><input type='radio' name='dm3' value='F' ".$tmparr[1]."> Female</td></tr>";

		echo "<tr><td>3. Age<br><input type='text' name='dm4' value='".stripslashes($data[3][2])."'></td></tr>";
	}
	echo "</table>";
}

// Timetamps a rater when she begins the survey
function startSurvey($rid){
	$conn=dbConnect();
	$query="update RATER set STARTDT=NOW() where RID=$rid and STARTDT is NULL";
	mysql_query($query);
}

// Timestamps a rater when she completes a survey
function finishSurvey($rid){
	$conn=dbConnect();
	$query="update RATER set ENDDT=NOW(), EXPIRED='Y' where RID=$rid";
	mysql_query($query);

	// check to see if all answers are in - if they are email the admin
	areWeDoneYet($rid);
}

// sends an email if everybody has completed their surveys
function areWeDoneYet($rid){
	$conn=dbConnect();
	$query="select b.PID from RATER a, CANDIDATE b where a.CID=b.CID and a.RID=$rid";
	$rs=mysql_query($query);
	if($rs){
		$rows=dbRes2Arr($rs);
		$pid=$rows[0][0];
		if($pid!=0){
			$query="select count(a.RID) from RATER a, CANDIDATE b where a.CID=b.CID and a.EXPIRED='N' and b.PID=$pid";
			$rs=mysql_query($query);
			if($rs){
				$rows=dbRes2Arr($rs);
				$cnt=$rows[0][0];
				if(0==$rows){
					// we're done - send an email to the consultant pointing out that everyone has answered the survey
					$query="select distinct a.EMAIL from CONSULTANT a, CANDCONS b, CANDIDATE c where a.CONID=b.CONID and b.CID=c.CID and c.PID=$pid";
					$rs=mysql_query($query);
					$rows=dbRes2Arr($rs);
					$to=$rows[0][0];
					$query="select distinct a.EMAIL from CONSULTANT a, ADMINS b where a.CONID=b.ADMID";
					$rs=mysql_query($query);
					$rows=dbRes2Arr($rs);
					$from=$rows[0][0];
					$query="select distinct DESCR from PROGRAM where PID=$pid";
					$rs=mysql_query($query);
					$rows=dbRes2Arr($rs);
					$progName=$rows[0][0];
					$body="All candidates and respondents have submitted surveys for your Conflict Dynamics Profile program $progName (program id $pid)".getDisclaimer();
					$subject="Conflict Dynamics Program $progName";
					sendGenericMail($to,$from,$subject,$body,NULL);
				}
			}
		}
	}
}
?>
