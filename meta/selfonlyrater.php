<?php
require_once "dbfns.php";
// allows a self-only rater to log in, and verifies expiration status
// Won't allow any "usual" candidates in, only self only candiadtes
function selfOnlyRaterLogin($uid,$pin){
	/*$conn=dbConnect();
	$query="select a.EXPIRED from RATER a, CANDIDATE b where a.EMAIL='".stripslashes($uid)."' and a.RID=$pin and a.CID=b.CID and b.OTH='N'";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row?$row[0]:false;*/
	$mysqli=dbiConnect();
	if (!($query = $mysqli->prepare("select a.EXPIRED from RATER a, CANDIDATE b where UPPER(a.EMAIL)=? and a.RID=? and a.CID=b.CID and b.OTH='N'"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_param("si", strtoupper(stripslashes($uid)), $pin)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if (!$query->execute()) {
		echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_result($out_EXPIRED)) {
		echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$row = $query->fetch();
	if(!$row){
		return false;
	}else{
		return $out_EXPIRED;
	}
	$query->close();
}

// Resolves a Candidate's name by CID
function getCandidateName($cid){
	/*$conn=dbConnect();
	$query="select a.FNAME, a.LNAME from CANDIDATE a where a.CID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row?stripslashes($row[0])." ".stripslashes($row[1]):false;*/
	$mysqli=dbiConnect();
	if (!($query = $mysqli->prepare("select a.FNAME, a.LNAME from CANDIDATE a where a.CID=?"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_param("i", $cid)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if (!$query->execute()) {
		echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_result($out_FNAME, $out_LNAME)) {
		echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$row = $query->fetch();
	$query->close();
	if(!$row){
		return false;
	}else{
		return $out_FNAME." ".$out_LNAME;
	}
}

// Gets the language for a self only rater
function getSelfOnlyRaterLanguage($rid){
	$conn=dbConnect();
	$query="select LID from RATERLANG where RID=$rid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row?$row[0]:false;
}

?>
