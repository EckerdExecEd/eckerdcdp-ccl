<?php
include_once "report.php";
include_once "multilingual.php";

// a standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);

// active /passice contsructive/destructive
define("AC",1);
define("PC",2);
define("AD",3);
define("PD",4);

//--------------- 
// Scoring
//---------------

function getNumRaters($cid){
	$conn=dbConnect();
	// 2005-04-14: The original query caught some partial responses, so it has been replaced with the query from scoring.php
	//	$query="select a.CATID, count(distinct a.RID) from RATER a, RATERRESP b where a.RID=b.RID and a.CATID<>1 and a.CID=$cid and b.VAL is not NULL group by a.CATID order by a.CATID asc";
	$query="select CATID,count(*)  from RATER where CID=$cid and ENDDT is not NULL group by CATID order by CATID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function getRaterInteraction($cid){
	$conn=dbConnect();
	// 2005-04-29: The original query caught some partial responses, so it has been replaced with the query from scoring.php
	//$query="select a.CATID, b.TXT,count(b.TXT) from RATER a, RATERDEMOGR b where a.RID=b.RID and b.DMID=2 and a.cid=$cid group by a.CATID, b.TXT order by a.CATID asc";
	$query="select a.CATID, b.TXT,count(b.TXT) from RATER a, RATERDEMOGR b where a.RID=b.RID and a.ENDDT is not NULL and b.DMID=2 and a.cid=$cid group by a.CATID, b.TXT order by a.CATID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// Returns an array populated with raters by category
function getRatersByCat($cid){
	$rows=getNumRaters($cid);
	if(!$rows){
		return false;
	}
	$data=array(0,0,0,0,0,0,0,0);
	foreach($rows as $row){
		$data[$row[0]]=$row[1];	
	}
	$data[5]=($data[3]+$data[4]);
	return $data;
}

//----------------------------------------------------
//------ Reporting
//----------------------------------------------------
function getCandidateName($cid){
	$conn=dbConnect();
	$query="select FNAME, LNAME from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0]." ".$row[1];	
}

function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function writeHeader($cid,&$pdf,$lid,$pos=false){
	$name=getCandidateName($cid);
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);

	if(false!=$pos){
		// If we're here we should draw a band on top
		// Why do we have to divide by 2 ????
		// start 1 1/8 from top
		$y=PAGE_HEIGHT-(72*1.125/2);
		// 1/4 inch tall
		$height=(72*.25)/2;
		// the width is 1 3/4 less than the page
		$width=PAGE_WIDTH-(72*1.75/2);
		// Use font size 10.0
		$size=10.0;
		
		$flid=getFLID("meta","md0");
		$mltxt=getMLText($flid,"1",$lid);
		
		if("left"==$pos){
			//a left justified band
			$x=0;
			pdf_setcolor($pdf,'both','rgb',0,0,0,0);
			pdf_rect($pdf,$x,$y,$width,$height);
			pdf_fill_stroke($pdf);
			$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
			pdf_setfont($pdf,$font,$size);
			pdf_setcolor($pdf,'both','rgb',1,1,1,0);
			$what=strtoupper($mltxt[1]);
			$x=(72*1.75)/2;
			pdf_show_xy($pdf,$what,$x,$y-1);
		}
		elseif("right"==$pos){
			// a right justified band
			$x=(72*1.75)/2;
			pdf_setcolor($pdf,'both','rgb',0,0,0,0);
			pdf_rect($pdf,$x,$y,$width,$height);
			pdf_fill_stroke($pdf);
			$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
			pdf_setfont($pdf,$font,$size);
			pdf_setcolor($pdf,'both','rgb',1,1,1,0);
			$what=strtoupper("$mltxt[2] $name");
			$w=pdf_stringwidth($pdf,$what,$font,$size);
			// make it end 1 inch from the right edge
			// and it should sort of stick out of the band at the bottom
			$x=PAGE_WIDTH-$w-(72/2);
			pdf_show_xy($pdf,$what,$x,$y-1);
		}
	}
	return $name;	
}

// These are some canned functions to get fonts etc.
function getH1(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,17);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",20.4);
	return $font;
}

function getH2(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,16);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",19.2);
	return $font;
}

function getH3(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,13);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",15.6);
	return $font;
}

function getH4(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,12);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.4);
	return $font;
}

function getSubtext(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getBody(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getBody2(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",12.0);
	return $font;
}

function getQuote(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",12.0);
	return $font;
}

function getAttribution(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,8.5);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",10.2);
	return $font;
}

function getTableHead(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTableSide(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,9.5);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTableText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTableTextSm(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,9.5);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTinyText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,6);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTinyWhiteText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,6);
	pdf_setcolor($pdf,'both','rgb',1,1,1,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getSuperTinyText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,5);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTinyItalicText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
	pdf_setfont($pdf,$font,7);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",8);
	return $font;
}

function getSuperTinyWhiteText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,5);
	pdf_setcolor($pdf,'both','rgb',1,1,1,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTinyRedText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,6);
	pdf_setcolor($pdf,'both','rgb',0.718,0.0,0.02,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTinyGreenText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,6);
	pdf_setcolor($pdf,'both','rgb',0.608,0.667,0.098,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getSmallText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,8);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getXSmallText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,8);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",10.0);
	return $font;
}

function getSkinnyText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,24);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getRedItalic(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
	pdf_setfont($pdf,$font,9);
	pdf_setcolor($pdf,'both','rgb',1,0,0,0);
	pdf_set_value($pdf,"leading",12.0);
	return $font;
}

function getWhiteBold(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,9);
	pdf_setcolor($pdf,'both','rgb',1,1,1,0);
	pdf_set_value($pdf,"leading",12.0);
	return $font;
}

function getSmallBlackItalic(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
	pdf_setfont($pdf,$font,6);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",8.0);
	return $font;
}

function getPage19Text(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,9);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",12.0);
	return $font;
}

function writeReportFooter(&$pdf,$page,$lid,$pos=false){
	if(false!=$pos){
		// .75 inches from bottom
		$y=72*0.75;
		$x=0;
		if("right"==$pos){
			// 1 1/4 inch from right edge
			$x=PAGE_WIDTH-(72*1.25);
		}
		elseif("left"==$pos){
			// 1 inch from left edge
			$x=72;
		}
		
		$flid=getFLID("meta","md0");
		$mltxt=getMLText($flid,"1",$lid);
		
		$size=6.5;
		pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
		$font=pdf_findfont($pdf,"Helvetica","host",0);
		pdf_setfont($pdf,$font,$size);
		$what=$mltxt[3];
		pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,$size),$y);
		
		$size=10.0;
		$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
		pdf_setfont($pdf,$font,$size);
		pdf_show_xy($pdf,$page,$x,$y);
	}
	pdf_end_page($pdf);
}	

function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");
	
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}
	writeReportFooter($pdf,$name,"","1");
}

// This just inserts some blank pages
function renderBlankPage($cid,&$pdf,$page,$pid,$lid){
	writeHeader($cid,$pdf,$lid,false);
	writeReportFooter($pdf,$page,$lid,false);
}

// This is the cover page "i"
function renderPageI($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,false);
	
	$flid=getFLID("meta","md0");
	$mltxt=getMLText($flid,"1",$lid);

	$x=(72*4.875);
	$y=PAGE_HEIGHT-(72*0.875);	
	$font=getSkinnyText($pdf);
	pdf_show_xy($pdf,"MERCER DELTA",$x,$y);
	
	$x=(72*6.125);
	$y=PAGE_HEIGHT-(72*1.125);	
	$font=getBody($pdf);
	pdf_show_xy($pdf,"Organizational Consulting",$x,$y);

	$x=(72*5.0);
	$y=PAGE_HEIGHT-(72*2.6125);	
	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.15);
	
	// draw "the band"
	pdf_setcolor($pdf,"both","rgb",(129/255),(171/255),(186/255),0);
	$x=(72*0.25);
	$sz=(72*0.28);
	$sp=(72*0.125);
	$y=PAGE_HEIGHT-(72*3.95);
	for($i=0;$i<20;$i++){
		pdf_rect($pdf,$x+($i*($sz+$sp)),$y,$sz,$sz);
		pdf_rect($pdf,$x+($i*($sz+$sp)),$y-($sz+$sp),$sz,$sz);
		pdf_rect($pdf,$x+($i*($sz+$sp)),$y-(2*($sz+$sp)),$sz,$sz);
		if($i==5||$i==6||$i==7){
			pdf_fill_stroke($pdf);
		}
		else{
			pdf_stroke($pdf);
		}
	}
	
	$x=(72*2.25);
	$y=PAGE_HEIGHT-(72*4.95);
	$font=getBody($pdf);
	pdf_show_xy($pdf,"EXECUTIVE LEARNING CENTER",$x,$y);
	
	$y=PAGE_HEIGHT-(72*5.60);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[13],$x,$y);

	$y=PAGE_HEIGHT-(72*5.90);
	$font=getH1($pdf);
	pdf_show_xy($pdf,strtoupper($name),$x,$y);

	$y=PAGE_HEIGHT-(72*6.20);
	$font=getBody($pdf);
	$rptdt=date('M d, Y');
	pdf_show_xy($pdf,$rptdt,$x,$y);
	
	$y=PAGE_HEIGHT-(72*7.0);
	$font=getH4($pdf);
	pdf_show_xy($pdf,"Feedback Report",$x,$y);

	$y=PAGE_HEIGHT-(72*7.20);
	$font=getBody($pdf);
	pdf_show_xy($pdf,"Sal Capobianco, Ph. D | Mark Davis, Ph. D. | Linda Kraus, Ph. D.",$x,$y);

	$font=getSmallText($pdf);
	$y=PAGE_HEIGHT-(72*9.0);
	pdf_show_xy($pdf,"1631 NW Thurman Street, Suite 100",$x,$y);
	$y=PAGE_HEIGHT-(72*9.25);
	pdf_show_xy($pdf,"Portland, OR 97209 USA",$x,$y);
	$y=PAGE_HEIGHT-(72*9.5);
	pdf_show_xy($pdf,"+ 1 866.237.4685; + 503.223.5678",$x,$y);
	$y=PAGE_HEIGHT-(72*9.75);
	pdf_show_xy($pdf,"HTTP://elc.oliverwyman.com",$x,$y);

	$y=PAGE_HEIGHT-(72*10.5);
	
	$img=pdf_open_image_file($pdf,"JPEG","../images/mmclogo.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.3);

	// Here we should figure out how many "Others"
	writeReportFooter($pdf,$page,$lid,false);
}

function renderPageII($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,false);
	
	$x=(72*1.75)/2;
	$y=PAGE_HEIGHT-(72*10.5);	
	$font=getBody($pdf);
	pdf_show_xy($pdf,"This version of the Conflict Dynamics Profile report was designed by the Mercer Delta Executive Learning Center", $x, $y); 
	pdf_continue_text($pdf,"and is used by permisssion from Eckerd College."); 
	writeReportFooter($pdf,$page,$lid,false);
}


// This is the content page "iii"
function renderPageIII($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");
	
	$flid=getFLID("meta","md0");
	$mltxt=getMLText($flid,"1",$lid);

	$x=(72*1.75)/2;
	$y=PAGE_HEIGHT-(72*2.75);	
	$font=getH1($pdf);

	pdf_show_xy($pdf,strtoupper($mltxt[21]),$x,$y);
	
	$y=PAGE_HEIGHT-(72*3.5);	
	$font=getSubtext($pdf);
	
	pdf_show_xy($pdf,$mltxt[22],$x,$y);
	for($i=23;$i<=32;$i++){
		pdf_continue_text($pdf,"");
		pdf_continue_text($pdf,$mltxt[$i]);
	}		
	
	// determine the composition of the rater population
	$raters=getNumRaters($cid);
	if($raters){
		// get rater interaction by rater category
		$interaction=getRaterInteraction($cid);
		$dummy=array();
		$boss=array(0,0,0,0,0);
		$peer=array(0,0,0,0,0);
		$dr=array(0,0,0,0,0);
		$howWell=array($dummy,$dummy,$boss,$peer,$dr);
		foreach($interaction as $act){
			$cat=$act[0];
			$val=$act[1];
			$cnt=$act[2];
			$howWell[$cat][$val]=$cnt;
		}
		
		// Build the appropriate text
		// the counts and interaction levels
		$descr=array("","Self","Boss","Peers","Direct Reports");
		$cont=array("","Hardly","Somewhat","Well","Extremely Well");
		$number=array(0,0,0,0,0);
		$interaction=array("","","","","");
		foreach($raters as $rater){
			$number[$rater[0]]=$rater[1];
			// Yikes, all this to display the interaction level!
			$str="";
			$catData=$howWell[$rater[0]];
			for($i=1;$i<5;$i++){
				if(0!=$catData[$i]){
					if(0!=strlen($str)){
						$str=$str."; ";	
					}
					$str=$str.$cont[$i]."(".$catData[$i].")";
				}
			}
			$interaction[$rater[0]]=$str;
			// Wow!
		}

		// Display the results
		$y=150;
		$font=getSubText($pdf);
		pdf_show_xy($pdf,"Rater",$x,$y);
		pdf_show_xy($pdf,"Number",$x+100,$y);
		pdf_show_xy($pdf,"How Well Does Rater Know the Individual?",$x+175,$y);
		
		// Underline
		pdf_moveto($pdf,$x,$y-3);
		pdf_lineto($pdf,$x+450,$y-3);
		pdf_fill_stroke($pdf);
		
		$font=getBody($pdf);
		$oCnt=0;
		for($i=2;$i<5;$i++){
			$y-=20;
			pdf_show_xy($pdf,$descr[$i],$x,$y);
			pdf_show_xy($pdf,$number[$i],$x+115,$y);
			pdf_show_xy($pdf,$interaction[$i],$x+175,$y);
			// determine whether to show data for Other than Self
			if(2==$i){
				// Any Boss...
				if($number[$i]>0){
					$noOther=false;
				}
			}
			elseif(3==$i||4==$i){
				// ...or any combination of at least 3 in any other category
				$oCnt+=$number[$i];
				if($oCnt>=3){
					$noOther=false;
				}
			}
			
		}
		
	}
	
	writeReportFooter($pdf,$page,$lid,"right");
}

// This is the introduction page, right jsutified
function renderPage1($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","md3");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	// Introduction
	$title=$mltxt[1];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);

	$y=PAGE_HEIGHT-(72*1.75);
	
	pdf_show_xy($pdf,$mltxt[11],$x,$y);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	if(isset($mltxt[18])){
		pdf_continue_text($pdf,$mltxt[18]);
	}
	
	pdf_continue_text($pdf,$mltxt[21]);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,$mltxt[26]);
	pdf_continue_text($pdf,$mltxt[27]);
	pdf_continue_text($pdf,$mltxt[28]);
	pdf_continue_text($pdf,"   ");
	
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf,$mltxt[32]);
	pdf_continue_text($pdf,$mltxt[33]);
	pdf_continue_text($pdf,$mltxt[34]);
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf,$mltxt[37]);
	pdf_continue_text($pdf,$mltxt[38]);
	pdf_continue_text($pdf,$mltxt[39]);
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,$mltxt[41]);
	pdf_continue_text($pdf,$mltxt[42]);
	pdf_continue_text($pdf,$mltxt[43]);
	pdf_continue_text($pdf,$mltxt[44]);
	pdf_continue_text($pdf,$mltxt[45]);
	pdf_continue_text($pdf,$mltxt[46]);
	pdf_continue_text($pdf,$mltxt[47]);
	pdf_continue_text($pdf,$mltxt[48]);
	//pdf_continue_text($pdf,$mltxt[49]);

	writeReportFooter($pdf,$page,$lid,"right");
}

// Second page of report - "Responses to conflict"
function renderPage2($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","md3");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	// Introduction
	$title=$mltxt[49];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*1.5);
	
	pdf_show_xy($pdf,$mltxt[50],$x,$y);
	pdf_continue_text($pdf,$mltxt[51]);
	pdf_continue_text($pdf,$mltxt[52]);
	pdf_continue_text($pdf,$mltxt[53]);
	pdf_continue_text($pdf,$mltxt[53]);
	pdf_continue_text($pdf,$mltxt[55]);
	pdf_continue_text($pdf,$mltxt[56]);
	pdf_continue_text($pdf,$mltxt[57]);
	pdf_continue_text($pdf,"   ");

	$y=PAGE_HEIGHT-(72*3.5);
	$font=getH2($pdf);
	$title=$mltxt[60];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*3.75);
	
	pdf_show_xy($pdf,$mltxt[61],$x,$y);
	pdf_continue_text($pdf,$mltxt[62]);
	pdf_continue_text($pdf,$mltxt[63]);
	pdf_continue_text($pdf,$mltxt[64]);
	pdf_continue_text($pdf,$mltxt[65]);
	pdf_continue_text($pdf,$mltxt[66]);
	pdf_continue_text($pdf,"   ");


	$y=PAGE_HEIGHT-(72*5.25);
	$font=getH2($pdf);
	$title=$mltxt[70];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*5.5);
	
	pdf_show_xy($pdf,$mltxt[71],$x,$y);
	pdf_continue_text($pdf,$mltxt[72]);
	pdf_continue_text($pdf,$mltxt[73]);
	pdf_continue_text($pdf,$mltxt[74]);
	pdf_continue_text($pdf,$mltxt[75]);
	pdf_continue_text($pdf,$mltxt[76]);
	pdf_continue_text($pdf,"   ");

	$y=PAGE_HEIGHT-(72*7.125);
	$font=getH2($pdf);
	$title=$mltxt[80];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*7.5);
	
	pdf_show_xy($pdf,$mltxt[81],$x,$y);
	pdf_continue_text($pdf,$mltxt[82]);
	pdf_continue_text($pdf,$mltxt[83]);
	pdf_continue_text($pdf,$mltxt[84]);
	pdf_continue_text($pdf,$mltxt[85]);
	pdf_continue_text($pdf,$mltxt[86]);
	pdf_continue_text($pdf,"   ");

	writeReportFooter($pdf,$page,$lid,"left");
}

// Thirdd page of report - "Organizational Perspective on conflict"
function renderPage3($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","md3");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	$title=$mltxt[90];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*1.5);
	
	pdf_show_xy($pdf,$mltxt[91],$x,$y);
	pdf_continue_text($pdf,$mltxt[92]);
	pdf_continue_text($pdf,$mltxt[93]);
	pdf_continue_text($pdf,$mltxt[94]);
	pdf_continue_text($pdf,$mltxt[95]);
	pdf_continue_text($pdf,"   ");


	$y=PAGE_HEIGHT-(72*3.0);
	$font=getH2($pdf);
	$title=$mltxt[100];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*3.25);
	
	pdf_show_xy($pdf,$mltxt[101],$x,$y);
	pdf_continue_text($pdf,$mltxt[102]);
	pdf_continue_text($pdf,$mltxt[103]);
	pdf_continue_text($pdf,$mltxt[104]);
	pdf_continue_text($pdf,"   ");

	$y=PAGE_HEIGHT-(72*4.375);
	$font=getH2($pdf);
	$title=$mltxt[110];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*4.75);
	
	pdf_show_xy($pdf,$mltxt[111],$x,$y);
	pdf_continue_text($pdf,$mltxt[112]);
	pdf_continue_text($pdf,$mltxt[113]);
	pdf_continue_text($pdf,$mltxt[114]);
	pdf_continue_text($pdf,$mltxt[115]);
	pdf_continue_text($pdf,"   ");

	$y=PAGE_HEIGHT-(72*6);
	$font=getH2($pdf);
	$title=$mltxt[120];
	
	pdf_show_xy($pdf,$title,$x,$y);
	
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*6.375);
	
	pdf_show_xy($pdf,$mltxt[121],$x,$y);
	pdf_continue_text($pdf,$mltxt[122]);
	pdf_continue_text($pdf,$mltxt[123]);
	pdf_continue_text($pdf,$mltxt[124]);
	pdf_continue_text($pdf,$mltxt[125]);
	pdf_continue_text($pdf,$mltxt[126]);
	pdf_continue_text($pdf,$mltxt[127]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,$mltxt[130]);
	pdf_continue_text($pdf,$mltxt[131]);
	pdf_continue_text($pdf,$mltxt[132]);
	pdf_continue_text($pdf,$mltxt[133]);
	pdf_continue_text($pdf,$mltxt[134]);
	pdf_continue_text($pdf,$mltxt[135]);
	pdf_continue_text($pdf,$mltxt[136]);
	pdf_continue_text($pdf,$mltxt[137]);

	writeReportFooter($pdf,$page,$lid,"right");
}

// Fourth page of report - "Final Note - continued"
function renderPage4($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","md3");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getBody($pdf);
	
	pdf_show_xy($pdf,$mltxt[140],$x,$y);
	pdf_continue_text($pdf,$mltxt[141]);
	pdf_continue_text($pdf,$mltxt[142]);
	pdf_continue_text($pdf,$mltxt[143]);
	pdf_continue_text($pdf,$mltxt[144]);
	pdf_continue_text($pdf,$mltxt[145]);
	pdf_continue_text($pdf,$mltxt[146]);
	pdf_continue_text($pdf,$mltxt[147]);
	pdf_continue_text($pdf,$mltxt[148]);
	pdf_continue_text($pdf,$mltxt[149]);
	pdf_continue_text($pdf,$mltxt[150]);
	pdf_continue_text($pdf,$mltxt[151]);
	pdf_continue_text($pdf,$mltxt[152]);
	pdf_continue_text($pdf,$mltxt[153]);
	pdf_continue_text($pdf,$mltxt[154]);
	pdf_continue_text($pdf,$mltxt[155]);
	pdf_continue_text($pdf,$mltxt[156]);
	pdf_continue_text($pdf,$mltxt[157]);
	pdf_continue_text($pdf,$mltxt[158]);
	pdf_continue_text($pdf,$mltxt[159]);


	writeReportFooter($pdf,$page,$lid,"left");
}

// Fifth page of report - "Final Note - continued"
function renderPage5($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","md5");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	$title=$mltxt[1];
	
	pdf_show_xy($pdf,$title,$x,$y);
	$font=getSubText($pdf);
	$y=PAGE_HEIGHT-(72*1.75);
	pdf_show_xy($pdf,$mltxt[52],$x,$y);
	
	for($i=53;$i<=93;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
	}

	$font=getBody($pdf);
	$x=$x+150;
	pdf_show_xy($pdf,$mltxt[22],$x,$y);
	
	for($i=3;$i<=43;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
	}

	writeReportFooter($pdf,$page,$lid,"right");
}

// Constructive

// Sixth page of report - First constructive graphs
function renderPage6($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");

	// define RGB values for some colors
	// light red
	$r1=239/255;
	$g1=193/255;
	$b1=175/255;

	// light orange
	$r2=254/255;
	$g2=232/255;
	$b2=174/255;

	// light green
	$r3=235/255;
	$g3=241/255;
	$b3=195/255;
	
	// light blue
	$r4=228/255;
	$g4=236/255;
	$b4=237/255;

	// gray
	$r5=230/255;
	$g5=230/255;
	$b5=230/255;

	// Graph width should be 6.25 inches
	$width=72*6.25;
	// Each box should be a multiple of .25 inches
	$ystep=72*0.25;
	
	// This is the left margin
	$x=(72*1.75/2);

	// Get the text for the page
	$flid=getFLID("meta","md4");
	$mltxt=getMLText($flid,"1",$lid);

	$y=PAGE_HEIGHT-(72*1.25);
	$font=getH3($pdf);
	pdf_show_xy($pdf,$mltxt[11],$x,$y);
	
	// Graphics + text for "Constructive"
	//========================================================================
	$y=PAGE_HEIGHT-(72*5);
	$height=14*$ystep;

	// Gray rectangle on top
	pdf_setcolor($pdf,"both","rgb",$r5,$g5,$b5,0);
	pdf_rect($pdf,$x,$y+(13*$ystep),$width,$ystep);
	pdf_fill_stroke($pdf);
	
	// green recatngleon top
	$xstep=$width*(2/6.25);
	pdf_setcolor($pdf,"both","rgb",$r3,$g3,$b3,0);
	pdf_rect($pdf,$x,$y+(5*$ystep),$xstep,8*$ystep);
	pdf_fill_stroke($pdf);
	
	// blue rectangle below
	$xstep=$width*(2/6.25);
	pdf_setcolor($pdf,"both","rgb",$r4,$g4,$b4,0);
	pdf_rect($pdf,$x,$y,$xstep,5*$ystep);
	pdf_fill_stroke($pdf);

	// Horizontal and vertical lines
	pdf_setcolor($pdf,"stroke","rgb",0,0,0,0);
	pdf_setlinewidth($pdf,0.25);
	$draw=array(false,true,false,true,false,true,false,true,false,true,false,true,false,true);
	for($i=0;$i<=13;$i++){
		if($draw[$i]){
			pdf_moveto($pdf,$x,$y+($i*$ystep));
			pdf_lineto($pdf,$x+$width,$y+($i*$ystep));
		}
	}
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x+$xstep,$y+$height);
	pdf_stroke($pdf);

	// Outer rectangle
	pdf_setlinewidth($pdf,0.5);
	pdf_rect($pdf,$x,$y,$width,$height);
	pdf_stroke($pdf);
	
	// Headers
	$padding=5;
	$font=getTableHead($pdf);
	pdf_show_boxed($pdf,$mltxt[53],$x+$padding,$y+(13*$ystep),$xstep,$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[54],$x+($width/2),$y+(13*$ystep),$xstep,$ystep,"left","");
	
	$font=getTableSide($pdf);
	$data=array($mltxt[43],"",$mltxt[42],"",$mltxt[41],"",$mltxt[40],"",$mltxt[39],"",$mltxt[38],"",$mltxt[37]);
	$offs=0.25;
	for($i=0;$i<=12;$i++){
		pdf_show_xy($pdf,$data[$i],$x+$padding,$y+(($i+$offs)*$ystep));
		$offs=-0.25;
	}
	
	$font=getTableText($pdf);
	$data=array($mltxt[93],$mltxt[92],"",$mltxt[91],"",$mltxt[90],"",$mltxt[89],"",$mltxt[88],"",$mltxt[87]);
	$h=$ystep;
	for($i=0;$i<=11;$i++){
		pdf_show_boxed($pdf,$data[$i],$x+$xstep+$padding,$y+($i*$ystep),$width-$xstep-(2*$padding),$h,"left","");
		$h=2*$ystep;
	}
	
	// Sub title
	$y=PAGE_HEIGHT-(72*5.75);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[33],$x,$y);
	pdf_continue_text($pdf,$mltxt[34]);

	// The graph
	$y=PAGE_HEIGHT-(72*10);
	$img=pdf_open_image_file($pdf,"JPEG","../images/01_Constructive_Responses.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.275);

	graphPage6($cid,$pdf,$x,$y);
	
	writeReportFooter($pdf,$page,$lid,"left");
}

function graphPage6($cid,&$pdf,$x,$y){
	$data=getConstructiveResponses($cid);
	// Number of scales
	$scales=count($data)/2;
	$font=getTinyText($pdf);
	$ytop=$y+260;
	$xtop=$x+110;
	$ystep=15;
	// Numbers
	for($i=0;$i<$scales;$i++){
		pdf_show_xy($pdf,round($data[$i][0]),$xtop,$ytop);
		$ytop-=$ystep;
		pdf_show_xy($pdf,round($data[$i+$scales][0]),$xtop,$ytop);
		$ytop-=$ystep;
	}
	
	// Scales
	// Self:
	$ytop=$y+260;
	$xtop=$x+132;
	$ystep=30.25;
	$heigth=$ystep/5;
	
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	for($i=0;$i<$scales;$i++){
		$val=round($data[$i][0]);
		$val=$val-35;
		if($val>=0){
			if($val>30){
				// Don't run over edge
				$val=31;
			}
			$width=11.75*$val;
			pdf_rect($pdf,$xtop,$ytop,$width,$heigth);
		}
		else{
			// backward graph
			pdf_rect($pdf,$xtop-10,$ytop,9,$heigth);
		}
		$ytop-=$ystep;
	}
	pdf_fill_stroke($pdf);
	
	// Others:
	$ytop=$y+245;
	$xtop=$x+132;
	$ystep=30.25;
	$heigth=$ystep/5;
	
	pdf_setcolor($pdf,"both","rgb",1,1,1,0);
	for($i=0;$i<$scales;$i++){
		$val=round($data[$i+$scales][0]);
		$val=$val-35;
		if($val>=0){
			if($val>30){
				// Don't run over edge
				$val=31;
			}
			$width=11.75*$val;
			pdf_rect($pdf,$xtop,$ytop,$width,$heigth);
		}
		else{
			// backward graph
			pdf_rect($pdf,$xtop-10,$ytop,9,$heigth);
		}
		$ytop-=$ystep;
	}
	pdf_fill_stroke($pdf);

}


function renderPage7($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");

	// Graph width should be 6.25 inches
	$width=72*6.25;
	// Each box should be a multiple of .25 inches
	$ystep=72*0.25;
	
	// This is the left margin
	$x=(72*1.75/2);

	// Get the text for the page
	$flid=getFLID("meta","md4");
	$mltxt=getMLText($flid,"1",$lid);

	$y=PAGE_HEIGHT-(72*1.25);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[110],$x,$y);
	
	// The graph
	
	$y=PAGE_HEIGHT-(72*5.5);
	$img=pdf_open_image_file($pdf,"JPEG","../images/02_Constructive_Responses_Rater.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.275);

	graphPage7($cid,$pdf,$x,$y);

	writeReportFooter($pdf,$page,$lid,"right");
}


function graphPage7($cid,&$pdf,$x,$y){
	// Get self and otehrs data just liek on page 6
	$data=getConstructiveResponses($cid);

	// Number of scales
	$scales=count($data)/2;
	$font=getTinyText($pdf);
	$ytop=$y+270;
	$xtop=$x+110;
	$ystep=15;
	// Numbers
	for($i=0;$i<$scales;$i++){
		pdf_show_xy($pdf,round($data[$i][0]),$xtop,$ytop);
		$ytop-=$ystep;
		pdf_show_xy($pdf,round($data[$i+$scales][0]),$xtop,$ytop);
		$ytop-=$ystep;
	}
	
	// Scales
	// Self:
	$ytop=$y+270;
	$xtop=$x+132;
	$ystep=30.25;
	$heigth=$ystep/5;
	
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	for($i=0;$i<$scales;$i++){
		$val=round($data[$i][0]);
		$val=$val-35;
		if($val>=0){
			if($val>30){
				// Don't run over edge
				$val=31;
			}
			$width=11.75*$val;
			pdf_rect($pdf,$xtop,$ytop,$width,$heigth);
		}
		else{
			// backward graph
			pdf_rect($pdf,$xtop-10,$ytop,9,$heigth);
		}
		$ytop-=$ystep;
	}
	pdf_fill_stroke($pdf);
	
	// Others:
	$ytop=$y+255;
	$xtop=$x+132;
	$ystep=30.25;
	$heigth=$ystep/5;

	pdf_setcolor($pdf,"both","rgb",1,1,1,0);
	for($i=0;$i<$scales;$i++){
		$val=round($data[$i+$scales][0]);
		$val=$val-35;
		if($val>=0){
			if($val>30){
				// Don't run over edge
				$val=31;
			}
			$width=11.75*$val;
			pdf_rect($pdf,$xtop,$ytop,$width,$heigth);
		}
		else{
			// backward graph
			pdf_rect($pdf,$xtop-10,$ytop,9,$heigth);
		}
		// save current y position
		$ypos[$i]=$ytop;
		$ytop-=$ystep;
	}
	pdf_fill_stroke($pdf);

	// Now it's time to fill out the "bubbles"
	// Get both active and passive constructive data
	$act=getActiveConstructiveResponses($cid);
	$pass=getPassiveConstructiveResponses($cid);
	// merge the data set into one
	$data=array_merge($act,$pass);
	
	// These arrays contain average values and agreement by rater
	$avg=array();
	$agr=array();
	for($i=0;$i<$scales;$i++){
		$avg[$i]=array(0,0,0,0,0);
		$agr[$i]=array(0,0,0,0,0);
	}
	
	$sid=-1;
	$j=-1;
	
	// fill the array with values
	for($i=0;$i<count($data);$i++){
		if($data[$i][6]!=$sid){
			$j++;
//			$ytop-=$ystep;
			$sid=$data[$i][6];
		}
		$cat=$data[$i][3];
		$avg[$j][$cat]=round($data[$i][0]);
		$agr[$j][$cat]=round($data[$i][1]);
	}

	// print out the values in a suitable bubble
	$ytop=$y+255;
	for($i=0;$i<$scales;$i++){
		showBubbles($pdf,$avg[$i],$agr[$i],$xtop,$ytop);
		$ytop-=$ystep;
	}
}

// Destructive

// Eigth page of report - First destructive graphs
function renderPage8($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");

	// define RGB values for some colors
	// light red
	$r1=239/255;
	$g1=193/255;
	$b1=175/255;

	// light orange
	$r2=254/255;
	$g2=232/255;
	$b2=174/255;

	// light green
	$r3=235/255;
	$g3=241/255;
	$b3=195/255;
	
	// light blue
	$r4=228/255;
	$g4=236/255;
	$b4=237/255;

	// gray
	$r5=230/255;
	$g5=230/255;
	$b5=230/255;

	// Graph width should be 6.25 inches
	$width=72*6.25;
	// Each box should be a multiple of .25 inches
	$ystep=72*0.25;
	
	// This is the left margin
	$x=(72*1.75/2);

	// Get the text for the page
	$flid=getFLID("meta","md4");
	$mltxt=getMLText($flid,"1",$lid);

	$y=PAGE_HEIGHT-(72*1);
	$font=getH3($pdf);
	pdf_show_xy($pdf,$mltxt[21],$x,$y);
	
	// Graphics + text for "Destructive"
	//========================================================================
	$y=PAGE_HEIGHT-(72*5.25);
	$height=15*$ystep;

	// Gray rectangle on top
	pdf_setcolor($pdf,"both","rgb",$r5,$g5,$b5,0);
	pdf_rect($pdf,$x,$y+(15*$ystep),$width,$ystep);
	pdf_fill_stroke($pdf);
	
	// red recatngle on top
	$xstep=$width*(2/6.25);
	pdf_setcolor($pdf,"both","rgb",$r1,$g1,$b1,0);
	pdf_rect($pdf,$x,$y+(7*$ystep),$xstep,8*$ystep);
	pdf_fill_stroke($pdf);
	
	// orange rectangle below
	$xstep=$width*(2/6.25);
	pdf_setcolor($pdf,"both","rgb",$r2,$g2,$b2,0);
	pdf_rect($pdf,$x,$y+$ystep,$xstep,6*$ystep);
	pdf_fill_stroke($pdf);

	// Horizontal and vertical lines
	pdf_setcolor($pdf,"stroke","rgb",0,0,0,0);
	pdf_setlinewidth($pdf,0.25);
	$draw=array(false,false,true,true,true,false,true,false,true,false,true,false,true,false,true,false,true);
	for($i=0;$i<=15;$i++){
		if($draw[$i]){
			pdf_moveto($pdf,$x,$y+$ystep+($i*$ystep));
			pdf_lineto($pdf,$x+$width,$y+$ystep+($i*$ystep));
		}
	}
	pdf_moveto($pdf,$x+$xstep,$y+$ystep);
	pdf_lineto($pdf,$x+$xstep,$y+$ystep+$height);
	pdf_stroke($pdf);

	// Outer rectangle
	pdf_setlinewidth($pdf,0.5);
	pdf_rect($pdf,$x,$y+$ystep,$width,$height);
	pdf_stroke($pdf);
	
	// Headers
	$padding=5;
	$font=getTableHead($pdf);
	pdf_show_boxed($pdf,$mltxt[55],$x+$padding,$y+(15*$ystep),$xstep,$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[56],$x+($width/2),$y+(15*$ystep),$xstep,$ystep,"left","");
	
	$font=getTableSide($pdf);
	pdf_show_xy($pdf,$mltxt[51],$x+$padding,$y+1.75*$ystep);
	pdf_show_xy($pdf,$mltxt[50],$x+$padding,$y+3.25*$ystep);
	pdf_show_xy($pdf,$mltxt[49],$x+$padding,$y+4.25*$ystep);
	pdf_show_xy($pdf,$mltxt[48],$x+$padding,$y+5.75*$ystep);
	pdf_show_xy($pdf,$mltxt[47],$x+$padding,$y+7.75*$ystep);
	pdf_show_xy($pdf,$mltxt[46],$x+$padding,$y+9.75*$ystep);
	pdf_show_xy($pdf,$mltxt[45],$x+$padding,$y+11.75*$ystep);
	pdf_show_xy($pdf,$mltxt[44],$x+$padding,$y+13.75*$ystep);

	$font=getTableText($pdf);
	pdf_show_boxed($pdf,$mltxt[101],$x+$xstep+$padding,$y+$ystep,$width-$xstep-(3*$padding),2*$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[100],$x+$xstep+$padding,$y+3*$ystep,$width-$xstep-(3*$padding),$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[99],$x+$xstep+$padding,$y+4*$ystep,$width-$xstep-(3*$padding),$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[98],$x+$xstep+$padding,$y+5*$ystep,$width-$xstep-(3*$padding),2*$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[97],$x+$xstep+$padding,$y+7*$ystep,$width-$xstep-(3*$padding),2*$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[96],$x+$xstep+$padding,$y+9*$ystep,$width-$xstep-(3*$padding),2*$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[95],$x+$xstep+$padding,$y+11*$ystep,$width-$xstep-(3*$padding),2*$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[94],$x+$xstep+$padding,$y+13*$ystep,$width-$xstep-(3*$padding),2*$ystep,"left","");
	
	// Sub title
	$y=PAGE_HEIGHT-(72*5.5);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[35],$x,$y);
	pdf_continue_text($pdf,$mltxt[36]);

	// The graph
	
	$y=PAGE_HEIGHT-(72*10);
	$img=pdf_open_image_file($pdf,"JPEG","../images/03_Destructive_Response.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.275);

	graphPage8($cid,$pdf,$x,$y);

	writeReportFooter($pdf,$page,$lid,"left");
	
}

function graphPage8($cid,&$pdf,$x,$y){
	$data=getDestructiveResponses($cid);
	// Number of scales
	$scales=count($data)/2;
	$font=getTinyText($pdf);
	$ytop=$y+290;
	$xtop=$x+110;
	$ystep=15;
	// Numbers
	for($i=0;$i<$scales;$i++){
		pdf_show_xy($pdf,round($data[$i][0]),$xtop,$ytop);
		$ytop-=$ystep;
		pdf_show_xy($pdf,round($data[$i+$scales][0]),$xtop,$ytop);
		$ytop-=$ystep;
	}
	
	// Scales
	// Self:
	$ytop=$y+290;
	$xtop=$x+132;
	$ystep=30.25;
	$heigth=$ystep/5;
	
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	for($i=0;$i<$scales;$i++){
		$val=round($data[$i][0]);
		$val=$val-35;
		if($val>=0){
			if($val>30){
				// Don't run over edge
				$val=31;
			}
			$width=11.75*$val;
			pdf_rect($pdf,$xtop,$ytop,$width,$heigth);
		}
		else{
			// backward graph
			pdf_rect($pdf,$xtop-10,$ytop,9,$heigth);
		}
		$ytop-=$ystep;
	}
	pdf_fill_stroke($pdf);
	
	// Others:
	$ytop=$y+275;
	$xtop=$x+132;
	$ystep=30.25;
	$heigth=$ystep/5;
	
	pdf_setcolor($pdf,"both","rgb",1,1,1,0);
	for($i=0;$i<$scales;$i++){
		$val=round($data[$i+$scales][0]);
		$val=$val-35;
		if($val>=0){
			if($val>30){
				// Don't run over edge
				$val=31;
			}
			$width=11.75*$val;
			pdf_rect($pdf,$xtop,$ytop,$width,$heigth);
		}
		else{
			// backward graph
			pdf_rect($pdf,$xtop-10,$ytop,9,$heigth);
		}
		$ytop-=$ystep;
	}
	pdf_fill_stroke($pdf);

}

function renderPage9($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");

	// Graph width should be 6.25 inches
	$width=72*6.25;
	// Each box should be a multiple of .25 inches
	$ystep=72*0.25;
	
	// This is the left margin
	$x=(72*1.75/2);

	// Get the text for the page
	$flid=getFLID("meta","md4");
	$mltxt=getMLText($flid,"1",$lid);

	$y=PAGE_HEIGHT-(72*1.25);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[111],$x,$y);
	
	// The graph
	
	$y=PAGE_HEIGHT-(72*5.75);
	$img=pdf_open_image_file($pdf,"JPEG","../images/04_Destructive_Response_Rater.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.275);

	graphPage9($cid,$pdf,$x,$y);

	writeReportFooter($pdf,$page,$lid,"right");
}

function graphPage9($cid,&$pdf,$x,$y){
	// Get self and otehrs data just liek on page 8
	$data=getDestructiveResponses($cid);

	// Number of scales
	$scales=count($data)/2;
	$font=getTinyText($pdf);
	$ytop=$y+300;
	$xtop=$x+110;
	$ystep=15;
	// Numbers
	for($i=0;$i<$scales;$i++){
		pdf_show_xy($pdf,round($data[$i][0]),$xtop,$ytop);
		$ytop-=$ystep;
		pdf_show_xy($pdf,round($data[$i+$scales][0]),$xtop,$ytop);
		$ytop-=$ystep;
	}
	
	// Scales
	// Self:
	$ytop=$y+300;
	$xtop=$x+132;
	$ystep=30.25;
	$heigth=$ystep/5;
	
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	for($i=0;$i<$scales;$i++){
		$val=round($data[$i][0]);
		$val=$val-35;
		if($val>=0){
			if($val>30){
				// Don't run over edge
				$val=31;
			}
			$width=11.75*$val;
			pdf_rect($pdf,$xtop,$ytop,$width,$heigth);
		}
		else{
			// backward graph
			pdf_rect($pdf,$xtop-10,$ytop,9,$heigth);
		}
		$ytop-=$ystep;
	}
	pdf_fill_stroke($pdf);
	
	// Others:
	$ytop=$y+285;
	$xtop=$x+132;
	$ystep=30.25;
	$heigth=$ystep/5;

	pdf_setcolor($pdf,"both","rgb",1,1,1,0);
	for($i=0;$i<$scales;$i++){
		$val=round($data[$i+$scales][0]);
		$val=$val-35;
		if($val>=0){
			if($val>30){
				// Don't run over edge
				$val=31;
			}
			$width=11.75*$val;
			pdf_rect($pdf,$xtop,$ytop,$width,$heigth);
		}
		else{
			// backward graph
			pdf_rect($pdf,$xtop-10,$ytop,9,$heigth);
		}
		// save current y position
		$ypos[$i]=$ytop;
		$ytop-=$ystep;
	}
	pdf_fill_stroke($pdf);

	// Now it's time to fill out the "bubbles"
	// Get both active and passive constructive data
	$act=getActiveDestructiveResponses($cid);
	$pass=getPassiveDestructiveResponses($cid);
	// merge the data set into one
	$data=array_merge($act,$pass);
	
	// These arrays contain average values and agreement by rater
	$avg=array();
	$agr=array();
	for($i=0;$i<$scales;$i++){
		$avg[$i]=array(0,0,0,0,0);
		$agr[$i]=array(0,0,0,0,0);
	}
	
	$sid=-1;
	$j=-1;
	
	// fill the array with values
	for($i=0;$i<count($data);$i++){
		if($data[$i][6]!=$sid){
			$j++;
			$sid=$data[$i][6];
		}
		$cat=$data[$i][3];
		$avg[$j][$cat]=round($data[$i][0]);
		$agr[$j][$cat]=round($data[$i][1]);
	}

	// print out the values in a suitable bubble
	$ytop=$y+285;
	for($i=0;$i<$scales;$i++){
		showBubbles($pdf,$avg[$i],$agr[$i],$xtop,$ytop);
		$ytop-=$ystep;
	}
}

// This function prints out agreement values in a suitably colored "bubble"
function showBubbles(&$pdf,$avg,$agr,$x,$y){
	
	$width=72/8;
	$heigth=72/8;
	
	$boffs=0;
	$poffs=0;
	$doffs=0;
	$div=3;
	
	// Figure out overlap
	if(($avg[2]==$avg[3])&&($avg[2]==$avg[4])){
		// all three are equal
		$boffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	elseif($avg[2]==$avg[3]){
		// boss and peers are equal
		$boffs=$heigth/$div;
		$poffs=-$heigth/$div;
	}
	elseif($avg[2]==$avg[4]){
		// boss and DRs are equal
		$boffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	elseif($avg[3]==$avg[4]){
		// peers and DRs are equal
		$poffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	
	// Boss
	if($avg[2]>0){
		$val=$avg[2]-35;
		if($val>30){
			$val=30.5;	
		}
		elseif($val<0){
			$val=-0.75;	
		}
		$xpos=$x+(11.75*$val);
		$ypos=$y;

		// boss colors
		$R=1;
		$G=179/255;
		$B=15/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
	
		if($agr[2]<10){
			// High rater agreement
			pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$boffs,1+$width/2);
		}
		elseif($agr[2]>20){
			// Low
			pdf_rect($pdf,$xpos-$width,$ypos-($heigth/8)+$boffs,$width*2.5,$heigth);
		}
		else{
			// Moderate
			pdf_rect($pdf,$xpos-($width/8),$ypos-($heigth/8)+$boffs,$width,$heigth);
		}
		pdf_fill_stroke($pdf);
		
		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[2],$xpos,$ypos+$boffs);
	}
	
	// Peer
	if($avg[3]>0){
		$val=$avg[3]-35;
		if($val>30){
			$val=30.5;	
		}
		elseif($val<0){
			$val=-0.75;	
		}
		$xpos=$x+(11.75*$val);
		$ypos=$y;

		// peers colors
		$R=122/255;
		$G=165/255;
		$B=180/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
		
		if($agr[3]<10){
			// High rater agreement
			pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$poffs,1+$width/2);
		}
		elseif($agr[3]>20){
			// Low
			pdf_rect($pdf,$xpos-$width,$ypos-($heigth/8)+$poffs,$width*2.5,$heigth);
		}
		else{
			// Moderate
			pdf_rect($pdf,$xpos-($width/8),$ypos-($heigth/8)+$poffs,$width,$heigth);
		}
		pdf_fill_stroke($pdf);
		

		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[3],$xpos,$ypos+$poffs);
	}
	
	// DR
	if($avg[4]>0){
		$val=$avg[4]-35;
		if($val>30){
			$val=30.5;	
		}
		elseif($val<0){
			$val=-0.75;	
		}
		$xpos=$x+(11.75*$val);
		$ypos=$y;

		// dr colors
		$R=178/255;
		$G=128/255;
		$B=166/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
		
		if($agr[4]<10){
			// High rater agreement
			pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$doffs,1+$width/2);
		}
		elseif($agr[4]>20){
			// Low
			pdf_rect($pdf,$xpos-$width,$ypos-($heigth/8)+$doffs,$width*2.5,$heigth);
		}
		else{
			// Moderate
			pdf_rect($pdf,$xpos-($width/8),$ypos-($heigth/8)+$doffs,$width,$heigth);
		}
		pdf_fill_stroke($pdf);
		

		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[4],$xpos,$ypos+$doffs);
	}
	
}

// The Top/Bottom 3 page
// This one is a bear!!!!!!!!
function renderPage10($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");
	// Get the text for the page
	$flid=getFLID("meta","mdTB3");
	$mltxt=getMLText($flid,"1",$lid);

	// Title
	$font=getH3($pdf);
	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.0);
	pdf_show_xy($pdf,$mltxt[1],$x,$y);

	// Header 1
	$font=getH4($pdf);
	$y=PAGE_HEIGHT-(72*1.5);
	pdf_show_xy($pdf,$mltxt[2],$x,$y);
	
	// The left margin is scooted in about 1/4 inch
	$x=$x+(72*0.25);

	// Okay, here we go now!
	// This is a totally ridiculous graph,almost impossible to construct
	// programmatically within size and time constraints, but even so we try!
	
	// The top three are the Highest Constructive and the Lowest Destructive
	$cData=getTopBottomConstructiveResponses($cid,"desc");
	$dData=getTopBottomDestructiveResponses($cid,"asc");
	$data=array();
	
	// Get the three highest Constructive
	// and the three lowest destructive
	for($i=0;$i<3;$i++){
		$data[$i]=$cData[$i];
		$data[$i+3]=$dData[$i];
	}

	// Are there any among the bottom 3 Destructive that stick out more than the ones we have now?
	// bubble sort the data
	$done=true;
	do{
		$done=true;
		for($i=0;$i<5;+$i++){
			if($data[$i][5]<$data[$i+1][5]){
				$done=false;
				$tmp=$data[$i];
				$data[$i]=$data[$i+1];
				$data[$i+1]=$tmp;
			}
		}
	}while(!$done);

	
	// This is just for debugging
//	pdf_continue_text($pdf,$cData[0][0]."-".$cData[0][5]." , ".$cData[1][0]."-".$cData[1][5]." , ".$cData[2][0]."-".$cData[2][5]);
//	pdf_continue_text($pdf,$dData[0][0]."-".$dData[0][5]." , ".$dData[1][0]."-".$dData[1][5]." , ".$dData[2][0]."-".$dData[2][5]);
//	pdf_continue_text($pdf,$data[0][1]."-".$data[0][5]." , ".$data[1][1]."-".$data[1][5]." , ".$data[2][1]."-".$data[2][5]);
	
	$blurb=buildTopSentence($data,$mltxt);
	$y=PAGE_HEIGHT-(72*2.25);
	$font=getBody($pdf);
	pdf_show_boxed($pdf,$blurb,$x,$y,400,50,"left","");

	// Draw first graph
	$y=PAGE_HEIGHT-(72*4.25);
	drawTopGraph($pdf,$data,$x,$y);	
	
	// Header 2
	$font=getH4($pdf);
	$y=PAGE_HEIGHT-(72*5);
	$x=(72*1.75/2);
	pdf_show_xy($pdf,$mltxt[8],$x,$y);

	// The top three are the Highest Constructive and the Lowest Destructive
	$x=$x+(72*0.25);

	// The bottom three are the Lowest Constructive and the Highest Destructive
	$cData=getTopBottomConstructiveResponses($cid,"asc");
	$dData=getTopBottomDestructiveResponses($cid,"desc");
	$data=array();
	
	// Get the three highest Constructive
	for($i=0;$i<3;$i++){
		$data[$i]=$cData[$i];
		$data[$i+3]=$dData[$i];
	}
	
	// Are there any among the bottom 3 Destructive that stick out more than the ones we have now?
	// bubble sort the data
	$done=true;
	do{
		$done=true;
		for($i=0;$i<5;+$i++){
			if($data[$i][5]<$data[$i+1][5]){
				$done=false;
				$tmp=$data[$i];
				$data[$i]=$data[$i+1];
				$data[$i+1]=$tmp;
			}
		}
	}while(!$done);

	// Only for debugging
//	pdf_continue_text($pdf,$cData[0][0]."-".$cData[0][5]." , ".$cData[1][0]."-".$cData[1][5]." , ".$cData[2][0]."-".$cData[2][5]);
//	pdf_continue_text($pdf,$dData[0][0]."-".$dData[0][5]." , ".$dData[1][0]."-".$dData[1][5]." , ".$dData[2][0]."-".$dData[2][5]);
//	pdf_continue_text($pdf,$data[0][1]."-".$data[0][5]." , ".$data[1][1]."-".$data[1][5]." , ".$data[2][1]."-".$data[2][5]);

	$blurb=buildBottomSentence($data,$mltxt);
	$y=PAGE_HEIGHT-(72*5.75);
	$font=getBody($pdf);
	pdf_show_boxed($pdf,$blurb,$x,$y,400,50,"left","");
	
	// Draw second graph
	$y=PAGE_HEIGHT-(72*7.75);
	drawTopGraph($pdf,$data,$x,$y);	
	
	writeReportFooter($pdf,$page,$lid,"left");
}

// This function return the constructive responses either in ascending or decsending order
// But only for Others
function getTopBottomConstructiveResponses($cid,$dir){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.CID, a.SID, round(ABS(a.AVGSCORE-50)) from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID<8 and a.CATID=6 order by a.CATID asc, a.AVGSCORE $dir";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This function return the desstructive responses either in ascending or decsending order
// But only for Others
function getTopBottomDestructiveResponses($cid,$dir){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.CID, a.SID, round(ABS(a.AVGSCORE-50)) as DIFF from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID<16 and b.SID>7 and a.CATID=6 order by a.CATID asc, a.AVGSCORE $dir";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This function constructs a sentence to suit Top/Bottom profile
function buildTopSentence($data,$mltxt){
	$respType=array(getResponseType($data[0][4]),getResponseType($data[1][4]),getResponseType($data[2][4]));
	$hasConstr=false;
	$hasDestr=false;
	// So, what repsonse types do we have here?
	for($i=0;$i<3;$i++){
		if($respType[$i]==AD||$respType[$i]==PD){
			$hasConstr=true;
		}
		else{
			$hasDestr=true;
		}
	}
	
	$rc="An error occurred";
	// How do we begin the sentence?
	if($hasDestr&&$hasConstr){
		// has both
		$rc=$mltxt[3];
		// start with the destructive
		$mul=false;
		for($i=0;$i<3;$i++){
			if($respType[$i]==AD||$respType[$i]==PD){
				if($mul){
					$rc=$rc.$mltxt[12];	
				}
				$rc=$rc.$data[$i][1];
				$mul=true;
			}
		}
		// move on to the constructive
		$rc=$rc.$mltxt[6];
		$mul=false;
		for($i=0;$i<3;$i++){
			if($respType[$i]==AC||$respType[$i]==PC){
				if($mul){
					$rc=$rc.$mltxt[13];	
				}
				$rc=$rc.$data[$i][1];
				$mul=true;
			}
		}
		$rc=$rc.$mltxt[11];
	}
	elseif($hasDestr){
		// only destructive
		$rc=$mltxt[3];
		$rc=$rc.$data[0][1].", ".$data[1][1].", ".$mltxt[12].$data[2][1].$mltxt[5];
	}
	elseif($hasConstr){
		// only constructive
		$rc=$mltxt[4];
		$rc=$rc.$data[0][1].", ".$data[1][1].", ".$mltxt[13].$data[2][1].$mltxt[5];
	}
	return $rc;
}

function buildBottomSentence($data,$mltxt){
	$respType=array(getResponseType($data[0][4]),getResponseType($data[1][4]),getResponseType($data[2][4]));
	$hasConstr=false;
	$hasDestr=false;
	// So, what repsonse types do we have here?
	for($i=0;$i<3;$i++){
		if($respType[$i]==AD||$respType[$i]==PD){
			$hasConstr=true;
		}
		else{
			$hasDestr=true;
		}
	}
	
	$rc="An error occurred";
	// How do we begin the sentence?
	if($hasDestr&&$hasConstr){
		// has both
		$rc=$mltxt[9];
		// start with the constructive
		$mul=false;
		for($i=0;$i<3;$i++){
			if($respType[$i]==AC||$respType[$i]==PC){
				if($mul){
					$rc=$rc.$mltxt[12];	
				}
				$rc=$rc.$data[$i][1];
				$mul=true;
			}
		}
		// move on to the destructive
		$rc=$rc.$mltxt[10];
		$mul=false;
		for($i=0;$i<3;$i++){
			if($respType[$i]==AD||$respType[$i]==PD){
				if($mul){
					$rc=$rc.$mltxt[13];	
				}
				$rc=$rc.$data[$i][1];
				$mul=true;
			}
		}
		$rc=$rc.$mltxt[11];
	}
	elseif($hasDestr){
		// only destructive
		$rc=$mltxt[14];
		$rc=$rc.$data[0][1].", ".$data[1][1].", ".$mltxt[12].$data[2][1].$mltxt[5];
	}
	elseif($hasConstr){
		// only constructive
		$rc=$mltxt[9];
		$rc=$rc.$data[0][1].", ".$data[1][1].", ".$mltxt[13].$data[2][1].$mltxt[11];
	}
	return $rc;
}

// what response type is a specific scale?
function getResponseType($scale){
	if($scale<=4){
		return AC;
	}
	elseif($scale<=7){
		return PC;
	}
	elseif($scale<=11){
		return AD;
	}
	return PD;	
}

// Pick which graphs we should use
function pickGraph($pdf,$data,$x,$y){
	$tp="";
	$sz=0.2;
	$respType=array(getResponseType($data[0][4]),getResponseType($data[1][4]),getResponseType($data[2][4]));
	// Note that the scale factor $sz varies by image, due to 
	// poor image consistency
	// This is the top image
	switch($respType[0]){
	case AC:
		$tp="AC";
		$sz=0.2;
	break;
	case PC:
		$tp="PC";
		$sz=0.2;
	break;
	case AD:
		$tp="AD";
		$sz=0.2425;
	break;
	case PD:
		$tp="PD";
		$sz=0.2425;
	break;
	}

	$img=pdf_open_image_file($pdf,"JPEG","../images/topbottom/Top$tp.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y+110,$sz);
	
	// This is the Middle image
	switch($respType[1]){
	case AC:
		$tp="AC";
		$sz=0.2425;
	break;
	case PC:
		$tp="PC";
		$sz=0.2425;
	break;
	case AD:
		$tp="AD";
		$sz=0.2;
	break;
	case PD:
		$tp="PD";
		$sz=0.2;
	break;
	}

	$img=pdf_open_image_file($pdf,"JPEG","../images/topbottom/Mid$tp.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y+83,$sz);
	
	// This is the bottom image
	switch($respType[2]){
	case AC:
		$tp="AC";
		$sz=0.2428;
	break;
	case PC:
		$tp="PC";
		$sz=0.2428;
	break;
	case AD:
		$tp="AD";
		$sz=0.2;
	break;
	case PD:
		$tp="PD";
		$sz=0.2;
	break;
	}

	$img=pdf_open_image_file($pdf,"JPEG","../images/topbottom/Bottom$tp.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,$sz);
	
	$font=getXSmallText($pdf);
	pdf_show_boxed($pdf,strtoupper($data[0][1]),$x+5,$y+110,60,25,"center","");
	pdf_show_boxed($pdf,strtoupper($data[1][1]),$x+5,$y+83,60,25,"center","");
	pdf_show_boxed($pdf,strtoupper($data[2][1]),$x+5,$y+55,60,25,"center","");
	
}

// get self and Others data for a single SID
function getSelfAndOthersResponses($cid,$sid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.CID, a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID=$sid and a.CATID in (1,6) order by a.CATID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// get Agreement for Boss, Peers and Direct Reports for a single SID 
function getAgreementBySID($cid,$sid){
	$conn=dbConnect();
	$query="select a.AVGSCORE,a.AGREEMENTSCORE,b.DESCR,a.CATID,a.VALIDRATERS,a.CID,a.SID from REPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID=$sid and a.CATID not in(1,6) order by a.CATID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// Plot the data points and bars for the top bottom
function drawTopBottomBars($pdf,$data,$x,$y){
	// Number of scales
	$scales=count($data);
	$font=getTinyText($pdf);
	$ytop=$y+128;
	$xtop=$x+98;
	$ystep=13.5;
	// Numbers
	for($i=0;$i<$scales;$i+=2){
		pdf_show_xy($pdf,round($data[$i][0]),$xtop,$ytop);
		$ytop-=$ystep;
		pdf_show_xy($pdf,round($data[$i+1][0]),$xtop,$ytop);
		$ytop-=$ystep;
	}
	
	// draw the self and others bars
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	$xtop=$x+118;
	$ytop=$y+127;
	$ystep=13.5;
	
	for($i=0;$i<$scales;$i+=2){
		$val=round($data[$i][0]);
		$val-=35;
		if($val<=0){
			pdf_rect($pdf,$xtop-10,$ytop,8,$ystep/2);
		}
		elseif($val>30){
			pdf_rect($pdf,$xtop,$ytop,320,$ystep/2);
		}
		else{
			pdf_rect($pdf,$xtop,$ytop,$val*10.35,$ystep/2);
		}			
		$ytop-=(2*$ystep);
	}
	pdf_fill_stroke($pdf);

	pdf_setcolor($pdf,"both","rgb",1,1,1,0);
	$xtop=$x+118;
	$ytop=$y+114;
	$ystep=13.5;
	
	for($i=1;$i<$scales;$i+=2){
		$val=round($data[$i][0]);
		$val-=35;
		if($val<=0){
			pdf_rect($pdf,$xtop-10,$ytop,8,$ystep/2);
		}
		elseif($val>30){
			pdf_rect($pdf,$xtop,$ytop,320,$ystep/2);
		}
		else{
			pdf_rect($pdf,$xtop,$ytop,$val*10.35,$ystep/2);
		}			
		$ytop-=(2*$ystep);
	}
	pdf_fill_stroke($pdf);
}


// Draw a graph for the Top bottom reponses
function drawTopGraph($pdf,$data,$x,$y){
	// First pick the graph we need for each position
	pickGraph($pdf,$data,$x,$y);
	
	// get the Self/Others data for this
	$graphData=array();
	for($i=0;$i<3;$i++){
		$arr=getSelfAndOthersResponses($data[$i][3],$data[$i][4]);
		$graphData=array_merge($graphData,$arr);
	}
	drawTopBottomBars($pdf,$graphData,$x,$y);

	// It's bubble time!
	$graphData=array();
	for($i=0;$i<3;$i++){
		$arr=getAgreementBySID($data[$i][3],$data[$i][4]);
		$graphData=array_merge($graphData,$arr);
	}
	drawTopBottomBubbles($pdf,$graphData,$x,$y);
}

function drawBottomGraph($pdf,$data,$x,$y){
	// First pick the graph we need for each position
	pickGraph($pdf,$data,$x,$y);
	
	// get the Self/Others data for this
	$graphData=array();
	for($i=0;$i<3;$i++){
		$arr=getSelfAndOthersResponses($data[$i][3],$data[$i][4]);
		$graphData=array_merge($graphData,$arr);
	}
	drawTopBottomBars($pdf,$graphData,$x,$y);
	
	// It's bubble time!
	$graphData=array();
	for($i=0;$i<3;$i++){
		$arr=getAgreementBySID($data[$i][3],$data[$i][4]);
		$graphData=array_merge($graphData,$arr);
	}
	drawTopBottomBubbles($pdf,$graphData,$x,$y);
}

// Draws the agreement bubbles for top/bottom graphs
function drawTopBottomBubbles($pdf,$data,$x,$y){
	// structure the data
	// into agreement and average score
	$scales=3;
	$avg=array();
	$agr=array();
	for($i=0;$i<$scales;$i++){
		$avg[$i]=array(0,0,0,0,0);
		$agr[$i]=array(0,0,0,0,0);
	}
	
	$sid=-1;
	$j=-1;
	// fill the array with values
	for($i=0;$i<count($data);$i++){
		if($data[$i][6]!=$sid){
			$j++;
			$sid=$data[$i][6];
		}
		$cat=$data[$i][3];
		$avg[$j][$cat]=round($data[$i][0]);
		$agr[$j][$cat]=round($data[$i][1]);
	}

	// We now have two arrays, each containing Boss, Peer and DR data for
	// each of the three scales
	// Iterate and render the data
	$ytop=$y+115;
	$xtop=$x+115;
	$ystep=27;
	for($i=0;$i<$scales;$i++){
		showTopBottomBubbles($pdf,$avg[$i],$agr[$i],$xtop,$ytop);
		$ytop-=$ystep;
	}
}

// A tweaked bubble drawing function for top/bottom graph
function showTopBottomBubbles(&$pdf,$avg,$agr,$x,$y){
	
	$width=72/8;
	$heigth=72/8;
	
	$boffs=0;
	$poffs=0;
	$doffs=0;
	$div=3;
	
	// Figure out overlap
	if(($avg[2]==$avg[3])&&($avg[2]==$avg[4])){
		// all three are equal
		$boffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	elseif($avg[2]==$avg[3]){
		// boss and peers are equal
		$boffs=$heigth/$div;
		$poffs=-$heigth/$div;
	}
	elseif($avg[2]==$avg[4]){
		// boss and DRs are equal
		$boffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	elseif($avg[3]==$avg[4]){
		// peers and DRs are equal
		$poffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	
	// Boss
	if($avg[2]>0){
		$val=$avg[2]-35;
		if($val>30){
			$val=30.75;	
		}
		elseif($val<0){
			$val=-0.5;	
		}
		$xpos=$x+(10.3*$val);
		$ypos=$y;

		// boss colors
		$R=1;
		$G=179/255;
		$B=15/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
	
		if($agr[2]<10){
			// High rater agreement
			pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$boffs,1+$width/2);
		}
		elseif($agr[2]>20){
			// Low
			pdf_rect($pdf,$xpos-$width,$ypos-($heigth/8)+$boffs,$width*2.5,$heigth);
		}
		else{
			// Moderate
			pdf_rect($pdf,$xpos-($width/8),$ypos-($heigth/8)+$boffs,$width,$heigth);
		}
		pdf_fill_stroke($pdf);
		
		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[2],$xpos,$ypos+$boffs);
	}
	
	// Peer
	if($avg[3]>0){
		$val=$avg[3]-35;
		if($val>30){
			$val=30.5;	
		}
		elseif($val<0){
			$val=-0.75;	
		}
		$xpos=$x+(11.75*$val);
		$ypos=$y;

		// peers colors
		$R=122/255;
		$G=165/255;
		$B=180/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
		
		if($agr[3]<10){
			// High rater agreement
			pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$poffs,1+$width/2);
		}
		elseif($agr[3]>20){
			// Low
			pdf_rect($pdf,$xpos-$width,$ypos-($heigth/8)+$poffs,$width*2.5,$heigth);
		}
		else{
			// Moderate
			pdf_rect($pdf,$xpos-($width/8),$ypos-($heigth/8)+$poffs,$width,$heigth);
		}
		pdf_fill_stroke($pdf);
		

		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[3],$xpos,$ypos+$poffs);
	}
	
	// DR
	if($avg[4]>0){
		$val=$avg[4]-35;
		if($val>30){
			$val=30.5;	
		}
		elseif($val<0){
			$val=-0.75;	
		}
		$xpos=$x+(11.75*$val);
		$ypos=$y;

		// dr colors
		$R=178/255;
		$G=128/255;
		$B=166/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
		
		if($agr[4]<10){
			// High rater agreement
			pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$doffs,1+$width/2);
		}
		elseif($agr[4]>20){
			// Low
			pdf_rect($pdf,$xpos-$width,$ypos-($heigth/8)+$doffs,$width*2.5,$heigth);
		}
		else{
			// Moderate
			pdf_rect($pdf,$xpos-($width/8),$ypos-($heigth/8)+$doffs,$width,$heigth);
		}
		pdf_fill_stroke($pdf);
		

		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[4],$xpos,$ypos+$doffs);
	}
	
}

// Page 11 - Scale Profile
function renderPage11($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","mdSC");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH3($pdf);
	pdf_show_xy($pdf,$mltxt[0],$x,$y);

	$y=PAGE_HEIGHT-(72*1.5);
	$font=getBody($pdf);
	pdf_show_xy($pdf,$mltxt[1],$x,$y);
	pdf_continue_text($pdf,$mltxt[2]);
	pdf_continue_text($pdf,$mltxt[3]);
	pdf_continue_text($pdf,$mltxt[4]);
	pdf_continue_text($pdf,$mltxt[5]);
	pdf_continue_text($pdf,$mltxt[6]);
	pdf_continue_text($pdf,$mltxt[7]);
	pdf_continue_text($pdf,$mltxt[8]);
	pdf_continue_text($pdf,$mltxt[9]);
	pdf_continue_text($pdf,$mltxt[10]);
	
	if(false==$noOther){
		$rows=getScaleScore($cid,"2");
		if($rows){

			$y=PAGE_HEIGHT-(72*3.75);
			$font=getH3($pdf);

			pdf_show_xy($pdf,$mltxt[11],$x,$y);
			pdf_continue_text($pdf," ");
			$font=getSubText($pdf);
			pdf_continue_text($pdf,$mltxt[12]);
			$font=getBody($pdf);
			$fix04052005=false;
			foreach($rows as $row){
				if(strlen($row[0])>0){
					pdf_continue_text($pdf,"> ".$row[0].".");
					$fix04052005=true;
				}
			}
			if($fix04052005==false){
				pdf_continue_text($pdf,"> No data available");
			}		
			pdf_continue_text($pdf,"   ");
		}
		else{
			$y=PAGE_HEIGHT-(72*3.75);
			$font=getH3($pdf);

			pdf_show_xy($pdf,$mltxt[11],$x,$y);
			pdf_continue_text($pdf," ");
			$font=getSubText($pdf);
			pdf_continue_text($pdf,$mltxt[12]);
			$font=getBody($pdf);
			pdf_continue_text($pdf,"> No data available");
			pdf_continue_text($pdf,"   ");
		}
		
	
		$doCommon=true;
		$rows3=getScaleScore($cid,"3");
		$rows4=getScaleScore($cid,"4");
		if($rows3 && $rows4){
			if(round($rows3[0][2])>=3 &&round($rows4[0][2])>=3){
				$doCommon=false;
				$font=getH3($pdf);
				pdf_continue_text($pdf," ");
				pdf_continue_text($pdf,$mltxt[13]);
				pdf_continue_text($pdf," ");
				$font=getSubText($pdf);
				pdf_continue_text($pdf,$mltxt[14]);
				$font=getBody($pdf);
				foreach($rows3 as $row){
					pdf_continue_text($pdf,"> ".$row[0].".");
				}
	
				pdf_continue_text($pdf,"   ");
				pdf_continue_text($pdf," ");
				$font=getH3($pdf);
				pdf_continue_text($pdf,$mltxt[15]);
				pdf_continue_text($pdf," ");
				$fonr=getSubText($pdf);
				pdf_continue_text($pdf,$mltxt[16]);
				$font=getBody($pdf);
				foreach($rows4 as $row){
					pdf_continue_text($pdf,"> ".$row[0].".");
				}
			}
		}

		if($doCommon){
			// Can we display combinedd Peers/Dr?
			$rows=getScaleScore($cid,"5");
			if($rows){
				$realRatCount=getRatersByCat($cid);
				if(round($rows[0][2])>=3){
					if($realRatCount[3]<1){
						// No Peers
						$font=getH3($pdf);
						pdf_continue_text($pdf," ");
						pdf_continue_text($pdf,"Peer Feedback");
						pdf_continue_text($pdf," ");
						$font=getSubText($pdf);
						pdf_continue_text($pdf,"With regard to conflict, your peers see you as someone who:");
						$font=getBody($pdf);
						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf,"> No Data Available");
			
						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf," ");
						$font=getH3($pdf);
						pdf_continue_text($pdf,"Direct Report Feedback");
						pdf_continue_text($pdf," ");
						$font=getSubText($pdf);
						pdf_continue_text($pdf,"With regard to conflict, your direct reports see you as someone who:");
						$font=getBody($pdf);
						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf,"   ");
						foreach($rows as $row){
							pdf_continue_text($pdf,"> ".$row[0].".");
						}
					}
					elseif($realRatCount[4]<1){
						// No Direct Reports
						$font=getH3($pdf);
						pdf_continue_text($pdf," ");
						pdf_continue_text($pdf,"Peer Feedback");
						pdf_continue_text($pdf," ");
						$font=getSubText($pdf);
						pdf_continue_text($pdf,"With regard to conflict, your peers see you as someone who:");
						$font=getBody($pdf);
						pdf_continue_text($pdf,"   ");
						foreach($rows as $row){
							pdf_continue_text($pdf,"> ".$row[0].".");
						}
			
						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf," ");
						$font=getH3($pdf);
						pdf_continue_text($pdf,"Direct Report Feedback");
						pdf_continue_text($pdf," ");
						$font=getSubText($pdf);
						pdf_continue_text($pdf,"With regard to conflict, your direct reports see you as someone who:");
						$font=getBody($pdf);
						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf,"   ");
						pdf_continue_text($pdf,"> No Data Available");
					}
					else{
						// We can still display them together
						$font=getH3($pdf);
						pdf_continue_text($pdf," ");
						pdf_continue_text($pdf,"P/R Feedback");
						pdf_continue_text($pdf," ");
						$font=getSubText($pdf);
						pdf_continue_text($pdf,"With regard to conflict, your peers and direct reports see you as someone who:");
						$font=getBody($pdf);
						pdf_continue_text($pdf,"   ");
						foreach($rows as $row){
							pdf_continue_text($pdf,"> ".$row[0].".");
						}
					}
				}
			}
			else{
					// Couldn't draw peers, dr or p/r by any means
					$font=getH3($pdf);
					pdf_continue_text($pdf," ");
					pdf_continue_text($pdf,"P/R Feedback");
					pdf_continue_text($pdf," ");
					$font=getSubText($pdf);
					pdf_continue_text($pdf,"With regard to conflict, your peers and direct reports see you as someone who:");
					$font=getBody($pdf);
					pdf_continue_text($pdf,"   ");
					pdf_continue_text($pdf,"> No data available");
			}
		}
	}
	else{
		$font=getBody($pdf);
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf,"No data available");
	}

	writeReportFooter($pdf,$page,$lid,"right");
}

// Page 12 - Discrepancy Profile
// Note: we don't have individual questions rated as positive or negative
// only scales are categorized as 'active"/"passive", "constructve"/"destructive"
// Thus no "visual clue" lines, instead we use black for all
// indeed there's no concept of "positive"/"negative" in the instrument
// Also, there is no bubble color defined for "combined peers/dr".
// Thus we have to skip those sections where we combine data.
function renderPage12($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","mdDP");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	pdf_show_xy($pdf,$mltxt[1],$x,$y);

	// Static text
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*1.75);
	pdf_show_xy($pdf,$mltxt[2],$x,$y);
	for($i=3;$i<14;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
	}
	
	if(false==$noOther){
		// Scale/Place the image
		$y=PAGE_HEIGHT-(72*9.75);
		$sz=0.24;
		$img=pdf_open_image_file($pdf,"JPEG","../images/07_Discrepancy_Profile.jpg","",0);
		pdf_place_image($pdf,$img,$x,$y,$sz);
	
		// Blot out the silly positive/negative stuff
		pdf_setcolor($pdf,"both","rgb",1,1,1,0);
		pdf_rect($pdf,$x+100,$y+5,100,25);
		pdf_fill_stroke($pdf);
		
		$rows=getRatersByCat($cid);
		if(false!=$rows[2]&&$rows[2]>0){
			$y=PAGE_HEIGHT-(72*5.5);
			drawDiscrepancyGraph($pdf,$x,$y,$cid,2);
		}
		else{
		}
	
		if(false!=$rows[3]&&$rows[3]>=3){
			$y=PAGE_HEIGHT-(72*7.125);
			// we have more than 3 peers
			drawDiscrepancyGraph($pdf,$x,$y,$cid,3);
		}
		else{
		}

		if(false!=$rows[4]&&$rows[4]>=3){
			$y=PAGE_HEIGHT-(72*8.8);
			// we have more than 3 dr
			drawDiscrepancyGraph($pdf,$x,$y,$cid,4);
		}
		else{
		}

	}
	else{
		$font=getBody($pdf);
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf,"No data available");
	}
	
	writeReportFooter($pdf,$page,$lid,"left");
}

function drawDiscrepancyGraph(&$pdf,$x,$y,$cid,$catid){
	$ytop=$y;
	$xtop=$x;
	$width=225;
	
	// determine what we should draw
	switch($catid){
	case 2:
		$ch="B";
		$R=1;
		$G=179/255;
		$B=15/255;
		$ytop=$y+57;
		$ystep=17;
	break;
	case 3:
		$ch="P";
		$R=122/255;
		$G=165/255;
		$B=180/255;
		$ytop=$y+57;
		$ystep=17;
	break;
	case 4:
		$ch="D";
		$R=178/255;
		$G=128/255;
		$B=166/255;
		$ytop=$y+57;
		$ystep=17;
	break;
	default:
		$ytop=$y;
	}
	
	//  Draw dynamic data	
	// select b.DESCR, a.CATID,a.ITEMID,a.AVGSCORE from DISCREPANCYPROFILE a, DISCREPANCYITEM b where a.SID=b.SID and a.ITEMID=b.ITEMID and a.CID=$cid and a.CATID=$catid order by CATID
	$rows=getDiscrepancyProfile($cid,$catid);
	foreach($rows as $row){
		//select b.CATID, AVG(a.VAL),COUNT(a.RID) from RATERRESP a, RATER b where a.RID=b.RID and a.ITEMID=$itemid and b.CID=$cid and b.CATID in (1,$catid) and a.VAL is not NULL group by b.CATID order by b.CATID asc
		$details=getDiscrepancyProfileGraph($cid,$catid,$row[2]);
		if($details){
			$printIt=true;
			$num=$details[1][2];
			// Always print for Boss
			if(2!=$catid){
				if($num<3){
					$printIt=false;	
				}
			}

			// Only print for peers and DRs if we have enough
			if($printIt){
				$font=getTinyText($pdf);
				pdf_show_boxed($pdf,$row[0],$x,$ytop,$width,$ystep,"right","");
				// show if they're not the same, i.e. there's actually a discrepancy
				$self=round($details[0][1]);
				$rat=round($details[1][1]);
				if($self!=$rat){
					// Draw line between the two boxes
					pdf_setcolor($pdf,"both","rgb",0,0,0,0);
					pdf_moveto($pdf,$x+237+(($self-1)*42),$ytop+3);
					pdf_lineto($pdf,$x+237+(($rat-1)*42),$ytop+3);
					// draw the self-box
					pdf_circle($pdf,$x+237+(($self-1)*42),$ytop+3,4);
					pdf_fill_stroke($pdf);
					$font=getTinyWhiteText($pdf);
					pdf_show_xy($pdf,"S",$x+236+(($self-1)*42),$ytop+1);
					
					// draw the rater box
					pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
					pdf_circle($pdf,$x+237+(($rat-1)*42),$ytop+3,4);
					pdf_fill_stroke($pdf);
					$font=getTinyText($pdf);
					pdf_show_xy($pdf,$ch,$x+236+(($rat-1)*42),$ytop+1);
				}
				else{
				}
			}
			else{
				$font=getTinyText($pdf);
				pdf_show_xy($pdf,"Insufficient number of raters",$x+250,$ytop+1);
			}

			$ytop-=$ystep;
		}
		
	}
}

// Page 13, static text for the Dynamic Conflict Sequence
function renderPage13($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","mdDCS");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	pdf_show_xy($pdf,$mltxt[1],$x,$y);

		// Static text
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*1.75);
	pdf_show_xy($pdf,$mltxt[2],$x,$y);
	for($i=3;$i<=27;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
	}
	
	writeReportFooter($pdf,$page,$lid,"right");
}


// Page 14, graphs for the Dynamic Conflict Sequence
function renderPage14($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1);
	
	$flid=getFLID("meta","mdDCS");
	$mltxt=getMLText($flid,"1",$lid);

	// First heading
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[30],$x,$y);

	// First graph
	// Draw the first bar graph
	$data=array();
	$graphIt=false;
	
	if(false==$noOther){
		// Get Self Data
		$rows=getDynamicSequence($cid,"1");
		//get Others Data
		$rows1=getDynamicSequence($cid,"6");
		if(false!=$rows&&false!=$rows1){
			$graphIt=true;
			$y=PAGE_HEIGHT-(72*5.25);
			$sz=0.25;
			$img=pdf_open_image_file($pdf,"JPEG","../images/08_Con_vs_Dest_Over_Time.jpg","",0);
			pdf_place_image($pdf,$img,$x,$y,$sz);
			// know where to put each value, since they're not graphed in order
			// Start with Self
			$idx=array();
			$idx[16]=0;
			$idx[17]=3;
			$idx[18]=1;
			$idx[19]=4;
			$idx[20]=2;
			$idx[21]=5;
			foreach($rows as $row){
				$sid=$row[1];
				$i=$idx[$sid];
				$data[$i]=$row[0];
			}
			// Then go to Others
			$idx[16]=6;
			$idx[17]=7;
			$idx[18]=8;
			$idx[19]=9;
			$idx[20]=10;
			$idx[21]=11;
			foreach($rows1 as $row){
				$sid=$row[1];
				$i=$idx[$sid];
				$data[$i]=$row[0];
			}
			$offs=260;
			graphDynamicSequence($pdf,$x,$y,$data,$offs);
		}
		else{
			pdf_continue_text($pdf," ");
			pdf_continue_text($pdf,"Can't graph conflict squence");
		}
	}
	else{
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf,"No data available");
	}	

	
	// second heading
	$y=PAGE_HEIGHT-(72*5.5);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[31],$x,$y);

	// Second graph
	// First we should check and see if we can get to the bubbles
	$showBubbles=false;

	//AVGSCORE,SID,CATID,VALIDRATERS
	$havePeers=false;
	$haveDRs=false;
//	$doCombo=false;
	$haveBoss=false;
	// Get the data here
	$rows=getDynamicSequence($cid);
	if($rows){
		// See what we really have
		foreach($rows as $row){
			$catid=$row[2];
			$cnt=$row[3];
			if(3==$catid&&$cnt>=3){
				$havePeers=true;
			}
			elseif(4==$catid&&$cnt>=3){
				$haveDRs=true;
				
			}
			// We don't do the combined graph in this report
//			elseif(5==$catid&&$cnt>=3){
//				if(!($havePeers&&$haveDRs)){
//					$doCombo=true;
//					$havePeers=false;
//					$haveDRs=false;
//				}
//			}
			elseif(2==$catid){
				$haveBoss=true;
			}
		}
		// If we have at least one of these we can show the bubbles
		$showBubbles=($haveBoss||$haveDRs||$havePeers);
	}
	
	if($graphIt&&$showBubbles){
		$y=PAGE_HEIGHT-(72*10);
		$sz=0.25;
		$img=pdf_open_image_file($pdf,"JPEG","../images/09_Con_vs_Dest_Detailed.jpg","",0);
		pdf_place_image($pdf,$img,$x,$y,$sz);
	
		// Draw the bar graphs
		$offs=263;
		graphDynamicSequence($pdf,$x,$y,$data,$offs);
		
		// Then do the Bubbles
		graphDynamicBubbles($pdf,$x,$y,$rows,$offs,$cid);
	}
	else{
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf," ");
		pdf_continue_text($pdf,"No detail data available");
	}	

	writeReportFooter($pdf,$page,$lid,"left");
}

// Graph the Dynamic Conflict Sequence
function graphDynamicSequence(&$pdf,$x,$y,$data,$offs){
	$scales=count($data)/2;
	$font=getTinyText($pdf);
	$ytop=$y+$offs;
	$xtop=$x+100;
	$ystep=13.5;
	// Numbers
	for($i=0;$i<$scales;$i++){
		if($i==($scales/2)){
			// skip to second graph	
			$ytop-=40;
		}
		pdf_show_xy($pdf,round($data[$i]),$xtop,$ytop);
		$ytop-=$ystep;
		pdf_show_xy($pdf,round($data[$i+$scales]),$xtop,$ytop);
		$ytop-=$ystep;
	}
	

	// draw the self and others bars
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	$xtop=$x+120;
	$ytop=$y+$offs;
	$ystep=14;
	

	for($i=0;$i<$scales;$i++){
		if($i==$scales/2){
			// skip to second graph	
			$ytop-=35;
		}
		$val=round($data[$i]);
		$val-=35;
		if($val<=0){
			pdf_rect($pdf,$xtop-10,$ytop,9,$ystep/2);
		}
		elseif($val>30){
			pdf_rect($pdf,$xtop,$ytop,332,$ystep/2);
		}
		else{
			pdf_rect($pdf,$xtop,$ytop,$val*10.7,$ystep/2);
		}			
		$ytop-=(2*$ystep);
	}
	pdf_fill_stroke($pdf);

	pdf_setcolor($pdf,"both","rgb",1,1,1,0);
	$xtop=$x+120;
	$ytop=$y+$offs;
	$ystep=14;
	$ytop-=$ystep; // move it down one notch
	
	for($i=0;$i<$scales;$i++){
		if($i==$scales/2){
			// skip to second graph	
			$ytop-=35;
		}
		$val=round($data[$i+$scales]);
		$val-=35;
		if($val<=0){
			pdf_rect($pdf,$xtop-10,$ytop,9,$ystep/2);
		}
		elseif($val>30){
			pdf_rect($pdf,$xtop,$ytop,332,$ystep/2);
		}
		else{
			pdf_rect($pdf,$xtop,$ytop,$val*10.7,$ystep/2);
		}			
		$ytop-=(2*$ystep);
	}
	pdf_fill_stroke($pdf);
}

// draws the bubbles for the dynamic sequence detail graph
function graphDynamicBubbles(&$pdf,$x,$y,$data,$offs,$cid){

	// How many of peers and drs do we have?
	$peerCnt=0;
	$drCnt=0;
	$bossCnt=0;
	$raters=getNumRaters($cid);
	if($raters){
		foreach($raters as $rater){
			if(2==$rater[0]){
				$bossCnt=$rater[1];
			}
			elseif(3==$rater[0]){
				$peerCnt=$rater[1];
			}
			elseif(4==$rater[0]){
				$drCnt=$rater[1];
			}
		}
	}
	else{
		return false;	
	}
	
	// Iterate over each SID, i.e. 6 of them
	$ytop=$y+$offs;
	$ystep=14;
	$scales=6;
	$ytop-=$ystep;
	for($i=0;$i<$scales;$i++){
		if($i==$scales/2){
			// skip to second graph	
			$ytop-=35;
		}
		$bubDat=array(0,0,0,0,0,0,0);
		//AVGSCORE,SID,CATID,VALIDRATERS
		$idx=$data[$i][2];
		$val=$data[$i][0];
		$bubDat[$idx]=round($val);
		$idx=$data[$i+$scales][2];
		$val=$data[$i+$scales][0];
		$bubDat[$idx]=round($val);
		$idx=$data[$i+(2*$scales)][2];
		$val=$data[$i+(2*$scales)][0];
		$bubDat[$idx]=round($val);
		$idx=$data[$i+(3*$scales)][2];
		$val=$data[$i+(3*$scales)][0];
		$bubDat[$idx]=round($val);
		$idx=$data[$i+(4*$scales)][2];
		$val=$data[$i+(4*$scales)][0];
		$bubDat[$idx]=round($val);
		$idx=$data[$i+(5*$scales)][2];
		$val=$data[$i+(5*$scales)][0];
		$bubDat[$idx]=round($val);
		// Don't show these
		if($drCnt<3){
			$bubDat[4]=0;
		}
		if($peerCnt<3){
			$bubDat[3]=0;
		}
		placeDynamicBubbles($pdf,$x+118,$ytop,$bubDat);
		$ytop-=(2*$ystep);
	}
}

// This function actually draws the bubbles for the above
function placeDynamicBubbles(&$pdf,$x,$y,$avg){
	$width=72/8;
	$heigth=72/8;
	
	$boffs=0;
	$poffs=0;
	$doffs=0;
	$div=3;
	
	// Figure out overlap for non-zero values
	if($avg[2]>0&&($avg[2]==$avg[3])&&($avg[2]==$avg[4])){
		// all three are equal
		$boffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	elseif($avg[2]>0&&$avg[2]==$avg[3]){
		// boss and peers are equal
		$boffs=$heigth/$div;
		$poffs=-$heigth/$div;
	}
	elseif($avg[2]>0&&$avg[2]==$avg[4]){
		// boss and DRs are equal
		$boffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	elseif($avg[3]>0&&$avg[3]==$avg[4]){
		// peers and DRs are equal
		$poffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	
	// Boss
	if($avg[2]>0){
		$val=$avg[2]-35;
		if($val>30){
			$val=30.5;	
		}
		elseif($val<0){
			$val=-0.65;	
		}
		$xpos=$x+(10.7*$val);
		$ypos=$y;

		// boss colors
		$R=1;
		$G=179/255;
		$B=15/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
	
		pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$boffs,1+$width/2);
		pdf_fill_stroke($pdf);
		
		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[2],$xpos,$ypos+$boffs);
	}
	
	// Peer
	if($avg[3]>0){
		$val=$avg[3]-35;
		if($val>30){
			$val=30.5;	
		}
		elseif($val<0){
			$val=-0.65;	
		}
		$xpos=$x+(10.7*$val);
		$ypos=$y;

		// peers colors
		$R=122/255;
		$G=165/255;
		$B=180/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
		
		pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$poffs,1+$width/2);
		pdf_fill_stroke($pdf);

		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[3],$xpos,$ypos+$poffs);
	}
	
	// DR
	if($avg[4]>0){
		$val=$avg[4]-35;
		if($val>30){
			$val=30.5;	
		}
		elseif($val<0){
			$val=-0.65;	
		}
		$xpos=$x+(10.7*$val);
		$ypos=$y;

		// dr colors
		$R=178/255;
		$G=128/255;
		$B=166/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
		
		pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$doffs,1+$width/2);
		pdf_fill_stroke($pdf);
		

		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[4],$xpos,$ypos+$doffs);
	}
}

// Page 15 - Organizational Perspective
function renderPage15($cid,&$pdf,$page,$pid,$lid,&$noOther,&$indices){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.5);
	
	$flid=getFLID("meta","mdOP");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	pdf_show_xy($pdf,$mltxt[1],$x,$y);

	// Static text
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*1.75);
	pdf_show_xy($pdf,$mltxt[2],$x,$y);
	for($i=3;$i<=12;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
	}
	
	// Get the data
	// And Graph it
	$rows=getOrgPersp($cid);
	if($rows){
		$y=PAGE_HEIGHT-(72*9.75);
		drawOrgGraph($pdf,$x,$y,$rows,$cid,$noOther,$indices);	
		$y=PAGE_HEIGHT-(72*10);
		
		$font=getTinyItalicText($pdf);
		pdf_show_xy($pdf,$mltxt[13],$x,$y);
		pdf_continue_text($pdf,$mltxt[14]);
	}
	else{
		$font=getH3($pdf);
		pdf_show_xy($pdf,"Cannot graph organizational perspective",100,450);	
	}
	writeReportFooter($pdf,$page,$lid,"right");
}
// Draw the Organizational perspective graph on Page 15
// a.ITEMID,a.SCORE,a.CATID 
function drawOrgGraph(&$pdf,$x,$y,$data,$cid,$noOther,&$indices){
	// The item text
	$txt=array();
	$txt[0]="Being insensitive to the other person's point of view";
	$txt[1]="Failing to work with the other person to create solutions";
	$txt[2]="Failing to communicate honestly with the other person by expressing thoughts and feelings";
	$txt[3]="Ignoring opportunities to reach out to the other person and repair things";
	$txt[4]="Reacting impulsively rather than analyzing the situation and thinking about the best response";
	$txt[5]="Responding immediately to conflict rather than letting emotions settle down";
	$txt[6]="Failing to adapt and be flexible during conflict situations";
	$txt[7]="Arguing vigorously for one's own position, trying to win at all costs";
	$txt[8]="Expressing anger, raising one's voice, using harsh, angry words";
	$txt[9]="Laughing at the other person, ridiculing the other, using sarcasm";
	$txt[10]="Obstructing or retaliating against the other, trying to get revenge later";
	$txt[11]="Avoiding or ignoring the other person, acting distant and aloof";
	$txt[12]="Giving in to the other person in order to avoid further conflict";
	$txt[13]="Concealing one's true emotions even though feeling upset";
	$txt[14]="Replaying the incident over in one's mind, criticizing oneself for not handling it better";

	// 1. How many bosses, peers and DRs do we have - all depends on that
	$bossNo=0;
	$peerNo=0;
	$drNo=0;
	$raters=getNumRaters($cid);
	if($raters){
		foreach($raters as $rater){
			if(3==$rater[0]){
				$peerNo=$rater[1];
			}
			elseif(4==$rater[0]){
				$drNo=$rater[1];
			}
			elseif(2==$rater[0]){
				$bossNo=$rater[1];
			}
		}
	}

	// 2. Examine the data and put it in appropriate places
	$selfData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$bossData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$peerData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$drData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$comboData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

	$hasPeers=false;
	$hasDRs=false;
	$doCombo=$false;	
	foreach($data as $row){
		// the rows are returned in this order: a.ITEMID,a.SCORE,a.CATID
		$item=$row[0];
		$score=$row[1];
		$cat=$row[2];
		if(1==$cat){
			$selfData[$item-100]=$score;	
		}
		elseif(2==$cat){
			$bossData[$item-64]=$score;	
		}
		elseif(3==$cat){
			$peerData[$item-64]=$score;
			$hasPeers=true;
		}
		elseif(4==$cat){
			$drData[$item-64]=$score;
			$hasDRs=true;
		}
		elseif(5==$cat){
			$comboData[$item-64]=$score;
			$doCombo=true;
			$hasPeers=false;
			$hasDRs=false;
		}
	}

	// This is so that we can do the highlights etc. the right way
	// Only use the ones that we're going to use, zero out the rest
	if($drNo<3){
		$drData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	}
	if($peerNo<3){
		$peerData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	}
	if($doCombo==false||($peerNo+$drNo<3)){
		$comboData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	}
	// Keep track of the top scores
	$totScore=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$idx1=0;
	$idx2=0;
	$idx3=0;
	for($i=0;$i<count($selfData);$i++){
		$totScore[$i]=$selfData[$i]+$bossData[$i]+$peerData[$i]+$drData[$i]+$comboData[$i];
		// Check if we should change the ranking
		if($totScore[$i]>$totScore[$idx1]){
			$idx3=$idx2;
			$idx2=$idx1;
			$idx1=$i;
		}
		elseif($totScore[$i]>$totScore[$idx2]){
			$idx3=$idx2;
			$idx2=$i;
		}
		elseif($totScore[$i]>$totScore[$idx3]){
			$idx3=$i;
		}
	}
	
	// Don't want to do thata again: We're going to reuse this later on the next page
	$indices[0]=array($idx1,$selfData[$idx1],$bossData[$idx1],$peerData[$idx1],$drData[$idx1]);
	$indices[1]=array($idx2,$selfData[$idx2],$bossData[$idx2],$peerData[$idx2],$drData[$idx2]);
	$indices[2]=array($idx3,$selfData[$idx3],$bossData[$idx3],$peerData[$idx3],$drData[$idx3]);
	
	// Now we can finally draw the grid etc.
	//Draw the outline - depends on how many rows we're to display
	// we'll always display Self and Boss even if Boss has no data at all
	$columns=2;
	if($peerNo>3&&$hasPeers){
		$columns++;
	}
	if($drNo>3&&$hasDRs){
		$columns++;
	}
	if($doCombo){
		//$columns++;
		if(($drNo+$peerNo)>3){
			$columns++;	
		}
	}
	
	$height=425;
	$width=450;
	$ystep=$height/16;
	$xstep=$width/10;
	// Draw a gray box at top
	$R=230/255;
	$G=230/255;
	$B=230/255;
	pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
	pdf_rect($pdf,$x,$y+$height-$ystep,$width,$ystep);
	pdf_fill_stroke($pdf);
	// Fill in the yellow stuff
	$R=227/255;
	$G=234/255;
	$B=23/255;
	pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
	// We have to do this since they go in the "wrong order"
	if($idx1<8){
		pdf_rect($pdf,$x,$y+$height-((9+$idx1)*$ystep),6*$xstep,$ystep);
	}
	else{
		pdf_rect($pdf,$x,$y+$height-(($idx1-7)*$ystep),6*$xstep,$ystep);
	}
	if($idx2<8){
		pdf_rect($pdf,$x,$y+$height-((9+$idx2)*$ystep),6*$xstep,$ystep);
	}
	else{
		pdf_rect($pdf,$x,$y+$height-(($idx2-7)*$ystep),6*$xstep,$ystep);
	}
	if($idx3<8){
		pdf_rect($pdf,$x,$y+$height-((9+$idx3)*$ystep),6*$xstep,$ystep);
	}
	else{
		pdf_rect($pdf,$x,$y+$height-(($idx3-7)*$ystep),6*$xstep,$ystep);
	}
	pdf_fill_stroke($pdf);
	
	// Draw the text
	$font=getSubText($pdf);
	pdf_show_boxed($pdf,"Responses to Conflict",$x,$y+$height-$ystep,6*$xstep,$ystep,"center","");
	pdf_show_boxed($pdf,"Self",$x+(6*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	if($bossNo<2){
		pdf_show_boxed($pdf,"Boss",$x+(7*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	}
	else{
		pdf_show_boxed($pdf,"Boss",$x+(7*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	}
	if($hasPeers){
		pdf_show_boxed($pdf,"Peers",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	}
	if($hasPeers){
		pdf_show_boxed($pdf,"Direct Reports",$x+(9*$xstep),$y+$height-$ystep,$xstep,$ystep*1.2,"center","");
	}
	if($doCombo){
		if(0==$peerNo||0==$drNo){
			pdf_show_boxed($pdf,"Peers",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
			pdf_show_boxed($pdf,"Direct Reports",$x+(9*$xstep),$y+$height-$ystep,$xstep,$ystep*1.2,"center","");
		}
		else{
			pdf_show_boxed($pdf,"Peers/ Reports",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep*1.2,"center","");
		}
	}

	$font=getBody2($pdf);
	for($i=0;$i<16;$i++){
		pdf_show_boxed($pdf,$txt[$i],$x+3,$y+$height-round(($i+2)*$ystep),round(6*$xstep),$ystep,"left","");	
	}

	// 5. Show data
	// The darned things are displayed out of order for no apparent reason
	// Thus we do this
	// Start with the last 7
	$font=getH3($pdf);
	for($i=8;$i<15;$i++){
		// First do self
		$val=$selfData[$i];
		$severity="";
		if($val>1.0){
			$severity="M";
		}
		if($val>2.0){
			$severity="S";	
		}
		if($severity!=""){
			// light red
			$R=239/255;
			$G=193/255;
			$B=175/255;
			if($severity=="M"){
				// light orange
				$R=254/255;
				$G=232/255;
				$B=174/255;
			}
			pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
			pdf_rect($pdf,$x+(6*$xstep),$y+$height-(($i-8+2)*$ystep),$xstep,$ystep);
			pdf_fill($pdf);
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_show_boxed($pdf,$severity,$x+round(6*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
		}
		// Boss
		$val=$bossData[$i];
		$severity="";
		if($bossNo<2){
			if($val>1.0){
				$severity="M";
			}
			if($val>2.0){
				$severity="S";	
			}
		}
		else{
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";	
			}
		}
		if($severity!=""){
			// light red
			$R=239/255;
			$G=193/255;
			$B=175/255;
			if($severity=="M"){
				// light orange
				$R=254/255;
				$G=232/255;
				$B=174/255;
			}
			pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
			pdf_rect($pdf,$x+(7*$xstep),$y+$height-(($i-8+2)*$ystep),$xstep,$ystep);
			pdf_fill($pdf);
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_show_boxed($pdf,$severity,$x+round(7*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
		}
		// Peers
		if($hasPeers){
			$val=$peerData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";	
			}
			if($severity!=""){
				// light red
				$R=239/255;
				$G=193/255;
				$B=175/255;
				if($severity=="M"){
					// light orange
					$R=254/255;
					$G=232/255;
					$B=174/255;
				}
				pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
				pdf_rect($pdf,$x+(8*$xstep),$y+$height-(($i-8+2)*$ystep),$xstep,$ystep);
				pdf_fill($pdf);
				pdf_setcolor($pdf,"both","rgb",0,0,0,0);
				pdf_show_boxed($pdf,$severity,$x+round(8*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
			}
		}
		
		// Direct Reports
		if($hasPeers){
			$val=$drData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";	
			}
			if($severity!=""){
				// light red
				$R=239/255;
				$G=193/255;
				$B=175/255;
				if($severity=="M"){
					// light orange
					$R=254/255;
					$G=232/255;
					$B=174/255;
				}
				pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
				pdf_rect($pdf,$x+(9*$xstep),$y+$height-(($i-8+2)*$ystep),$xstep,$ystep);
				pdf_fill($pdf);
				pdf_setcolor($pdf,"both","rgb",0,0,0,0);
				pdf_show_boxed($pdf,$severity,$x+round(9*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
			}
		}
		
		// Combined Peers/DRs
		if($doCombo){
			$whichCol=8;
			if($peerNo==0){
				$whichCol=9;
			}
			$val=$comboData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";	
			}
			if($severity!=""){
				// light red
				$R=239/255;
				$G=193/255;
				$B=175/255;
				if($severity=="M"){
					// light orange
					$R=254/255;
					$G=232/255;
					$B=174/255;
				}
				pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
				pdf_rect($pdf,$x+($whichCol*$xstep),$y+$height-(($i-8+2)*$ystep),$xstep,$ystep);
				pdf_fill($pdf);
				pdf_setcolor($pdf,"both","rgb",0,0,0,0);
				pdf_show_boxed($pdf,$severity,$x+round($whichCol*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
			}
		}
	}
	
	// Then do the first 8
	for($i=0;$i<8;$i++){
		// First do self
		$val=$selfData[$i];
		$severity="";
		if($val>1.0){
			$severity="M";
		}
		if($val>2.0){
			$severity="S";	
		}
		if($severity!=""){
			// light red
			$R=239/255;
			$G=193/255;
			$B=175/255;
			if($severity=="M"){
				// light orange
				$R=254/255;
				$G=232/255;
				$B=174/255;
			}
			pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
			pdf_rect($pdf,$x+(6*$xstep),$y+$height-(($i+7+2)*$ystep),$xstep,$ystep);
			pdf_fill($pdf);
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_show_boxed($pdf,$severity,$x+round(6*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
		}
		// Boss
		$val=$bossData[$i];
		$severity="";
		if($bossNo<2){
			if($val>1.0){
				$severity="M";
			}
			if($val>2.0){
				$severity="S";	
			}
		}
		else{
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";	
			}
		}
		if($severity!=""){
			// light red
			$R=239/255;
			$G=193/255;
			$B=175/255;
			if($severity=="M"){
				// light orange
				$R=254/255;
				$G=232/255;
				$B=174/255;
			}
			pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
			pdf_rect($pdf,$x+(7*$xstep),$y+$height-(($i+7+2)*$ystep),$xstep,$ystep);
			pdf_fill($pdf);
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_show_boxed($pdf,$severity,$x+round(7*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
		}
		
		// Peers
		if($hasPeers){
			$val=$peerData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";	
			}
			if($severity!=""){
				// light red
				$R=239/255;
				$G=193/255;
				$B=175/255;
				if($severity=="M"){
					// light orange
					$R=254/255;
					$G=232/255;
					$B=174/255;
				}
				pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
				pdf_rect($pdf,$x+(8*$xstep),$y+$height-(($i+7+2)*$ystep),$xstep,$ystep);
				pdf_fill($pdf);
				pdf_setcolor($pdf,"both","rgb",0,0,0,0);
				pdf_show_boxed($pdf,$severity,$x+round(8*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
			}
		}
		
		// Direct Reports
		if($hasPeers){
			$val=$drData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";	
			}
			if($severity!=""){
				// light red
				$R=239/255;
				$G=193/255;
				$B=175/255;
				if($severity=="M"){
					// light orange
					$R=254/255;
					$G=232/255;
					$B=174/255;
				}
				pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
				pdf_rect($pdf,$x+(9*$xstep),$y+$height-(($i+7+2)*$ystep),$xstep,$ystep);
				pdf_fill($pdf);
				pdf_setcolor($pdf,"both","rgb",0,0,0,0);
				pdf_show_boxed($pdf,$severity,$x+round(9*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
			}
		}
		
		// Combined Peers/DRs
		if($doCombo){
			$whichCol=8;
			if($peerNo==0){
				$whichCol=9;
			}
			$val=$comboData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>2.49){
				$severity="S";	
			}
			if($severity!=""){
				// light red
				$R=239/255;
				$G=193/255;
				$B=175/255;
				if($severity=="M"){
					// light orange
					$R=254/255;
					$G=232/255;
					$B=174/255;
				}
				pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
				pdf_rect($pdf,$x+($whichCol*$xstep),$y+$height-(($i+7+2)*$ystep),$xstep,$ystep);
				pdf_fill($pdf);
				pdf_setcolor($pdf,"both","rgb",0,0,0,0);
				pdf_show_boxed($pdf,$severity,$x+round($whichCol*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
			}
		}
	}

	// The Grid, finally
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,$x,$y,(6+$columns)*$xstep,$height);
	// 1-3 vertical lines
	for($i=0;$i<$columns;$i++){
		pdf_moveto($pdf,$x+((6+$i)*$xstep),$y+$height);
		pdf_lineto($pdf,$x+((6+$i)*$xstep),$y);
	}
	// horizontal lines
	for($i=0;$i<15;$i++){
		pdf_moveto($pdf,$x,$y+$height-$ystep-($i*$ystep));	
		pdf_lineto($pdf,$x+((6+$columns)*$xstep),$y+$height-$ystep-($i*$ystep));	
	}
	pdf_stroke($pdf);
	
}

function graphPage16(&$pdf,$x,$y,$data,$offs){

	// The item text
	// NOTE: The "Constructive" ones don't have text that fits into this context
	// It should be stuff like "Fail to Reach Out" etc
	$txt=array();
	$txt[0]="Winning at All Costs";
	$txt[1]="Displaying Anger";
	$txt[2]="Demeaning Others";
	$txt[3]="Retaliating";
	$txt[4]="Avoiding";
	$txt[5]="Yielding";
	$txt[6]="Hiding Emotions";
	$txt[7]="Self- Criticizing";


	$txt[8]="Perspective Taking";
	$txt[9]="Creating Solutions";
	$txt[10]="Expressing Emotions";
	$txt[11]="Reaching Out";
	$txt[12]="Reflective Thinking";
	$txt[13]="Delay Responding";
	$txt[14]="Adapting";


	$scales=3;
	$ytop=$y+$offs;
	$xtop=$x+15;
	$ystep=13.5;

	// Do these instead of the bogus ones
	$font=getTinyText($pdf);
	for($i=0;$i<=3;$i+=0.5){
		// Draw in the correct scales instead of the phone one
		pdf_show_xy($pdf,$i,$xtop+116+($i*105),$y+48.5);
	}
	
	$self=array(0,0,0);
	$others=array(0,0,0);
	// Scales and numbers
	for($i=0;$i<$scales;$i++){
		
		$font=getTinyText($pdf);
		$self[$i]=round($data[$i][1]*10)/10;
		pdf_show_xy($pdf,$self[$i],$xtop+93,$ytop);
		$ytop-=$ystep;
		
		// Get combined others
		$val=0;
		$div=0;
		if($data[$i][2]>0){
			$val+=$data[$i][2];
			$div++;
		}
		if($data[$i][3]>0){
			$val+=$data[$i][3];
			$div++;
		}
		if($data[$i][4]>0){
			$val+=$data[$i][4];
			$div++;
		}
		if($div>0){
			$val=$val/$div;	
			$others[$i]=round($val*10)/10;
			pdf_show_xy($pdf,$others[$i],$xtop+93,$ytop);
		}
		$font=getXSmallText($pdf);
		$idx=$data[$i][0];
		pdf_show_boxed($pdf,strtoupper($txt[$idx]),$xtop,$ytop-5,60,2*$ystep,"center","");		
		$ytop-=$ystep;
	}
	

	// draw the self and others bars
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	$xtop=$x+120;
	$ytop=$y+$offs;
	$ystep=14;

	for($i=0;$i<3;$i++){
		$val=$self[$i];
		if(0<$val){
			pdf_rect($pdf,$xtop,$ytop,8+($val*108),$ystep/2);
		}
		$ytop-=(2*$ystep);
	}
	pdf_fill_stroke($pdf);

	pdf_setcolor($pdf,"both","rgb",1,1,1,0);
	$xtop=$x+120;
	$ytop=$y+$offs;
	$ystep=14;
	$ytop-=$ystep; // move it down one notch
	
	for($i=0;$i<3;$i++){
		$val=$others[$i];
		if(0<$val){
			pdf_rect($pdf,$xtop,$ytop,8+($val*108),$ystep/2);
		}
		$ytop-=(2*$ystep);
	}
	pdf_fill_stroke($pdf);

	$xtop=$x+120;
	$ytop=$y+$offs;
	$ystep=14;
	$ytop-=$ystep; // move it down one notch
	
	for($i=0;$i<3;$i++){
		placeOrgBubbles($pdf,$xtop,$ytop,$data[$i]);
		$ytop-=(2*$ystep);
	}
}

function placeOrgBubbles(&$pdf,$x,$y,$avg){
	$width=72/8;
	$heigth=72/8;
	
	$boffs=0;
	$poffs=0;
	$doffs=0;
	$div=3;
	
	// Figure out overlap for non-zero values
	if($avg[2]>0&&($avg[2]==$avg[3])&&($avg[2]==$avg[4])){
		// all three are equal
		$boffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	elseif($avg[2]>0&&$avg[2]==$avg[3]){
		// boss and peers are equal
		$boffs=$heigth/$div;
		$poffs=-$heigth/$div;
	}
	elseif($avg[2]>0&&$avg[2]==$avg[4]){
		// boss and DRs are equal
		$boffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	elseif($avg[3]>0&&$avg[3]==$avg[4]){
		// peers and DRs are equal
		$poffs=$heigth/$div;
		$doffs=-$heigth/$div;
	}
	
	// Boss
	if($avg[2]>0){
		$val=$avg[2];
		$xpos=$x+8+(108*$val);
		$ypos=$y;

		// boss colors
		$R=1;
		$G=179/255;
		$B=15/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
	
		pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$boffs,1+$width/2);
		pdf_fill_stroke($pdf);
		
		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[2],$xpos,$ypos+$boffs);
	}
	
	// Peer
	if($avg[3]>0){
		$val=$avg[3];
		$xpos=$x+8+(108*$val);
		$ypos=$y;

		// peers colors
		$R=122/255;
		$G=165/255;
		$B=180/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
		
		pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$poffs,1+$width/2);
		pdf_fill_stroke($pdf);

		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[3],$xpos,$ypos+$poffs);
	}
	
	// DR
	if($avg[4]>0){
		$val=$avg[4];
		$xpos=$x+8+(108*$val);
		$ypos=$y;

		// dr colors
		$R=178/255;
		$G=128/255;
		$B=166/255;
		pdf_setcolor($pdf,'both','rgb',$R,$G,$B,0);
		
		pdf_circle($pdf,$xpos+($width/4),$ypos+($heigth/4)+$doffs,1+$width/2);
		pdf_fill_stroke($pdf);

		$font=getTinyText($pdf);
		pdf_show_xy($pdf,$avg[4],$xpos,$ypos+$doffs);
	}
}


// Page 16 - Organizational Perspective + Hot Buttons Profile
function renderPage16($cid,&$pdf,$page,$pid,$lid,&$noOther,&$indices){
	$name=writeHeader($cid,$pdf,$lid,"left");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);
	
	$flid=getFLID("meta","mdOP");
	$mltxt=getMLText($flid,"1",$lid);

	// ORGANIZAATIONAL PERSPECTIVE Continued
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[15],$x,$y);

	$y=PAGE_HEIGHT-(72*3.5);
	$sz=0.25;
	$img=pdf_open_image_file($pdf,"JPEG","../images/10_Behavior_Watch.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,$sz);
	
	// Blot out stuff we don't have data for
	pdf_setcolor($pdf,"both","rgb",1,1,1,0);
	pdf_rect($pdf,$x+170,$y,200,30);
	pdf_rect($pdf,$x+130,$y+48,320,10);
	// Also blot out the scale, since we don't use a scale from 35-65 but from 1 trough 3
	pdf_fill_stroke($pdf);
	
	$offs=132;	
	graphPage16(&$pdf,$x,$y,$indices,$offs);
	
	//------------------------------------------------
	// Move on to static text for HOT BUTTONS PROFILE
	//------------------------------------------------
	$y=PAGE_HEIGHT-(72*4);
	
	$flid=getFLID("meta","mdHB");
	$mltxt=getMLText($flid,"1",$lid);
	
	$font=getH3($pdf);
	pdf_show_xy($pdf,$mltxt[1],$x,$y);

	$y=PAGE_HEIGHT-(72*4.25);
	$font=getBody($pdf);
	pdf_show_xy($pdf,$mltxt[11],$x,$y);
	for($i=12;$i<15;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
	}
	pdf_continue_text($pdf,"");
	for($i=15;$i<21;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
	}
	
	$y=PAGE_HEIGHT-(72*6.5);
	$font=getSubText($pdf);
	pdf_show_xy($pdf,$mltxt[2],$x,$y);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");
	
	pdf_continue_text($pdf,$mltxt[3]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");
	
	pdf_continue_text($pdf,$mltxt[4]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");

	pdf_continue_text($pdf,$mltxt[5]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");

	pdf_continue_text($pdf,$mltxt[6]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");

	pdf_continue_text($pdf,$mltxt[7]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");

	$xtop=$x+125;
	$font=getBody($pdf);
	pdf_show_xy($pdf,$mltxt[21],$xtop,$y);
	for($i=22;$i<35;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
		if($i==22||$i==25||$i==27||$i==30||$i==32){
			pdf_continue_text($pdf,"");
		}
	}
	
	
	writeReportFooter($pdf,$page,$lid,"left");
}

// Page 17 - Hot Buttons continued
function renderPage17($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);

	$flid=getFLID("meta","mdHB");
	$mltxt=getMLText($flid,"1",$lid);
	$font=getSubText($pdf);
	pdf_show_xy($pdf,$mltxt[8],$x,$y);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");
	
	pdf_continue_text($pdf,$mltxt[9]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,"");
	
	pdf_continue_text($pdf,$mltxt[10]);

	$xtop=$x+125;
	$font=getBody($pdf);
	pdf_show_xy($pdf,$mltxt[35],$xtop,$y);
	for($i=36;$i<=40;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
		if($i==36||$i==38){
			pdf_continue_text($pdf,"");
		}
	}

	$y=PAGE_HEIGHT-(72*3.5);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[50],$x,$y);
	pdf_continue_text($pdf,$mltxt[51]);
	

	// Sea Urchin Graph begins here
	$data=getHotButtons($cid);
	if($data){			
		$y=PAGE_HEIGHT-(72*6.75);
		$img=pdf_open_image_file($pdf,"JPEG","../images/11_Hot_Button.jpg","",0);
		pdf_place_image($pdf,$img,$x,$y,0.245);
		drawButtonGraph($pdf,$x,$y,$data);	
	}
	else{
		pdf_continue_text($pdf,"");
		pdf_continue_text($pdf,"");
		pdf_continue_text($pdf,"Cannot graph hot buttons");	
	}

	writeReportFooter($pdf,$page,$lid,"right");
}


function plotSeaUrchin(&$pdf,$x,$y){
	$font=pdf_findfont($pdf,"ZapfDingbats","builtin",0);
	pdf_setfont($pdf,$font,28.0);
	pdf_show_xy($pdf,"W",$x,$y);
//	pdf_show_xy($pdf,"WHXVM",$x,$y);
}

function drawButtonGraph($pdf,$startx,$starty,$data){
	$n=9;
	$xbase=$startx+(72*1.65);
	$ybase=$starty+(72*0.375);
	$ystep=(72*0.2875);
	$xstep=(72*0.7125/5);
	$h=(72*0.0625);
	$d=(72*0.0125);
	
	for($i=0;$i<$n;$i++){
		$val=round($data[$i][0]);
		if($val<35){
			$font=getTinyGreenText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// Backward box
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_rect($pdf,$xbase-$xstep-1,$ybase+$d+($i*$ystep),$xstep-2.5,$h);
			pdf_fill_stroke($pdf);
		}
		elseif($val>65){
			$font=getTinyRedText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// Box to the limit
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_rect($pdf,$xbase,$ybase+$d+($i*$ystep),31*$xstep,$h);
			pdf_fill_stroke($pdf);
			plotSeaUrchin($pdf,$xbase+(30.5*$xstep)-4,$ybase+($i*$ystep)-7);
		}
		elseif($val==35){
			$font=getTinyText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// nothing
		}
		else{
			$font=getTinyText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			pdf_rect($pdf,$xbase,$ybase+$d+($i*$ystep),(($val-35)*$xstep),$h);
			pdf_fill_stroke($pdf);
			plotSeaUrchin($pdf,$xbase+(($val-35)*$xstep)-5,$ybase+($i*$ystep)-7);
		}
	}
}	

function renderPage18($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"left");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1);
	
	$flid=getFLID("meta","mdDWS");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	pdf_show_xy($pdf,$mltxt[1],$x,$y);

	// Static text
	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*1.5);
	pdf_show_xy($pdf,$mltxt[2],$x,$y);
	for($i=3;$i<9;$i++){
		pdf_continue_text($pdf,$mltxt[$i]);
	}
	for($i=9;$i<13;$i++){
		pdf_continue_text($pdf,"");
		pdf_continue_text($pdf,$mltxt[$i]);
	}
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	
	// The table
	$y=PAGE_HEIGHT-(72*5.5);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[18],$x,$y);
	
	// gray
	$R=230/255;
	$G=230/255;
	$B=230/255;

	$y=PAGE_HEIGHT-(72*6);
	$hgth=20;
	pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
	pdf_rect($pdf,$x,$y,100,$hgth);
	pdf_rect($pdf,$x+125,$y,100,$hgth);
	pdf_rect($pdf,$x+250,$y,200,$hgth);
	pdf_fill_stroke($pdf);
	
	$font=getSubText($pdf);
	pdf_show_boxed($pdf,$mltxt[19],$x,$y,100,$hgth,"center","");
	pdf_show_boxed($pdf,$mltxt[20],$x+125,$y,100,$hgth,"center","");
	pdf_show_boxed($pdf,$mltxt[21],$x+250,$y,200,$hgth,"center","");

	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	$font=getBody($pdf);
	
	$y=PAGE_HEIGHT-(72*6.5);
	$ytop=$y;
	$hgth=25;
	
	for($i=22;$i<=30;$i++){
		pdf_show_xy($pdf,$mltxt[$i],$x,$ytop);
		$ytop-=$hgth;
	}

	$ytop=$y;
	for($i=22;$i<=30;$i++){
		pdf_moveto($pdf,$x+125,$ytop);
		pdf_lineto($pdf,$x+225,$ytop);
		pdf_moveto($pdf,$x+250,$ytop);
		pdf_lineto($pdf,$x+450,$ytop);
		$ytop-=$hgth;
	}
	pdf_fill_stroke($pdf);
	
	writeReportFooter($pdf,$page,$lid,"left");
}

// Page 19 - Responses to Conflict
function renderPage19($cid,&$pdf,$page,$pid,$lid,&$noOther){
	$name=writeHeader($cid,$pdf,$lid,"right");

	// Fill in the yellow stuff
	$R=227/255;
	$G=234/255;
	$B=23/255;

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1);
	
	$flid=getFLID("meta","mdRTC");
	$mltxt=getMLText($flid,"1",$lid);

	$font=getH2($pdf);
	pdf_show_xy($pdf,$mltxt[1],$x,$y);
	
	$ystep=17;
	$y=PAGE_HEIGHT-(72*2.125);
	$ytop=$y;
	
	// 4 yellow boxes
	pdf_setcolor($pdf,"both","rgb",$R,$G,$B,0);
	pdf_rect($pdf,$x+30,$ytop-$ystep,176,$ystep);
	pdf_rect($pdf,$x+30,$ytop-7*$ystep,176,$ystep);
	pdf_rect($pdf,$x+30,$ytop-12*$ystep,176,$ystep);
	pdf_rect($pdf,$x+30,$ytop-17*$ystep,176,$ystep);
	pdf_fill_stroke($pdf);
	
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	// 4 black boxes
	pdf_rect($pdf,$x+207,$ytop-$ystep,44,$ystep);
	pdf_rect($pdf,$x+207,$ytop-7*$ystep,44,$ystep);
	pdf_rect($pdf,$x+207,$ytop-12*$ystep,44,$ystep);
	pdf_rect($pdf,$x+207,$ytop-17*$ystep,44,$ystep);
	pdf_fill_stroke($pdf);
	
	// vertical lines 5 x 2 lines
	pdf_moveto($pdf,$x+30,$y);
	pdf_lineto($pdf,$x+30,$y-10*$ystep);
	pdf_moveto($pdf,$x+207,$y);
	pdf_lineto($pdf,$x+207,$y-10*$ystep);
	pdf_moveto($pdf,$x+251,$y);
	pdf_lineto($pdf,$x+251,$y-10*$ystep);
	pdf_moveto($pdf,$x+360,$y);
	pdf_lineto($pdf,$x+360,$y-10*$ystep);
	pdf_moveto($pdf,$x+470,$y);
	pdf_lineto($pdf,$x+470,$y-10*$ystep);
	
	pdf_moveto($pdf,$x+30,$y-11*$ystep);
	pdf_lineto($pdf,$x+30,$y-21*$ystep);
	pdf_moveto($pdf,$x+207,$y-11*$ystep);
	pdf_lineto($pdf,$x+207,$y-21*$ystep);
	pdf_moveto($pdf,$x+251,$y-11*$ystep);
	pdf_lineto($pdf,$x+251,$y-21*$ystep);
	pdf_moveto($pdf,$x+360,$y-11*$ystep);
	pdf_lineto($pdf,$x+360,$y-21*$ystep);
	pdf_moveto($pdf,$x+470,$y-11*$ystep);
	pdf_lineto($pdf,$x+470,$y-21*$ystep);
	
	// horizontal lines
	for($i=0;$i<22;$i++){
		pdf_moveto($pdf,$x+30,$ytop);
		pdf_lineto($pdf,$x+470,$ytop);
		$ytop-=$ystep;
	}
	pdf_stroke($pdf);

	
	// Now we move on to text
	$ytop=$y;
	// The column headers outside the boxes
	$font=getSubText($pdf);
	pdf_show_boxed($pdf,$mltxt[2],$x+205,$ytop,50,2*$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[3],$x+280,$ytop,50,2*$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[4],$x+365,$ytop,110,2*$ystep,"center","");
	
	// Red Italic Text in Boxes
	$font=getRedItalic($pdf);
	pdf_show_boxed($pdf,$mltxt[5],$x+250,$y-$ystep,110,$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[6],$x+360,$y-$ystep,110,$ystep,"center","");

	pdf_show_boxed($pdf,$mltxt[5],$x+250,$y-7*$ystep,110,$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[6],$x+360,$y-7*$ystep,110,$ystep,"center","");

	pdf_show_boxed($pdf,$mltxt[6],$x+250,$y-12*$ystep,110,$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[5],$x+360,$y-12*$ystep,110,$ystep,"center","");

	pdf_show_boxed($pdf,$mltxt[6],$x+250,$y-17*$ystep,110,$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[5],$x+360,$y-17*$ystep,110,$ystep,"center","");
	
	// White header text
	$font=getWhiteBold($pdf);
	pdf_show_boxed($pdf,$mltxt[7],$x+30,$y-$ystep,176,$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[8],$x+30,$y-7*$ystep,176,$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[9],$x+30,$y-12*$ystep,176,$ystep,"center","");
	pdf_show_boxed($pdf,$mltxt[10],$x+30,$y-17*$ystep,176,$ystep,"center","");
	
	// The example
	$font=getSmallBlackItalic($pdf);
	pdf_show_boxed($pdf,$mltxt[12],$x+215,$y-2.5*$ystep,25,1.5*$ystep,"center","");
	$font=getPage19Text($pdf);
	pdf_show_boxed($pdf,$mltxt[11],$x+35,$y-2*$ystep,171,$ystep,"left","");
	
	// Item text
	$ytop-=3*$ystep;
	
	for($i=20;$i<=26;$i++){
		if($i==24){
			$ytop-=$ystep;	
		}
		pdf_show_boxed($pdf,$mltxt[$i],$x+35,$ytop,171,$ystep,"left","");
		$ytop-=$ystep;
	}

	$ytop-=2*$ystep;
	
	for($i=30;$i<=37;$i++){
		if($i==34){
			$ytop-=$ystep;	
		}
		pdf_show_boxed($pdf,$mltxt[$i],$x+35,$ytop,171,$ystep,"left","");
		$ytop-=$ystep;
	}

	// A big, surrounding rectangle
	$y=PAGE_HEIGHT-(72*7.5);
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	pdf_rect($pdf,$x,$y,500,450);
	pdf_stroke($pdf);
	writeReportFooter($pdf,$page,$lid,"right");
}

?>
