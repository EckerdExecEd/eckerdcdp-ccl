<?php
/**
 * Function accepts a string, measures it and returns the X coordinate to
 * begin from when centering,
 * $string = string you want to center
 * $coord = X coordinate you want to center from  
 **/  
function centerXcord($pdf, $string, $coord=300){
  $font=pdf_get_value($pdf, 'font',0);
  $fontsize=pdf_get_value($pdf, 'fontsize',0);
  $width=pdf_stringwidth($pdf,$string,$font,$fontsize);
  $retVal=ceil($coord -($width/2));
  return $retVal;
}

function pdfStrLen($pdf,$str){
  return pdf_stringwidth($pdf,$str,pdf_get_value($pdf, 'font',0), pdf_get_value($pdf, 'fontsize',0));
}


function rightXcord($pdf, $string, $rStart=550){
  $width=pdf_stringwidth($pdf,$string,pdf_get_value($pdf, 'font',0), pdf_get_value($pdf, 'fontsize',0));
  $retVal=($rStart-$width);
  return $retVal;
}

function pdfUnderline($pdf,$string,$x,$y){
  $sX=$x + pdfStrLen($pdf,$string);
  pdf_moveto($pdf,$x,$y-1.5);
	pdf_lineto($pdf,$sX,$y-1.5);
	pdf_stroke($pdf);  

	pdf_show_xy($pdf,$string,$x,$y);
}

/**
 * Function accepts Hex (HTML) color and returns cmyk array from
 * functions rgb2cmyk(hex2rgb(HEX))
 * $hex - HTML color that you want the CMYK format for. 
 **/   
function hex2cmyk($hex){
  return rgb2cmyk(hex2rgb($hex));
}

function hex2rgb($hex) {
  $color = str_replace('#','',$hex);
  $rgb = array('r' => hexdec(substr($color,0,2)),
               'g' => hexdec(substr($color,2,2)),
               'b' => hexdec(substr($color,4,2)));
  return $rgb;
}

function rgb2cmyk($var1,$g=0,$b=0) {
   if(is_array($var1)) {
      $r = $var1['r'];
      $g = $var1['g'];
      $b = $var1['b'];
   }
   else $r=$var1;
   $cyan    = 255 - $r;
   $magenta = 255 - $g;
   $yellow  = 255 - $b;
   $black   = min($cyan, $magenta, $yellow);
   $cyan    = @(($cyan    - $black) / (255 - $black)) * 255;
   $magenta = @(($magenta - $black) / (255 - $black)) * 255;
   $yellow  = @(($yellow  - $black) / (255 - $black)) * 255;
   return array('c' => $cyan / 255,
                'm' => $magenta / 255,
                'y' => $yellow / 255,
                'k' => $black / 255);
}
?>
