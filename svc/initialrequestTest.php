<?php
// Entry point for the Inital SOAP request
require_once "class.InitialParserTest.php";

echo "1.";

// Create a SAX parser and a helper object
$xml=xml_parser_create();
echo "2.";

$p=new InitialParser;

echo "3.";

// associate the helper object with the SAX parser
xml_set_object($xml,$p);

echo "4.";

// associate the handlers
xml_set_element_handler($xml,'start_element','end_element');

echo "5.";

xml_set_character_data_handler($xml,'character_data');

echo "6.";

xml_set_default_handler($xml,'default_handler');

echo "7.";

xml_parser_set_option($xml,XML_OPTION_CASE_FOLDING,false);

echo "8.";

$rc=xml_parse($xml,$HTTP_RAW_POST_DATA,false);

echo "9.";

xml_parser_free($xml);

echo "10";

echo trim($p->soapResponse());
?>