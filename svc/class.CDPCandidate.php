<?php
require_once "../meta/candidate.php";
class CDPCandidate{
	
	var $cand;
	var $prog;
	var $status=0;
	var $cid=false;
	var $errortext;
	
	function getError(){
		return $this->status;
	}

	function getCandErrorText(){ 
  		return $this->errortext;
	}
  	
	function CDPCandidate($cand,$prog){
		$this->cand=$cand;
		$this->prog=$prog;
		$this->errortext="";
	}

	function bioRescore($cid){
		$conn=dbConnect();
		$query="update CANDIDATE set FNAME='".$this->cand['19']."',LNAME='".$this->cand['20']."',EMAIL='".$this->cand['3']."' where CID=$cid";
//		echo $query;
		$rs=mysql_query($query);
		if($rs==false){
			$this->status=810;
			$this->errortext.="<br>Error 810 - Unable to update CANDIDATE using $query <br>";			
		}
		return $rs;
	}

	function process(){
		$this->cid=false;
		// This one doesn't exist yet, so we must insert the candidate
		$other=(3==$this->prog['instrument_Type'])?'N':'Y';
		$candData=array($this->cand['pid'],$this->cand['19'],$this->cand['20'],$this->cand['3'],
						'Y',$other,$this->prog['CONID'],$this->cand['language']);
		$this->cid=candInsert($candData);
		if(false!=$this->cid){
			$conn=dbConnect();
			$query="update CANDIDATE set ALTID='".$this->cand['clientidentification']."' where CID=".$this->cid." and PID=".$this->cand['pid'];
			if(false===mysql_query($query)){
				$this->status=903;
			  $this->errortext.="<br>Error 903 - Unable to update CANDIDATE using $query <br>";					
				$this->cid=false;
			}
		}
		return $this->cid;
	}
	
	function getCid(){
		if(false===$this->cid){
			$conn=dbConnect();
			$query="select CID from CANDIDATE where ALTID='".$this->cand['clientidentification']."' and PID=".$this->cand['pid'];
	//		echo $query."\n";
			$rs=mysql_query($query);
			if(false==$rs){
	//			echo "query failed\n";
				$this->status=902;
				$this->errortext.="<br>Error 902 - CANDIDATE not found using $query <br>";
				if($conn){ mysql_close($conn); } // 05-Mar-2009 ???		
				return false;
			}
			$row=mysql_fetch_row($rs);
			if(false==$row){
			  if($conn){ mysql_close($conn); } // 05-Mar-2009 ???		
				return false;
			}
			$this->cid=$row[0];
			if($conn){ mysql_close($conn); } // 05-Mar-2009 ???	
		}
		return $this->cid;
	}


	// check and see if this candidate already exists
	// based on the External ID (CANDIDATE.ALTID)
	function exists(){
		$conn=dbConnect();
		$query="select count(*) from CANDIDATE where ALTID='".$this->cand['clientidentification']."' and PID=".$this->cand['pid'];
//		echo $query."\n";
		$rs=mysql_query($query);
		if(false==$rs){
//			echo "query failed\n";
			$this->status=904;
      $this->errortext.="<br>Error 904 - No CANDIDATE found using $query<br>";			
			if($conn){ mysql_close($conn); } // 05-Mar-2009 ???	
			return false;
		}
		$row=mysql_fetch_row($rs);
		if(false==$row){
		  if($conn){ mysql_close($conn); } // 05-Mar-2009 ???	
			return false;
		}
		if($conn){ mysql_close($conn); } // 05-Mar-2009 ???	
		return $row[0];
	}
	
}
?>
