<?php
require_once "soap/lib/nusoap.php";
require_once "../meta/mailfns.php";
//$host=getURLRoot()."/svc/reportrequest.php";
// Testing against CCL server
$host="http://www2.onlinecdp.org/ccl/svc/reportrequest.php";
$cl=new soapclient($host);

if($err=$cl->getError()){
	echo $err;
	exit;
}

$bod=getBody360();

$rc=$cl->send($bod,$host,0,300);

// some debugging stuff
echo "<xmp>\n";
//echo $cl->request."\n\n";
echo $cl->response."\n";
//echo $cl->document."\n";
echo "\n</xmp>";

function getBody360(){
	return "<?xml version=\"1.0\"?>
		<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
			<soap:Header>
				<Routing>
					<Login>CCLTest</Login>
					<Password>ccltest010</Password>
					<ClientKey>6300</ClientKey>
					<CustomerNumber>200517</CustomerNumber>
					<ShipTo>335704</ShipTo>
					<BillTo>335704</BillTo>
				</Routing>
			</soap:Header>
			<soap:Body>
				<Score_Request>
					<ScoreRequest>
						<requestorkey>600001</requestorkey>
						<vendorkey>1030162</vendorkey>
					</ScoreRequest>
				</Score_Request>
		</soap:Body>
		</soap:Envelope>";
}

?>
