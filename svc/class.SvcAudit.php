<?php
require_once "../meta/dbfns.php";
class SvcAudit{
  
    function insertAuditRec($cid,$altid,$rtype,$startdt){
      $qry = "insert into SVCAUDIT (CID, ALTID, RTYPE, STARTDT) VALUES
              ($cid, '$altid', '$rtype', '$startdt')";
      $conn=dbConnect();
      mysql_query($qry);
      $rid = mysql_insert_id();
      if($conn){ mysql_close($conn); } // 05-Mar-2009 ???		
      return $rid;
    } 
  
   function updateAuditRec($auditNum,$cid,$altid,$rtype,$enddt){
      $qry = "update SVCAUDIT set ENDDT = '$enddt' WHERE CID = $cid
              and ALTID = '$altid' and RTYPE = '$rtype'";
      $conn=dbConnect();
      mysql_query($qry);       
      if($conn){ mysql_close($conn); } // 05-Mar-2009 ???		 
   }

  function insertAuditXML($auditNum,$cid,$altid,$rtype,$reqxml,$respxml,$startdt,$enddt){
      $conn=dbConnect();
      $qry = sprintf("insert into SVCXML (`RECID`,`CID`, `ALTID`, `RTYPE`, 
                                  `REQXML`, `RESPXML`, `STARTDT`, `ENDDT`) VALUES
                      (%d, %d, '%s', '%s', '%s', '%s', '%s', '%s')",
                      $auditNum,
                      $cid,
                      $altid,
                      $rtype,
                      mysql_real_escape_string($reqxml,$conn),
                      mysql_real_escape_string($respxml,$conn),
                      $startdt,
                      $enddt);   
       mysql_query($qry, $conn);
       if($conn){ mysql_close($conn); } // 05-Mar-2009 ???		 
  }
  
  function insertReqAuditXML($auditNum,$cid,$altid,$rtype,$reqxml,$startdt){
      $conn=dbConnect();
      $qry = sprintf("insert into SVCXML (`RECID`,`CID`, `ALTID`, 
                                          `RTYPE`,`REQXML`,`STARTDT`) VALUES
                      (%d, %d, '%s', '%s', '%s', '%s')",
                      $auditNum,
                      $cid,
                      $altid,
                      $rtype,
                      mysql_real_escape_string($reqxml,$conn),
                      $startdt);   
       mysql_query($qry, $conn);
       if($conn){ mysql_close($conn); } // 05-Mar-2009 ???		 
  } 

  function updateRespAuditXML($auditNum,$cid,$altid,$rtype,$respxml,$enddt){
      $conn=dbConnect();
      $qry = sprintf("update SVCXML set `RESPXML` = '%s',
                             `ENDDT` = '%s'
                       where `RECID` = %d
                         and `CID` = %d
                         and `ALTID` = '%s'
                         and `RTYPE` = '%s'",
                         mysql_real_escape_string($respxml,$conn),
                         $enddt,
                         $auditNum,
                         $cid,
                         $altid,
                         $rtype);   
       mysql_query($qry, $conn);
       if($conn){ mysql_close($conn); } // 05-Mar-2009 ???		 
  }     
   
   function getTS(){
        return date('Y-n-j H:i:s');
   }

  function logSvcError($type,$from,$msg,$email=false){
  /**
   *  General function for logging errors
   **/
    $conn = db_connect();
    	 $qry = "insert into SVCLOG (LOGTYPE,LOGFROM,LOGMSG,LOGDATE) values
    	      ('$type','$from','$msg',NOW())";
        mysql_query($qry, $conn);
    if($conn){ mysql_close($conn); } // 26-Mar-2009 ???
  
    if($email){
      switch($email){
        case "PC" :
          $to = 'To: Pat Cheely <dtiservices@thediscoverytec.com>';
          break;
        default :
          $to = 'To: Pat Cheely <dtiservices@thediscoverytec.com>';
          break;
      }
  
      $subject='CCL Service Error';
      $from = 'From: EKD support <EKDsystemlog@onlinecdp.org>';
      $message = $msg;
      mail($to, $subject, $message, $from);
    }
  
  }
   
}
?>
