<?php
header("Content-Type: text/xml");
// Entry point for the Status SOAP request
require_once "class.StatusParser.php";

$xml=xml_parser_create();
$p=new StatusParser;

// associate the helper object with the SAX parser
xml_set_object($xml,$p);

// associate teh handlers
xml_set_element_handler($xml,'start_element','end_element');
xml_set_character_data_handler($xml,'character_data');
xml_parser_set_option($xml,XML_OPTION_CASE_FOLDING,false);

// parse the incoming request
//echo "POSTDATA:\n";
//echo $HTTP_RAW_POST_DATA;
//echo "END POSTDATA:\n";

$rc=xml_parse($xml,$HTTP_RAW_POST_DATA,false);
xml_parser_free($xml);

$resp =trim($p->soapResponse());
echo $resp;

$p->audit->insertAuditXML($p->auditNum,
                          $p->vendorkey,
                          $p->requestorkey,
                          'SRQ',
                          $HTTP_RAW_POST_DATA,
                          $resp,
                          $p->bTS,
                          $p->eTS);
?>
