<?php
// This represensts a Self-only Report
require_once "../meta/scoreandreportselfonly.php";

class SelfOnlyReport{
	var $cid;
	var $pid;
	var $status=0;
	var $pdf="";
	var $rawscales="\n";
	
	function getStatus(){
		return $this->status;
	}

	function getPDF(){
		return $this->pdf;
	}
	
	function SelfOnlyReport($cid,$pid){
		$this->cid=$cid;
		$this->pid=$pid;
	}

	function process(){
		// 1. see if we have enough stuff to score
		if(false==isItComplete($this->cid,$this->pid)){
			$this->status=812;
		}
		$this->doScore();
		$this->doScales();
		$this->doReport();
	}
	
	// Scoring
	function doScore(){
		if(0==$this->status){
			// 2. get rid of old stuff
			getRidOfOldScores($this->cid,$this->pid);
			
			// 3. Calculate Scale Scores
			if(false==computeSelfOnlyScaleScores($this->cid,$this->pid)){
				$this->status=804;
			}
			else{
				// 4. generate the self-only report scores
				if(false==calculateSelfOnlyReportScores($this->cid,$this->pid)){
					$this->status=804;
				}
			}
		}
	}
	
	//Reporting 
	function doReport(){
		if(0==$this->status){
			$cid=$this->cid;
			$pid=$this->pid;
			$lid=1;
			// 5. Generate the PDF Report
			$pdf=pdf_new();
			pdf_set_parameter($pdf,"license","L700102-010500-734253-ZZ4ND2-FMQ922");
			pdf_open_file($pdf,"");
			pdf_set_info($pdf,'Creator','conflictdynamics.org');
			pdf_set_info($pdf,'Author','conflictdynamics.org');
			pdf_set_info($pdf,'Title','Conflict Dynamics Profile');
			$page=1;
			renderPage1($cid,$pdf,$page,$pid,$lid);
			renderPage2($cid,$pdf,$page,$pid,$lid);
			renderPage3($cid,$pdf,$page,$pid,$lid);
			renderPage4($cid,$pdf,$page,$pid,$lid);
			renderPage5($cid,$pdf,$page,$pid,$lid);
			renderPage6($cid,$pdf,$page,$pid,$lid);
			renderPage7($cid,$pdf,$page,$pid,$lid);
			renderPage8($cid,$pdf,$page,$pid,$lid);
			pdf_close($pdf);
			$this->pdf=base64_encode(pdf_get_buffer($pdf));
			pdf_delete($pdf);
		}
	}

	function doScales(){
		if(0==$this->status){
			$conn=dbConnect();
			$query="select a.SID,a.DESCR,round(b.AVGSCORE,1) from SCALE a, SELFONLYREPORTSCORE b where a.SID=b.SID and b.CID=".$this->cid." order by a.SID";
//			echo $query;
			$rs=mysql_query($query);
			if(false==$rs){
				$this->status=804;
				return;
			}
			$rows=dbRes2Arr($rs);
			foreach($rows as $row){
				$scale=$row[1];
				$score=$row[2];
				$this->rawscales=$this->rawscales."<scores scale='$scale' scorelength='1'>\n<score rater_type='1'>$score</score>\n</scores>\n";
			}
		}
	}
	
	// Get the raw scales
	function getScales(){
		return $this->rawscales;
	}
	
}

?>
