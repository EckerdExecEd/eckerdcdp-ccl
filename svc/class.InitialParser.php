<?php
// Parser the CCL Initial request
//========================================= Added 02/27/2009
require_once "class.SvcAudit.php";
//==========================================================
require_once "class.TagStack.php";
require_once "class.Credentials.php";
require_once "class.CDPProgram.php";
require_once "class.CDPCandidate.php";
require_once "class.CDPRater.php";
require_once "class.WaitTime.php";

// 11-22-2006 Changed so that we use the elements requestor_key and vendor_key
// instead of clientidentification

// 11-30-2006: Jonathan says to use requestorkey and vendorkey across the board

class InitialParser{
	
	var $tags;
	var $creds;
	var $prog;
	var $attr;
	var $cand;
	var $forms;
	var $form;
	var $demos;
	var $demo;
	var $catid=0;
	var $bio=false;	
	var $cid=-1;
	var $req_key;	// requesteor's id
	
	// status set to 0 while we're processing
	// will be set to error code or 1 on success
	var $status=0;
	var $wait=0;
	var $error=false;
	
	//========================================= Added 02/27/2009
	// Audit data for performance and usage
	var $bTS;
	var $eTS;
	var $auditNum;
	var $reqType;
	
	//==========================================================
	// constructor
	function InitialParser(){
	  //========================================= Added 02/27/2009
	  $this->audit= new SvcAudit();
	  $this->bTS=$this->audit->getTS();
	  //==========================================================
	  
		$this->tags= new TagStack();
		$this->creds=new Credentials();
		$this->prog=array();
		$this->cand=array();
		$this->forms=array();
		$this->demos=array();
	}

	// check the credentials
	function checkCredentials(){
		if($this->creds->hasCredentials()){
			$this->status=$this->creds->verify();
			if(0==$this->status){
				// succesful login, get the Consultant ID
				$this->prog['CONID']=$this->creds->getConid();
			}
		}
	}

	// The three required parser functions
	function start_element($parser,$tag,$attributes){
		// as long aa we habve no errors
		if(0==$this->status){
			$this->tags->push($tag);
			$this->attr=$attributes;
			if("form"==$tag){
				// beginning of form - create new form and demo
				$this->form=array();
				$this->demo=array();
			}
			// debugging
//			echo "\nStart of: ".$this->tags->top()." nesting ".$this->tags->count();
		}
	}

	function end_element($parser,$tag){
		// as long as we have no errors
		if(0==$this->status){
			if("soap:Envelope"==$tag){
				// the entire message has been parsed
				// process the collected data
				$this->process();
			}
			if("form"==$tag){
				// end of the form - add to the forms and demo arrays
				$this->forms[]=$this->form;
				$this->demos[]=$this->demo;
			}
			// debugging
//			echo "\nEnd of: ".$this->tags->top()." nesting ".$this->tags->count();
			unset($this->attr);
			$this->tags->pop();
		}
	}
	
	function character_data($parser,$data){
		// as long aa we have no errors
		if(0==$this->status){
			$data=trim($data);
			$data=mysql_escape_string($data);
			$tag=$this->tags->top();
			if("response"==$tag){
				$idx=$this->attr['item'];
				if(isset($this->form[$idx])){
					// we already have things in here
					$previous=$this->form[$idx];
					$data=$previous.$data;
				}
				$this->form[$idx]=$data;
			}
			elseif("demographic"==$tag){
				$idx=$this->attr['item'];
				$this->demo[$idx]=$data;
				if(1==$this->catid){
					if($idx==19||$idx==20||$idx==3){
						// This is the self, we need to save some data
						$idx=$this->attr['item'];
            $this->cand[$idx]=($idx==3)? $data : charset_decode_utf_8($data);
					}
				}
			}
			elseif("RaterType"==$tag){
				// Note: rater type goes into demo[0]
				$this->catid=$data;
				$this->demo[0]=$data;
			}
			elseif("language"==$tag){
				$this->cand['language']=$data;
			}
			elseif("instrument_language"==$tag){
				$this->cand['instrument_language']=$data;
			}
			elseif("Login"==$tag){
				$this->creds->setUID($data);
				$this->checkCredentials();
			}
			elseif("Password"==$tag){
				$this->creds->setPWD($data);
				$this->checkCredentials();
			}
			elseif("EventID"==$tag){
				$this->prog['EventID']=$data;
			}
			elseif("EventName"==$tag){
				$this->prog['EventName']=$data;
			}
			elseif("EventLocation"==$tag){
				$this->prog['EventLocation']=$data;
			}
			elseif("EventDate"==$tag){
				$this->prog['EventDate']=$data;
			}
			elseif("instrument_Type"==$tag){
				if("CDPIND"==$data){
					$this->prog['instrument_Type']=3;
				}
				else{
					$this->prog['instrument_Type']=1;
				}
			}
			elseif("requestorkey"==$tag){
				$this->client['clientidentification']=$data;
				$this->cand['clientidentification']=$data;
				$this->req_key=$data;
			}
			elseif("vendorkey"==$tag){
				//echo "Set the vendorkey to |$data|";
				if(strlen(trim($data))>0){
					$this->cid=$data;
				}
				else{
					$this->cid=-1;	
				}
			}
			
		}
	}
	
	// Returns a SOAP response based on the Initial request
	function soapResponse(){
		if(0==$this->status){
			// success - set sttaus to 1
			$this->status=1;
			// and get the estimated wait time
			$wt=new WaitTime();
			
			if($this->bio){
				$this->wait=$wt->getBioWaitTime();
			}
			else{
				$this->wait=$wt->getInitialWaitTime($this->cid,$this->prog['instrument_Type']);
			}
		}
		$rc="<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
	<soap:Body>
		<SCORING_RESPONSEResponse>
			<SCORING_RESPONSEResult>
				<requestorkey>".$this->req_key."</requestorkey>
				<!-- CDP system generated unique key -->
				<vendorkey>".$this->cid."</vendorkey> 
				<statuscode>".$this->status."</statuscode>
				<waittime>".$this->wait."</waittime>
			</SCORING_RESPONSEResult>
		</SCORING_RESPONSEResponse>
	</soap:Body>
</soap:Envelope>";
		return trim($rc);
	}
	
	// Process the collected data
	function process(){
		// start with the program
		// This will either return the PID of an existing program, or
		// if it doesn't exist it will create it
		$program=new CDPProgram($this->prog);
		$pid=$program->process();

		if(false===$pid){
			$this->status=$program->getError();
			$this->audit->logSvcError('E','class.CDPProgram.php',$program->getPrgErrorText(),'PC');
//			echo "Status (195): ".$this->status."\n";
		}else{
			// create the candidate
			$this->cand['pid']=$pid;
			$cand=new CDPCandidate($this->cand,$this->prog);
			
			// First we should check if the candidate exists
			// if she does we can do a bio rescore, nothing else
			// 11-22-2006: We look for an existing CID, passed in via vendorkey
			if(-1!=$this->cid){
				// we already have one of these
				// do the biorescore here
//				echo "\nBio Rescore\n";
				//$this->cid=$cand->getCid();
				$this->bio=true;
				//========================================= Added 07/22/2009
				//Insert audit record into SVCAUDIT table and capture the record ID
				$this->reqType='BRQ'; //Bio Rescore request
				$this->auditNum=$this->audit->insertAuditRec($this->cid,
                                                     $this->req_key,
                                                     $this->reqType,
                                                     $this->bTS);
				//==========================================================				
				if(false==$cand->bioRescore($this->cid)){
					$this->status=$cand->getError();
					$this->audit->logSvcError('E','class.CDPCandidate.php',$cand->getCandErrorText(),'PC');
//					echo "Status (219):".$this->status."\n";
				}else{
					//========================================= Added 02/27/2009
					   $this->eTS=$this->audit->getTS();
					   $this->audit->updateAuditRec($this->auditNum,
                                          $this->cid,
                                          $this->req_key,
                                          $this->reqType,
                                          $this->eTS);
					//==========================================================				
				}
			}else{
				// new candidate - we should do a full score
				// process the candidate and the raters
				$this->cid=$cand->process();
				if(false===$this->cid){
					// an error of some kind occurred
					$this->status=$cand->getError();
					$this->audit->logSvcError('E','class.CDPCandidate.php',$cand->getCandErrorText(),'PC');
//					echo "Status (229):".$this->status."\n";
				}
				else{
					// if we're here we have the program, the candidate so now it's time
					// to process the raters
					//========================================= Added 02/27/2009
					  //Insert audit record into SVCAUDIT table and capture the record ID
				    $this->reqType='IRQ'; //Initial request					  
					  $this->auditNum=$this->audit->insertAuditRec($this->cid,
                                                      $this->req_key,
                                                      $this->reqType,
                                                      $this->bTS);
					//==========================================================
					for($i=0;$i<count($this->forms);$i++){
						// iterate over each rater
						$rat=new CDPRater($this->cid,$this->prog['instrument_Type'],$this->demos[$i],$this->forms[$i]);
						$rid=$rat->process();
//						echo "RID: $rid\n";
						if(false==$rid){
							$this->status=$rat->getError();
					    $this->audit->logSvcError('E','class.CDPRater.php',$rat->getRatErrorText(),'PC');							
							break;
						}
					}
					//========================================= Added 02/27/2009
					   $this->eTS=$this->audit->getTS();
					   $this->audit->updateAuditRec($this->auditNum,
                                          $this->cid,
                                          $this->req_key,
                                          $this->reqType,
                                          $this->eTS);
					//==========================================================
				}
			}
		}
		
// 		Debugging only!
//		echo "\nPID: $pid***********************\n";
//		echo "\nCID: ".$this->cid."***********************\n";
//		echo var_dump($this->prog);
//		echo var_dump($this->cand);
//		echo var_dump($this->demo);
//		echo var_dump($this->form);
	}

}

function charset_decode_utf_8($string){
    if (! ereg("[\200-\237]", $string) and ! ereg("[\241-\377]", $string)) 
        return $string;
    
    return utf8_decode($string);
}

function fixEncoding($in_str){ 
  $cur_encoding = mb_detect_encoding($in_str) ; 
  if($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")) 
    return $in_str; 
  else 
    return utf8_encode($in_str); 
}

function utf16_to_utf8($str) {
  // Per Don Norton's email "The utf encofing is utf-16. It�s been this way all along", 
  // we are placing this function to convert UTF16 which PHP doesn't handle well to 
  // UTF8 before inserting into MySQL.
    $c0 = ord($str[0]);
    $c1 = ord($str[1]);
 
    if ($c0 == 0xFE && $c1 == 0xFF) {
        $be = true;
    } else if ($c0 == 0xFF && $c1 == 0xFE) {
        $be = false;
    } else {
        return $str;
    }
 
    $str = substr($str, 2);
    $len = strlen($str);
    $dec = '';
    for ($i = 0; $i < $len; $i += 2) {
        $c = ($be) ? ord($str[$i]) << 8 | ord($str[$i + 1]) : 
                ord($str[$i + 1]) << 8 | ord($str[$i]);
        if ($c >= 0x0001 && $c <= 0x007F) {
            $dec .= chr($c);
        } else if ($c > 0x07FF) {
            $dec .= chr(0xE0 | (($c >> 12) & 0x0F));
            $dec .= chr(0x80 | (($c >>  6) & 0x3F));
            $dec .= chr(0x80 | (($c >>  0) & 0x3F));
        } else {
            $dec .= chr(0xC0 | (($c >>  6) & 0x1F));
            $dec .= chr(0x80 | (($c >>  0) & 0x3F));
        }
    }
    return $dec;
}
?>
