<?php
// This represensts a 360 Report
require_once "../meta/score.php";
require_once "../meta/individualreport.php";

class Report{
	var $cid;
	var $pid;
	var $status=0;
	var $pdf="";
	var $rawscales="\n";
	
	function getStatus(){
		return $this->status;
	}

	function getPDF(){
		return $this->pdf;
	}
	
	function Report($cid,$pid){
		$this->cid=$cid;
		$this->pid=$pid;
	}

	function process(){
		$this->doScore();
		$this->doScales();
		$this->doReport();
	}
	
	// Scoring
	function doScore(){
		if(0==$this->status){
			$cid=$this->cid;
			$pid=$this->pid;
			
			// -1. Clean out old stuff
			getRidOfOldScores($cid,$pid);
		
			// 0. see if this candidate exists
			$candNm=getScoreCandidateName($cid, $pid);
			if(!$candNm){
				$this->status=810;	
			}
			else{
				// 1. See how many raters we have in each category
				$ratNum=getValidRaterCount($cid);
				
				// 2. Calculate Scale Scores
				if(0==$ratNum[1]){
					// No self rater
					$this->status=812;
//					echo var_dump($ratNum);
				}
				else{
					// if here we can calculate the self report
					$this->pdf=$this->pdf.computeRaterScaleScores($cid,$cid,$pid,"1");
		
					// calculate Boss scale scores
					if($ratNum[2]>0){
						$raters=getRaterIds($cid,"2");
						if($raters){
							foreach($raters as $rater){
								$this->pdf=$this->pdf.computeRaterScaleScores($rater[0],$cid,$pid,"2");
							}
						}
					}
		
					// calculate Peer scale scores
					if($ratNum[3]>0){
						$raters=getRaterIds($cid,"3");
						if($raters){
							foreach($raters as $rater){
								$this->pdf=$this->pdf.computeRaterScaleScores($rater[0],$cid,$pid,"3");
							}
						}
					}
		
					// calculate Direct Report scale scores
					if($ratNum[4]>0){
						$raters=getRaterIds($cid,"4");
						if($raters){
							foreach($raters as $rater){
								$this->pdf=$this->pdf.computeRaterScaleScores($rater[0],$cid,$pid,"4");
							}
						}
					}
						
					// 3. Calculate the rolled up Report Scores and Rater Agreement Scores
					$this->pdf=$this->pdf.calculateIndividualReportScores($cid,$pid,$ratNum);
				
				}
				
			}
		}
	}
	
	//Reporting 
	function doReport(){
		if(0==$this->status){
			$cid=$this->cid;
			$pid=$this->pid;
			$lid=1;
			// 5. Generate the PDF Report

			$pdf=pdf_new();
			pdf_set_parameter($pdf,"license","L700102-010500-734253-ZZ4ND2-FMQ922");
			pdf_open_file($pdf,"");

			pdf_set_info($pdf,'Creator','conflictdynamics.org');
			pdf_set_info($pdf,'Author','conflictdynamics.org');
			pdf_set_info($pdf,'Title','Conflict Dynamics Profile');

			$page=1;
			// Default to NOT showing any data for Others of any kind
			$noOther=true;
			renderPage1($cid,$pdf,$page,$noOther);
			renderPage2($cid,$pdf,$page,$noOther);
			renderPage3($cid,$pdf,$page,$noOther);
			renderPage4($cid,$pdf,$page,$noOther);
			renderPage5($cid,$pdf,$page,$noOther);
			renderPage6($cid,$pdf,$page,$noOther);
			renderPage7($cid,$pdf,$page,$noOther);
			renderPage8($cid,$pdf,$page,$noOther);
			renderPage9($cid,$pdf,$page,$noOther);
			renderPage10($cid,$pdf,$page,$noOther);
			renderPage11($cid,$pdf,$page,$noOther);
			renderPage12($cid,$pdf,$page,$noOther);
			renderPage13($cid,$pdf,$page,$noOther);
			renderPage14($cid,$pdf,$page,$noOther);
			renderPage15($cid,$pdf,$page,$noOther);
			renderPage16($cid,$pdf,$page,$noOther);
			renderPage17($cid,$pdf,$page,$noOther);
			renderPage18($cid,$pdf,$page,$noOther);
			renderPage19($cid,$pdf,$page,$noOther);
			renderPage20($cid,$pdf,$page,$noOther);
			renderPage21($cid,$pdf,$page,$noOther);
			renderPage22($cid,$pdf,$page,$noOther);
			
			pdf_close($pdf);
			$this->pdf=base64_encode(pdf_get_buffer($pdf));
			pdf_delete($pdf);
		}
	}

	function doScales(){
		if(0==$this->status){
			$conn=dbConnect();
			$query="select a.SID,a.DESCR,b.CATID,round(b.AVGSCORE,1) from SCALE a, REPORTSCORE b where a.SID=b.SID and b.CID=".$this->cid." and b.CATID<>6 order by a.SID,b.CATID";
			$rs=mysql_query($query);
			if(false==$rs){
				$this->status=804;
				return;
			}
			$rows=dbRes2Arr($rs);
			$scale="";
			$stop="";
			foreach($rows as $row){
				if($row[1]!=$scale){
					// we're starting a new scale	
					$scale=$row[1];
					$this->rawscales=$this->rawscales."$stop<scores scale='$scale' scorelength='1'>\n";
				}
				$cat=$row[2];
				$score=$row[3];
				if($cat==1||$score>0){
					// Everything for self, only non-zerp for raters
					$this->rawscales=$this->rawscales."<score rater_type='$cat'>$score</score>\n";
				}
				$stop="</scores>\n";
			}
			$this->rawscales=$this->rawscales.$stop;
		}
	}
	
	// Get the raw scales
	function getScales(){
		return $this->rawscales;
	}
	
}
?>
