<?php
require_once "class.TagStack.php";
require_once "class.Credentials.php";
require_once "class.CDPProgram.php";
require_once "class.CDPCandidate.php";
require_once "class.CDPRater.php";
require_once "class.WaitTime.php";

class InitialParser{
	
	var $tags;
	var $creds;
	var $prog;
	var $attr;
	var $cand;
	var $forms;
	var $form;
	var $demos;
	var $demo;
	var $catid=0;
	var $bio=false;	
	var $cid=-1;
	var $req_key;	// requesteor's id
	
	// status set to 0 while we're processing
	// will be set to error code or 1 on success
	var $status=0;
	var $wait=0;
	var $error=false;
	// constructor
	function InitialParser(){
		$this->tags= new TagStack();
		$this->creds=new Credentials();
		$this->prog=array();
		$this->cand=array();
		$this->forms=array();
		$this->demos=array();
	}

	// check the credentials
	function checkCredentials(){
		if($this->creds->hasCredentials()){
			$this->status=$this->creds->verify();
			if(0==$this->status){
				// succesful login, get the Consultant ID
				$this->prog['CONID']=$this->creds->getConid();
			}
		}
	}

	// The three required parser functions
	function start_element($parser,$tag,$attributes){
		// as long aa we habve no errors
		if(0==$this->status){
			$this->tags->push($tag);
			$this->attr=$attributes;
			if("form"==$tag){
				// beginning of form - create new form and demo
				$this->form=array();
				$this->demo=array();
			}
			// debugging
//			echo "\nStart of: ".$this->tags->top()." nesting ".$this->tags->count();
		}
	}


	function end_element($parser,$tag){
		// as long as we have no errors
		if(0==$this->status){
			if("soap:Envelope"==$tag){
				// the entire message has been parsed
				// process the collected data
//				$this->process();
			}
			if("form"==$tag){
				// end of the form - add to the forms and demo arrays
				$this->forms[]=$this->form;
				$this->demos[]=$this->demo;
			}
			// debugging
//			echo "\nEnd of: ".$this->tags->top()." nesting ".$this->tags->count();
			unset($this->attr);
			$this->tags->pop();
		}
	}
	
	function character_data($parser,$data){
		// as long aa we have no errors
		if(0==$this->status){
			$data=trim($data);
			$data=mysql_escape_string($data);
			$tag=$this->tags->top();
			if("response"==$tag){
				$idx=$this->attr['item'];
				if(isset($this->form[$idx])){
					// we already have things in here
					$previous=$this->form[$idx];
					$data=$previous.$data;
				}
				$this->form[$idx]=$data;
			}
			elseif("demographic"==$tag){
				$idx=$this->attr['item'];
				$this->demo[$idx]=$data;
				if(1==$this->catid){
					if($idx==19||$idx==20||$idx==3){
						// This is the self, we need to save some data
						$idx=$this->attr['item'];
						$this->cand[$idx]=$data;
					}
				}
			}
			elseif("RaterType"==$tag){
				// Note: rater type goes into demo[0]
				$this->catid=$data;
				$this->demo[0]=$data;
			}
			elseif("language"==$tag){
				$this->cand['language']=$data;
			}
			elseif("instrument_language"==$tag){
				$this->cand['instrument_language']=$data;
			}
			elseif("Login"==$tag){
echo "$tag - $data";
				$this->creds->setUID($data);
				$this->checkCredentials();
			}
			elseif("Password"==$tag){
echo "$tag - $data";
				$this->creds->setPWD($data);
				$this->checkCredentials();
			}
			elseif("EventID"==$tag){
				$this->prog['EventID']=$data;
			}
			elseif("EventName"==$tag){
				$this->prog['EventName']=$data;
			}
			elseif("EventLocation"==$tag){
				$this->prog['EventLocation']=$data;
			}
			elseif("EventDate"==$tag){
				$this->prog['EventDate']=$data;
			}
			elseif("instrument_Type"==$tag){
				if("CDPIND"==$data){
					$this->prog['instrument_Type']=3;
				}
				else{
					$this->prog['instrument_Type']=1;
				}
			}
			elseif("requestorkey"==$tag){
				$this->client['clientidentification']=$data;
				$this->cand['clientidentification']=$data;
				$this->req_key=$data;
			}
			elseif("vendorkey"==$tag){
				//echo "Set the vendorkey to |$data|";
				if(strlen(trim($data))>0){
					$this->cid=$data;
				}
				else{
					$this->cid=-1;	
				}
			}
			
		}
	}
	
	function default_handler($parser,$data){
		// Do nothing
	}
	
	function soapResponse(){
		return " Done!";
	}
}
?>

