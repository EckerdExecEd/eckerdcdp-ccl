<?php
require_once "soap/lib/nusoap.php";
require_once "../meta/mailfns.php";
$host=getSvcURLRoot()."/svc/initialrequest.php";
//$host="http://onlinecdp.org/dev/svc/initialrequest.php";
$host="http://www2.onlinecdp.org/ccl/svc/initialrequest.php";
$cl=new soapclient($host);

if($err=$cl->getError()){
	echo $err;
	exit;
}

$bod=getBody360();
//$bod=newBody();

//$bod=$cl->serializeEnvelope($bod);

$rc=$cl->send($bod,$host);

// some debugging stuff
echo "<xmp>\n";
//echo $host;
//echo phpinfo();
//echo $cl->request."\n\n";
echo $cl->response."\n";
//echo $cl->document."\n";
echo "\n</xmp>";

function getBody360(){
	$rc="<?xml version=\"1.0\"?>
			<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
			<soap:Header>
			<Routing>
				<Login>CCLTest</Login>
				<Password>ccltest010</Password>
				<ClientKey>12345</ClientKey>
				<CustomerNumber>5432</CustomerNumber>
				<ShipTo>1</ShipTo>
				<BillTo>1</BillTo>
			</Routing>
			</soap:Header>
			<soap:Body>
			<ReportContext>
				<EventID>666</EventID>
				<EventName>Test 360</EventName>
				<EventDate>08/08/2007</EventDate>
				<EventLocation>Winston Salem, NC</EventLocation>
				<BudgetCode>2314</BudgetCode>
			</ReportContext>
			<SCORING_RESPONSE>
				<scoring_request>
					<report>
						<instrument_Type>CDP360</instrument_Type>
						<report_type>1</report_type>
						<!-- will ignore for now -->
						<priority_key></priority_key>
						<request_date>10/10/2006</request_date>
						<report_options>
							<!-- 1=English -->
							<language>1</language>  
							<output_format>PDF</output_format>
							<return_scores>YES</return_scores>
						</report_options>
						<scoring_options>
							<!-- 1=English -->
							<instrument_language>1</instrument_language>
							<norm_group></norm_group> 
						</scoring_options>
					</report>
					<individuals>
						<individual> 
							<!-- This is CCLs identifier -->
							<requestorkey>600001</requestorkey>
							<!--  This is CDPs identifier -->
							<vendorkey></vendorkey> 
						</individual>
					</individuals>
					<forms>
						<form>
							<!-- 1 is \"Self\" -->
								<RaterType>1</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">Test Org</demographic>
								<demographic item=\"2\" name=\"workphone\">222.333.5555</demographic>
								<demographic item=\"3\" name=\"email\">qwert@nasa.org</demographic>
								<demographic item=\"4\" name=\"sex\">F</demographic>
								<demographic item=\"5\" name=\"age\">36</demographic>
								<demographic item=\"6\" name=\"ethnicity\">10</demographic>
								<demographic item=\"12\" name=\"ethnexpl\"></demographic>
								<demographic item=\"13\" name=\"degree\">1</demographic>
								<demographic item=\"14\" name=\"orglevel\">6</demographic>
								<demographic item=\"15\" name=\"orgtypea\">1</demographic>
								<demographic item=\"16\" name=\"orgtypeb\">3</demographic>
								<demographic item=\"17\" name=\"function\">19</demographic>
								<demographic item=\"18\" name=\"funcexpl\"></demographic>
								<demographic item=\"19\" name=\"firstname\">Bobby</demographic>
								<demographic item=\"20\" name=\"lastname\">Test</demographic>
							</demographics>
							<responses items=\"114\">
								<response item=\"1\"></response>
								<response item=\"2\">NULL</response>
								<response item=\"3\">6</response>
								<response item=\"4\"> </response>
								<response item=\"5\">5</response>
								<response item=\"6\">4</response>
								<response item=\"7\">3</response>
								<response item=\"8\">2</response>
								<response item=\"9\">1</response>
								<response item=\"10\">3</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">3</response>
								<response item=\"14\">4</response>
								<response item=\"15\">5</response>
								<response item=\"16\">4</response>
								<response item=\"17\">3</response>
								<response item=\"18\">2</response>
								<response item=\"19\">1</response>
								<response item=\"20\">3</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">3</response>
								<response item=\"24\">4</response>
								<response item=\"25\">5</response>
								<response item=\"26\">4</response>
								<response item=\"27\">3</response>
								<response item=\"28\">2</response>
								<response item=\"29\">1</response>
								<response item=\"30\">3</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">3</response>
								<response item=\"34\">4</response>
								<response item=\"35\">5</response>
								<response item=\"36\">4</response>
								<response item=\"37\">3</response>
								<response item=\"38\">2</response>
								<response item=\"39\">1</response>
								<response item=\"40\">3</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">3</response>
								<response item=\"44\">4</response>
								<response item=\"45\">5</response>
								<response item=\"46\">4</response>
								<response item=\"47\">3</response>
								<response item=\"48\">2</response>
								<response item=\"49\">1</response>
								<response item=\"50\">3</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">3</response>
								<response item=\"54\">4</response>
								<response item=\"55\">5</response>
								<response item=\"56\">4</response>
								<response item=\"57\">3</response>
								<response item=\"58\">2</response>
								<response item=\"59\">1</response>
								<response item=\"60\">3</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">3</response>
								<response item=\"64\">4</response>
								<response item=\"65\">5</response>
								<response item=\"66\">4</response>
								<response item=\"67\">3</response>
								<response item=\"68\">2</response>
								<response item=\"69\">1</response>
								<response item=\"70\">3</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">3</response>
								<response item=\"74\">4</response>
								<response item=\"75\">5</response>
								<response item=\"76\">4</response>
								<response item=\"77\">3</response>
								<response item=\"78\">2</response>
								<response item=\"79\">1</response>
								<response item=\"80\">3</response>
								<response item=\"81\">1</response>
								<response item=\"82\">2</response>
								<response item=\"83\">3</response>
								<response item=\"84\">4</response>
								<response item=\"85\">5</response>
								<response item=\"86\">4</response>
								<response item=\"87\">3</response>
								<response item=\"88\">2</response>
								<response item=\"89\">1</response>
								<response item=\"90\">3</response>
								<response item=\"91\">1</response>
								<response item=\"92\">2</response>
								<response item=\"93\">3</response>
								<response item=\"94\">4</response>
								<response item=\"95\">5</response>
								<response item=\"96\">4</response>
								<response item=\"97\">3</response>
								<response item=\"98\">2</response>
								<response item=\"99\">1</response>
								<response item=\"100\">3</response>
								<response item=\"101\">1</response>
								<response item=\"102\">2</response>
								<response item=\"103\">3</response>
								<response item=\"104\">4</response>
								<response item=\"105\">5</response>
								<response item=\"106\">4</response>
								<response item=\"107\">3</response>
								<response item=\"108\">2</response>
								<response item=\"109\">1</response>
								<response item=\"110\">3</response>
								<response item=\"111\">1</response>
								<response item=\"112\">2</response>
								<response item=\"113\">3</response>
								<response item=\"114\">4</response>
							</responses>
						</form>
						<form>
								<RaterType>2</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">aaaa</demographic>
								<demographic item=\"2\" name=\"contact\">3</demographic>
								<demographic item=\"3\" name=\"sex\">F</demographic>
								<demographic item=\"4\" name=\"age\">43</demographic>
							</demographics>
							<responses items=\"80\">
								<response item=\"1\">1</response>
								<response item=\"2\">2</response>
								<response item=\"3\">2</response>
								<response item=\"4\">3</response>
								<response item=\"5\">2</response>
								<response item=\"6\">4</response>
								<response item=\"7\">4</response>
								<response item=\"8\">2</response>
								<response item=\"9\">5</response>
								<response item=\"10\">1</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">2</response>
								<response item=\"14\">3</response>
								<response item=\"15\">2</response>
								<response item=\"16\">4</response>
								<response item=\"17\">4</response>
								<response item=\"18\">2</response>
								<response item=\"19\">5</response>
								<response item=\"20\">1</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">2</response>
								<response item=\"24\">3</response>
								<response item=\"25\">2</response>
								<response item=\"26\">4</response>
								<response item=\"27\">4</response>
								<response item=\"28\">2</response>
								<response item=\"29\">5</response>
								<response item=\"30\">1</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">2</response>
								<response item=\"34\">3</response>
								<response item=\"35\">2</response>
								<response item=\"36\">4</response>
								<response item=\"37\">4</response>
								<response item=\"38\">2</response>
								<response item=\"39\">5</response>
								<response item=\"40\">1</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">2</response>
								<response item=\"44\">3</response>
								<response item=\"45\">2</response>
								<response item=\"46\">4</response>
								<response item=\"47\">4</response>
								<response item=\"48\">2</response>
								<response item=\"49\">5</response>
								<response item=\"50\">1</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">2</response>
								<response item=\"54\">3</response>
								<response item=\"55\">2</response>
								<response item=\"56\">4</response>
								<response item=\"57\">4</response>
								<response item=\"58\">2</response>
								<response item=\"59\">5</response>
								<response item=\"60\">1</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">2</response>
								<response item=\"64\">3</response>
								<response item=\"65\">2</response>
								<response item=\"66\">4</response>
								<response item=\"67\">4</response>
								<response item=\"68\">2</response>
								<response item=\"69\">5</response>
								<response item=\"70\">1</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">2</response>
								<response item=\"74\">3</response>
								<response item=\"75\">2</response>
								<response item=\"76\">4</response>
								<response item=\"77\">4</response>
								<response item=\"78\">2</response>
								<response item=\"79\">comment</response>
								<response item=\"80\">another comment</response>
							</responses>
						</form>
						<form>
								<RaterType>3</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">aaaa</demographic>
								<demographic item=\"2\" name=\"contact\">3</demographic>
								<demographic item=\"3\" name=\"sex\">F</demographic>
								<demographic item=\"4\" name=\"age\">43</demographic>
							</demographics>
							<responses items=\"80\">
								<response item=\"1\">1</response>
								<response item=\"2\">2</response>
								<response item=\"3\">2</response>
								<response item=\"4\">3</response>
								<response item=\"5\">2</response>
								<response item=\"6\">4</response>
								<response item=\"7\">4</response>
								<response item=\"8\">2</response>
								<response item=\"9\">5</response>
								<response item=\"10\">1</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">2</response>
								<response item=\"14\">3</response>
								<response item=\"15\">2</response>
								<response item=\"16\">4</response>
								<response item=\"17\">4</response>
								<response item=\"18\">2</response>
								<response item=\"19\">5</response>
								<response item=\"20\">1</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">2</response>
								<response item=\"24\">3</response>
								<response item=\"25\">2</response>
								<response item=\"26\">4</response>
								<response item=\"27\">4</response>
								<response item=\"28\">2</response>
								<response item=\"29\">5</response>
								<response item=\"30\">1</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">2</response>
								<response item=\"34\">3</response>
								<response item=\"35\">2</response>
								<response item=\"36\">4</response>
								<response item=\"37\">4</response>
								<response item=\"38\">2</response>
								<response item=\"39\">5</response>
								<response item=\"40\">1</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">2</response>
								<response item=\"44\">3</response>
								<response item=\"45\">2</response>
								<response item=\"46\">4</response>
								<response item=\"47\">4</response>
								<response item=\"48\">2</response>
								<response item=\"49\">5</response>
								<response item=\"50\">1</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">2</response>
								<response item=\"54\">3</response>
								<response item=\"55\">2</response>
								<response item=\"56\">4</response>
								<response item=\"57\">4</response>
								<response item=\"58\">2</response>
								<response item=\"59\">5</response>
								<response item=\"60\">1</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">2</response>
								<response item=\"64\">3</response>
								<response item=\"65\">2</response>
								<response item=\"66\">4</response>
								<response item=\"67\">4</response>
								<response item=\"68\">2</response>
								<response item=\"69\">5</response>
								<response item=\"70\">1</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">2</response>
								<response item=\"74\">3</response>
								<response item=\"75\">2</response>
								<response item=\"76\">4</response>
								<response item=\"77\">4</response>
								<response item=\"78\">2</response>
								<response item=\"79\">comment</response>
								<response item=\"80\">another comment</response>
							</responses>
						</form>
						<form>
								<RaterType>3</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">aaaa</demographic>
								<demographic item=\"2\" name=\"contact\">3</demographic>
								<demographic item=\"3\" name=\"sex\">F</demographic>
								<demographic item=\"4\" name=\"age\">43</demographic>
							</demographics>
							<responses items=\"80\">
								<response item=\"1\">1</response>
								<response item=\"2\">2</response>
								<response item=\"3\">2</response>
								<response item=\"4\">3</response>
								<response item=\"5\">2</response>
								<response item=\"6\">4</response>
								<response item=\"7\">4</response>
								<response item=\"8\">2</response>
								<response item=\"9\">5</response>
								<response item=\"10\">1</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">2</response>
								<response item=\"14\">3</response>
								<response item=\"15\">2</response>
								<response item=\"16\">4</response>
								<response item=\"17\">4</response>
								<response item=\"18\">2</response>
								<response item=\"19\">5</response>
								<response item=\"20\">1</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">2</response>
								<response item=\"24\">3</response>
								<response item=\"25\">2</response>
								<response item=\"26\">4</response>
								<response item=\"27\">4</response>
								<response item=\"28\">2</response>
								<response item=\"29\">5</response>
								<response item=\"30\">1</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">2</response>
								<response item=\"34\">3</response>
								<response item=\"35\">2</response>
								<response item=\"36\">4</response>
								<response item=\"37\">4</response>
								<response item=\"38\">2</response>
								<response item=\"39\">5</response>
								<response item=\"40\">1</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">2</response>
								<response item=\"44\">3</response>
								<response item=\"45\">2</response>
								<response item=\"46\">4</response>
								<response item=\"47\">4</response>
								<response item=\"48\">2</response>
								<response item=\"49\">5</response>
								<response item=\"50\">1</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">2</response>
								<response item=\"54\">3</response>
								<response item=\"55\">2</response>
								<response item=\"56\">4</response>
								<response item=\"57\">4</response>
								<response item=\"58\">2</response>
								<response item=\"59\">5</response>
								<response item=\"60\">1</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">2</response>
								<response item=\"64\">3</response>
								<response item=\"65\">2</response>
								<response item=\"66\">4</response>
								<response item=\"67\">4</response>
								<response item=\"68\">2</response>
								<response item=\"69\">5</response>
								<response item=\"70\">1</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">2</response>
								<response item=\"74\">3</response>
								<response item=\"75\">2</response>
								<response item=\"76\">4</response>
								<response item=\"77\">4</response>
								<response item=\"78\">2</response>
								<response item=\"79\">comment</response>
								<response item=\"80\">another comment</response>
							</responses>
						</form>
						<form>
								<RaterType>3</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">aaaa</demographic>
								<demographic item=\"2\" name=\"contact\">3</demographic>
								<demographic item=\"3\" name=\"sex\">F</demographic>
								<demographic item=\"4\" name=\"age\">43</demographic>
							</demographics>
							<responses items=\"80\">
								<response item=\"1\">1</response>
								<response item=\"2\">2</response>
								<response item=\"3\">2</response>
								<response item=\"4\">3</response>
								<response item=\"5\">2</response>
								<response item=\"6\">4</response>
								<response item=\"7\">4</response>
								<response item=\"8\">2</response>
								<response item=\"9\">5</response>
								<response item=\"10\">1</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">2</response>
								<response item=\"14\">3</response>
								<response item=\"15\">2</response>
								<response item=\"16\">4</response>
								<response item=\"17\">4</response>
								<response item=\"18\">2</response>
								<response item=\"19\">5</response>
								<response item=\"20\">1</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">2</response>
								<response item=\"24\">3</response>
								<response item=\"25\">2</response>
								<response item=\"26\">4</response>
								<response item=\"27\">4</response>
								<response item=\"28\">2</response>
								<response item=\"29\">5</response>
								<response item=\"30\">1</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">2</response>
								<response item=\"34\">3</response>
								<response item=\"35\">2</response>
								<response item=\"36\">4</response>
								<response item=\"37\">4</response>
								<response item=\"38\">2</response>
								<response item=\"39\">5</response>
								<response item=\"40\">1</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">2</response>
								<response item=\"44\">3</response>
								<response item=\"45\">2</response>
								<response item=\"46\">4</response>
								<response item=\"47\">4</response>
								<response item=\"48\">2</response>
								<response item=\"49\">5</response>
								<response item=\"50\">1</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">2</response>
								<response item=\"54\">3</response>
								<response item=\"55\">2</response>
								<response item=\"56\">4</response>
								<response item=\"57\">4</response>
								<response item=\"58\">2</response>
								<response item=\"59\">5</response>
								<response item=\"60\">1</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">2</response>
								<response item=\"64\">3</response>
								<response item=\"65\">2</response>
								<response item=\"66\">4</response>
								<response item=\"67\">4</response>
								<response item=\"68\">2</response>
								<response item=\"69\">5</response>
								<response item=\"70\">1</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">2</response>
								<response item=\"74\">3</response>
								<response item=\"75\">2</response>
								<response item=\"76\">4</response>
								<response item=\"77\">4</response>
								<response item=\"78\">2</response>
								<response item=\"79\">comment</response>
								<response item=\"80\">another comment</response>
							</responses>
						</form>
						<form>
								<RaterType>4</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">aaaa</demographic>
								<demographic item=\"2\" name=\"contact\">3</demographic>
								<demographic item=\"3\" name=\"sex\">F</demographic>
								<demographic item=\"4\" name=\"age\">43</demographic>
							</demographics>
							<responses items=\"80\">
								<response item=\"1\">1</response>
								<response item=\"2\">2</response>
								<response item=\"3\">2</response>
								<response item=\"4\">3</response>
								<response item=\"5\">2</response>
								<response item=\"6\">4</response>
								<response item=\"7\">4</response>
								<response item=\"8\">2</response>
								<response item=\"9\">5</response>
								<response item=\"10\">1</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">2</response>
								<response item=\"14\">3</response>
								<response item=\"15\">2</response>
								<response item=\"16\">4</response>
								<response item=\"17\">4</response>
								<response item=\"18\">2</response>
								<response item=\"19\">5</response>
								<response item=\"20\">1</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">2</response>
								<response item=\"24\">3</response>
								<response item=\"25\">2</response>
								<response item=\"26\">4</response>
								<response item=\"27\">4</response>
								<response item=\"28\">2</response>
								<response item=\"29\">5</response>
								<response item=\"30\">1</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">2</response>
								<response item=\"34\">3</response>
								<response item=\"35\">2</response>
								<response item=\"36\">4</response>
								<response item=\"37\">4</response>
								<response item=\"38\">2</response>
								<response item=\"39\">5</response>
								<response item=\"40\">1</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">2</response>
								<response item=\"44\">3</response>
								<response item=\"45\">2</response>
								<response item=\"46\">4</response>
								<response item=\"47\">4</response>
								<response item=\"48\">2</response>
								<response item=\"49\">5</response>
								<response item=\"50\">1</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">2</response>
								<response item=\"54\">3</response>
								<response item=\"55\">2</response>
								<response item=\"56\">4</response>
								<response item=\"57\">4</response>
								<response item=\"58\">2</response>
								<response item=\"59\">5</response>
								<response item=\"60\">1</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">2</response>
								<response item=\"64\">3</response>
								<response item=\"65\">2</response>
								<response item=\"66\">4</response>
								<response item=\"67\">4</response>
								<response item=\"68\">2</response>
								<response item=\"69\">5</response>
								<response item=\"70\">1</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">2</response>
								<response item=\"74\">3</response>
								<response item=\"75\">2</response>
								<response item=\"76\">4</response>
								<response item=\"77\">4</response>
								<response item=\"78\">2</response>
								<response item=\"79\">comment</response>
								<response item=\"80\">another comment</response>
							</responses>
						</form>
						<form>
								<RaterType>4</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">aaaa</demographic>
								<demographic item=\"2\" name=\"contact\">3</demographic>
								<demographic item=\"3\" name=\"sex\">F</demographic>
								<demographic item=\"4\" name=\"age\">43</demographic>
							</demographics>
							<responses items=\"80\">
								<response item=\"1\">1</response>
								<response item=\"2\">2</response>
								<response item=\"3\">2</response>
								<response item=\"4\">3</response>
								<response item=\"5\">2</response>
								<response item=\"6\">4</response>
								<response item=\"7\">4</response>
								<response item=\"8\">2</response>
								<response item=\"9\">5</response>
								<response item=\"10\">1</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">2</response>
								<response item=\"14\">3</response>
								<response item=\"15\">2</response>
								<response item=\"16\">4</response>
								<response item=\"17\">4</response>
								<response item=\"18\">2</response>
								<response item=\"19\">5</response>
								<response item=\"20\">1</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">2</response>
								<response item=\"24\">3</response>
								<response item=\"25\">2</response>
								<response item=\"26\">4</response>
								<response item=\"27\">4</response>
								<response item=\"28\">2</response>
								<response item=\"29\">5</response>
								<response item=\"30\">1</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">2</response>
								<response item=\"34\">3</response>
								<response item=\"35\">2</response>
								<response item=\"36\">4</response>
								<response item=\"37\">4</response>
								<response item=\"38\">2</response>
								<response item=\"39\">5</response>
								<response item=\"40\">1</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">2</response>
								<response item=\"44\">3</response>
								<response item=\"45\">2</response>
								<response item=\"46\">4</response>
								<response item=\"47\">4</response>
								<response item=\"48\">2</response>
								<response item=\"49\">5</response>
								<response item=\"50\">1</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">2</response>
								<response item=\"54\">3</response>
								<response item=\"55\">2</response>
								<response item=\"56\">4</response>
								<response item=\"57\">4</response>
								<response item=\"58\">2</response>
								<response item=\"59\">5</response>
								<response item=\"60\">1</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">2</response>
								<response item=\"64\">3</response>
								<response item=\"65\">2</response>
								<response item=\"66\">4</response>
								<response item=\"67\">4</response>
								<response item=\"68\">2</response>
								<response item=\"69\">5</response>
								<response item=\"70\">1</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">2</response>
								<response item=\"74\">3</response>
								<response item=\"75\">2</response>
								<response item=\"76\">4</response>
								<response item=\"77\">4</response>
								<response item=\"78\">2</response>
								<response item=\"79\">comment</response>
								<response item=\"80\">another comment</response>
							</responses>
						</form>
						<form>
								<RaterType>4</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">aaaa</demographic>
								<demographic item=\"2\" name=\"contact\">3</demographic>
								<demographic item=\"3\" name=\"sex\">F</demographic>
								<demographic item=\"4\" name=\"age\">43</demographic>
							</demographics>
							<responses items=\"80\">
								<response item=\"1\">1</response>
								<response item=\"2\">2</response>
								<response item=\"3\">2</response>
								<response item=\"4\">3</response>
								<response item=\"5\">2</response>
								<response item=\"6\">4</response>
								<response item=\"7\">4</response>
								<response item=\"8\">2</response>
								<response item=\"9\">5</response>
								<response item=\"10\">1</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">2</response>
								<response item=\"14\">3</response>
								<response item=\"15\">2</response>
								<response item=\"16\">4</response>
								<response item=\"17\">4</response>
								<response item=\"18\">2</response>
								<response item=\"19\">5</response>
								<response item=\"20\">1</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">2</response>
								<response item=\"24\">3</response>
								<response item=\"25\">2</response>
								<response item=\"26\">4</response>
								<response item=\"27\">4</response>
								<response item=\"28\">2</response>
								<response item=\"29\">5</response>
								<response item=\"30\">1</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">2</response>
								<response item=\"34\">3</response>
								<response item=\"35\">2</response>
								<response item=\"36\">4</response>
								<response item=\"37\">4</response>
								<response item=\"38\">2</response>
								<response item=\"39\">5</response>
								<response item=\"40\">1</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">2</response>
								<response item=\"44\">3</response>
								<response item=\"45\">2</response>
								<response item=\"46\">4</response>
								<response item=\"47\">4</response>
								<response item=\"48\">2</response>
								<response item=\"49\">5</response>
								<response item=\"50\">1</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">2</response>
								<response item=\"54\">3</response>
								<response item=\"55\">2</response>
								<response item=\"56\">4</response>
								<response item=\"57\">4</response>
								<response item=\"58\">2</response>
								<response item=\"59\">5</response>
								<response item=\"60\">1</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">2</response>
								<response item=\"64\">3</response>
								<response item=\"65\">2</response>
								<response item=\"66\">4</response>
								<response item=\"67\">4</response>
								<response item=\"68\">2</response>
								<response item=\"69\">5</response>
								<response item=\"70\">1</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">2</response>
								<response item=\"74\">3</response>
								<response item=\"75\">2</response>
								<response item=\"76\">4</response>
								<response item=\"77\">4</response>
								<response item=\"78\">2</response>
								<response item=\"79\">comment</response>
								<response item=\"80\">another comment</response>
							</responses>
						</form>
						<form>
								<RaterType>4</RaterType>
							<demographics>
								<demographic item=\"1\" name=\"orgname\">aaaa</demographic>
								<demographic item=\"2\" name=\"contact\">3</demographic>
								<demographic item=\"3\" name=\"sex\">F</demographic>
								<demographic item=\"4\" name=\"age\">43</demographic>
							</demographics>
							<responses items=\"80\">
								<response item=\"1\">1</response>
								<response item=\"2\">2</response>
								<response item=\"3\">2</response>
								<response item=\"4\">3</response>
								<response item=\"5\">2</response>
								<response item=\"6\">4</response>
								<response item=\"7\">4</response>
								<response item=\"8\">2</response>
								<response item=\"9\">5</response>
								<response item=\"10\">1</response>
								<response item=\"11\">1</response>
								<response item=\"12\">2</response>
								<response item=\"13\">2</response>
								<response item=\"14\">3</response>
								<response item=\"15\">2</response>
								<response item=\"16\">4</response>
								<response item=\"17\">4</response>
								<response item=\"18\">2</response>
								<response item=\"19\">5</response>
								<response item=\"20\">1</response>
								<response item=\"21\">1</response>
								<response item=\"22\">2</response>
								<response item=\"23\">2</response>
								<response item=\"24\">3</response>
								<response item=\"25\">2</response>
								<response item=\"26\">4</response>
								<response item=\"27\">4</response>
								<response item=\"28\">2</response>
								<response item=\"29\">5</response>
								<response item=\"30\">1</response>
								<response item=\"31\">1</response>
								<response item=\"32\">2</response>
								<response item=\"33\">2</response>
								<response item=\"34\">3</response>
								<response item=\"35\">2</response>
								<response item=\"36\">4</response>
								<response item=\"37\">4</response>
								<response item=\"38\">2</response>
								<response item=\"39\">5</response>
								<response item=\"40\">1</response>
								<response item=\"41\">1</response>
								<response item=\"42\">2</response>
								<response item=\"43\">2</response>
								<response item=\"44\">3</response>
								<response item=\"45\">2</response>
								<response item=\"46\">4</response>
								<response item=\"47\">4</response>
								<response item=\"48\">2</response>
								<response item=\"49\">5</response>
								<response item=\"50\">1</response>
								<response item=\"51\">1</response>
								<response item=\"52\">2</response>
								<response item=\"53\">2</response>
								<response item=\"54\">3</response>
								<response item=\"55\">2</response>
								<response item=\"56\">4</response>
								<response item=\"57\">4</response>
								<response item=\"58\">2</response>
								<response item=\"59\">5</response>
								<response item=\"60\">1</response>
								<response item=\"61\">1</response>
								<response item=\"62\">2</response>
								<response item=\"63\">2</response>
								<response item=\"64\">3</response>
								<response item=\"65\">2</response>
								<response item=\"66\">4</response>
								<response item=\"67\">4</response>
								<response item=\"68\">2</response>
								<response item=\"69\">5</response>
								<response item=\"70\">1</response>
								<response item=\"71\">1</response>
								<response item=\"72\">2</response>
								<response item=\"73\">2</response>
								<response item=\"74\">3</response>
								<response item=\"75\">2</response>
								<response item=\"76\">4</response>
								<response item=\"77\">4</response>
								<response item=\"78\">2</response>
								<response item=\"79\">comment</response>
								<response item=\"80\">another comment</response>
							</responses>
						</form>
					</forms>
					<optionals> 
						<!-- not sure how we would use this ? -->
						<scoring_parameter>
						</scoring_parameter>
						<report_parameter>
						</report_parameter>
					</optionals>
				</scoring_request>
			</SCORING_RESPONSE>
			</soap:Body>
			</soap:Envelope>";
	return $rc;	
}

function newBody(){
	$rc="<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
  <soap:Header>
    <Routing>
      <Login>CCLTest</Login>
      <Password>ccltest010</Password>
      <ClientKey>92284</ClientKey>
      <CustomerNumber>312049</CustomerNumber>
      <ShipTo>335704</ShipTo>
      <BillTo>335704</BillTo>
    </Routing>
  </soap:Header>
  <soap:Body>
<SCORING_RESPONSE>
<scoring_request>
		<ReportContext>
			<EventID>28760</EventID>
			<EventName>Test Event 3</EventName>
			<EventDate>12/30/2006</EventDate>
			<EventLocation>Greensboro, NC</EventLocation>
			<BudgetCode>048-49484</BudgetCode>
		</ReportContext>
				<report>
					<instrument_Type>CDP360</instrument_Type>
					<report_type>3</report_type>
					<priority_key>1</priority_key>
					<request_date>12/11/2006</request_date>
					<report_options>
						<language>1</language>  
						<output_format>PDF</output_format>
						<return_scores>YES</return_scores>
					</report_options>
					<scoring_options>					
						<instrument_language>1</instrument_language>
						<norm_group>1</norm_group> 
					</scoring_options>
				</report>
				<individuals>
					<individual> 		
						<requestorkey>68894</requestorkey>		
						<vendorkey></vendorkey> 
					</individual>
				</individuals>
				<forms>
					<form>
						<RaterType>1</RaterType>
						<demographics>
							<demographic item=\"1\" name=\"orgname\">Eastridge</demographic>
							<demographic item=\"2\" name=\"workphone\">336-549-5951</demographic>
							<demographic item=\"3\" name=\"email\">jshannon@eastrige.net</demographic>
							<demographic item=\"4\" name=\"sex\">M</demographic>
							<demographic item=\"5\" name=\"age\">33</demographic>
							<demographic item=\"6\" name=\"ethnicity\">9</demographic>
							<demographic item=\"12\" name=\"ethnexpl\"></demographic>
							<demographic item=\"13\" name=\"degree\">2</demographic>
							<demographic item=\"14\" name=\"orglevel\"></demographic>
							<demographic item=\"15\" name=\"orgtypea\"></demographic>
							<demographic item=\"16\" name=\"orgtypeb\"></demographic>
							<demographic item=\"17\" name=\"function\"></demographic>
							<demographic item=\"18\" name=\"funcexpl\"></demographic>
							<demographic item=\"19\" name=\"firstname\">Ingersoll</demographic>
							<demographic item=\"20\" name=\"lastname\">Rand</demographic>
						</demographics>
						<responses items=\"114\">
							<response item=\"1\">2</response>
							<response item=\"2\">2</response>
							<response item=\"3\">2</response>
							<response item=\"4\">2</response>
							<response item=\"5\">2</response>
							<response item=\"6\">2</response>
							<response item=\"7\">2</response>
							<response item=\"8\">2</response>
							<response item=\"9\">2</response>
							<response item=\"10\">2</response>
							<response item=\"11\">2</response>
							<response item=\"12\">2</response>
							<response item=\"13\">2</response>
							<response item=\"14\">2</response>
							<response item=\"15\">2</response>
							<response item=\"16\">2</response>
							<response item=\"17\">2</response>
							<response item=\"18\">2</response>
							<response item=\"19\">2</response>
							<response item=\"20\">2</response>
							<response item=\"21\">2</response>
							<response item=\"22\">2</response>
							<response item=\"23\">2</response>
							<response item=\"24\">2</response>
							<response item=\"25\">2</response>
							<response item=\"26\">2</response>
							<response item=\"27\">2</response>
							<response item=\"28\">2</response>
							<response item=\"29\">2</response>
							<response item=\"30\">2</response>
							<response item=\"31\">2</response>
							<response item=\"32\">2</response>
							<response item=\"33\">2</response>
							<response item=\"34\">2</response>
							<response item=\"35\">2</response>
							<response item=\"36\">2</response>
							<response item=\"37\">2</response>
							<response item=\"38\">2</response>
							<response item=\"39\">2</response>
							<response item=\"40\">2</response>
							<response item=\"41\">2</response>
							<response item=\"42\">2</response>
							<response item=\"43\">2</response>
							<response item=\"44\">2</response>
							<response item=\"45\">2</response>
							<response item=\"46\">2</response>
							<response item=\"47\">2</response>
							<response item=\"48\">2</response>
							<response item=\"49\">2</response>
							<response item=\"50\">2</response>
							<response item=\"51\">2</response>
							<response item=\"52\">2</response>
							<response item=\"53\">2</response>
							<response item=\"54\">2</response>
							<response item=\"55\">1</response>
							<response item=\"56\">1</response>
							<response item=\"57\">1</response>
							<response item=\"58\">1</response>
							<response item=\"59\">1</response>
							<response item=\"60\">1</response>
							<response item=\"61\">1</response>
							<response item=\"62\">1</response>
							<response item=\"63\">1</response>
							<response item=\"64\">1</response>
							<response item=\"65\">1</response>
							<response item=\"66\">1</response>
							<response item=\"67\">1</response>
							<response item=\"68\">1</response>
							<response item=\"69\">1</response>
							<response item=\"70\">1</response>
							<response item=\"71\">1</response>
							<response item=\"72\">1</response>
							<response item=\"73\">1</response>
							<response item=\"74\">1</response>
							<response item=\"75\">1</response>
							<response item=\"76\">1</response>
							<response item=\"77\">1</response>
							<response item=\"78\">1</response>
							<response item=\"79\">1</response>
							<response item=\"80\">1</response>
							<response item=\"81\">1</response>
							<response item=\"82\">1</response>
							<response item=\"83\">1</response>
							<response item=\"84\">1</response>
							<response item=\"85\">1</response>
							<response item=\"86\">1</response>
							<response item=\"87\">1</response>
							<response item=\"88\">1</response>
							<response item=\"89\">1</response>
							<response item=\"90\">1</response>
							<response item=\"91\">1</response>
							<response item=\"92\">1</response>
							<response item=\"93\">1</response>
							<response item=\"94\">1</response>
							<response item=\"95\">1</response>
							<response item=\"96\">1</response>
							<response item=\"97\">1</response>
							<response item=\"97\">1</response>
							<response item=\"98\">1</response>
							<response item=\"99\">1</response>
							<response item=\"100\">1</response>
							<response item=\"101\">1</response>
							<response item=\"102\">1</response>
							<response item=\"103\">1</response>
							<response item=\"104\">1</response>
							<response item=\"105\">1</response>
							<response item=\"106\">1</response>
							<response item=\"107\">1</response>
							<response item=\"108\">1</response>
							<response item=\"109\">1</response>
							<response item=\"110\">1</response>
							<response item=\"111\">1</response>
							<response item=\"112\">1</response>
							<response item=\"113\">1</response>
							<response item=\"114\">1</response>
						</responses>
					</form>
					<form>
						<RaterType>2</RaterType>
						<demographics>
							<demographic item=\"4\" name=\"sex\">F</demographic>
							<demographic item=\"5\" name=\"age\">24</demographic>
							<demographic item=\"6\" name=\"ethnicity\">10</demographic>
						</demographics>
						<responses items=\"80\">
							<response item=\"1\">2</response>
							<response item=\"2\">2</response>
							<response item=\"3\">2</response>
							<response item=\"4\">2</response>
							<response item=\"5\">2</response>
							<response item=\"6\">2</response>
							<response item=\"7\">2</response>
							<response item=\"8\">2</response>
							<response item=\"9\">2</response>
							<response item=\"10\">2</response>
							<response item=\"11\">2</response>
							<response item=\"12\">2</response>
							<response item=\"13\">2</response>
							<response item=\"14\">2</response>
							<response item=\"15\">2</response>
							<response item=\"16\">2</response>
							<response item=\"17\">2</response>
							<response item=\"18\">2</response>
							<response item=\"19\">2</response>
							<response item=\"20\">2</response>
							<response item=\"21\">2</response>
							<response item=\"22\">2</response>
							<response item=\"23\">2</response>
							<response item=\"24\">2</response>
							<response item=\"25\">2</response>
							<response item=\"26\">2</response>
							<response item=\"27\">2</response>
							<response item=\"28\">2</response>
							<response item=\"29\">2</response>
							<response item=\"30\">2</response>
							<response item=\"31\">2</response>
							<response item=\"32\">2</response>
							<response item=\"33\">2</response>
							<response item=\"34\">2</response>
							<response item=\"35\">2</response>
							<response item=\"36\">2</response>
							<response item=\"37\">2</response>
							<response item=\"38\">2</response>
							<response item=\"39\">2</response>
							<response item=\"40\">2</response>
							<response item=\"41\">2</response>
							<response item=\"42\">2</response>
							<response item=\"43\">2</response>
							<response item=\"44\">2</response>
							<response item=\"45\">2</response>
							<response item=\"46\">2</response>
							<response item=\"47\">2</response>
							<response item=\"48\">2</response>
							<response item=\"49\">2</response>
							<response item=\"50\">2</response>
							<response item=\"51\">2</response>
							<response item=\"52\">2</response>
							<response item=\"53\">2</response>
							<response item=\"54\">2</response>
							<response item=\"55\">1</response>
							<response item=\"56\">1</response>
							<response item=\"57\">1</response>
							<response item=\"58\">1</response>
							<response item=\"59\">1</response>
							<response item=\"60\">1</response>
							<response item=\"61\">1</response>
							<response item=\"62\">1</response>
							<response item=\"63\">1</response>
							<response item=\"64\">1</response>
							<response item=\"65\">1</response>
							<response item=\"66\">1</response>
							<response item=\"67\">1</response>
							<response item=\"68\">1</response>
							<response item=\"69\">1</response>
							<response item=\"70\">1</response>
							<response item=\"71\">1</response>
							<response item=\"72\">1</response>
							<response item=\"73\">1</response>
							<response item=\"74\">1</response>
							<response item=\"75\">1</response>
							<response item=\"76\">1</response>
							<response item=\"77\">1</response>
							<response item=\"78\">1</response>
							<response item=\"79\">Boss Comments 1</response>
							<response item=\"80\">Boss Comments 2</response>
						</responses>
					</form>
					<form>
						<RaterType>4</RaterType>
						<demographics>
							<demographic item=\"4\" name=\"sex\">F</demographic>
							<demographic item=\"5\" name=\"age\">21</demographic>
							<demographic item=\"6\" name=\"ethnicity\">9</demographic>
						</demographics>
						<responses items=\"80\">
							<response item=\"1\">2</response>
							<response item=\"2\">2</response>
							<response item=\"3\">2</response>
							<response item=\"4\">2</response>
							<response item=\"5\">2</response>
							<response item=\"6\">2</response>
							<response item=\"7\">2</response>
							<response item=\"8\">2</response>
							<response item=\"9\">2</response>
							<response item=\"10\">2</response>
							<response item=\"11\">2</response>
							<response item=\"12\">2</response>
							<response item=\"13\">2</response>
							<response item=\"14\">2</response>
							<response item=\"15\">2</response>
							<response item=\"16\">2</response>
							<response item=\"17\">2</response>
							<response item=\"18\">2</response>
							<response item=\"19\">2</response>
							<response item=\"20\">2</response>
							<response item=\"21\">2</response>
							<response item=\"22\">2</response>
							<response item=\"23\">2</response>
							<response item=\"24\">2</response>
							<response item=\"25\">2</response>
							<response item=\"26\">2</response>
							<response item=\"27\">2</response>
							<response item=\"28\">2</response>
							<response item=\"29\">2</response>
							<response item=\"30\">2</response>
							<response item=\"31\">2</response>
							<response item=\"32\">2</response>
							<response item=\"33\">2</response>
							<response item=\"34\">2</response>
							<response item=\"35\">2</response>
							<response item=\"36\">5</response>
							<response item=\"37\">5</response>
							<response item=\"38\">5</response>
							<response item=\"39\">5</response>
							<response item=\"40\">5</response>
							<response item=\"41\">5</response>
							<response item=\"42\">5</response>
							<response item=\"43\">5</response>
							<response item=\"44\">4</response>
							<response item=\"45\">4</response>
							<response item=\"46\">3</response>
							<response item=\"47\">1</response>
							<response item=\"48\">1</response>
							<response item=\"49\">1</response>
							<response item=\"50\">1</response>
							<response item=\"51\">1</response>
							<response item=\"52\">1</response>
							<response item=\"53\">1</response>
							<response item=\"54\">1</response>
							<response item=\"55\">1</response>
							<response item=\"56\">1</response>
							<response item=\"57\">1</response>
							<response item=\"58\">1</response>
							<response item=\"59\">1</response>
							<response item=\"60\">1</response>
							<response item=\"61\">1</response>
							<response item=\"62\">1</response>
							<response item=\"63\">1</response>
							<response item=\"64\">1</response>
							<response item=\"65\">1</response>
							<response item=\"66\">1</response>
							<response item=\"67\">1</response>
							<response item=\"68\">1</response>
							<response item=\"69\">1</response>
							<response item=\"70\">1</response>
							<response item=\"71\">1</response>
							<response item=\"72\">1</response>
							<response item=\"73\">1</response>
							<response item=\"74\">1</response>
							<response item=\"75\">1</response>
							<response item=\"76\">1</response>
							<response item=\"77\">1</response>
							<response item=\"78\">1</response>
							<response item=\"79\">Other comments 1</response>
							<response item=\"80\">Other Comments 2</response>
						</responses>
					</form>
				</forms>
			</scoring_request>
		</SCORING_RESPONSE>
  </soap:Body>
</soap:Envelope>";
	return $rc;	
}

?>
