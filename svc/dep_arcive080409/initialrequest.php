<?php
header("Content-Type: text/xml");
echo "<?xml version=\"1.0\" ?>\n";
// Entry point for the Inital SOAP request
// Note: we're using the test calss
require_once "class.InitialParser.php";

// Create a SAX parser and a helper object
$xml=xml_parser_create();
$p=new InitialParser;

// associate the helper object with the SAX parser
xml_set_object($xml,$p);

// associate the handlers
xml_set_element_handler($xml,'start_element','end_element');
xml_set_character_data_handler($xml,'character_data');
xml_set_default_handler($xml,'default_handler');
xml_parser_set_option($xml,XML_OPTION_CASE_FOLDING,false);

// parse the incoming request
//echo "POSTDATA:\n";
//echo $HTTP_RAW_POST_DATA;
//echo "END POSTDATA:\n";

$rc=xml_parse($xml,$HTTP_RAW_POST_DATA,false);
xml_parser_free($xml);

echo trim($p->soapResponse());
?>