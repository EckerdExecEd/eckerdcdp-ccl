<?php
// 02/29/2008 TRM: Added additional check to encure that only 1-5 get inserted
// into RATERRESP and everything else is NULL
require_once "../meta/rater.php";
require_once "../meta/survey.php";
class CDPRater{
	
	var $cid;
	var $demo;
	var $form;
	var $catid;
	var $err=0;
	var $tid;
	var $errortext;	
	
	function getError(){
		return $this->err;
	}

	function getRatErrorText(){ 
  		return $this->errortext;
	}
	
	function CDPRater($cid,$tid,$demo,$form){
		$this->cid=$cid;
		$this->catid=$demo[0];
		$this->demo=$demo;
		$this->form=$form;
		$this->tid=$tid;
		$this->errortext="";
	}
	
	function process(){
		// If this is anybody other than self we need to create a rater
		$rid=false;
		if(1!=$this->catid){
			// this is other than self
			// leave some fields blank since we don't have the data
			$ratData=array($this->cid,$this->catid,'','','',2);
			$rid=addRater($ratData);
		}
		else{
			// this is self
			// we already have the Self rater, so we don't have to add
			// her
			$rid=$this->cid;
		}
		if(false==$rid){
			$this->err=920;
			$this->errortext.="<br>Error 920 - rid is false for CID=".$this->cid." and CATID=".$this->catid." <br>";
			return false;
		}
		
		// we now have a valid rater
		// 1. Process demographics data
		if(false==$this->addDemogr($rid)){
			$this->err=921;
			$this->errortext.="<br>Error 921 - Unable to add demographics for Rater $rid under CID ".$this->cid."<br>";
			return false;
		}
		
		// 2. Process the items
		if(false==$this->addResponse($rid)){
			$this->err=922;
			$this->errortext.="<br>Error 922 - Unable to add Responses for Rater $rid under CID ".$this->cid."<br>";			
			return false;
		}
		
		// 3. Process the comments
		if(false==$this->addComment($rid)){			
			$this->err=923;
			$this->errortext.="<br>Error 923 - Unable to add comments for Rater $rid under CID ".$this->cid."<br>";			
			return false;
		}

		// Mark it as complete
		if(false==$this->expireTheRater($rid)){
			$this->err=924;
			$this->errortext.="<br>Error 924 - Unable to mark $rid as complete under CID ".$this->cid."<br>";			
			return false;
		}
		
		return $rid;
	}
	
	// Add Demographics
	function addDemogr($rid){
		$query="insert into ";
		if($this->catid==1){
			// Self
			$query=$query." DEMOGR (CID,DMID,TID,TXT) values ";
			$end=18;
		}
		else{
			// Other
			$query=$query." RATERDEMOGR (RID,DMID,TID,TXT) values ";
			$end=4;
		}
		$comma="";
		for($i=1;$i<=$end;$i++){
		  $cktid=(isset($this->tid))? $this->tid : "";
		  $ckdemo=(isset($this->demo[$i]))? $this->demo[$i] : "";
		  $query=$query."$comma($rid,$i,".$cktid.",'".$ckdemo."') ";
			$comma=",";
		}
		$conn=dbConnect();
		$rs=mysql_query($query);
		if($conn){ mysql_close($conn); } // 05-Mar-2009 ???
		return $rs;
	}
	
	// Add the responses to the Items
	function addResponse($rid){
		$end=78;
		if($this->catid==1){
			if($this->tid==3){
				$end=99;
			}
			else{
				$end=114;
			}
		}
		$query="insert into RATERRESP (ITEMID,TID,RID,VAL) values ";
		$comma="";
		for($i=1;$i<=$end;$i++){
//
// 02/29/2008 Make sure everything other than 1-5 explicitly becomes NULL
//			$query=$query."$comma($i,".$this->tid.",$rid,'".$this->form[$i]."') ";

			$val=$this->form[$i];
			if($val!='1'&&$val!='2'&&$val!='3'&&$val!='4'&&$val!='5'){
				$val="NULL";
			}
			$query=$query."$comma($i,".$this->tid.",$rid,$val) ";
			$comma=",";
		}
		$conn=dbConnect();
		$rs=mysql_query($query);
		if($conn){ mysql_close($conn); } // 05-Mar-2009 ???
		return $rs;
	}
	
	function addComment($rid){
		// 2007/01/08 - Changed condition form ==2 to >=2
		// to mitigate defect 537.
		if($this->catid>=2){
			// Only for "real" raters
			$query="insert into RATERCMT (ITEMID,TID,RID,VAL) values ";
			$comma="";
			for($i=79;$i<=80;$i++){
				$query=$query."$comma($i,".$this->tid.",$rid,'".charset_decode_utf_8($this->form[$i])."') ";
				$comma=",";
			}
			$conn=dbConnect();
			$rs=mysql_query($query);
			if($conn){ mysql_close($conn); } // 05-Mar-2009 ???
			return $rs;
		}
		return true;
	}
	
	function expireTheRater($rid){
		$conn=dbConnect();
		$query="update RATER set EXPIRED='Y', STARTDT=NOW(), ENDDT=NOW() where RID=$rid";
		$rs=mysql_query($query);
		if($conn){ mysql_close($conn); } // 05-Mar-2009 ???
		return $rs;
	}
	
}
?>
