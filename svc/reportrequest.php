<?php
header("Content-Type: text/xml");
// Entry point for the Report SOAP request
require_once "class.ReportParser.php";

$xml=xml_parser_create();
$p=new ReportParser($HTTP_RAW_POST_DATA);

// associate the helper object with the SAX parser
xml_set_object($xml,$p);

// associate teh handlers
xml_set_element_handler($xml,'start_element','end_element');
xml_set_character_data_handler($xml,'character_data');
xml_parser_set_option($xml,XML_OPTION_CASE_FOLDING,false);

// parse the incoming request
//echo "POSTDATA:\n";
//echo $HTTP_RAW_POST_DATA;
//echo "END POSTDATA:\n";

$rc=xml_parse($xml,$HTTP_RAW_POST_DATA,false);
xml_parser_free($xml);

$resp =trim($p->soapResponse());

echo $resp;

$p->audit->updateRespAuditXML($p->auditNum,
                              $p->vendorkey,
                              $p->requestorkey,
                              'RRQ',
                              $resp,
                              $p->eTS);
/*
$p->audit->insertAuditXML($p->auditNum,
                          $p->vendorkey,
                          $p->requestorkey,
                          'RRQ',
                          $HTTP_RAW_POST_DATA,
                          $resp,
                          $p->bTS,
                          $p->eTS);
*/                          
?>
