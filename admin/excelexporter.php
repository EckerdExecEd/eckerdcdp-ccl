<?php

//require_once "dliconfig.php";

if (strlen($_REQUEST['export'])) {
	// --------------------------------
	// Export the Excel spreadsheet
	// --------------------------------
	if ($_REQUEST['export'] == 1) {

		$prefix = $_REQUEST['prefix'];

		if (strlen($_REQUEST[$prefix . 'exportNumRows']) == 0) {
			// ------------------------------------------
			// SQL query saved in session variable 
			// ------------------------------------------
			// Create Excel file
			excelExport($_REQUEST["fn"]);
		
		} else {
			// ------------------------------------------
			// Data array
			// ------------------------------------------
			$dataArray = array();
			$styleArray = array();
			$numRows = $_REQUEST[$prefix . 'exportNumRows'];	
		
			for ($i=0; $i<$numRows; $i++) {
				$numCols = count($_REQUEST[$prefix . 'exportRow' . $i]);
				for ($j=0; $j<$numCols; $j++) {
					$dataArray[$i][$j] = $_REQUEST[$prefix . 'exportRow' . $i][$j];
					if (isset($_REQUEST[$prefix . 'exportStyle' . $i . '_' . $j])) {
						$styleArray[$i][$j] = $_REQUEST[$prefix . 'exportStyle' . $i . '_' . $j];
					}
				}
			}
			
			// Create Excel file
			excelExport($_REQUEST["fn"], $dataArray, $styleArray);

		}
	
	} else {
		echo '<h4>Invalid export option.</h4>';
		
	}

}

function exportLink( $queryOrArray, 
											$filename="extraction", 
											$linkOption=0, 
											$flyoverText='Export to a Microsoft Excel file',
											$prefix='', 
											$linkName="Export to Excel", 
											$cellStyles=array()) {
	/**
	 *	This function creates a link to export a SQL query or an array of data to a Microsoft Excel 
	 *	spreadsheet.
	 *
	 *	Parameters:
	 *		$queryOrArray - This can either be an SQL query or a two dimension array of data.
	 *		                You can only have one export link on a page that uses an SQL query.  (This
	 *		                is because we use a session variable.)  You can have multiple export links
	 *		                that use data arrays on the same page, but you must set the $prefix variable
	 *		                to be unique for each one.
	 *		$filename     - This is the default name of the Excel file that is created by clicking
	 *		                on the link.
	 *		$linkOption   - This is used to define the link appearances.  It can have the following values:
	 *		                      0 - Show link name and Excel icon
	 *		                      1 - Show only link name
	 *		                      2 - Show only Excel icon
	 *		$flyoverText  - The text that is displayed when the cursor is placed over the Excel link.
	 *		$prefix 			- Appended to all hidden variables (Only needed if you have more than one
	 *	  	              export link that uses a data array on a page that use a data array.)
	 *		$linkName			- The name of the Excel link.  This is not display if $linkOption=2.
	 *
	 *	NOTES: 1. You can only have one export link that uses an SQL query per page. (Because we use a session variable.)
	 *	       2. You can have multiple export links per page, but you must specify a unique $prefix
	 *	          for each one.
	 **/
										
  
	$thisUrl = 'http://www.onlinecdp.org/ccl/admin/excelexporter.php';
											
	if (is_array($queryOrArray)) {
		// $queryOrArray is an array of data
		// Create a form and save array in hidden variables
		$form .= "\n" . '<form name="frm' . $prefix . 'exportData" action="' . $thisUrl . '?export=1&fn=' . $filename . '&prefix=' . $prefix . '" method="POST" style="margin:0px;">';
		$numRows = count($queryOrArray);
		for ($i=0; $i<$numRows; $i++) {
			$numCols = count($queryOrArray[$i]);
			for ($j=0; $j<$numCols; $j++) {
				$value = $queryOrArray[$i][$j];
				$form .= "\n" . '<input type="hidden" name="' .  $prefix . 'exportRow' . $i . '[]" value="' . $value . '" />';
			} 
		}
		foreach ($cellStyles as $row=>$cols) {
			foreach($cols as $col=>$value) {
				if (strpos($value, '"') === false) $form .= "\n" . '<input type="hidden" name="' .  $prefix . 'exportStyle' . $row  . '_' . $col . '" value="' . $value . '" />';
					else $form .= "\n" . '<input type="hidden" name="' .  $prefix . 'exportStyle' . $row  . '_' . $col . '" value=\'' . $value . '\' />';
			}
		}
		$form .= "\n" . '	<a href="javascript:' .  $prefix . 'exportToExcel()" title="' . str_replace('"', '&quot;', htmlentities($flyoverText)) . '" style="vertical-align:top;margin:0px;">';
		if (($linkOption == 0) || ($linkOption == 1)) $form .= str_replace(" ", "&nbsp;", htmlentities($linkName));
		if (($linkOption == 0) || ($linkOption == 2)) $form .= '<img src="../images/excel.gif" border="0" style="margin-left:6px;">';
		$form .= '</a>';
		$form .= "\n" . '<input type="hidden" name="' .  $prefix . 'exportNumRows" value="' . $numRows . '" />';
		$form .= "\n" . '<input type="hidden" name="' .  $prefix . 'exportNumCols" value="' . $numCols . '" />';
		$form .= "\n" . '</form>';
		$form .= "\n" . '<script language="JavaScript">';
		$form .= "\n" . 'function ' .  $prefix . 'exportToExcel() {';
		$form .= "\n\t" . 'document.forms["frm' .  $prefix . 'exportData"].submit();';
		$form .= "\n" . '}';
		$form .= "\n" . '</script>';

		return $form;
		
	} else {
		// $queryOrArray is an SQL query
		// Create a link and save sql query in a session variable
		session_start();
		$_SESSION["excelQuery"] = $queryOrArray;
		$link = '<a style="font:normal 10pt arial;vertical-align:center;" ';
		$link .= 'href="' . $thisUrl . '?export=1&fn=' . $filename . '" ';
		$link .= ' title="' . htmlentities($flyoverText) . '">';
		if (($linkOption == 0) || ($linkOption == 1)) $link .= str_replace(" ", "&nbsp;", htmlentities($linkName));
		if (($linkOption == 0) || ($linkOption == 2)) $link .= '<img src="../images/excel.gif" border="0" style="padding-left:6px;">';
		$link .= '</a>';
	
		return $link;

	}
}

function excelExport( $filename='extraction', $dataArray=array(), $styleArray=array() ) {
	//------------------------------------------------------------------------
	// This function creates a Microsoft Excel spreadsheet.
	//	$filename   - the default name of the Excel spreadsheet.
	//	$dataArray  - two dimensional array of data.  If this is omitted then
	//	              the data is retrieved using the SQL query that is stored
	//                in the variable $_SESSION['excelQuery'].  The array should
	//                be of the format:
	//                     $dataArray[row][col] = value
	//	$styleArray - this is 2 dimensional array of styles for individual cells.
	//                The format of the cells is:
	//                     $styleArray[row][col]
	//                There does not need to be an entry for every cell.  This
	//                only works when a data array is used, i.e. not the
	//                $_SESSION['excelQuery'].  The styles must be valid
	//                Excel format.  Some examples are:
	//                     <Font ss:Color="#FF0000"/>
	//                     <Font ss:Size="14" ss:Italic="1"/>
	//                     <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
	//                     <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
	//                     <Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/> <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/><Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/><Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/></Borders>
	//                
	//------------------------------------------------------------------------
	$badChars = array( '¡',      '¢',      '£',      '¤',      '¥',      '¦',      '§',      '¨',      '©', 
										 'ª',      '«',      '¬',      '®',      '¯',      '°',      '±',      '²',      '³',  
										 '´',      'µ',      '¶',      '·',      '¸',      '¹',      'º', 
										 '»',      '¼',      '½',      '¾',      '¿', 
										 'À',      'Á',      'Â',      'Ã',      'Ä',      'Å',      'Æ', 
										 'Ç',      'È',      'É',      'Ê',      'Ë',
										 'Ì',      'Í',      'Î',      'Ï', 
										 'Ð',      'Ñ',      'Ò',      'Ó',      'Ô',      'Õ',      'Ö',      '×',      'Ø', 
										 'Ù',      'Ú',      'Û',      'Ü',      'Ý',      'Þ',      'ß',    
										 'á',      'á',      'â',      'ã',      'ä',      'å',      'æ',
										 'ç',      'è',      'é',      'ê',      'ë', 
										 'ì',      'í',      'î',      'ï',
										 'ð',      'ñ',      'ò',      'ó',      'ô',      'õ',      'ö',      '÷',      'ø',
										 'ù',      'ú',      'û',      'ü',      'ý',      'þ',      'ÿ');
	$htmlCodes = array('&#161;', '&#162;', '&#163;', '&#164;', '&#165;', '&#166;', '&#167;', '&#168;', '&#169;', 
										 '&#170;', '&#171;', '&#172;', '&#174;', '&#175;', '&#176;', '&#177;', '&#178;', '&#179;', 
										 '&#180;', '&#181;', '&#182;', '&#183;', '&#184;', '&#185;', '&#186;', 
										 '&#187;', '&#188;', '&#189;', '&#190;', '&#191;', 
										 '&#192;', '&#193;', '&#194;', '&#195;', '&#196;', '&#197;', '&#198;', 
										 '&#199;', '&#200;', '&#201;', '&#202;', '&#203;', 
										 '&#204;', '&#205;', '&#206;', '&#207;', 
										 '&#208;', '&#209;', '&#210;', '&#211;', '&#212;', '&#213;', '&#214;', '&#215;', '&#216;', 
										 '&#217;', '&#218;', '&#219;', '&#220;', '&#221;', '&#222;', '&#223;', 
										 '&#224;', '&#225;', '&#226;', '&#227;', '&#228;', '&#229;', '&#230;', 
										 '&#231;', '&#232;', '&#233;', '&#234;', '&#235;', 
										 '&#236;', '&#237;', '&#238;', '&#239;',
										 '&#240;', '&#241;', '&#242;', '&#243;', '&#244;', '&#245;', '&#246;', '&#247;', '&#248;', 
										 '&#249;', '&#250;', '&#251;', '&#252;', '&#253;', '&#254;', '&#255;');

	if (count($dataArray) == 0) {
		// ----------------------------------------
		// Get the data using an SQL query
		// ----------------------------------------

		// Execute SQL query
		session_start();
		require_once "db1.php";
		$query = $_SESSION["excelQuery"];                 
		$db->setFetchMode(DB_FETCHMODE_ASSOC);
		$export = $db->getAll($query);

		$data .= <<<EOD
<?xml version='1.0'?>
<?mso-application progid='Excel.Sheet'?>
<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>
	<Styles>
	</Styles>
	<Worksheet ss:Name='$filename.xls'>
		<Table>
EOD;

		// Column heading data
		if (count($export) > 0) {
			$data .= "\n\t\t\t<Row>";
			foreach ($export[0] as $key=>$value) {
				$data .= "\n\t\t\t\t<Cell><Data ss:Type='String'>" . $key . "</Data></Cell>";
			}
			$data .= "\n\t\t\t</Row>\n";
		}

		//Row Data
		foreach ($export as $row=>$fields) {
			if (count($fields) > 0) {
				$data .= "\n\t\t\t<Row>";
				foreach($fields as $value) {                                             
					if ((is_numeric($value)) && (substr($value, 0, 1) !== "+")) {
						$data .= "\n\t\t\t\t<Cell><Data ss:Type='Number'>" . $value . "</Data></Cell>";
					} else {
						$data .= "\n\t\t\t\t<Cell><Data ss:Type='String'>" . str_replace($badChars, $htmlCodes, $value) . "</Data></Cell>";
					}
				} 
				$data .= "\n\t\t\t</Row>\n";
			}
		} 

		//Final XML Blurb
		$data .= <<<EOD
		
		</Table>
	</Worksheet>
</Workbook>
EOD;

	} else {
		// ----------------------------------------
		// Get data from the array that was passed
		// ----------------------------------------

		$numRows = count($dataArray);

		$data .= <<<EOD
<?xml version='1.0'?>
<?mso-application progid='Excel.Sheet'?>
<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>
	<Styles>
EOD;

		for ($i=0; $i<$numRows; $i++) {
			$numCols = count($dataArray[$i]);
			for ($j=0; $j<$numCols; $j++) {
				if (isset($styleArray[$i][$j])) {
	        $data .= "\n\t\t" . '<Style ss:ID="s' . $i . '_' . $j . '">';
	        $styles = explode(';', $styleArray[$i][$j]);
	        for ($k=0; $k<count($styles); $k++) {
		        $data .= "\n\t\t\t" . $styles[$k];
		      }
	        $data .= "\n\t\t" . '</Style>';
	      }
			}
		}

		$data .= <<<EOD

	</Styles>
	<Worksheet ss:Name='$filename.xls'>
		<Table>
EOD;

		// Row Data
		for ($i=0; $i<$numRows; $i++) {
      $data .= "\n\t\t\t<Row>";
			$numCols = count($dataArray[$i]);
			for ($j=0; $j<$numCols; $j++) {
				if (isset($styleArray[$i][$j])) $styleId = 'ss:StyleID="s' . $i . '_' . $j . '"';
					else $styleId = '';
				if ((is_numeric($value)) && (substr($value, 0, 1) !== "+")) {
	        $data .= "\n\t\t\t\t<Cell $styleId><Data ss:Type='Number'>" . $dataArray[$i][$j] . "</Data></Cell>";
	      } else {
	        $data .= "\n\t\t\t\t<Cell $styleId><Data ss:Type='String'>" . str_replace($badChars, $htmlCodes, $dataArray[$i][$j]) . "</Data></Cell>";
	      }
			}
      $data .= "\n\t\t\t</Row>\n";
		}

		//Final XML Blurb
		$data .= <<<EOD

		</Table>
	</Worksheet>
</Workbook>
EOD;
	}

	if ($data == "") { 
		$data = "\n(0) Records Found!\n";                         
	}

	// -----------------------------------------------
	// Send the Excel file
	// -----------------------------------------------
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=$filename.xls;");
	header("Content-Type: application/ms-excel");
	header("Pragma: no-cache");
	header("Expires: 0");

	if (strlen($header)) {
		print "$header\n$data";  
	} else {
		echo $data;  
	}

}

?>
