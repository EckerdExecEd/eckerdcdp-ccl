<?php 
require_once "../meta/consultant.php";
require_once "admfn.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
$conid=$_POST['conid'];
$msg="";

if("save"==$_POST['what']){
    $pwd=$_POST['pwd'];
    if(chgConsultantPwd($conid,$pwd)){
	$msg="<font color='#00aa00'>Successfully changed password</font>";
    }
    else{
	$msg="<font color='#aa0000'>Error changing password</font>";
    }
}
writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Change Consultant Password",$msg);
?>
<form name="pwdfrm" action="conspwd.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="conid" value="<?=$conid?>">
<table border=1 cellpadding=5>
<tr><td>Type New Password</td><td><input type="text" name="pwd"></td></tr>
<tr><td>Retype New Password</td><td><input type="text" name="pwdrt"></td></tr>
<tr><td colspan=2><input type="button" onClick="javascript:chkForm(pwdfrm);" value="Save Changes"></td></tr>
</table>
</form>

<?php
$urls=array('listcons.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">
function chkForm(frm){
    if(frm.pwd.value.length<1){
	alert("Please provide a valid password!");
    }
    else if(frm.pwd.value!=frm.pwdrt.value){
	alert("The two passwords don't match!");
    }
    else{
	frm.what.value='save';
        frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>
