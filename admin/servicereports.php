<?php
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
require_once "admfn.php";
require_once "../meta/dbfns.php";
require_once "excelexporter.php";
$msg="";
$reqHTML="";
$rptHTML="";
$sHTML="";

$useJS=<<<EOD
<!-- datepicker -->\n
<script type="text/javascript" src="datepicker.js"></script>\n
<link rel="stylesheet" type="text/css" href="datepicker.css" />
EOD;

if(isset($_POST['what'])&&$_POST['what']=='s'){
  foreach($_POST as $key=>$val){
    $reqHTML.="$key => $val<br>";
  }

  $bdate=$_POST['bdate'];
  $edate=$_POST['edate'];
  $reqDetail=getServiceRequestDetails($bdate,$edate);
  $reqCnts=getRequestCounts($reqDetail);
  
  list($sHTML,$dHTML)=drawReqReport($reqCnts,$reqDetail,$bdate,$edate);
}else{
  list($bdate,$edate)=getBandEdates();
}

writeHead("Conflict Dynamics Profile - Admin",false,$useJS);
writeBody("CCL Service Usage Report",$msg);
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
//echo $reqHTML;
echo drawReportForm($bdate,$edate,$sHTML);
if(strlen($dHTML)>0)
  echo $dHTML;
  
?>
<script language="Javascript">
  function getReport(){
    var ok2submit=false;
    var msg="Need to validate data";
    <!-- Add form validation here -->
    ok2submit=true;
    if(ok2submit){
      document.svcrptform.what.value='s';
      document.svcrptform.submit();
    }else
      alert(msg);
  }
</script>
<?php
writeFooter(false);

//Functions for Page ===========================================================
function drawReportForm($bdate,$edate,$sHTML){
$sHTML=(strlen($sHTML)>0)? "<div style=\"font-size:110%;font-weight:bold;\">Summary:</div>".$sHTML : $sHTML;

$formHTML=<<<EOD
<form name="svcrptform" action="servicereports.php" method=POST>
 <table cellpadding=10>
  <tr>
  <td>
  <table border=1 cellpadding=5>
    <tr><td colspan=2 style="text-align:center;"></div>Request Report</div>
    <div style="font-size:70%">Enter the dates for the time period you want to review below.</div></td></tr>
    <tr><td>Begin date <span style="color:#6f6f6f;font-size:90%;">(YYYY-MM-DD)</span>:</td>
        <td><input type="text" name="bdate" size=10 value="$bdate" /><button type="button" onclick="displayDatePicker('bdate');">Select</button></td></tr>
    <tr><td>End date <span style="color:#6f6f6f;font-size:70%;">(YYYY-MM-DD)</span>:</td>
        <td><input type="text" name="edate" size=10 value="$edate" /><button type="button" onclick="displayDatePicker('edate');">Select</button></td></tr>
    <tr><td colspan=2 style="text-align:center;">
      <input type="hidden" name="what" value="">
      <button type="button" onClick="getReport();">Submit</button>
    </td></tr>
    <tr><td colspan=2 style="text-align:center;" bgcolor="palegoldenrod" >** This report only shows succesfully completed requests. **
    </td></tr>        
  </table>
  </td>
  <td>
    $sHTML
  </td>
  </tr>
 </table>
</form>
EOD;

return $formHTML;
}

function getServiceRequestDetails($bdate,$edate){
  $bdate=$bdate.' 00:00:00';
  $edate=$edate.' 59:59:59';
  $qry="SELECT a.CID, a.ALTID, c.FNAME, c.LNAME, a.RTYPE, a.STARTDT, b.ENDDT, d.PID, d.CONID, d.EXTID, d.EXTNAME, d.LOCATION, d.EXTDATE 
          FROM SVCAUDIT a, 
              (SELECT z.CID, z.ALTID, z.ENDDT 
                 FROM SVCAUDIT z
                WHERE z.RTYPE = 'RRQ'
                  AND z.ENDDT >= '$bdate' 
                  AND z.ENDDT <= '$edate')b,
               CANDIDATE c,
               WSProgram d
         WHERE a.CID = b.CID
           AND a.ALTID = b.ALTID
           AND a.CID = c.CID
           AND c.PID = d.PID
           AND a.RTYPE IN ('IRQ','BRQ')
     ORDER BY a.STARTDT ASC";
     
  $res=fetchArray($qry,'B');
  //die($qry);        
  return $res;       
}

function getRequestCounts($res){
  $reqCnts=array('ALL'=>0,'IRQ'=>0,'BRQ'=>0);
  foreach($res as $row){
    $reqCnts[$row['RTYPE']]= $reqCnts[$row['RTYPE']] + 1;
    $reqCnts['ALL']=$reqCnts['ALL']+1;
  }
 
  return $reqCnts;
}

function drawReqReport($reqCnts,$reqDetail,$bdate,$edate){
  $excelLnk=exportLink($reqDetail,'cclSvcRequests',1,'Export to a Microsoft Excel file','','Excel');
  $sHTML="<table border=1 cellpadding=5 >
           <tr><td colspan=2>Report requests made between $bdate and $edate</td></tr>
           <tr><td>Initial Report Requests:</td><td style=\"text-align:right;background-color:aliceblue;\">".$reqCnts['IRQ']."</td></tr>
           <tr><td>Bio-rescore Report Requests:</td><td style=\"text-align:right;background-color:aliceblue;\">".$reqCnts['BRQ']."</td></tr> 
           <tr><td>Total Report Requests:</td><td style=\"text-align:right;background-color:aliceblue;\">".$reqCnts['ALL']."</td></tr>  
          </table>";
  $tHTML="";        
 if($reqCnts['ALL'] > 0){
    $tHTML.="<br>
             
             <table border=1 cellpadding=5 >
              <tr><td colspan=12>
                <span style=\"position:relative;float:left;font-size:100%;font-weight:bold;\">Details:</span>
                <span style=\"position:relative;float:right;font-size:100%;font-weight:bold;color:red;\">$excelLnk</span>              </td></tr>
              <tr><th style=\"font-size:85%;color:white;background-color:#002142;\">CID</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">ALTID</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">NAME</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">REQUEST</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">REQUESTED</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">FULFILLED</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">PROGRAMID</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">CONID</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">EXTID</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">EXTNAME</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">LOCATION</th>
                  <th style=\"font-size:85%;color:white;background-color:#002142;\">EXTDATE</th>
                </tr>";
    $x=0;
    foreach($reqDetail as $row){
    $color=(($x%2)==0)? 'white' : 'aliceblue';
    $location=(strlen($row['LOCATION'])>0)? $row['LOCATION'] : 'No data';
    $tHTML.="<tr style=\"background-color:$color;\"><td>".$row['CID']."</td>
                  <td>".$row['ALTID']."</td>
                  <td>".$row['FNAME']." ".$row['LNAME']."</td>
                  <td>".$row['RTYPE']."</td>
                  <td>".$row['STARTDT']."</td>
                  <td>".$row['ENDDT']."</td>
                  <td>".$row['PID']."</td>
                  <td>".$row['CONID']."</td>
                  <td>".$row['EXTID']."</td>
                  <td>".$row['EXTNAME']."</td>
                  <td>".$location."</td>
                  <td>".$row['EXTDATE']."</td>
              </tr>";  
     $x++;
    }
             
    $tHTML.="</table>";
  }
  return array($sHTML,$tHTML);                   
}

function getBandEdates(){
  $yr=date('Y');
  $mt=date('m');
  $bdate="$yr-$mt-01";
  $rs=strtotime($bdate);
  $rs = strtotime('-1 second', strtotime('+1 month', $rs));
  $edate=date('Y-m-d', $rs);
  return array($bdate,$edate);
}
?>
