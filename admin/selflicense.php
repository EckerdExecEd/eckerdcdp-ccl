<?php 
require_once "../meta/licfns.php";
require_once "admfn.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="";

if("add"==$_POST['what']){
    $idx=$_POST['idx'];
    $qty=$_POST["qty$idx"];
    $conid=$_POST["conid$idx"];
    $pmt=$_POST["pmt$idx"];
    // Insert $qty type "3" licenses
    if(!insertLicense($conid,$qty,$pmt,"3")){
	$msg="<font color='aa0000'>Unable to add license(s)</font>";
    }
    else{
	$msg="<font color='00aa00'>Successfully added $qty license(s)</font>";
	if(!sendReceipt($conid,$qty)){
	    $msg=$msg."<br><font color='aa0000'>Unable to send email receipt</font>";
	}
    }
}
writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Self-Only License Management",$msg);
$narrow=$_POST['narrow'];
?>
<form name="listfrm" action="selflicense.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="idx" value="">
<table border=1 cellpadding=5>

<tr>
<td colspan=3 align="left">
Search:&nbsp;
<input type="text" name="narrow" value="<?=$narrow?>">&nbsp;
<input type="submit" value="Refresh View!">
</td>
</tr>

<?php
    if(!listAllLicenses($narrow,"Y","listfrm","3")){
		echo "<font color='#aa0000'>Error listing licenses</font></br>";
    }
?>

</table>
</form>

<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">
function subForm(frm,idx){
    frm.what.value="add";
    frm.idx.value=idx;
    frm.submit();
}
</script>
<?php
writeFooter(false);
?>
