<?php
require_once "admfn.php";
require_once '../meta/dbfns.php';
require_once '../meta/candidate.php';

session_start();
if(empty($_SESSION['admid'])){
  die("Not Logged in.");
}

$msg="";


if($_POST['what'] == "del"){
	$cid=$_POST['cid'];
	$conid=$_POST['conid'];
	if(!deleteCandidate($cid,$conid)){
		$msg="<span style=\"color:#aa0000;\">Error deleting candidate</span>\n";
	}
	else{
	  $msg="<span style=\"color:#aa0000;\">Candidate has been deleted.</span>\n";
	}
}

writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Candidate Search Results",$msg);

  $and = "";
  $txt = "";
  if($_GET['fname']!=""){
    $fname=$_GET['fname'];
    $txt = $fname;
    $and = "and cand.FNAME like '$fname%'";
 }
  if($_GET['lname']!=""){
    $lname=$_GET['lname'];
    $txt = $lname;
    $and = "and cand.LNAME like '$lname%'";
  }
  if($_GET['email']!=""){
    $email=$_GET['email'];
    $txt = $email;
    $and = "and cand.EMAIL like '$email%'";
  }
  $results = searchCandidates($and);

  if(count($results)==0){
    echo "<div>Search for '$txt' returned zero results.</div><br />\n";
  }
  else{
    $html = "";
  	foreach($results as $row){
      $html .= "<tr>\n";
      $html .= "<td style=\"white-space:nowrap;\"><small>$row[LNAME], $row[FNAME]</small></td>\n";
      $html .= "<td><small>$row[EMAIL]</small></td><td><small>$row[CID]</small></td>\n";
      //$html .= "<td><small>".utf8_decode($row['DESCR'])."</small></td>\n";
      $html .= "<td><small>".$row['lnam'].", ".$row['fnam']."</small></td>\n";
      $html .= "<td><small>$row[descr]</small></td>\n";
      $html .= "<td style=\"white-space:nowrap;\">\n";
      $html .= "<input type='button' value='Edit' onClick=\"javascript:frm2.cid.value='$row[CID]';frm2.what.value='edit';frm2.action='admcanddetail.php';frm2.submit();\">\n";
      $html .= "<input type='button' value='Delete' onClick=\"javascript:frm2.cid.value='$row[CID]';frm2.conid.value='$row[CONID]';frm2.what.value='del';verifyDelete();\">\n";
      $html .= "<input type='button' value='Use' onClick=\"javascript:frm2.cid.value='$row[CID]';frm2.pin.value='$row[CID]';frm2.uid.value='$row[EMAIL]';frm2.conid.value='$row[CONID]';frm2.action='admconshome.php';frm2.submit();\">\n";
      $html .= "</td></tr>";
  	}

    echo <<<EOT
    <form name="frm2" method="post">
    <input type="hidden" name="what" value="">
    <input type="hidden" name="cid" value="">
    <input type="hidden" name="pin" value="">
    <input type="hidden" name="uid" value="">
    <input type="hidden" name="conid" value="">
    <table border="1" cellpadding="5">
    <tr bgcolor="#dddddd">
    <td><small>Name</small></td>
    <td><small>Email</small></td>
    <td><small>PIN</small></td>
    <!--<td><small>Language</small></td>-->
    <td><small>Consultant</small></td>
    <td><small>Program</small></td>
    <td><small>Actions</small></td>
    </tr>
    $html
    </table>
    </form>
EOT;
  }

?>

<script type="text/javascript">
function verifyDelete(){
var answer = confirm("Are you sure you want to delete this candidate?")
if(answer) document.frm2.submit();
else return false;
}
</script>

<?php
$urls=array('candSearch.php');
$txts=array('Back');
menu($urls,$txts,"");
writeFooter(false);

function searchCandidates($and){
  $conn=dbConnect();
  //$query="select cand.*,con.FNAME as fnam,con.LNAME as lnam,con.CONID,l.DESCR,p.DESCR as descr from CONSULTANT con,CANDCONS cc,CANDIDATE cand,RATERLANG rl,LANG l,PROGRAM p where con.CONID=cc.CONID and cc.CID=cand.CID and cand.PID=p.PID and cand.PID=rl.PID and rl.LID=l.LID $and";
  $query="select cand.*,con.FNAME as fnam,con.LNAME as lnam,con.CONID,p.DESCR as descr from CONSULTANT con,CANDCONS cc,CANDIDATE cand,PROGRAM p where con.CONID=cc.CONID and cc.CID=cand.CID and cand.PID=p.PID $and";
  $rs=mysql_query($query);
  //echo $query;
	return $rs?dbRes2Arr($rs):false;
}

?>
