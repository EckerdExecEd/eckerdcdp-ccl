<?php 
require_once "../meta/consultant.php";
require_once "admfn.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="Enter consultant data, click 'Add!'";

if("add"==$_POST['what']){
    // we use email address as unique user id
    $uid=$_POST['email'];
    if(!consExists($uid)){
        $fName=addslashes($_POST['fnam']);
	$lName=addslashes($_POST['lnam']);
	$eMail=$_POST['email'];
	$pwd=addslashes($_POST['pwd']);
	$orgid=addslashes($_POST['orgid']);
	$consData=array($fName,$lName,$eMail,$uid,$pwd,'Y',$orgid);
	if(consInsert($consData)){
	    $msg="<font color='#00aa00'>Successfully added consultant.</font>";
	}
	else{
	    $msg="<font color='#aa0000'>Error adding consultant.</font>";
	 }
    }
    else{
	$msg="<font color='#aa0000'>The email address already exists.</font>";
    }
}

writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Add Consultant",$msg);
?>
<form name="addfrm" action="addcons.php" method=POST>
<input type="hidden" name="what" value="">
<table border=1 cellpadding=5>

<tr>
<td align=left>First Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="fnam" value=""></td>
</tr>

<tr>
<td align=left>Last Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="lnam" value=""></td>
</tr>

<tr>
<td align=left>E-Mail<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="email" value=""></td>
</tr>

<tr>
<td align=left>Password<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="pwd" value=""></td>
</tr>

<tr>
<td align=left>Retype Password<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="pwdrt" value=""></td>
</tr>

<tr>
<td align=left>Select Organization<font color="#ff0000">*</font></td>
<td align=left><select name="orgid"><?=showAllOrganizations()?></select></td>
</tr>

<tr>
<td colspan=2 align="left">
<input type="button" onClick="javascript:chkForm(addfrm);" value="Add!">
</td>
</tr>

</table>
</form>
<small>Mandatory fields are marked </small><font color="#ff0000">*</font>.
<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<small><u>Note:</u> Please make sure you give the new password to the Consultant. Since all passwords are stored in encrypted form, it cannot be retrieved from the system. Also, remember all passwords are case sensitive.</small>
<script language="Javascript">
function chkForm(frm){
    if(frm.pwd.value.length<1||frm.fnam.value.length<1||frm.lnam.value.length<1||frm.email.value.length<1){
	alert("Please provide all mandatory fields!");
    }
    else if(frm.pwd.value!=frm.pwdrt.value){
	alert("The two passwords don't match!");
    }
    else{
	frm.what.value='add';
        frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>
