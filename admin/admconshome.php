<?php
require_once "../meta/dbfns.php";
$msg="";
session_start();
if(empty($_SESSION['admid'])){
  die("Not Logged in.");
}
$conid=$_POST['conid'];
// do a "fake login" so that we can impersonate the Candidate
$cid=$_POST['cid'];
$conn=dbConnect();
$query="select FNAME,LNAME from CANDIDATE a where a.CID=$cid";
$rs=mysql_query($query);
$row=mysql_fetch_row($rs) or die ("Invalid candidate id");

$_SESSION['cid']=$cid;
$_SESSION['cname']=$row[0]." ".$row[1];
$msg="You are now acting on behalf of Candidate ".$_SESSION['cname']."&nbsp;&nbsp;<small>(Candidate ID=".$_SESSION['cid'].")</small><br>";
require_once("../cons/consfn.php");
writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Candidate Menu for Consultant",$msg);
$urls=array('../usr/home.php','../admin/home.php');
$txts=array('OK','Go back to Admin Functions');
menu($urls,$txts,"");
writeFooter(false);
?>

