<?php
session_start();
if(empty($_SESSION['rid'])){
	die("You are not logged in");	
}
$rid=$_SESSION['rid'];
if(empty($_SESSION['cid'])){
	die("You are not logged in");	
}
$cid=$_SESSION['cid'];
$page=$_POST['page'];
if(strlen($page)<1){
	$page=1;	
}
require_once "selffns.php";
$what=$_POST['what'];

$lid=addslashes($_POST['lid']);
if(strlen($lid)<1){
	$lid="1";	
}
// get the corrcet language text
$flid=getFLID("assess","selfonlyexit.php");
$mlTxt=getMLText($flid,"3",$lid);

$msg="";
if("save"==$what){
	// This is for the Save and Exit functionality
	$frompage=$_POST['frompage'];
	insertSurvey($cid,$rid,"3");

	if("1"==$frompage){
		$data=array();
		$data[0]="";
		$data[1]=addslashes($_POST['dm1']);
		$data[2]=addslashes($_POST['dm2']);
		$data[3]=addslashes($_POST['dm3']);
		$data[4]=addslashes($_POST['dm4']);
		$data[5]=addslashes($_POST['dm5']);
		$data[6]=addslashes($_POST['dm6']);
		$data[7]=addslashes($_POST['dm7']);
		$data[8]=addslashes($_POST['dm8']);
		$data[9]=addslashes($_POST['dm9']);
		$data[10]=addslashes($_POST['dm10']);
		$data[11]=addslashes($_POST['dm11']);
		$data[12]=addslashes($_POST['dm12']);
		$data[13]=addslashes($_POST['dm13']);
		$data[14]=addslashes($_POST['dm14']);
		$data[15]=addslashes($_POST['dm15']);
		$data[16]=addslashes($_POST['dm16']);
		$data[17]=addslashes($_POST['dm17']);
		$data[18]=addslashes($_POST['dm18']);
		saveDemographics($cid,"3",$data);
	}
	else{
		// we came from any other page
		$keys=array();
		$values=array();
		$q1=getQ1($frompage,"3");
		$qN=getQn($frompage,"3");
		for($i=$q1;$i<=$qN;$i++){
			$item="resp$i";
			$val=$_POST[$item];
			if(strlen($val)>0){
				$keys[]=$i;
				$values[]=$val;
			}
		}
		saveItems($rid,"3",$keys,$values);
	}
	$msg="$mlTxt[1]<br>";
}

if("finish"==$what){
	// And this is for finishing the survey
	$msg="$mlTxt[3]<br>";
	finishSurvey($rid);
}

writeHeader();
$pfx="";
if("8"==$lid){
	$pfx="sp";
}
?>
<form action="<?=$pfx?>survey.php" name="frm1"	method=POST>
<?=$msg?>
<?=$mlTxt[2]?><br>
<input type="Submit" value="OK"> 
</form>
<?php
writeFooter();
?>
