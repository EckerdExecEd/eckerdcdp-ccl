<?php
// Entry point for Self Only Survey
// Note: this is VERY, VERY different from the standard version!
// We have to register the candidate here
session_start();
require_once "meta.php";
require_once "../meta/dbfns.php";
require_once "candfn.php";
require_once "../meta/multilingual.php";

$err=false;
$trace=true;

//1. get all the POST data
$participantID=addslashes($_POST["participantID"]);
$programID=addslashes($_POST["programID"]);
$title=addslashes($_POST["title"]);
$assessmentID=addslashes($_POST["assessmentID"]);
$assessmentName=addslashes($_POST["assessmentName"]);
$helpDeskEmail=addslashes($_POST["helpDeskEmail"]);
$client_ID=addslashes($_POST["Client_ID"]);
$gender=addslashes($_POST["gender"]);
$email=addslashes($_POST["email"]);
$workPhone=addslashes($_POST["workPhone"]);
$workFax=addslashes($_POST["workFax"]);
$name=addslashes($_POST["name"]);
$firstName=addslashes($_POST["firstName"]);
$lastName=addslashes($_POST["lastName"]);
$clientCode=addslashes($_POST["clientCode"]);
// ML
// Added language ID
// Note the name is different due to Mercer Delta's specific requirements
// in other versions, as well as subsequent pages it's "lid"
$lid=addslashes($_POST["SS_LanguageID"]);
$cid=false;

//2. See if we need to add it
$add=candidateExists($participantID);

//3. If necessary add the candidate
if($add){
	$conid=getMDID();
	$other=(TID_360==$assessmentID)?"Y":"N";
	$pid=getPID($programID);
	$pgmData=array($pid,$firstName,$lastName,$email,"Y",$other,$conid);
	echo "<br>";
	$cid=candInsert($pgmData);
	if(false==$cid){
		$msg="Error adding CDP Candidate";
		$err=true;
	}
	else{
		if($trace){
			$msg="Added new CDP candidate - OK<br>";
		}
		// If we're here we should create the Mercer-Delta specific stuff
		$candData=array($participantID,$programID,$title,$assessmentID,$assessmentName,$helpDeskEmail,$client_ID,$gender,$email,
			$workPhone,$workFax,$name,$firstName,$lastName,$clientCode,$cid);

		$rc=insertMDParticipant($candData);
		if(false==$rc){
			$msg=$msg."Error adding Mercer-Delta Participant";
			$err=true;
		}
		else{
			if($trace){
				$msg=$msg."Added Mercer-Delta Participant - OK<br>";
			}
		}
	}

	if(!$err){
		// I we're here we added the candidate correctly
		// Set the multilingual options
		if(strlen($lid)<1){
			// default to Enlish
			$lid=ENGLISH;
		}
		if(false==insertCandidateLanguage($cid,$lid,true)){
			$msg=$msg."Error adding Mercer-Delta Participant";
			$err=true;
		}
		else{
			if($trace){
				$msg=$msg."Set Multilingual Options - OK<br>";
			}
		}
	}

}

if($err){
	die("An error occurred - Unable to proceed");	
}
// If we're here, we should have a valid participant in the database
$cid=getCID($participantID);
if(false!=$cid&&0<strlen($cid)){
	$_SESSION['cid']=$cid;
	$_SESSION['rid']=$cid;
}
if(empty($_SESSION['cid'])){
	die("You supplied an Invalid Participant ID");	
}

echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title>Internet Assessment System - Respondent</title>";
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">";
echo "<style>.ivanC10400569321381 {";
echo "VISIBILITY: hidden; POSITION: absolute";
echo "}</style></head>";
echo "<body class=\".body\">";
echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"750\" border=\"0\">";
echo "<tbody>";
echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
echo "<tr valign=\"top\" align=\"right\" bgcolor=\"#4f8d97\" height=\"22\"><td colspan=3></td></tr>";
echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
echo "<tr valign=\"center\" height=\"42\" bgcolor=\"#253355\">";
echo "<td align=\"left\"><img height=\"64\" src=\"../index_files/eclogotop.gif\" width=\"155\" border=\"0\"></td>";
echo "<td><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td>";
echo "<td align=\"right\"><img height=\"64\" src=\"../index_files/ldi-title.gif\" width=\"430\" border=\"0\"></td></tr>";
echo "</table><br>&nbsp;</br>";

// get all the page data
$flid=getFLID("md","selfonly.php");
$mlText=getMLText($flid,"3",$lid);
?>
<form action="" method=POST name="frm1">
<input type="hidden" name="page" value="1">
<input type="hidden" name="assessmentID" value="<?=$assessmentID?>">
<input type="hidden" name="participantID" value="<?=$participantID?>">
<input type="hidden" name="Completed" value="False">
<input type="hidden" name="lid" value="<?=$lid?>">
<input type="hidden" name="SS_LanguageID" value="<?=$lid?>">

<p>
<?php
//Interpersonal conflict is extremely common, both at home and in the workplace.<br>
//When such conflicts arise, there are many different ways to react, and none of<br>
//them is always right or always wrong.  The following items ask about the way<br> 
//you usually respond before, during, and after interpersonal conflicts occur<br>
//in your life.  Please answer each one as honestly and as accurately as you can.
echo $mlText[1];
?>
</p>
<p>
<?php
require_once "../meta/survey.php";
$prompt=$mlText[2];
$cnt=getNoAnswerCount($cid,"3");
if(0!=$cnt&&114>$cnt){
	//"Welcome back!<br>You have not provided answers to the following questions:<br>";
	echo $mlText[5];
	echo implode(getUnansweredItems($cid,"3"),",");
	$prompt=$mlText[3];	
}
?>
</p>
<p>
<input type="button" onClick="javascript:frm1.action='selfonlysurvey.php';frm1.submit();" value="<?=$prompt?>" >
&nbsp;&nbsp;
<input type="button" onClick="javascript:frm1.action='<?=RETURN_URL?>';frm1.submit();" value="<?=$mlText[4]?>">
</p>
</form>
</body>
</html>
<?php

// convert a string to yyy-mm-dd format
function mySQLDate($day){
	$dt=strtotime($day);
	return strftime("%Y-%m-%d",$dt);
}

// see if this Mercer-Delta candidate already exists in the database
function candidateExists($partID){
	$conn=dbConnect();
	$query="select count(*) from MDParticipant where participantID=$partID";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0]==0;
}

// get the Mercer-Delta automated user
function getMDID(){
	$conn=dbConnect();
	$rs=mysql_query("select CONID from CONSULTANT where UID='".MERCER_DELTA_CONS."'");
	$row=mysql_fetch_row($rs);
	return $row[0];
}

// Insert a new candidate    
function candInsert($candData){ 
	$conn=dbConnect();
	// This looks funny, but is correct since self is also a RATER
	$i=getKey("RATER","RID");
	// Insert the CANDIDATE row
	$query="insert into CANDIDATE (CID,PID,FNAME,LNAME,EMAIL,EXPIRED,ARCHFLAG,SELF,OTH) values ( $i,$candData[0],'$candData[1]','$candData[2]','$candData[3]','N','N','$candData[4]','$candData[5]')";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	// Insert the CANDCONS row
	$query="insert into CANDCONS (CID,CONID) values ( $i,$candData[6])";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return insertSelfRater($i);
}
    
// insert the self rater
function insertSelfRater($cid){
	$conn=dbConnect();
	$query="insert into RATER (RID,CID,CATID,FNAM,LNAM,EMAIL,STARTDT,ENDDT,EXPIRED) select CID,CID,1,FNAME,LNAME,EMAIL,STARTDT,ENDDT,EXPIRED from CANDIDATE where CID=$cid";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return insertSelfSurvey($cid);
}
    
// insert the self survey
function insertSelfSurvey($cid){
	$conn=dbConnect();
	$query="insert into RATERINSTR (RID,TID,EXPIRED) select RID,1,EXPIRED from RATER where CID=$cid and CATID=1";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return $cid;
}

// Insert the Mercer-Delta participant
function insertMDParticipant($data){
	$conn=dbConnect();
	$query="Insert into MDParticipant (participantID,programID,title,assessmentID,assessmentName,helpDeskEmail,Client_ID,gender,email, ";
	$query=$query."workPhone,workFax,name,firstName,lastName,clientCode,CID) ";
	$query=$query." values ($data[0],$data[1],'$data[2]',$data[3],'$data[4]','$data[5]',$data[6],'$data[7]','$data[8]', ";
	$query=$query."'$data[9]','$data[10]','$data[11]','$data[12]','$data[13]','$data[14]',$data[15]) ";
	if(false==mysql_query($query)){
		echo $query."<br>";
		return false;
	}
	return true;
}

// get the CID for a participant
function getCID($partID){
	$conn=dbConnect();
	$query="select CID from MDParticipant where participantID=$partID ";
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query."<br>";
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row[0];
}

// get the PID for a program
function getPID($programID){
	$conn=dbConnect();
	$query="select PID from MDData where programID=$programID ";
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query."<br>";
		return false;
	}
	$row=mysql_fetch_row($rs);
	return $row[0];
}

//
function getName($partID){
	$conn=dbConnect();
	$query="select name, firstName, lastName from MDParticipant where participantID=$partID ";
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query."<br>";
		return false;
	}
	$row=mysql_fetch_array($rs);
	return (0<strlen($row[0]))?$row[0]:$row[1]." ".$row[2];
}

?>
