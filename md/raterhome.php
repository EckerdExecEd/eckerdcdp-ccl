<?php 
$msg="";
session_start();
if(empty($_SESSION['cid'])){
    die("Not Logged in.");
}
require_once "meta.php";
require_once "../meta/candidate.php";
require_once "../meta/rater.php";
require_once "candfn.php";
$msg="";
$rid=$_POST['rid'];
$cid=$_SESSION['cid'];
$catid=$_POST['catid'];
$what=$_POST['what'];
$participantID=addslashes($_POST["ParticipantID"]);
$programID=addslashes($_POST["ProgramID"]);
$assessmentID=addslashes($_POST["AssessmentID"]);

if("add"==$what){
//	$idx=$_POST['idx'];
	$email=addslashes($_POST["email"]);
	if(!raterEmailExists($cid,$email)){
		$ratData=array();
		$ratData[0]=$cid;
		$ratData[1]=$catid;
		$ratData[2]=addslashes($_POST["fname"]);
		$ratData[3]=addslashes($_POST["lname"]);
		$ratData[4]=$email;
		$ratData[5]="2";
		if(1>strlen($email)||1>strlen($ratData[2])||1>strlen($ratData[3])){
				$msg="<font color=\"#aa0000\">You must provide first name, last name and email address</font>";
		}
		else{
			$rid=addRater($ratData);
			if(!$rid){
				$msg="<font color=\"#aa0000\">Unable to add rater</font>";
			}
//			else{
//				$msg="<font color=\"#00aa000\">Successfully added rater</font>";
//			}
		}
	}
	else{
		$msg="<font color=\"#aa0000\">Email address already exists</font>";
	}
}
// Delete an existing rater
if("del"==$what){
	$i=deleteRater($rid);
	if(!$rid){
		$msg="<font color=\"#aa0000\">Unable to delete rater</font>";
	}
	else{
		$msg="<font color=\"#00aa000\">Successfully deleted rater</font>";
	}
}

// Save rater data
if("save"==$what){
	$idx=$_POST['idx'];
	$email=addslashes($_POST["email$idx"]);
	if(!raterEmailExists($cid,$email,$rid)){
		$ratData=array();
		$ratData[0]=$cid;
		$ratData[1]=$catid;
		$ratData[2]=addslashes($_POST["fname$idx"]);
		$ratData[3]=addslashes($_POST["lname$idx"]);
		$ratData[4]=$email;
		$ratData[5]=$rid;
		if(!saveRater($ratData)){
			$msg="<font color=\"#aa0000\">Unable to save data</font>";
		}
		else{
			$msg="<font color=\"#00aa000\">Successfully saved data</font>";
		}
	}
	else{
		$msg="<font color=\"#aa0000\">Email address already exists</font>";
	}
}

writeHead("Conflict Dynamics Profile - Candidates",false);
writeBody("Please use this page to register your raters",$msg);
?>
<!-- 06012006: set type=RR in query string -->
<form action="raterhome.php?type=RR" name="frm1" method=POST>
<form name="frm1" action="raterhome.php" method=POST>
<input type="hidden" name="cid" value="<?=$cid?>">
<input type="hidden" name="rid" value="">
<input type="hidden" name="what" value="">
<input type="hidden" name="idx" value="">
<input type="hidden" name="type" value="RR">
<input type="hidden" name="completed" value="true">
<input type="hidden" name="AssessmentID" value="<?=$assessmentID?>">
<input type="hidden" name="ProgramID" value="<?=$programID?>">
<input type="hidden" name="ParticipantID" value="<?=$participantID?>">
<p>
<u>Instructions for adding raters:</u><br>
<b>You must add raters one at a time.</b> For each new rater, enter their first name, last name, email address and indicate their<br> 
relationship to you. Then click the <b>Add</b> button. (Click here to review the <a href="raterhint.php" target="tips">Rater Selection Tips</a>.)
</p>
<p>
If you are unable to complete your Rater Selection during this session, you may exit by clicking the <b>Logout</b> button. This will<br>
return you to the Assessments page. If you logout your Added raters will be saved, but marked incomplete until you log back in and<br>
submit your raters as final.
</p>
<p>
When you have completed registering all raters, click on the <b>Save as Final Selection</b> button. You will then return to the Assessments<br>page in Program Center.
</p>
<p>
<?
// How many have we entered?
$catCount2=getCatCount($cid,"2");
$catCount3=getCatCount($cid,"3");
$catCount4=getCatCount($cid,"4");
if(($catCount2+$catCount3+$catCount2)<14){
	// we still have room to enter a few more raters
?>
<table border=1>
<tr>
<td>First name</td>
<td>Last name</td>
<td>Email</td>
<td>Relationship</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><input type=text name="fname"></td>
<td><input type=text name="lname"></td>
<td><input type=text name="email"></td>
<td><select name="catid">
<option value=2 selected> Boss </option>
<option value=3> Peer </option>
<option value=4> Direct Report </option>
</select>
</td>
<td>
<input type=button onClick="javascript:frm1.what.value='add';frm1.submit();" value="Add">
</td>
</tr>
</table>
<?
}
else{
?>
You have registered the maximum number of raters.
<?
}
?>
</p>
<p>
<?php
if(($catCount2+$catCount3+$catCount2)>0){
	echo "<b>Selected raters</b><nr>";
	echo listRaterFormsMD($cid,"frm1");
}
?>
<script language="JavaScript">
function subForm(frm,catid){
    if(frm.lname.value.length<1||frm.fname.value.length<1||frm.email.value.length<1){
	alert("You must provide First and Last Names and Email address.");
    }
    else{
	frm.catid.value=catid;
	frm.what.value="add";
	frm.submit();
    }
}

function subEditForm(frm,rid){
    if(frm.lname.value.length<1||frm.fname.value.length<1||frm.email.value.length<1){
	alert("You must provide First and Last Names and Email address.");
    }
    else{
	frm.rid.value=rid;
	frm.what.value="save";
	frm.submit();
    }
}

</script>
<p>
<input type=button onClick="javascript:doneWithRR(frm1);" value="Save as Final Selection">
&nbsp;&nbsp;
<input type=button onClick="javascript:logOut(frm1);" value="Logout">
</p>
<p>
If you would like to register less than the minimum raters required or if you need assistance,
please contact
<a href="mailto:clientparticipantsupport@oliverwyman.com">Survey Support</a><br>
at 866-399-0996 (US) or 001-503-419-5364 (International).

</p>
<?php
echo "\n\n<script language=\"JavaScript\">\n";

$backURL=RETURN_URL;
//$backURL="test_showpost.php";
	
echo "function doneWithRR(frm){\n";
if($catCount4>=3&&$catCount3>=3&&$catCount2>=1){
	echo "if(confirm('Are you sure you are done registering raters?')){\n";
	echo "frm.action='$backURL';\n";
	echo "frm.submit();\n";
	echo "}\n";
}
else{
	echo "if(confirm('You have not entered the minimum number of raters in all categories. Are you sure you are done registering raters?')){\n";
	echo "frm.action='$backURL';\n";
	echo "frm.submit();\n";
	echo "}\n";
}
echo "}\n\n";
	
echo "function logOut(frm){\n";
echo "frm.action='$backURL';\n";
echo "frm.completed.value='false';\n";
echo "frm.submit();\n";
echo "}\n\n";
echo "</script>\n";

writeFooter(false);

// A Mercerized version of the function from meta/rater.php
function listRaterFormsMD($cid,$frm){
	$catCount2=getCatCount($cid,"2");
	$catCount3=getCatCount($cid,"3");
	$catCount4=getCatCount($cid,"4");
    echo "<p><table border=1 cellpadding=5>";
    echo "<tr>";
    echo "<td colspan=1><small>First Name</small></td>";
    echo "<td colspan=1><small>Last Name</small></td>";
    echo "<td colspan=1><small>Email</small></td>";
	echo "<td colspan=2>&nbsp</td></tr>";
	if($catCount2>0){
		echo "<tr><td colspan=7><small>Boss (1 recommended, 1 minimum required $catCount2 assigned)</small></td></tr>";
	}
	// Bosses 1-2
    $rows=getConsRaters($cid,"2");
	$idx=0;
	$cnt=0;
    if($rows){
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid=$row[0];
			$exp=("N"!=$row[8])?"Yes":"No";
			echo "<td colspan=1><input type='text' name='fname$idx' value='".stripslashes($row[3])."'></td>";
			echo "<td colspan=1><input type='text' name='lname$idx' value='".stripslashes($row[4])."'></td>";
			echo "<td colspan=1><input type='text' name='email$idx' value='".stripslashes($row[5])."'></td>";
			echo "<td colspan=1><input type='button' value='Save changes' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='save';$frm.submit();\"></td>";
			echo "<td colspan=1><input type='button' value='Delete' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='del';$frm.submit();\"></td>";
			echo "</tr>";
		}
    }
	
	// Peers 3-7
	if($catCount3>0){
		echo "<tr><td colspan=7><small>Peers (5 recommended, 3 minimum required $catCount3 assigned)</small></td></tr>";
	}
	
    $rows=getConsRaters($cid,"3");
	$cnt=0;
    if($rows){
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid=$row[0];
			$exp=("N"!=$row[8])?"Yes":"No";
			echo "<td colspan=1><input type='text' name='fname$idx' value='".stripslashes($row[3])."'></td>";
			echo "<td colspan=1><input type='text' name='lname$idx' value='".stripslashes($row[4])."'></td>";
			echo "<td colspan=1><input type='text' name='email$idx' value='".stripslashes($row[5])."'></td>";
			echo "<td colspan=1><input type='button' value='Save changes' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='save';$frm.submit();\"></td>";
			echo "<td colspan=1><input type='button' value='Delete' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='del';$frm.submit();\"></td>";
			echo "</tr>";
		}
    }
	
	// DRs
    if($catCount4>0){
		echo "<tr><td colspan=7><small>Direct Reports (5 recommended, 3 minimum required $catCount4 assigned)</small></td></tr>";
	}
    $rows=getConsRaters($cid,"4");
	$cnt=0;
    if($rows){
		foreach($rows as $row){
			$cnt++;
			$idx++;
			$rid=$row[0];
			$exp=("N"!=$row[8])?"Yes":"No";
			echo "<td colspan=1><input type='text' name='fname$idx' value='".stripslashes($row[3])."'></td>";
			echo "<td colspan=1><input type='text' name='lname$idx' value='".stripslashes($row[4])."'></td>";
			echo "<td colspan=1><input type='text' name='email$idx' value='".stripslashes($row[5])."'></td>";
			echo "<td colspan=1><input type='button' value='Save changes' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='save';$frm.submit();\"></td>";
			echo "<td colspan=1><input type='button' value='Delete' onClick=\"javascript:$frm.idx.value='$idx';$frm.rid.value='$rid';$frm.what.value='del';$frm.submit();\"></td>";
			echo "</tr>";
		}
    }
	echo "</table></p>";
}


?>

