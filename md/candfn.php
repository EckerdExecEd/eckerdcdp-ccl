<?php
function writeHead($title,$buffered){
    if($buffered){
	ob_start();
    }
	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title>$title</title>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
	echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">";
	echo "<style>.ivanC10400569321381 {";
	echo "VISIBILITY: hidden; POSITION: absolute";
	echo "}</style></head>";
}

function writeBody($title,$msg){
    echo "<body class=\".body\">";
    echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"750\" border=\"0\">";
    echo "<tbody>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"top\" align=\"right\" bgcolor=\"#4f8d97\" height=\"22\"><td colspan=3></td></tr>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"center\" height=\"42\" bgcolor=\"#253355\">";
    echo "<td align=\"left\"><img height=\"64\" src=\"../index_files/eclogotop.gif\" width=\"155\" border=\"0\"></td>";
    echo "<td><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td>";
    echo "<td align=\"right\"><img height=\"64\" src=\"../index_files/ldi-title.gif\" width=\"430\" border=\"0\"></td></tr>";
    echo "</table>"; 
	echo "<font face='Arial, Helvetica, Sans-Serif'><h2>$title.</h2>";
	if(strlen($msg)>0){
		echo "$msg<br>&nbsp;<br>";
	}
}

function writeFooter($buffered){
    if($buffered){
	ob_end_flush();
    }
    echo "</font> </body> </html>";
}

/*
function menu($cid){
    echo "<form name='frm1' method=POST>";
    echo "<table border=0 cellpadding=5>"; 
    if(empty($_SESSION['conid'])){
		echo '<tr><td onClick="javascript:frm1.action=\'index.php\';frm1.submit();"><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0><small>&nbsp;Log Out</small><td></tr>';
    }
    else{
		echo '<tr><td onClick="javascript:frm1.action=\'../cons/home.php\';frm1.submit();"><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0><small>&nbsp;Done</small><td></tr>';
    }
	echo "</table>"; 
	echo "</form>";
}
*/
?>
