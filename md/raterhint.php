<?php
require_once "candfn.php";
$msg="";
writeHead("Conflict Dynamics Profile",false);
writeBody("Rater Selection Tips",$msg);
?>
<ul>
<li>
Whenever possible, please select more than the minimum of raters since some raters may not<br>
respond. Receiving less than three responses from your Peers or Direct Reports will prevent<br>
you from receiving feedback from that rater relationship.
</li>
<li>
Choose raters that you know well enough to answer most of the items and give meaningful<br>
feedback.
</li>
<li>
If possible, choose raters whom you have worked with for at least three months.
</li>
<li>
Please contact the people you are asking to provide you with feedback. Make sure they<br>
will be available to complete the surveys by the deadline date.
</li>
</ul>
<?
writeFooter(false);
?>

