<?php

$msg = "";
session_start();

//require_once "../svc/class.TagStack.php";
require_once "../svc/class.Credentials.php";
require_once "../svc/class.CDPProgram.php";
require_once "../svc/class.CDPCandidate.php";
require_once "../svc/class.CDPRater.php";
require_once "../svc/class.WaitTime.php";

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

require_once "zupload.php";
//require_once "../meta/upload.php";
require_once "../cons/consfn.php";

$tid = $_POST['tid'];
$conid = $_SESSION['conid'];
$pid = $_POST['pid'];
$cid = false;

if (!is_uploaded_file($_FILES['dataFile']['tmp_name'])) {
	$msg = "<font color=\"#ff0000\">File read error</font>";	
} else {
	$msg = "Successfully uploaded " . $_FILES['dataFile']['name'];

	// Read in the data file and process it
	$dr = new DataReader($_FILES['dataFile']['tmp_name']);
	$cid = $dr->processCandidate();
	$msg .= $dr->processRaters();

	// The following line is for debugging
	//echo '<li>numRaters: ' . $dr->numRaters() . '<li>selfIdx: ' . $dr->selfIdx() . '<li>CID: ' . $cid . '<hr />' . $dr->showData();		// For debug

}

writeHead("Conflict Dynamics Profile - Consultants",false);
writeBody("Scoring Data File...",$msg);
?>
<form name="frm1" action="upload1.php" method=POST>
<input type=hidden name="pid" value="<?=$pid?>">
<input type=hidden name="tid" value="<?=$tid?>">
<table border=0>
<tr>
<td><input type="submit" value="Upload Another File"></td>
<td><input type="button" value="Cancel" onClick="javascript:frm1.action='../cons/home.php';frm1.submit();"></td>
</tr>
</table>
</form>	
<?
echo $inputData;
writeFooter(false);


// --------------------------------------------------------------------------------------------------

function setComplete() {
	//???????
	return true;
}

?>

