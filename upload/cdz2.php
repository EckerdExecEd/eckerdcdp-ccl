<?php

$msg = "";
session_start();

//require_once "../svc/class.TagStack.php";
require_once "../svc/class.Credentials.php";
require_once "../svc/class.CDPProgram.php";
require_once "../svc/class.CDPCandidate.php";
require_once "../svc/class.CDPRater.php";
require_once "../svc/class.WaitTime.php";

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

require_once "zupload.php";
//require_once "../meta/upload.php";
require_once "../cons/consfn.php";

$tid = $_POST['tid'];
$conid = $_SESSION['conid'];
$pid = $_POST['pid'];
$cid = false;

/* ???? Comment out temporarily until we are ready to test uploading files
// Get uploaded file
$uploadedFile = $_SERVER['DOCUMENT_ROOT'] . '/uploads/participants_' . $pid . '.xls';
if (!empty($_FILES['participantfile'])) {
	// Copy the file to uploads directory
	if (!move_uploaded_file($_FILES['participantfile']['tmp_name'], $uploadedFile)) {
		$msg = 'The file could not be uploaded.';
	}
		
	}	else {
		$msg = 'Could not upload the file.';
	}
}
*/

//???? Temporary variables for testing
$uploadedFile = 'C:/Development/xampp/htdocs/eckerd/dev/data/appleton.DAT';
$tid = 8;
$pid = 123456789;

if (strlen($msg)) {
	// Error uploading file
	$msg = '<font color="#ff0000">' . $msg . '</font>';	

} else {
	// Successful upload
	$msg = 'Successfully uploaded file.';

	// Read file into the array $contents
	$fp = fopen($uploadedFile,'r');
	$contents;
	$line = 0;
	$selfIdx = 0;
	$selfLen = 0;
	while(false != ($str = fgets($fp,1024))){
		// Discard empty CR-LF lines
		$str = trim($str);
		if (strlen($str) > 2){
			$contents[] = $str;
			$len = strlen($str);
			if($len > $selfLen){
				// Self has the longest entry, so we can save the index here
				$selfIdx = $line;
				$selfLen = $len;
			}
			$line++;
		}
	}
	//echo "Read $line lines<br>";
	// Close the temporary file
	fclose($fp);

	$inputData = showData($contents);		// For debug

	// Get the layout of data file
	$layout = getLayout();

	// First process the Self, in order to get the Candidate ID (CID)
	$readyToScore = true;

	// First process the Self, in order to get the Candidate ID (CID)
	$cid = processCandidate($pid, $tid, $conid, $contents[$selfIdx], $layout);
	if($cid == false){
		// can't continue
		$msg = '<font color="#ff0000">Error 1: unable to process data for Self</font>';
		$readyToScore=false;
	} else {
		// Process raters
		$numRows = count($contents);
		for ($row=0; $row<$numRows; $row++) {
			if ($row != $selfIdx) {
				$rc = processRater($pid, $tid, $cid, $contents[$row], $layout);
				$msg .= '<br>Processing rater ';
			}

			if($rc == false){
				$msg .= '<br><font color="#ff0000">Error 2: unable to process data for Rater $idx</font>';
				$readyToScore = false;
			} else{
				$msg .= '- OK!';
			}

		}

	}

	if ($readyToScore){
		//		echo "CID=$cid<br><br>";
		// Everything went well - we don't really score we just update the completion status
		if (setComplete($cid) == false){
			$msg .= '<br><font color="#ff0000">Error 3: unable to score - aborting</font>';
			$cid = false;
		}
	}
	else{
		$msg .= '<br><font color=\"#ff0000\">Error 4: unable to score - aborting</font>';
		$cid = false;
	}


}

//echo '<hr />' . $msg . '</h3>';
writeHead("Conflict Dynamics Profile - Consultants",false);
writeBody("Scoring Data File...",$msg);
?>
<form name="frm1" action="upload1.php" method=POST>
<input type=hidden name="pid" value="<?=$pid?>">
<input type=hidden name="tid" value="<?=$tid?>">
<table border=0>
<tr>
<td><input type="submit" value="Upload Another File"></td>
<td><input type="button" value="Cancel" onClick="javascript:frm1.action='../cons/home.php'4;frm1.submit();"></td>
</tr>
</table>
</form>	
<?
echo $inputData;
writeFooter(false);


// --------------------------------------------------------------------------------------------------

function getLayout( ){
	/**
	 *	This function defines the layout of the data
	 *	Each entry consists of a name for the field and an array of initial position and last position
	 **/
	$layout = array('seqnumber' => array(1, 9),
									'unknown' => array(10, 18),
									'type' => array(19, 21),
									'y' => array(23, 23),
									'num1' => array(25, 28),
									'num2' => array(30, 34),
									'n' => array(39, 39),
									'serialnumber' => array(41, 48),
									'lname' => array(49, 63),
									'fname' => array(64, 75),
									'middleinitial' => array(76, 76),
									'sex' => array(77, 82),
									'age' => array(83, 84),
									'race_nativeamerican' => array(85, 85),
									'race_asian' => array(86, 86),
									'race_black' => array(87, 87),
									'race_hispanic' => array(88, 88),
									'race_caucasian' => array(89, 89),
									'race_other' => array(90, 90),
									'degree' => array(91, 91),
									'profittype' => array(92, 92),
									'orgtype' => array(93, 94),
									'orglevel' => array(95, 95),
									'position' => array(96, 97),
									'relationship' => array(98, 98),
									'familiarity' => array(99, 99),
									'candidateid' => array(100, 110),
									'answers' => array(116, 129));

	return $layout;
}

function setComplete() {
	//???????
	return true;
}

function getValue($fieldname, $line, $layout) {
	// Get position of the field in the line
	$pos1 = $layout[$fieldname][0] - 1;
	$pos2 = $layout[$fieldname][1]; 
	
	$value = substr($line, $pos1, $pos2 - $pos1);
	
	return $value;
}

function getCandidateData($layout, $line) {
	/**
	 *
	 **/
}

function getRaterData($layout, $line) {
	/**
	 *
	 **/
	$data = array();
}


function showData($contents) {
	$str .= "\n" . '<table style="background-color:#f0f0f0;font:normal 9pt courier;">';
	$numLines = count($contents);
	
	// Headings
	$str .= "\n\t" . '<tr>';
	$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;">&nbsp;</td>';
	$count = 0;
	$dec = 0;
	for ($j=0; $j<230; $j++) {
		$count++;
		$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;width:5pxmargin-left:1px;margin-bottom:1px;">' . $dec . '</td>';
		if ($count == 9) {
			$count = -1;
			$dec++;
			if ($dec > 9) $dec = 0;
		}
	}
	$str .= "\n\t" . '</tr>';
	$str .= "\n\t" . '<tr>';
	$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;">&nbsp;</td>';
	$count = 0;
	for ($j=0; $j<230; $j++) {
		$count++;
		$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;width:5pxmargin-left:1px;margin-bottom:1px;">' . $count . '</td>';
		if ($count == 9) $count = -1;
	}
	$str .= "\n\t" . '</tr>';
	
	// Values
	for ($i=0; $i<$numLines; $i++) {
		$str .= "\n\t" . '<tr>';
		$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;">' . $i . '</td>';
		$numChars = strlen($contents[$i]);
		for ($ch=0; $ch<$numChars; $ch++) {
			$char = substr($contents[$i], $ch, 1);
			$str .= "\n\t\t" . '<td style="background-color:#ffffff;width:5pxmargin-left:1px;margin-bottom:1px;">' . $char . '</td>';
		}
		$str .= "\n\t" . '</tr>';
	}
	$str .= "\n" . '</table>';
	
	return $str;
}

?>

